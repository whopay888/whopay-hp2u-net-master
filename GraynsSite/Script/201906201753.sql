insert tbl_CreditAdvisor (AdvisorID, Description, Status, IsDeleted, CreatedBy, CreatedAt, UpdatedBy, UpdatedAt)
values('15', 'Credit Card high usage > 70%.
{0}
This may affect system scoring.
**To reduce below 70% by 30/{1} & apply after 15/{2}.
**Or to support with FD / Saving.', 'T', '0', 'SYSTEM', GETDATE(), 'SYSTEM' , GETDATE())