/*
   Thursday, July 4, 20193:34:14 PM
   User: dsradmin
   Server: 110.4.46.107,1533
   Database: DSR
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
USE [HP2U]
GO
/****** Object:  Table [dbo].[tbl_BankPolicy]    Script Date: 7/4/2019 3:36:00 PM ******/
DROP TABLE [dbo].[tbl_BankPolicy]
GO

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.tbl_BankPolicy
	(
	ID bigint NOT NULL IDENTITY (1, 1),
	BankID nvarchar(5) NULL,
	MinIncome decimal(18, 2) NULL,
	MaxIncome decimal(18, 2) NULL,
	Policy decimal(18, 2) NULL,
	isMain bit NULL,
	[Desc] nvarchar(50) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.tbl_BankPolicy SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
