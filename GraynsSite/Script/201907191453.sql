ALTER TABLE tbl_Parameter ALTER COLUMN [ParameterValue] nvarchar(500) NOT NULL;

insert into tbl_Parameter (Category, ParameterName, ParameterValue, Remarks, Sort)
values('Announcement', 'AnnouncementHeader', 'Testing Header', 'Login Page announcement division header', null)
insert into tbl_Parameter (Category, ParameterName, ParameterValue, Remarks, Sort)
values('Announcement', 'AnnouncementMessage', 'Testing', 'Login Page announcement division message', null)
insert into tbl_Parameter (Category, ParameterName, ParameterValue, Remarks, Sort)
values('Announcement', 'AnnouncementImage', '/Upload/Announcement/test.jpg', 'Login Page announcement division message', null)

insert into tbl_Menu (ShowText_EN, Name, ParentID, menuLevel, LinkUrl, Sort, IsDeleted, CreateBy, CreateAt, UpdateBy, UpdateAt, haveShorcut, Type)
VALUES ('Announcement Setting', 'Announcement Setting', '6', '1', '/Form/Setting/AnnouncementSetting.aspx', '15', '0', 'SYSTEM', GETDATE(), 'SYSTEM', GETDATE(), '0', '1')