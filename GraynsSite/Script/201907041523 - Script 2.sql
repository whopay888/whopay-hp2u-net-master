USE [HP2U]
GO
INSERT [dbo].[tbl_BankPolicy] ([BankID], [MinIncome], [MaxIncome], [Policy], [IsMain], [Desc]) VALUES (N'1', CAST(0.00 AS Decimal(18, 2)), CAST(5000.00 AS Decimal(18, 2)), CAST(60.00 AS Decimal(18, 2)), 0, N'AMB')
GO
INSERT [dbo].[tbl_BankPolicy] ([BankID], [MinIncome], [MaxIncome], [Policy], [IsMain], [Desc]) VALUES (N'1', CAST(5001.00 AS Decimal(18, 2)), CAST(10000.00 AS Decimal(18, 2)), CAST(65.00 AS Decimal(18, 2)), 0, N'AMB')
GO
INSERT [dbo].[tbl_BankPolicy] ([BankID], [MinIncome], [MaxIncome], [Policy], [IsMain], [Desc]) VALUES (N'1', CAST(10001.00 AS Decimal(18, 2)), CAST(99999999.00 AS Decimal(18, 2)), CAST(70.00 AS Decimal(18, 2)), 0, N'AMB')
GO
INSERT [dbo].[tbl_BankPolicy] ([BankID], [MinIncome], [MaxIncome], [Policy], [IsMain], [Desc]) VALUES (N'2', CAST(0.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(60.00 AS Decimal(18, 2)), 0, N'HLB')
GO
INSERT [dbo].[tbl_BankPolicy] ([BankID], [MinIncome], [MaxIncome], [Policy], [IsMain], [Desc]) VALUES (N'2', CAST(3001.00 AS Decimal(18, 2)), CAST(99999999.00 AS Decimal(18, 2)), CAST(85.00 AS Decimal(18, 2)), 0, N'HLB')
GO
INSERT [dbo].[tbl_BankPolicy] ([BankID], [MinIncome], [MaxIncome], [Policy], [IsMain], [Desc]) VALUES (N'3', CAST(0.00 AS Decimal(18, 2)), CAST(3500.00 AS Decimal(18, 2)), CAST(40.00 AS Decimal(18, 2)), 0, N'MB')
GO
INSERT [dbo].[tbl_BankPolicy] ([BankID], [MinIncome], [MaxIncome], [Policy], [IsMain], [Desc]) VALUES (N'3', CAST(3501.00 AS Decimal(18, 2)), CAST(99999999.00 AS Decimal(18, 2)), CAST(70.00 AS Decimal(18, 2)), 0, N'MB')
GO
INSERT [dbo].[tbl_BankPolicy] ([BankID], [MinIncome], [MaxIncome], [Policy], [IsMain], [Desc]) VALUES (N'4', CAST(0.00 AS Decimal(18, 2)), CAST(5000.00 AS Decimal(18, 2)), CAST(60.00 AS Decimal(18, 2)), 1, N'OCBC')
GO
INSERT [dbo].[tbl_BankPolicy] ([BankID], [MinIncome], [MaxIncome], [Policy], [IsMain], [Desc]) VALUES (N'4', CAST(5001.00 AS Decimal(18, 2)), CAST(8000.00 AS Decimal(18, 2)), CAST(70.00 AS Decimal(18, 2)), 1, N'OCBC')
GO
INSERT [dbo].[tbl_BankPolicy] ([BankID], [MinIncome], [MaxIncome], [Policy], [IsMain], [Desc]) VALUES (N'4', CAST(8001.00 AS Decimal(18, 2)), CAST(99999999.00 AS Decimal(18, 2)), CAST(80.00 AS Decimal(18, 2)), 1, N'OCBC')
GO
INSERT [dbo].[tbl_BankPolicy] ([BankID], [MinIncome], [MaxIncome], [Policy], [IsMain], [Desc]) VALUES (N'4', CAST(0.00 AS Decimal(18, 2)), CAST(7000.00 AS Decimal(18, 2)), CAST(60.00 AS Decimal(18, 2)), 0, N'OCBC')
GO
INSERT [dbo].[tbl_BankPolicy] ([BankID], [MinIncome], [MaxIncome], [Policy], [IsMain], [Desc]) VALUES (N'4', CAST(7001.00 AS Decimal(18, 2)), CAST(10000.00 AS Decimal(18, 2)), CAST(70.00 AS Decimal(18, 2)), 0, N'OCBC')
GO
INSERT [dbo].[tbl_BankPolicy] ([BankID], [MinIncome], [MaxIncome], [Policy], [IsMain], [Desc]) VALUES (N'4', CAST(10001.00 AS Decimal(18, 2)), CAST(99999999.00 AS Decimal(18, 2)), CAST(80.00 AS Decimal(18, 2)), 0, N'OCBC')
GO
INSERT [dbo].[tbl_BankPolicy] ([BankID], [MinIncome], [MaxIncome], [Policy], [IsMain], [Desc]) VALUES (N'5', CAST(0.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), CAST(55.00 AS Decimal(18, 2)), 0, N'RHB')
GO
INSERT [dbo].[tbl_BankPolicy] ([BankID], [MinIncome], [MaxIncome], [Policy], [IsMain], [Desc]) VALUES (N'5', CAST(2501.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(60.00 AS Decimal(18, 2)), 0, N'RHB')
GO
INSERT [dbo].[tbl_BankPolicy] ([BankID], [MinIncome], [MaxIncome], [Policy], [IsMain], [Desc]) VALUES (N'5', CAST(3001.00 AS Decimal(18, 2)), CAST(6000.00 AS Decimal(18, 2)), CAST(75.00 AS Decimal(18, 2)), 0, N'RHB')
GO
INSERT [dbo].[tbl_BankPolicy] ([BankID], [MinIncome], [MaxIncome], [Policy], [IsMain], [Desc]) VALUES (N'5', CAST(6001.00 AS Decimal(18, 2)), CAST(10000.00 AS Decimal(18, 2)), CAST(80.00 AS Decimal(18, 2)), 0, N'RHB')
GO
INSERT [dbo].[tbl_BankPolicy] ([BankID], [MinIncome], [MaxIncome], [Policy], [IsMain], [Desc]) VALUES (N'5', CAST(10001.00 AS Decimal(18, 2)), CAST(99999999.00 AS Decimal(18, 2)), CAST(85.00 AS Decimal(18, 2)), 0, N'RHB')
GO
INSERT [dbo].[tbl_BankPolicy] ([BankID], [MinIncome], [MaxIncome], [Policy], [IsMain], [Desc]) VALUES (N'6', CAST(0.00 AS Decimal(18, 2)), CAST(5000.00 AS Decimal(18, 2)), CAST(60.00 AS Decimal(18, 2)), 0, N'AFN')
GO
INSERT [dbo].[tbl_BankPolicy] ([BankID], [MinIncome], [MaxIncome], [Policy], [IsMain], [Desc]) VALUES (N'6', CAST(5001.00 AS Decimal(18, 2)), CAST(99999999.00 AS Decimal(18, 2)), CAST(80.00 AS Decimal(18, 2)), 0, N'AFN')
GO
INSERT [dbo].[tbl_BankPolicy] ([BankID], [MinIncome], [MaxIncome], [Policy], [IsMain], [Desc]) VALUES (N'7', CAST(0.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(55.00 AS Decimal(18, 2)), 0, N'ALZ')
GO
INSERT [dbo].[tbl_BankPolicy] ([BankID], [MinIncome], [MaxIncome], [Policy], [IsMain], [Desc]) VALUES (N'7', CAST(3001.00 AS Decimal(18, 2)), CAST(5000.00 AS Decimal(18, 2)), CAST(60.00 AS Decimal(18, 2)), 0, N'ALZ')
GO
INSERT [dbo].[tbl_BankPolicy] ([BankID], [MinIncome], [MaxIncome], [Policy], [IsMain], [Desc]) VALUES (N'7', CAST(5001.00 AS Decimal(18, 2)), CAST(99999999.00 AS Decimal(18, 2)), CAST(80.00 AS Decimal(18, 2)), 0, N'ALZ')
GO
INSERT [dbo].[tbl_BankPolicy] ([BankID], [MinIncome], [MaxIncome], [Policy], [IsMain], [Desc]) VALUES (N'8', CAST(0.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(65.00 AS Decimal(18, 2)), 0, N'CIMB')
GO
INSERT [dbo].[tbl_BankPolicy] ([BankID], [MinIncome], [MaxIncome], [Policy], [IsMain], [Desc]) VALUES (N'8', CAST(3001.00 AS Decimal(18, 2)), CAST(7000.00 AS Decimal(18, 2)), CAST(75.00 AS Decimal(18, 2)), 0, N'CIMB')
GO
INSERT [dbo].[tbl_BankPolicy] ([BankID], [MinIncome], [MaxIncome], [Policy], [IsMain], [Desc]) VALUES (N'8', CAST(7001.00 AS Decimal(18, 2)), CAST(99999999.00 AS Decimal(18, 2)), CAST(85.00 AS Decimal(18, 2)), 0, N'CIMB')
GO
INSERT [dbo].[tbl_BankPolicy] ([BankID], [MinIncome], [MaxIncome], [Policy], [IsMain], [Desc]) VALUES (N'9', CAST(0.00 AS Decimal(18, 2)), CAST(10000.00 AS Decimal(18, 2)), CAST(80.00 AS Decimal(18, 2)), 0, N'HSBC')
GO
INSERT [dbo].[tbl_BankPolicy] ([BankID], [MinIncome], [MaxIncome], [Policy], [IsMain], [Desc]) VALUES (N'9', CAST(10001.00 AS Decimal(18, 2)), CAST(99999999.00 AS Decimal(18, 2)), CAST(90.00 AS Decimal(18, 2)), 0, N'HSBC')
GO
INSERT [dbo].[tbl_BankPolicy] ([BankID], [MinIncome], [MaxIncome], [Policy], [IsMain], [Desc]) VALUES (N'10', CAST(0.00 AS Decimal(18, 2)), CAST(99999999.00 AS Decimal(18, 2)), CAST(80.00 AS Decimal(18, 2)), 0, N'PBB')
GO
INSERT [dbo].[tbl_BankPolicy] ([BankID], [MinIncome], [MaxIncome], [Policy], [IsMain], [Desc]) VALUES (N'11', CAST(0.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(60.00 AS Decimal(18, 2)), 0, N'MBS')
GO
INSERT [dbo].[tbl_BankPolicy] ([BankID], [MinIncome], [MaxIncome], [Policy], [IsMain], [Desc]) VALUES (N'11', CAST(3001.00 AS Decimal(18, 2)), CAST(99999999.00 AS Decimal(18, 2)), CAST(80.00 AS Decimal(18, 2)), 0, N'MBS')
GO
INSERT [dbo].[tbl_BankPolicy] ([BankID], [MinIncome], [MaxIncome], [Policy], [IsMain], [Desc]) VALUES (N'12', CAST(0.00 AS Decimal(18, 2)), CAST(5000.00 AS Decimal(18, 2)), CAST(70.00 AS Decimal(18, 2)), 0, N'KW')
GO
INSERT [dbo].[tbl_BankPolicy] ([BankID], [MinIncome], [MaxIncome], [Policy], [IsMain], [Desc]) VALUES (N'12', CAST(5001.00 AS Decimal(18, 2)), CAST(99999999.00 AS Decimal(18, 2)), CAST(75.00 AS Decimal(18, 2)), 0, N'KW')
GO
