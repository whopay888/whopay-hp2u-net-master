INSERT INTO tbl_Parameter (Category, ParameterName, ParameterValue, Remarks)
VALUES	(N'AdviceDetail', 'TradeReference_LessThanAmount', '1000', 'Additional advice less than amount (default 1000)')
INSERT INTO tbl_Parameter (Category, ParameterName, ParameterValue, Remarks)
VALUES	(N'AdviceDetail', 'CreditCard_LimitedUsage', '70', 'Credit card limit percentage')
INSERT INTO tbl_Parameter (Category, ParameterName, ParameterValue, Remarks)
VALUES	(N'AdviceDetail', 'CountFacilities_YoungAge', '25', 'Young age setting')
UPDATE tbl_Parameter
SET Category = N'AdviceDetail', ParameterName = 'CountFacilities_LoanAmountThreshold', Remarks = 'Big loan amount threshold'
WHERE (Category = 'Advise') AND (ParameterName = 'LoanAmountThreshold') AND (ParameterValue = '500000')