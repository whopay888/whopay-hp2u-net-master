USE [HP2U]
GO
/****** Object:  UserDefinedFunction [dbo].[Func_CalculateNewRepayment]    Script Date: 6/28/2019 10:06:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[Func_CalculateNewRepayment]
(
	-- Add the parameters for the function here
	@LoanAmount decimal(18,2),
	 @Tenure Decimal(18,2) , 
	@Interest decimal(18,2) -- in %
	
)
RETURNS decimal(18,2)
AS
BEGIN

Declare @month_Tenure decimal(18,10) = @tenure * 12
Declare @Month_Interest decimal(18,10) = @interest /100
--Declare @power_Loan decimal(18,10) = -(@Month_tenure)
--Declare @Denom_Loan decimal(18,10) = Power ((1 + @month_interest ) , @Power_Loan)

--Declare @Monthly_Repayment Decimal(18,4) = @LoanAmount * (@month_Interest / (1-@Denom_Loan));

Declare @Monthly_Repayment decimal(18, 4) = (@LoanAmount + (@LoanAmount * @Month_Interest * @tenure)) / @month_Tenure;

Return @Monthly_Repayment; 


END
