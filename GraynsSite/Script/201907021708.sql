update tbl_CreditAdvisor set Remark='AKPK advise', Category='AKPK' where AdvisorID='6'
update tbl_CreditAdvisor set Remark='Special Attention Account advise', Category='SAA' where AdvisorID='7'
update tbl_CreditAdvisor set Remark='Legal Status advise', Category='LS' where AdvisorID='8'
update tbl_CreditAdvisor set Remark='Used before CCRIS facilities count', Category='CCRIS' where AdvisorID='14'
update tbl_CreditAdvisor set Remark='Credit card advise.
{0} = Credit cards number and precent used
{1} = Current month
{2} = Next month', Category='CC' where AdvisorID='15'
update tbl_CreditAdvisor set Remark='Trade reference advise.
{0} = Company name
{1} = Amount of charge
{2} = If below certain amount, AdvisorID: 21', Category='TR' where AdvisorID='16'
update tbl_CreditAdvisor set Remark='Trade reference advise header.', Category='TR' where AdvisorID='17'
update tbl_CreditAdvisor set Remark='Facilities count separator (if there''s more than one applicant)', Category='CCRIS' where AdvisorID='18'