/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2014 (12.0.2000)
    Source Database Engine Edition : Microsoft SQL Server Express Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2017
    Target Database Engine Edition : Microsoft SQL Server Standard Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [DSR]
GO
/****** Object:  StoredProcedure [dbo].[SP_RegisterUser]    Script Date: 12/9/2018 11:13:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[SP_RegisterUser]
	-- Add the parameters for the stored procedure here
	@RoleID AS nvarchar(50) , 
	@Username as nvarchar(50) , 
	@Password As nvarchar(max), 
	@Name As nvarchar(250) , 
	@Email As nvarchar(250) , 
	@Mobile As nvarchar(100),
	@IsChangePassword AS BIT 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	INSERT INTO tbl_MemberInfo
	(MemberId , Username , fullname , email , mobile , Roles ,signupdate , status , IsDeleted)
	VALUES
	--(dbo.Func_NewMemberID(@RoleID) ,@Username , @Name , @Email ,@Mobile ,@RoleID  , getdate(),'A' , '0');
	(newId() ,@Username , @Name , @Email ,@Mobile ,@RoleID  , getdate(),'A' , '0');

	DECLARE @newmemberid AS NVARCHAR(50);

	SET @newmemberid = (SELECT TOP 1 memberid FROM tbl_MemberInfo WHERE Id =scope_identity());

	IF(@newmemberid IS NOT NULL)
	BEGIN

		UPDATE tbl_MemberInfo SET DisplayName = @newmemberid WHERE MemberId =@newmemberid;

		INSERT INTO tbl_Login
		(login_password	,login_status , login_role , login_id,firstLogin, IsChangedPassword)
		VALUES
		(@Password , 1 , @RoleID , @newmemberid,0,@IsChangePassword);

	END

	select @newmemberid As 'ID';
	
END
