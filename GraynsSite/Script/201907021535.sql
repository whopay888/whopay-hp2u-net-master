insert into tbl_CreditAdvisor(AdvisorID, Description, Status, Remark, Category, IsDeleted, CreatedBy, CreatedAt, UpdatedBy, UpdatedAt)
values('18', '---------------------------------------------------------', 'T', '{0} = AdvisorID: 10 (Age>30)
 {0} = AdvisorID: 11 (Age<=30) / AdvisorID: 10 (Age>30)
 {1} = AdvisorID: 13', 'CCRIS', 0, 'SYSTEM', GETDATE(), 'SYSTEM', GETDATE());

update tbl_CreditAdvisor set Description = 'No bank facility. No CCRIS reference
To provide supporting document
 - FD / Saving
 {0}
 {1}', UpdatedAt = getdate(), UpdatedBy = 'SYSTEM' WHERE AdvisorID = '9';

update tbl_CreditAdvisor set Description = ' - House Title / Car Cert', Remark = 'Used for AdvisorID: 18 in {0}
When Age is more than 30', Category = 'CCRIS',
UpdatedAt = getdate(), UpdatedBy = 'SYSTEM' WHERE AdvisorID = '10';

update tbl_CreditAdvisor set Description = ' - Degree Cert', Remark = 'Used for AdvisorID: 18 in {0}
When Age is less than or equal to 30', Category = 'CCRIS',
UpdatedAt = getdate(), UpdatedBy = 'SYSTEM' WHERE AdvisorID = '11';

update tbl_CreditAdvisor set Description = 'big loan amount', Remark = 'Used for AdvisorID: 13 in {0}
When Loan Amount is bigger than certain amount.', Category = 'CCRIS',
UpdatedAt = getdate(), UpdatedBy = 'SYSTEM' WHERE AdvisorID = '12';

update tbl_CreditAdvisor set Description = '**Advise to bring in joint applicant due to {0}', 
Remark = 'Used for AdvisorID: 18 in {1}', Category = 'CCRIS',
UpdatedAt = getdate(), UpdatedBy = 'SYSTEM' WHERE AdvisorID = '13';

insert into tbl_CreditAdvisor(AdvisorID, Description, Status, Remark, Category, IsDeleted, CreatedBy, CreatedAt, UpdatedBy, UpdatedAt)
values('19', 'young age', 'T', 'Used for AdvisorID: 13 in {0}
When Age is less than or equal to 30', 'CCRIS', 0, 'SYSTEM', GETDATE(), 'SYSTEM', GETDATE());

insert into tbl_CreditAdvisor(AdvisorID, Description, Status, Remark, Category, 
IsDeleted, CreatedBy, CreatedAt, UpdatedBy, UpdatedAt)
values('20', 'no CCRIS reference.', 'T', 'Used after all advise in this section', 'CCRIS', 
0, 'SYSTEM', GETDATE(), 'SYSTEM', GETDATE());

update tbl_CreditAdvisor set Remark=null where AdvisorID = '18'
update tbl_CreditAdvisor set Remark='{0} = AdvisorID: 10 (Age>30)
 {0} = AdvisorID: 11 (Age<=30) / AdvisorID: 10 (Age>30)
 {1} = AdvisorID: 13', Category = 'CCRIS' where AdvisorID = '9'
