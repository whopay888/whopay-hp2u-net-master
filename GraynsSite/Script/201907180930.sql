/*
   Thursday, July 18, 20199:29:43 AM
   User: hp2uadmin
   Server: 110.4.46.107,1533
   Database: HP2U
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.tbl_SponsorshipImage
	(
	ID bigint NOT NULL IDENTITY (1, 1),
	ImageName nvarchar(100) NULL,
	ImageURL nvarchar(MAX) NULL,
	isDeleted bit NOT NULL,
	UploadedAt datetime NULL,
	UploadedBy nvarchar(200) NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.tbl_SponsorshipImage ADD CONSTRAINT
	DF_tbl_SponsorshipImage_isDeleted DEFAULT 0 FOR isDeleted
GO
ALTER TABLE dbo.tbl_SponsorshipImage ADD CONSTRAINT
	PK_tbl_SponsorshipImage PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.tbl_SponsorshipImage SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

insert into tbl_Menu (ShowText_EN, Name, ParentID, menuLevel, LinkUrl, Sort, IsDeleted, CreateBy, CreateAt, UpdateBy, UpdateAt, Type)
values('Sponsorship Image Setting', 'Sponsorship Image Setting', 
(select rowID from tbl_Menu where Name = 'Setting' and ShowText_EN = 'Setting' and IsDeleted = 0), 
'1', '/Form/Setting/SponsorshipImage_Setting.aspx', '14', 0, 'SYSTEM', GETDATE(), 'SYSTEM', GETDATE(), '1')

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.tbl_SponsorshipImage ADD
	isActive bit NULL
GO
ALTER TABLE dbo.tbl_SponsorshipImage SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.tbl_SponsorshipImage ADD
	[From] datetime NULL,
	[To] datetime NULL
GO
ALTER TABLE dbo.tbl_SponsorshipImage SET (LOCK_ESCALATION = TABLE)
GO
COMMIT



