/*
   Thursday, July 18, 20193:47:22 PM
   User: hp2uadmin
   Server: 110.4.46.107,1533
   Database: HP2U
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE hp2uadmin.tbl_TSponsorshipImage
	(
	ID bigint NOT NULL IDENTITY (1, 1),
	ImageName nvarchar(100) NULL,
	ImageURL nvarchar(MAX) NULL,
	isDeleted bit NOT NULL,
	UploadedAt datetime NULL,
	UploadedBy nvarchar(200) NULL,
	isActive bit NULL,
	[From] datetime NULL,
	[To] datetime NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE hp2uadmin.tbl_TSponsorshipImage ADD CONSTRAINT
	DF_tbl_TSponsorshipImage_isDeleted DEFAULT 0 FOR isDeleted
GO
ALTER TABLE hp2uadmin.tbl_TSponsorshipImage ADD CONSTRAINT
	PK_tbl_TSponsorshipImage PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE hp2uadmin.tbl_TSponsorshipImage SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
