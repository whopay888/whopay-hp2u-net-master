﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Util;
using Synergy.Model;
using Synergy.UI;
using Synergy.Controller;

namespace WMElegance.Form
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        LoginUserModel login_user = null;

        #region ErrorMsg Class

        error errormessage = new error();

        #endregion


        #region PAGE EVENT

        protected void Page_Load(object sender, EventArgs e)
        {
            login_user = (LoginUserModel)Session[KeyVal.loginsecure];
            txtNewPassword.Attributes.Add("Value", txtNewPassword.Text);
            txtNewPasswordConfirm.Attributes.Add("Value", txtNewPasswordConfirm.Text);
            if (!IsPostBack)
            {

                this.txtNewPassword.Focus();

            }
        }

        #endregion


        #region VALIDATION FUNCTION

        private Boolean Submit_Validation()
        {
            this.LblError.Text = String.Empty;
            Boolean Validate_Success = false;
            if (this.RequireField_Validation())
            {
                Validate_Success = true;
            }
            return Validate_Success;
        }

        private Boolean RequireField_Validation()
        {
            String NewLine = "<br/>";
            String ErrMsg = Resources.resource.InputRequiredField;
            String ErrField = String.Empty;


            String Password = this.txtNewPassword.Text.Trim();
            String ConfirmPasswird = this.txtNewPasswordConfirm.Text.Trim();

            if (String.IsNullOrEmpty(Password))
            { ErrField += "-" + Resources.resource.New_Password + "." + NewLine; }

            if (String.IsNullOrEmpty(ConfirmPasswird))
            { ErrField += "-" + Resources.resource.New_Password_Confirmation + NewLine; }

            if (String.IsNullOrEmpty(ErrField))
            { return true; }
            else
            {
                string strMsg = (ErrMsg + ErrField).Replace("<br/>", "\\n");
                //Show error Message ---------------------------                      
                this.LblError.Text = errormessage.AlertText(ErrMsg + ErrField);
                //----------------------------------------------
                sqlString.displayAlert(this, strMsg);
                return false;
            }
        }

        #endregion



        #region BUTTON EVENT

        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            if (this.Submit_Validation())
            {
                wwdb db = new wwdb();
                db.Execute("update tbl_login set IsChangedPassword = 1, login_password = N'" + secure.Encrypt(txtNewPassword.Text, true) + "' where login_id ='" + login_user.UserId + "'");
                Response.Redirect("/Form/Report/ClientList.aspx");
            }
        }

        #endregion

        protected void BtnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Form/Login.aspx");
        }
    }
}