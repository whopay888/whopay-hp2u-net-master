﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Login.Master" CodeBehind="ForgotPassword.aspx.cs" Inherits="WMElegance.Form.ForgotPassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <script src="../js/jquery-1.7.1.min.js"></script>
    <script>

        $(window).resize(function () {

            //centerContent();

        });


        function centerContent() {
            var container = $(window);
            var content = $('#slick .register-form');
            content.css("left", (container.width() - content.width()) / 2);
            content.css("top", (container.height() - content.height()) / 2);
        }

        $(document).ready(function () {
            var w = $(window).width();

        });
    </script>
    <style type="text/css">
        body {
            background-image: url('/img/login-bg.jpg');
            position: relative;
            width: 100% 100%;
            overflow: hidden;
            background-position: center left;
            z-index: 1;
            background-repeat: no-repeat;
            background-size: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   
    <div id="page-2">
        <section id="slick">
             <div id="maintenance" runat="server" visible="false" class="register-form" style="position: absolute; z-index: 2; background-color: rgba(255,255,255,0.95); background-image: url(/FileUpload/LoginURL/Background-Image/bg-card2.png);">
                <p class="image" style="margin-bottom: 0; text-align: center; width: 150px; margin: auto;">
                    <img src="../Images/logoNew.png" />
                </p>
                <div class="desc" style="margin-top: 2em; margin-bottom: 1em;">
                    We are currently upgrading our system to give you the fastest and reliable service.
                    <br />
                    <br />
                    Sorry for any inconvenience caused.
                </div>
            </div>



                <div class="register-form" id="actual" runat="server">
                    <div class="col-md-12">
                     <p class="image" style="width: 100%; margin: 20px auto 0;">
                         <img src="../Images/Company_Logo_Big.png" />
                     </p>
                    <br />
                    <p class="intro" style="text-align: center; color: #222; font-weight: 700; font-size: 16px;">Forgot Password</p>
                    </div>

                    <div id="divLogin" class="col-md-9">
                        <div class="field">
                            <asp:TextBox ID="TxtUserId" runat="server" placeholder="<%$ Resources:Resource, Username_Input%>" MaxLength="50" data-rule-required="true" TabIndex="1"></asp:TextBox>
                            <span class="entypo-user icon"></span>
                            <span class="slick-tip left"><%= GetGlobalResourceObject("resource", "MemberIDEnter")%></span>
                            <asp:HiddenField ID="HFEmail" runat="server" />
                        </div>
                        <!-- Username input -->
                        <div class="field">
                            <asp:TextBox ID="TxtVerificationCode" runat="server" data-rule-required="true" placeholder="<%$ Resources:Resource, VerificationCode_Input%>" MaxLength="50" TabIndex="2" />
                            <span class="entypo-lock icon"></span>
                            <span class="slick-tip left"><%= GetGlobalResourceObject("resource", "VerificationCodeEnter")%></span>
                        </div>
                        <!-- Password repeat input -->
                        <div class="field">
                            <div id="divImg" runat="server">
                            <asp:Image ID="Im1" runat="server" CssClass="related1" />
                            <a runat="server" onserverclick="Unnamed_ServerClick" id="test">
                                <img src="/Images/Refresh.png" class="related1" /></a>
                        </div>
                        <span class="slick-tip left">Repeat your password</span>
                        </div>
                        <!-- submit button -->
                        <asp:Button ID="BtnSubmit" Text="<%$ Resources:Resource, Submit%>" class="send" runat="server" OnClick="BtnSubmit_Click" ValidationGroup="g1" TabIndex="5" />
                        <div class="field"></div>
                        <asp:Button ID="btnBack" Text="<%$ Resources:Resource, Back%>" class="send" runat="server" OnClick="BtnBack_Click" TabIndex="4" />

                        

                        <div class="field" style="display: none;">
                            <asp:UpdatePanel ID="upHidden" runat="server">
                                <ContentTemplate>
                                    <input id="hidd_Count" runat="server" name="hidd_Count" type="hidden" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>


                    </div>
                    <div style="color: white; position: relative; float: left; font-size: 11px; margin: auto; padding: 0px;">
                        <asp:Label ID="LblError" runat="server" Text="" ForeColor="Red"></asp:Label>
                    </div>

                    <div class="field">
                        <asp:Label ID="LblResetComplete" runat="server" Text=""></asp:Label>
                    </div>

                    <!-- Terms agree button -->
                    <div class="w-47 mr-5 mt-5">
                    </div>

                </div>
           <%-- </div>--%>
            <asp:HiddenField ID="imgsrc" runat="server" />
        </section>

    </div>


    <script src="../Vielee/login/js/demo-2.js"></script>
</asp:Content>
