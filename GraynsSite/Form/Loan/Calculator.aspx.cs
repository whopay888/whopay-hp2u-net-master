﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Synergy.Helper;
using Synergy.Util;
using Synergy.Model;
using Synergy.Controller;
using System.IO;
using System.Globalization;
using System.Configuration;
using System.Xml.Serialization;
using CTOS.Model;
using System.Threading;
using HJT.Cls.Controller;

namespace HJT.Form.Loan
{
    public partial class Calculator : Page
    {
        LoginUserModel login_user = null;
        ReportController reportController = new ReportController();
        private const int row5 = 10;
        private const int row10 = 15;
        protected void Page_Load(object sender, EventArgs e)
        {
            login_user = (LoginUserModel)Session[KeyVal.loginsecure];
            spHeader.InnerText = "Calculator";
            if (!IsPostBack)
            {
                //txtRemark.Attributes.Add("readonly", "readonly");
                txtRemark.Enabled = false;
                bindData();
                Set5RowGridView();
                Set10RowGridView();

                if (Request["rid"] == null)
                {
                    ddlMemberID.Enabled = true;
                    rblReport.Enabled = true;
                    ReadOnlyField(false);
                    rblReport.Enabled = true;
                    hfRequestID.Value = "0";
                    hftemprequestId.Value = Guid.NewGuid().ToString();
                }
                else
                {
                    ddlMemberID.Enabled = false;
                    rblReport.Enabled = false;
                    hfRequestID.Value = sqlString.decryptURL(Request["rid"].ToString());
                    ReadOnlyField(true);
                    GetInformation();
                }

                ddlHaveCoApplicant_SelectedIndexChanged(null, null);

                if (txtMainIC.Text != String.Empty)
                {
                    if (lblMainAge.Text == String.Empty)
                    {
                        txtMainIC_TextChanged(null, null);
                    }
                    else if (lblMainAge.Text == "0")
                    {
                        txtMainIC_TextChanged(null, null);
                    }
                }

                if (txtCoIC.Text != String.Empty)
                {
                    if (lblCoAge.Text == String.Empty)
                    {
                        txtCoIC_TextChanged(null, null);
                    }
                    else if (lblCoAge.Text == "0")
                    {
                        txtCoIC_TextChanged(null, null);
                    }
                }
            }
            reportController = new ReportController(login_user.UserId, hfRequestID.Value);
        }

        #region Function
        protected bool validationFormTag1()
        {
            bool result;
            StringBuilder errMsg = new StringBuilder();

            //ddlMemberID
            if (ddlMemberID.SelectedIndex == 0)
                errMsg.Append(Resources.resource.SelectRequestName + "\\n");

            //Main Applicant Name
            if (String.IsNullOrEmpty(txtMainName.Text))
                errMsg.Append(Resources.resource.EnterApplicantName + "\\n");

            if (String.IsNullOrEmpty(txtMainIC.Text))
                errMsg.Append(Resources.resource.EnterIC + "\\n");
            else
            {
                if (!Validation.isValidMyCard(txtMainIC.Text))
                    errMsg.Append(Resources.resource.icOnlyNumberAllow + "\\n");

                if (!(ConvertHelper.ConvertToInt(lblMainAge.Text, 0) > 17 && ConvertHelper.ConvertToInt(lblMainAge.Text, 0) < 71))
                    errMsg.Append(Resources.resource.AgeNotAllow + "\\n");
            }

            //if (ddlMainArea.SelectedIndex == 0)
            //{
            //    errMsg.Append(Resources.resource.Please_Select_One_Area + "\\n");
            //}

            if (!String.IsNullOrEmpty(txtMainNetIncome.Text))
                if (!Validation.isDecimal(txtMainNetIncome.Text))
                    errMsg.Append(Resources.resource.OnlyAcceptDecimalNumber + "\\n");

            if (!String.IsNullOrEmpty(txtMainOtherIncome.Text))
                if (!Validation.isDecimal(txtMainOtherIncome.Text))
                    errMsg.Append(Resources.resource.OnlyAcceptDecimalNumber + "\\n");

            if (!String.IsNullOrEmpty(txtMainGrossIncome.Text))
                if (!Validation.isDecimal(txtMainGrossIncome.Text))
                    errMsg.Append(Resources.resource.OnlyAcceptDecimalNumber + "\\n");

            //if (String.IsNullOrEmpty(txtMainContact.Text))
            //{
            //    errMsg.Append(Resources.resource.EnterContactNo + "\\n");
            //}


            //Co Applicant Name
            if (ddlHaveCoApplicant.SelectedIndex != 0)
            {
                if (String.IsNullOrEmpty(txtCoName.Text))
                    errMsg.Append(Resources.resource.EnterApplicantName + "\\n");

                if (String.IsNullOrEmpty(txtCoIC.Text))
                    errMsg.Append(Resources.resource.EnterIC + "\\n");
                else
                {
                    if (!Validation.isValidMyCard(txtCoIC.Text))
                        errMsg.Append(Resources.resource.icOnlyNumberAllow + "\\n");

                    if (!(ConvertHelper.ConvertToInt(lblCoAge.Text, 0) > 17 && ConvertHelper.ConvertToInt(lblCoAge.Text, 0) < 71))
                        errMsg.Append(Resources.resource.AgeNotAllow + "\\n");
                }

                //if (ddlCoArea.SelectedIndex == 0)
                //{
                //    errMsg.Append(Resources.resource.Please_Select_One_Area + "\\n");
                //}

                //if (String.IsNullOrEmpty(txtCoContact.Text))
                //{
                //    errMsg.Append(Resources.resource.EnterContactNo + "\\n");
                //}

                if (!String.IsNullOrEmpty(txtCoNetIncome.Text))
                    if (!Validation.isDecimal(txtCoNetIncome.Text))
                        errMsg.Append(Resources.resource.OnlyAcceptDecimalNumber + "\\n");

                if (!String.IsNullOrEmpty(txtCoOtherIncome.Text))
                    if (!Validation.isDecimal(txtCoOtherIncome.Text))
                        errMsg.Append(Resources.resource.OnlyAcceptDecimalNumber + "\\n");

                if (!String.IsNullOrEmpty(txtCoGrossIncome.Text))
                    if (!Validation.isDecimal(txtCoGrossIncome.Text))
                        errMsg.Append(Resources.resource.OnlyAcceptDecimalNumber + "\\n");
            }


            if (errMsg.ToString().Trim() != string.Empty)
            {
                sqlString.displayAlert2(this, errMsg.ToString());
                return false;
            }
            else
                result = true;

            return result;
        }
        protected bool validationFormTag2()
        {
            bool result = false;
            StringBuilder errMsg = new StringBuilder();
            StringBuilder sql = new StringBuilder();

            if (!String.IsNullOrEmpty(txtLoanAmount.Text) || !String.IsNullOrEmpty(txtPurchasePrice.Text))
            {
                if (String.IsNullOrEmpty(txtTenure.Text))
                    errMsg.Append(Resources.resource.TenureCannotbeEmpty + "\\n");

                if (String.IsNullOrEmpty(txtInterest.Text))
                    errMsg.Append(Resources.resource.InterestCannotbeEmpty + "\\n");
            }

            if (!String.IsNullOrEmpty(txtTenure.Text))
                if (!Validation.isNumeric(txtTenure.Text))
                    errMsg.Append(Resources.resource.TenureShouldBeInteger + "\\n");

            if (!String.IsNullOrEmpty(txtInterest.Text))
            {
                if (!Validation.isDecimal(txtInterest.Text))
                    errMsg.Append(Resources.resource.InterestShouldBeDecimal + "\\n");
                else
                {
                    if (!(ConvertHelper.ConvertToDecimal(txtInterest.Text, 0) <= 100 && ConvertHelper.ConvertToDecimal(txtInterest.Text, 0) >= 0))
                        errMsg.Append(Resources.resource.InterestCheck + "\\n");
                }
            }

            if (ddlProjectName.SelectedIndex == 0)
                errMsg.Append(Resources.resource.SelectProjectName + "\\n");

            //if (String.IsNullOrEmpty(txtUnit.Text))
            //{
            //    errMsg.Append(Resources.resource.UnitCannotbeEmpty + "\\n");
            //}
            //else
            //{
            //    if (!Validation.isNumeric(txtUnit.Text))
            //    {
            //        errMsg.Append(Resources.resource.OnlyAcceptIntegerNumber + "\\n");
            //    }
            //}

            if (!String.IsNullOrEmpty(txtPurchasePrice.Text))
                if (!Validation.isDecimal(txtPurchasePrice.Text))
                    errMsg.Append(Resources.resource.OnlyAcceptDecimalNumber + "\\n");

            if (!String.IsNullOrEmpty(txtLoanAmount.Text))
                if (!Validation.isDecimal(txtLoanAmount.Text))
                    errMsg.Append(Resources.resource.OnlyAcceptDecimalNumber + "\\n");

            if (errMsg.ToString().Trim() != string.Empty)
            {
                sqlString.displayAlert2(this, errMsg.ToString());
                return false;
            }
            else
                result = true;

            return result;
        }
        protected bool validationFormTag3()
        {
            bool result = false;
            StringBuilder errMsg = new StringBuilder();
            StringBuilder sql = new StringBuilder();
            int counter = 0;

            // Main Gridview
            counter += CheckGVTextBox(gvMainHousingLoan, true);
            counter += CheckGVTextBox(gvMainCommercialLoan, true);
            counter += CheckGVTextBox(gvMainCreditCard, true);
            counter += CheckGVTextBox(gvMainHirePurchase, true);
            counter += CheckGVTextBox(gvMainOD, true);
            counter += CheckGVTextBox(gvMainPersonalLoan, true);
            counter += CheckGVTextBox(gvMainXProduct, true);

            //Co GridView
            if (ddlHaveCoApplicant.SelectedIndex != 0)
            {
                counter += CheckGVTextBox(gvCoCreditCard, false);
                counter += CheckGVTextBox(gvCoHirePurchase, false);
                counter += CheckGVTextBox(gvCoHousingLoan, false);
                counter += CheckGVTextBox(gvCoOD, false);
                counter += CheckGVTextBox(gvCoPersonalLoan, false);
                counter += CheckGVTextBox(gvCoXProduct, false);
                counter += CheckGVTextBox(gvCoCommercialLoan, false);
            }

            if (counter > 0)
                errMsg.Append(Resources.resource.OnlyAcceptDecimalNumber + "\\n");

            if (errMsg.ToString().Trim() != string.Empty)
            {
                sqlString.displayAlert2(this, errMsg.ToString());
                return false;
            }
            else
                result = true;

            return result;
        }

        private void bindData()
        {
            sqlString.bindControl(rblReport, "SELECT ParameterName as '0' , ParameterValue as '1' FROM tbl_parameter WITH (NOLOCK) WHERE category='ReportType';", "0", "1");
            sqlString.bindControl(ddlMainArea, "SELECT AreaName AS '0', AreaID AS '1' FROM tbl_Area WITH (NOLOCK) WHERE IsDeleted='0';", "0", "1", true);
            sqlString.bindControl(ddlCoArea, "SELECT AreaName AS '0', AreaID AS '1' FROM tbl_Area WITH (NOLOCK) WHERE IsDeleted='0';", "0", "1", true);
            sqlString.bindControl(ddlProjectName, "SELECT ProjectName AS '0', ProjectID AS '1' FROM tbl_ProjectSettings WITH (NOLOCK) WHERE Status = 'A' AND isDeleted = 'false';", "0", "1", true);
            ddlProjectName.Items.Insert(ddlProjectName.Items.Count, "Others");
            sqlString.bindControl(ddlMemberID, "SELECT m.FullName AS '0', m.MemberID AS '1' FROM tbl_Memberinfo m WITH (NOLOCK) WHERE m.Status = 'A' AND m.IsDeleted = 'False' ;", "0", "1", true);
            rblReport.SelectedIndex = 0;
        }
        private void ReadOnlyField(bool isEdit)
        {
            //txtMainName.ReadOnly = isEdit;
            //txtMainIC.ReadOnly = isEdit;
            //txtCoName.ReadOnly = isEdit;
            //txtCoIC.ReadOnly = isEdit;
            //txtMainNetIncome.ReadOnly = isEdit;
            //txtPurchasePrice.ReadOnly = isEdit;
            //txtLoanAmount.ReadOnly = isEdit;
        }
        private void GetInformation()
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            bool haveRecord = false;

            try
            {
                if (hfRequestID.Value != null)
                {
                    if (hfRequestID.Value != "0")
                    {
                        //Check tbl_TLoanLegalAction have Request Record or not
                        sql.Clear();
                        sql.Append(" SELECT COUNT(*) AS 'HaveRecord' ");
                        sql.Append(" FROM tbl_TLoanLegalAction WITH(NOLOCK) ");
                        sql.Append(" WHERE RequestID = N'" + secure.RC(hfRequestID.Value.ToString()) + "'");
                        db.OpenTable(sql.ToString());

                        if (db.RecordCount() > 0)
                            if (ConvertHelper.ConvertToDecimal(db.Item("HaveRecord").ToString(), 2) > 0)
                                haveRecord = true;
                            else
                                haveRecord = false;

                        sql.Append(" SELECT Remark FROM tbl_Request WITH(NOLOCK) WHERE RequestID = N'" + secure.RC(hfRequestID.Value.ToString()) + "'");
                        db.OpenTable(sql.ToString());
                        txtRemark.Text = db.Item("Remark");

                        //If have
                        if (haveRecord)
                        {
                            // Pass Request Name
                            sql.Clear();
                            sql.Append(" SELECT r.MemberID AS 'MemberID', r.ReportType AS 'ReportType' FROM tbl_Request r WITH(NOLOCK) ");
                            sql.Append(" INNER JOIN tbl_RequestApplicant ra WITH(NOLOCK) ON ra.RequestID = r.RequestID ");
                            sql.Append(" WHERE r.IsDeleted = 'false' AND r.RequestID = N'" + secure.RC(hfRequestID.Value.ToString()) + "'; ");
                            db.OpenTable(sql.ToString());

                            if (db.RecordCount() > 0)
                            {
                                ddlMemberID.SelectedValue = db.Item("MemberID");
                                rblReport.SelectedValue = db.Item("ReportType");
                            }

                            //Pass Main Applicant Record
                            sql.Clear();
                            sql.Append("SELECT Name, Age, AreaID, MyCard, ContactNumber, NetIncome, GrossIncome, OtherIncome ");
                            sql.Append("FROM tbl_TApplicants WITH(NOLOCK)");
                            sql.Append("WHERE RequestID = N'" + secure.RC(hfRequestID.Value.ToString()) + "' AND ApplicantType = N'MA';");
                            db.OpenTable(sql.ToString());

                            if (db.RecordCount() > 0)
                            {
                                txtMainName.Text = db.Item("Name");
                                txtMainIC.Text = db.Item("MyCard");
                                txtMainContact.Text = db.Item("ContactNumber");
                                string grossIncome = ConvertHelper.ConvertToDecimal(db.Item("GrossIncome").ToString(), 0).ToString("N");
                                string netIncome = ConvertHelper.ConvertToDecimal(db.Item("NetIncome").ToString(), 0).ToString("N");
                                string otherIncome = ConvertHelper.ConvertToDecimal(db.Item("OtherIncome").ToString(), 0).ToString("N");

                                txtMainGrossIncome.Text = Math.Round(double.Parse(grossIncome)).ToString();
                                txtMainNetIncome.Text = Math.Round(double.Parse(netIncome)).ToString();
                                txtMainOtherIncome.Text = Math.Round(double.Parse(otherIncome)).ToString();

                                ddlMainArea.SelectedValue = db.Item("AreaID");
                                lblMainAge.Text = db.Item("Age");
                            }

                            //Pass Co Applicant Record
                            sql.Clear();
                            sql.Append("SELECT Name, Age, AreaID, MyCard, ContactNumber, NetIncome, GrossIncome, OtherIncome ");
                            sql.Append("FROM tbl_TApplicants WITH(NOLOCK)");
                            sql.Append("WHERE RequestID = N'" + secure.RC(hfRequestID.Value.ToString()) + "' AND ApplicantType = N'CA';");
                            db.OpenTable(sql.ToString());

                            if (db.RecordCount() > 0)
                            {
                                txtCoName.Text = db.Item("Name");
                                txtCoIC.Text = db.Item("MyCard");
                                txtCoContact.Text = db.Item("ContactNumber");
                                string grossIncome = ConvertHelper.ConvertToDecimal(db.Item("GrossIncome"), 0).ToString("N");
                                string netIncome = ConvertHelper.ConvertToDecimal(db.Item("NetIncome"), 0).ToString("N");
                                string otherIncome = ConvertHelper.ConvertToDecimal(db.Item("OtherIncome"), 0).ToString("N");

                                txtCoGrossIncome.Text = Math.Round(double.Parse(grossIncome)).ToString();
                                txtCoNetIncome.Text = Math.Round(double.Parse(netIncome)).ToString();
                                txtCoOtherIncome.Text = Math.Round(double.Parse(otherIncome)).ToString();

                                ddlCoArea.SelectedValue = db.Item("AreaID");
                                lblCoAge.Text = db.Item("Age");
                                ddlHaveCoApplicant.SelectedIndex = 1;
                            }

                            //Pass Loan and Bank Legal Record
                            sql.Clear();
                            sql.Append(" SELECT ISNULL(Tenure, '0') AS 'Tenure', ISNULL(Interest, '0') AS 'Interest', ISNULL(PurchasePrice, '0') AS 'PurchasePrice', ISNULL(LoanAmount, '0') AS 'LoanAmount', ProjectID, ");
                            //sql.Append(" SELECT ISNULL(Tenure, '0') AS 'Tenure', ISNULL(Interest, '0') AS 'Interest', ISNULL(PurchasePrice, '0') AS 'PurchasePrice', ISNULL(LoanAmount, '0') AS 'LoanAmount', ProjectID, ISNULL(Unit, '0') AS 'Unit', ");
                            sql.Append(" CTOS, SAttention, Restructuring, DishonourCheque, CCRIS ");
                            sql.Append(" FROM tbl_TLoanLegalAction WITH(NOLOCK) ");
                            sql.Append(" WHERE RequestID = N'" + secure.RC(hfRequestID.Value.ToString()) + "'; ");
                            db.OpenTable(sql.ToString());

                            if (db.RecordCount() > 0)
                            {
                                txtTenure.Text = db.Item("Tenure");
                                //txtUnit.Text = db.Item("Unit");
                                txtInterest.Text = db.Item("Interest");
                                string purchasePrice = ConvertHelper.ConvertToDecimal(db.Item("PurchasePrice"), 0).ToString("N");
                                string loanAmount = ConvertHelper.ConvertToDecimal(db.Item("LoanAmount"), 0).ToString("N");

                                txtPurchasePrice.Text = Math.Round(double.Parse(purchasePrice)).ToString();
                                if (loanAmount.Equals("0.00") || loanAmount.Equals("0"))
                                    //loanAmount = (ConvertHelper.ConvertToDecimal(db.Item("PurchasePrice"), 0) * 90 / 100).ToString("N");
                                    loanAmount = (ConvertHelper.ConvertToDecimal(txtPurchasePrice.Text, 0) * reportController.PassBankMargin("12", db.Item("ProjectID"), hfRequestID.Value) / 100).ToString();
                                txtLoanAmount.Text = loanAmount;
                                ddlProjectName.SelectedValue = db.Item("ProjectID");
                                rblCTOS.SelectedValue = db.Item("CTOS");
                                rblSpecialAttention.SelectedValue = db.Item("SAttention");
                                rblCCRISAttention.SelectedValue = db.Item("CCRIS");
                                rblDishonourCheque.SelectedValue = db.Item("DishonourCheque");
                                rblRestructuring.SelectedValue = db.Item("Restructuring");
                            }

                            //Pass CCRIS PDF file
                            sql.Clear();
                            sql.Append(" SELECT DocsURL FROM tbl_RequestDocs WITH (NOLOCK) ");
                            sql.Append(" WHERE RequestID = N'" + secure.RC(hfRequestID.Value) + "' AND DocumentType = '" + DocumentType.CCRIS + "' ;");
                            db.OpenTable(sql.ToString());

                            if (db.RecordCount() > 0)
                            {
                                lblFileUpload.Text = db.Item("DocsURL");
                                Image imgDisplay = (Image)img_fu1;
                                imgDisplay.Visible = true;
                                imgDisplay.Attributes.Add("onclick", "window.open('" + secure.RC(lblFileUpload.Text) + "')");
                            }
                        }
                        //if don't have record
                        else
                        {
                            sql.Clear();
                            sql.AppendFormat(@"
                                SELECT r.MemberID, r.ReportType, ra.Name, ra.MyCard, ra.NetIncome, 
                                    ra.PurchasePrice, ra.LoanAmount, r.ProjectID AS ID, r.ProjectName, 
                                    tps.ProjectID, a.ContactNumber
                                FROM tbl_Request r WITH(NOLOCK)
                                LEFT JOIN tbl_RequestApplicant ra WITH(NOLOCK) ON ra.RequestID = r.RequestID
                                LEFT JOIN dbo.tbl_ProjectSettings tps ON r.ProjectID = tps.ID
								left join tbl_TApplicants a with(nolock) on a.RequestID = r.RequestID
                                WHERE r.RequestID = N'{0}'
                            ", hfRequestID.Value);
                            db.OpenTable(sql.ToString());

                            if (db.RecordCount() > 0)
                            {
                                ddlMemberID.SelectedValue = db.Item("MemberID");
                                rblReport.SelectedValue = db.Item("ReportType");
                                txtMainName.Text = db.Item("Name");
                                txtMainIC.Text = db.Item("MyCard");
                                txtMainContact.Text = db.Item("ContactNumber");
                                string netIncome = ConvertHelper.ConvertToDecimal(db.Item("NetIncome"), 0).ToString("N");
                                string purchasePrice = ConvertHelper.ConvertToDecimal(db.Item("PurchasePrice"), 0).ToString("N");
                                string loanAmount = ConvertHelper.ConvertToDecimal(db.Item("LoanAmount"), 0).ToString("N");

                                txtMainNetIncome.Text = Math.Round(double.Parse(netIncome)).ToString();
                                txtPurchasePrice.Text = Math.Round(double.Parse(purchasePrice)).ToString();
                                if (loanAmount.Equals("0.00") || loanAmount.Equals("0"))
                                    //loanAmount = (ConvertHelper.ConvertToDecimal(db.Item("PurchasePrice"), 0) * 90 / 100).ToString("N");
                                    loanAmount = (ConvertHelper.ConvertToDecimal(txtPurchasePrice.Text, 0) * reportController.PassBankMargin("12", db.Item("ProjectID"), hfRequestID.Value) / 100).ToString();
                                txtLoanAmount.Text = Math.Round(double.Parse(loanAmount)).ToString();
                                if (db.Item("ProjectID") != null && db.Item("ProjectID") != string.Empty)
                                    ddlProjectName.SelectedValue = db.Item("ProjectID");
                                if (db.Item("ProjectName") != null && db.Item("ProjectName") != string.Empty)
                                {
                                    ddlProjectName.SelectedValue = "Others";
                                    txtProjectNameOther.Visible = true;
                                    txtProjectNameOther.Text = db.Item("ProjectName");
                                }
                                if (db.RecordCount() > 1)
                                {
                                    ddlHaveCoApplicant.SelectedValue = "1";
                                    ddlHaveCoApplicant_SelectedIndexChanged(null, null);
                                    ddlHaveCoApplicant.Enabled = false;
                                    db.MoveNext();
                                    txtCoName.Text = db.Item("Name");
                                    txtCoIC.Text = db.Item("MyCard");
                                    string netInome = ConvertHelper.ConvertToDecimal(db.Item("NetIncome"), 0).ToString("N");
                                    txtCoNetIncome.Text = Math.Round(double.Parse(netIncome)).ToString();
                                    txtCoName.Enabled = false;
                                    //txtCoIC.Enabled = false;
                                }
                            }
                        }
                    }

                }
                RetrieveDataIntoGridView(gvMainHousingLoan, "1", hfRequestID.Value.ToString(), row5, true);
                RetrieveDataIntoGridView(gvMainCommercialLoan, "2", hfRequestID.Value.ToString(), row5, true);
                RetrieveDataIntoGridView(gvMainPersonalLoan, "3", hfRequestID.Value.ToString(), row5, true);
                RetrieveDataIntoGridView(gvMainOD, "4", hfRequestID.Value.ToString(), row5, true);
                RetrieveDataIntoGridView(gvMainXProduct, "5", hfRequestID.Value.ToString(), row10, true);
                RetrieveDataIntoGridView(gvMainHirePurchase, "6", hfRequestID.Value.ToString(), row5, true);
                RetrieveDataIntoGridView(gvMainCreditCard, "7", hfRequestID.Value.ToString(), row10, true);

                //sql.Clear();
                //sql.Append("SELECT * ");
                //sql.Append("FROM tbl_TLoanDetail WITH(NOLOCK)");
                //sql.Append("WHERE RequestID = N'" + secure.RC(hfRequestID.Value.ToString()) + "'");
                //db.OpenTable(sql.ToString());
                //var recordCount = db.RecordCount();
                //if (recordCount != 0)
                //{
                //    sql.Clear();
                //    sql.Append("DELETE FROM tbl_TLoanDetail WHERE RequestID=N'" + hfRequestID.Value + "'");
                //    db.Execute(sql.ToString());
                //}
                hfMainCCRIS.Value = "false";
                if (reportController.RetrieveXMLDataForMainCoApp(hfRequestID.Value.ToString(), txtMainName.Text, "", txtMainIC.Text.Trim(), "I",
                    ddlHaveCoApplicant.SelectedIndex > 0, lblMainAge.Text, txtLoanAmount.Text, hfMainCCRIS.Value, hfCoCCRIS.Value, true, false))
                    if (reportController.MAtype.Equals("CTOS")) //CTOS Main
                    {
                        PassDatatoLoanDetails(reportController.mainResultID, reportController.section_ccris, true);
                        btnViewMACTOSReport.Visible = true;
                    }
                    else if (reportController.MAtype.Equals("RAMCI")) //RAMCI Main
                    {
                        PassDatatoLoanDetails(reportController.mainResultID, reportController.detail, true);
                        btnViewMARAMCIReport.Visible = true;
                    }
                hfMainCCRIS.Value = "";
                // Co Applicant Gridview 
                if (ddlHaveCoApplicant.SelectedIndex != 0)
                {
                    RetrieveDataIntoGridView(gvCoHousingLoan, "1", hfRequestID.Value.ToString(), row5, false);
                    RetrieveDataIntoGridView(gvCoCommercialLoan, "2", hfRequestID.Value.ToString(), row5, false);
                    RetrieveDataIntoGridView(gvCoPersonalLoan, "3", hfRequestID.Value.ToString(), row5, false);
                    RetrieveDataIntoGridView(gvCoOD, "4", hfRequestID.Value.ToString(), row5, false);
                    RetrieveDataIntoGridView(gvCoXProduct, "5", hfRequestID.Value.ToString(), row10, false);
                    RetrieveDataIntoGridView(gvCoHirePurchase, "6", hfRequestID.Value.ToString(), row5, false);
                    RetrieveDataIntoGridView(gvCoCreditCard, "7", hfRequestID.Value.ToString(), row10, false);

                    hfCoCCRIS.Value = "false";
                    reportController.RetrieveXMLDataForMainCoApp(hfRequestID.Value.ToString(), txtCoName.Text, "", txtCoIC.Text.Trim(), "I",
                        ddlHaveCoApplicant.SelectedIndex > 0, lblCoAge.Text, txtLoanAmount.Text, hfMainCCRIS.Value, hfCoCCRIS.Value, false, false);
                    if (reportController.CAtype.Equals("CTOS")) //CTOS Co
                    {
                        PassDatatoLoanDetails(reportController.coResultID, reportController.section_ccris, false);
                        btnViewCACTOSReport.Visible = true;
                    }
                    else if (reportController.CAtype.Equals("RAMCI")) //RAMCI Co
                    {
                        PassDatatoLoanDetails(reportController.coResultID, reportController.detail, false);
                        btnViewCARAMCIReport.Visible = true;
                    }
                    hfCoCCRIS.Value = "";

                    if (btnCoCTOSCCRIS.Visible == false && btnCoRAMCICCRIS.Visible == false)
                    {
                        hfCoCCRIS.Value = "false";
                        reportController.RetrieveXMLDataForMainCoApp(hfRequestID.Value.ToString(), txtCoName.Text, "", txtCoIC.Text.Trim(), "I",
                            ddlHaveCoApplicant.SelectedIndex > 0, lblCoAge.Text, txtLoanAmount.Text, hfMainCCRIS.Value, hfCoCCRIS.Value, false, false);
                        if (reportController.CAtype.Equals("CTOS")) //CTOS Co
                        {
                            PassDatatoLoanDetails(reportController.coResultID, reportController.section_ccris, false);
                            btnViewCACTOSReport.Visible = true;
                        }
                        else if (reportController.CAtype.Equals("RAMCI")) //RAMCI Co
                        {
                            PassDatatoLoanDetails(reportController.coResultID, reportController.detail, false);
                            btnViewCARAMCIReport.Visible = true;
                        }
                        hfCoCCRIS.Value = "";
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtil.logError(ex.ToString(), sql.ToString());
                sqlString.displayAlert(this, KeyVal.UnknownError);
            }
            finally { db.Close(); }
        }
        private void Set5RowGridView()
        {
            List<int> rows = new List<int>();

            for (int i = 0; i < row5; i++)
                rows.Add(i);

            #region MainGridView
            gvMainCommercialLoan.DataSource = rows;
            gvMainCommercialLoan.DataBind();

            gvMainCreditCard.DataSource = rows;
            gvMainCreditCard.DataBind();

            gvMainHirePurchase.DataSource = rows;
            gvMainHirePurchase.DataBind();

            gvMainHousingLoan.DataSource = rows;
            gvMainHousingLoan.DataBind();
            gvMainHousingLoanRecalc.DataSource = rows;
            gvMainHousingLoanRecalc.DataBind();

            gvMainOD.DataSource = rows;
            gvMainOD.DataBind();

            gvMainPersonalLoan.DataSource = rows;
            gvMainPersonalLoan.DataBind();

            gvMainXProduct.DataSource = rows;
            gvMainXProduct.DataBind();

            #endregion

            #region CoGridView
            gvCoCommercialLoan.DataSource = rows;
            gvCoCommercialLoan.DataBind();

            gvCoCreditCard.DataSource = rows;
            gvCoCreditCard.DataBind();

            gvCoHirePurchase.DataSource = rows;
            gvCoHirePurchase.DataBind();

            gvCoHousingLoan.DataSource = rows;
            gvCoHousingLoan.DataBind();
            gvCoHousingLoanRecalc.DataSource = rows;
            gvCoHousingLoanRecalc.DataBind();

            gvCoOD.DataSource = rows;
            gvCoOD.DataBind();

            gvCoPersonalLoan.DataSource = rows;
            gvCoPersonalLoan.DataBind();

            gvCoXProduct.DataSource = rows;
            gvCoXProduct.DataBind();
            #endregion

        }
        private void Set10RowGridView()
        {
            List<int> rows = new List<int>();

            for (int i = 0; i < row10; i++)
                rows.Add(i);

            #region MainGridView

            gvMainCreditCard.DataSource = rows;
            gvMainCreditCard.DataBind();

            gvMainXProduct.DataSource = rows;
            gvMainXProduct.DataBind();

            #endregion

            #region CoGridView

            gvCoCreditCard.DataSource = rows;
            gvCoCreditCard.DataBind();

            gvCoXProduct.DataSource = rows;
            gvCoXProduct.DataBind();

            #endregion

        }
        #endregion

        #region DataPassing
        private int CheckGVTextBox(GridView gridviewname, bool isMain = true)
        {
            int errorCount = 0;
            try
            {
                for (int i = 0; i < row5; i++)
                {
                    TextBox txtoriginal;
                    TextBox txtoutstanding;
                    TextBox txtrepayment;

                    if (isMain)
                    {
                        txtoriginal = (TextBox)gridviewname.Rows[i].Cells[0].FindControl("txtMainOriginalVal");
                        txtoutstanding = (TextBox)gridviewname.Rows[i].Cells[1].FindControl("txtMainOutstandinglVal");
                        txtrepayment = (TextBox)gridviewname.Rows[i].Cells[2].FindControl("txtMainRepaymentlVal");
                    }
                    else
                    {
                        txtoriginal = (TextBox)gridviewname.Rows[i].Cells[0].FindControl("txtCoOriginalVal");
                        txtoutstanding = (TextBox)gridviewname.Rows[i].Cells[1].FindControl("txtCoOutstandinglVal");
                        txtrepayment = (TextBox)gridviewname.Rows[i].Cells[2].FindControl("txtCoRepaymentlVal");
                    }


                    if (!String.IsNullOrEmpty(txtoriginal.Text))
                    {
                        if (!Validation.isDecimal(txtoriginal.Text))
                        {
                            errorCount = errorCount + 1;
                        }
                    }

                    if (!String.IsNullOrEmpty(txtoutstanding.Text))
                    {
                        if (!Validation.isDecimal(txtoutstanding.Text))
                        {
                            errorCount = errorCount + 1;
                        }
                    }

                    if (!String.IsNullOrEmpty(txtrepayment.Text))
                    {
                        if (!Validation.isDecimal(txtrepayment.Text))
                        {
                            errorCount = errorCount + 1;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtil.logError(ex.ToString(), "Error for Gridview validation :" + gridviewname.ID);
                sqlString.displayAlert(this, KeyVal.UnknownError);
            }
            return errorCount;

        }
        private void ClearGVTextBox(GridView gridviewname, bool isMain = false)
        {
            if (isMain)
            {
                foreach (GridViewRow gvrow in gridviewname.Rows)
                {
                    ((TextBox)gvrow.Cells[0].FindControl("txtMainOriginalVal")).Text = "";
                    ((TextBox)gvrow.Cells[1].FindControl("txtMainOutstandinglVal")).Text = "";
                    ((TextBox)gvrow.Cells[2].FindControl("txtMainRepaymentlVal")).Text = "";
                }
            }
            else
            {
                foreach (GridViewRow gvrow in gridviewname.Rows)
                {
                    ((TextBox)gvrow.Cells[0].FindControl("txtCoOriginalVal")).Text = "";
                    ((TextBox)gvrow.Cells[1].FindControl("txtCoOutstandinglVal")).Text = "";
                    ((TextBox)gvrow.Cells[2].FindControl("txtCoRepaymentlVal")).Text = "";
                }
            }

        }
        public string ReturnXmlDocument(HttpRequestMessage request)
        {
            var doc = new XmlDocument();
            doc.Load(request.Content.ReadAsStreamAsync().Result);
            return doc.DocumentElement.OuterXml;
        }
        private void RetrieveDataIntoGridView(GridView gridviewname, string loanid, string requestid, int noofRows, bool isMain = true)
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();
            try
            {
                for (int i = 0; i < noofRows; i++)
                {
                    TextBox txtoriginal;
                    TextBox txtoutstanding;
                    TextBox txtrepayment;

                    if (isMain)
                    {
                        txtoriginal = (TextBox)gridviewname.Rows[i].Cells[0].FindControl("txtMainOriginalVal");
                        txtoutstanding = (TextBox)gridviewname.Rows[i].Cells[1].FindControl("txtMainOutstandinglVal");
                        txtrepayment = (TextBox)gridviewname.Rows[i].Cells[2].FindControl("txtMainRepaymentlVal");
                    }
                    else
                    {
                        txtoriginal = (TextBox)gridviewname.Rows[i].Cells[0].FindControl("txtCoOriginalVal");
                        txtoutstanding = (TextBox)gridviewname.Rows[i].Cells[1].FindControl("txtCoOutstandinglVal");
                        txtrepayment = (TextBox)gridviewname.Rows[i].Cells[2].FindControl("txtCoRepaymentlVal");
                    }

                    sql.Clear();
                    sql.Append(" SELECT TOP 1 OriginalValue, OutStandingValue, Repayment ");
                    sql.Append(" FROM tbl_TLoanDetail WITH(NOLOCK) ");
                    sql.Append(" WHERE RequestID = '" + requestid + "' AND LoanID = '" + loanid + "' AND RowID = '" + (i + 1) + "' AND isMain = '"
                        + isMain + "'");// AND BankID='12'; ");//Use Classic Bank Temp
                    db.OpenTable(sql.ToString());

                    if (db.RecordCount() > 0)
                    {
                        txtoriginal.Text = ConvertHelper.ConvertToDecimal(db.Item("OriginalValue").ToString(), 0).ToString("N");
                        txtoutstanding.Text = ConvertHelper.ConvertToDecimal(db.Item("OutStandingValue").ToString(), 0).ToString("N");
                        txtrepayment.Text = ConvertHelper.ConvertToDecimal(db.Item("Repayment").ToString(), 0).ToString("N");
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtil.logError(ex.ToString(), sql.ToString());
                sqlString.displayAlert(this, KeyVal.UnknownError);
            }
            finally { db.Close(); }
        }
        
        private void PassDatatoLoanDetails(string resultId, object reportSection, bool isMain)
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();
            string original = "";
            string outstanding = "";
            string repayment = "";
            string loanid = "";
            string reportType = string.Empty;

            section_ccris section_ccris;
            List<ccris_account_detail> CTOSaccounts = new List<ccris_account_detail>();
            List<special_attention_acc_detail> special_Attention_Accs = new List<special_attention_acc_detail>();

            RAMCI.Model.Report.xmlBanking_infoCcris_banking_details detail;
            List<RAMCI.Model.Report.xmlBanking_infoCcris_banking_detailsItem> RAMCIaccounts = new List<RAMCI.Model.Report.xmlBanking_infoCcris_banking_detailsItem>();
            List<RAMCI.Model.Report.xmlBanking_infoCcris_banking_detailsItem> special_attention_account = new List<RAMCI.Model.Report.xmlBanking_infoCcris_banking_detailsItem>();

            if (reportSection is section_ccris)
            {
                reportType = "CTOS";
                section_ccris = (section_ccris)reportSection;
                if (section_ccris.accounts != null && section_ccris.accounts.account != null)
                    CTOSaccounts = section_ccris.accounts.account.ToList();
                if (section_ccris.special_attention_accs != null)
                    special_Attention_Accs = section_ccris.special_attention_accs.ToList();
            }
            if (reportSection is RAMCI.Model.Report.xmlBanking_infoCcris_banking_details)
            {
                reportType = "RAMCI";
                detail = (RAMCI.Model.Report.xmlBanking_infoCcris_banking_details)reportSection;
                if (detail.outstanding_credit != null)
                    RAMCIaccounts = detail.outstanding_credit.ToList();
                if (detail.special_attention_account.FirstOrDefault() != null)
                    special_attention_account = detail.special_attention_account.ToList();
            }

            //Clear Gridview TextBox
            ClearGVTextBox(isMain ? gvMainCommercialLoan : gvCoCommercialLoan, isMain);
            ClearGVTextBox(isMain ? gvMainCreditCard : gvCoCreditCard, isMain);
            ClearGVTextBox(isMain ? gvMainHirePurchase : gvCoHirePurchase, isMain);
            ClearGVTextBox(isMain ? gvMainHousingLoan : gvCoHousingLoan, isMain);
            ClearGVTextBox(isMain ? gvMainOD : gvCoOD, isMain);
            ClearGVTextBox(isMain ? gvMainPersonalLoan : gvCoPersonalLoan, isMain);
            ClearGVTextBox(isMain ? gvMainXProduct : gvCoXProduct, isMain);

            sql.Clear();
            sql.Append(" SELECT CTOS_Result_Id ");
            sql.Append(" FROM tbl_CTOS_Report_Loan_Details WITH(NOLOCK) ");
            sql.Append(" WHERE CTOS_Result_Id = N'" + secure.RC(resultId) + "'");
            db.OpenTable(sql.ToString());

            if (db.RecordCount() <= 0)
            {
                if (reportType.Equals("CTOS"))
                {
                    if (CTOSaccounts.Count > 0)
                        reportController.InsertCTOSReport(CTOSaccounts, isMain);
                    if (special_Attention_Accs.Count > 0)
                        reportController.InsertCTOSReport(special_Attention_Accs, isMain);
                }
                if (reportType.Equals("RAMCI"))
                {
                    if (RAMCIaccounts != null)
                        reportController.InsertRAMCIReport(RAMCIaccounts, isMain);
                    if (special_attention_account != null)
                        reportController.InsertRAMCIReport(special_attention_account, isMain, (special_attention_account != null));
                }
            }

            if (reportType.Equals("CTOS") && CTOSaccounts != null)
                foreach (var _account in CTOSaccounts)
                {
                    string capacityCode = _account.capacity.code.ToString();//Own/Joint/Partner
                    string loanType = _account.sub_accounts[0].facility.code.ToString();
                    sql.Clear();
                    sql.Append("SELECT LoanID FROM tbl_LoanTypesXML WITH(NOLOCK) WHERE LoanTypeCode = '" + loanType + "';");
                    db.OpenTable(sql.ToString());
                    if (db.RecordCount() > 0)
                    {
                        loanid = db.Item("LoanID");
                        switch (loanid)
                        {
                            case "1"://Housing Loan
                                for (int i = 0; i < _account.sub_accounts.Count(); i++)
                                {
                                    original = string.IsNullOrEmpty(_account.limit.ToString()) ? "" : _account.limit.ToString();
                                    outstanding = string.IsNullOrEmpty(_account.sub_accounts[i].cr_positions[0].balance.ToString()) ? "" : _account.sub_accounts[i].cr_positions[0].balance.ToString();
                                    repayment = string.IsNullOrEmpty(_account.sub_accounts[i].cr_positions[0].inst_amount.ToString()) ? "" : _account.sub_accounts[i].cr_positions[0].inst_amount.ToString();
                                    if ((repayment.Equals("0") || string.IsNullOrEmpty(repayment)) && !string.IsNullOrEmpty(original))
                                        repayment = reportController.RepaymentEffective(12, ConvertHelper.ConvertToDecimal(original, 0), 1, lblMainAge.Text, lblCoAge.Text).ToString(); //temp
                                    if ((capacityCode.Equals("Joint") || capacityCode.Equals("Partner")) && !string.IsNullOrWhiteSpace(repayment) && double.TryParse(repayment, out double parseRepayment))
                                        repayment = (parseRepayment / 2).ToString();

                                    for (int j = 0; j < row5; j++)
                                    {
                                        TextBox txtOriginalHousingLoan = (TextBox)(isMain ? gvMainHousingLoan.Rows[j].Cells[0].FindControl("txtMainOriginalVal") : gvCoHousingLoan.Rows[j].Cells[0].FindControl("txtCoOriginalVal"));
                                        TextBox txtOutstandingHousingLoan = (TextBox)(isMain ? gvMainHousingLoan.Rows[j].Cells[1].FindControl("txtMainOutstandinglVal") : gvCoHousingLoan.Rows[j].Cells[1].FindControl("txtCoOutstandinglVal"));
                                        TextBox txtRepaymentHousingLoan = (TextBox)(isMain ? gvMainHousingLoan.Rows[j].Cells[2].FindControl("txtMainRepaymentlVal") : gvCoHousingLoan.Rows[j].Cells[2].FindControl("txtCoRepaymentlVal"));
                                        if (String.IsNullOrEmpty(txtOriginalHousingLoan.Text) && String.IsNullOrEmpty(txtOutstandingHousingLoan.Text) && String.IsNullOrEmpty(txtRepaymentHousingLoan.Text))
                                        {
                                            //DateTime approvalDate = DateTime.ParseExact(_account.approval_date, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
                                            //if (DateTime.Now.Subtract(approvalDate).Days <= (3 * 365))
                                            //{
                                            //    TextBox txtMainRepaymentValMainHousingLoanRecalc = (TextBox)gvMainHousingLoanRecalc.Rows[j].Cells[2].FindControl("txtMainRepaymentlVal");
                                            //    txtMainRepaymentValMainHousingLoanRecalc.Text = repayment;
                                            //    repayment = "0";
                                            //}
                                            txtOriginalHousingLoan.Text = original;
                                            txtOutstandingHousingLoan.Text = outstanding;
                                            txtRepaymentHousingLoan.Text = repayment;
                                            break;
                                        }
                                    }
                                }
                                break;
                            case "2"://Commercial Loan
                                for (int i = 0; i < _account.sub_accounts.Count(); i++)
                                {
                                    original = string.IsNullOrEmpty(_account.limit.ToString()) ? "" : _account.limit.ToString();
                                    outstanding = string.IsNullOrEmpty(_account.sub_accounts[i].cr_positions[0].balance.ToString()) ? "" : _account.sub_accounts[i].cr_positions[0].balance.ToString();
                                    repayment = string.IsNullOrEmpty(_account.sub_accounts[i].cr_positions[0].inst_amount.ToString()) ? "" : _account.sub_accounts[i].cr_positions[0].inst_amount.ToString();
                                    if ((repayment.Equals("0") || string.IsNullOrEmpty(repayment)) && !string.IsNullOrEmpty(outstanding))
                                        repayment = reportController.RepaymentEffective(12, ConvertHelper.ConvertToDecimal(outstanding, 0), 2, lblMainAge.Text, lblCoAge.Text).ToString();
                                    if ((capacityCode.Equals("Joint") || capacityCode.Equals("Partner")) && !string.IsNullOrWhiteSpace(repayment) && double.TryParse(repayment, out double parseRepayment))
                                        repayment = (parseRepayment / 2).ToString();

                                    for (int j = 0; j < row5; j++)
                                    {
                                        TextBox txtOriginalCommercialLoan = (TextBox)(isMain ? gvMainCommercialLoan.Rows[j].Cells[0].FindControl("txtMainOriginalVal") : gvCoCommercialLoan.Rows[j].Cells[0].FindControl("txtCoOriginalVal"));
                                        TextBox txtOutstandingCommercialLoan = (TextBox)(isMain ? gvMainCommercialLoan.Rows[j].Cells[1].FindControl("txtMainOutstandinglVal") : gvCoCommercialLoan.Rows[j].Cells[1].FindControl("txtCoOutstandinglVal"));
                                        TextBox txtRepaymentCommercialLoan = (TextBox)(isMain ? gvMainCommercialLoan.Rows[j].Cells[2].FindControl("txtMainRepaymentlVal") : gvCoCommercialLoan.Rows[j].Cells[2].FindControl("txtCoRepaymentlVal"));
                                        if (String.IsNullOrEmpty(txtOriginalCommercialLoan.Text) && String.IsNullOrEmpty(txtOutstandingCommercialLoan.Text) && String.IsNullOrEmpty(txtRepaymentCommercialLoan.Text))
                                        {
                                            txtOriginalCommercialLoan.Text = original;
                                            txtOutstandingCommercialLoan.Text = outstanding;
                                            txtRepaymentCommercialLoan.Text = repayment;
                                            break;
                                        }
                                    }
                                }
                                break;
                            case "3"://Personal Loan
                                for (int i = 0; i < _account.sub_accounts.Count(); i++)
                                {
                                    original = string.IsNullOrEmpty(_account.limit.ToString()) ? "" : _account.limit.ToString();
                                    outstanding = string.IsNullOrEmpty(_account.sub_accounts[i].cr_positions[0].balance.ToString()) ? "" : _account.sub_accounts[i].cr_positions[0].balance.ToString();
                                    repayment = string.IsNullOrEmpty(_account.sub_accounts[i].cr_positions[0].inst_amount.ToString()) ? "" : _account.sub_accounts[i].cr_positions[0].inst_amount.ToString();
                                    if ((repayment.Equals("0") || string.IsNullOrEmpty(repayment)) && !string.IsNullOrEmpty(outstanding))
                                        repayment = reportController.RepaymentFlat(12, ConvertHelper.ConvertToDecimal(outstanding, 0), 3).ToString();
                                    if ((capacityCode.Equals("Joint") || capacityCode.Equals("Partner")) && !string.IsNullOrWhiteSpace(repayment) && double.TryParse(repayment, out double parseRepayment))
                                        repayment = (parseRepayment / 2).ToString();

                                    for (int j = 0; j < row5; j++)
                                    {
                                        TextBox txtOriginalPersonalLoan = (TextBox)(isMain ? gvMainPersonalLoan.Rows[j].Cells[0].FindControl("txtMainOriginalVal") : gvCoPersonalLoan.Rows[j].Cells[0].FindControl("txtCoOriginalVal"));
                                        TextBox txtOutstandingPersonalLoan = (TextBox)(isMain ? gvMainPersonalLoan.Rows[j].Cells[1].FindControl("txtMainOutstandinglVal") : gvCoPersonalLoan.Rows[j].Cells[1].FindControl("txtCoOutstandinglVal"));
                                        TextBox txtRepaymentPersonalLoan = (TextBox)(isMain ? gvMainPersonalLoan.Rows[j].Cells[2].FindControl("txtMainRepaymentlVal") : gvCoPersonalLoan.Rows[j].Cells[2].FindControl("txtCoRepaymentlVal"));
                                        if (String.IsNullOrEmpty(txtOriginalPersonalLoan.Text) && String.IsNullOrEmpty(txtOutstandingPersonalLoan.Text) && String.IsNullOrEmpty(txtRepaymentPersonalLoan.Text))
                                        {
                                            txtOriginalPersonalLoan.Text = original;
                                            txtOutstandingPersonalLoan.Text = outstanding;
                                            txtRepaymentPersonalLoan.Text = repayment;
                                            break;
                                        }
                                    }
                                }
                                break;
                            case "4"://OverDraft
                                for (int i = 0; i < _account.sub_accounts.Count(); i++)
                                {
                                    original = string.IsNullOrEmpty(_account.limit.ToString()) ? "" : _account.limit.ToString();
                                    outstanding = string.IsNullOrEmpty(_account.sub_accounts[i].cr_positions[0].balance.ToString()) ? "" : _account.sub_accounts[i].cr_positions[0].balance.ToString();
                                    repayment = string.IsNullOrEmpty(_account.sub_accounts[i].cr_positions[0].inst_amount.ToString()) ? "" : _account.sub_accounts[i].cr_positions[0].inst_amount.ToString();
                                    if ((repayment.Equals("0") || string.IsNullOrEmpty(repayment)) && !string.IsNullOrEmpty(original))
                                        repayment = reportController.RepaymentFlat(12, ConvertHelper.ConvertToDecimal(original, 0), 4).ToString();
                                    if ((capacityCode.Equals("Joint") || capacityCode.Equals("Partner")) && !string.IsNullOrWhiteSpace(repayment) && double.TryParse(repayment, out double parseRepayment))
                                        repayment = (parseRepayment / 2).ToString();

                                    for (int j = 0; j < row5; j++)
                                    {
                                        TextBox txtOriginalOD = (TextBox)(isMain ? gvMainOD.Rows[j].Cells[0].FindControl("txtMainOriginalVal") : gvCoOD.Rows[j].Cells[0].FindControl("txtCoOriginalVal"));
                                        TextBox txtOutstandingOD = (TextBox)(isMain ? gvMainOD.Rows[j].Cells[1].FindControl("txtMainOutstandinglVal") : gvCoOD.Rows[j].Cells[1].FindControl("txtCoOutstandinglVal"));
                                        TextBox txtRepaymentOD = (TextBox)(isMain ? gvMainOD.Rows[j].Cells[2].FindControl("txtMainRepaymentlVal") : gvCoOD.Rows[j].Cells[2].FindControl("txtCoRepaymentlVal"));
                                        if (String.IsNullOrEmpty(txtOriginalOD.Text) && String.IsNullOrEmpty(txtOutstandingOD.Text) && String.IsNullOrEmpty(txtRepaymentOD.Text))
                                        {
                                            txtOriginalOD.Text = original;
                                            txtOutstandingOD.Text = outstanding;
                                            txtRepaymentOD.Text = repayment;
                                            //here
                                            break;
                                        }
                                    }
                                }
                                break;
                            case "5"://X-Product
                                for (int i = 0; i < _account.sub_accounts.Count(); i++)
                                {
                                    original = string.IsNullOrEmpty(_account.limit.ToString()) ? "" : _account.limit.ToString();
                                    outstanding = string.IsNullOrEmpty(_account.sub_accounts[i].cr_positions[0].balance.ToString()) ? "" : _account.sub_accounts[i].cr_positions[0].balance.ToString();
                                    repayment = string.IsNullOrEmpty(_account.sub_accounts[i].cr_positions[0].inst_amount.ToString()) ? "" : _account.sub_accounts[i].cr_positions[0].inst_amount.ToString();
                                    if ((repayment.Equals("0") || string.IsNullOrEmpty(repayment)) && !string.IsNullOrEmpty(outstanding))
                                        repayment = reportController.RepaymentEffective(12, ConvertHelper.ConvertToDecimal(outstanding, 0), 5, lblMainAge.Text, lblCoAge.Text).ToString();
                                    if ((capacityCode.Equals("Joint") || capacityCode.Equals("Partner")) && !string.IsNullOrWhiteSpace(repayment) && double.TryParse(repayment, out double parseRepayment))
                                        repayment = (parseRepayment / 2).ToString();

                                    for (int j = 0; j < row10; j++)
                                    {

                                        TextBox txtOriginalXProduct = (TextBox)(isMain ? gvMainXProduct.Rows[j].Cells[0].FindControl("txtMainOriginalVal") : gvCoXProduct.Rows[j].Cells[0].FindControl("txtCoOriginalVal"));
                                        TextBox txtOutstandingXProduct = (TextBox)(isMain ? gvMainXProduct.Rows[j].Cells[1].FindControl("txtMainOutstandinglVal") : gvCoXProduct.Rows[j].Cells[1].FindControl("txtCoOutstandinglVal"));
                                        TextBox txtRepaymentXProduct = (TextBox)(isMain ? gvMainXProduct.Rows[j].Cells[2].FindControl("txtMainRepaymentlVal") : gvCoXProduct.Rows[j].Cells[2].FindControl("txtCoRepaymentlVal"));
                                        if (String.IsNullOrEmpty(txtOriginalXProduct.Text) && String.IsNullOrEmpty(txtOutstandingXProduct.Text) && String.IsNullOrEmpty(txtRepaymentXProduct.Text))
                                        {
                                            txtOriginalXProduct.Text = original;
                                            txtOutstandingXProduct.Text = outstanding;
                                            txtRepaymentXProduct.Text = repayment;
                                            break;
                                        }
                                    }
                                }
                                break;
                            case "6"://Hire Purchase - Car Loan
                                for (int i = 0; i < _account.sub_accounts.Count(); i++)
                                {
                                    original = string.IsNullOrEmpty(_account.limit.ToString()) ? "" : _account.limit.ToString();
                                    outstanding = string.IsNullOrEmpty(_account.sub_accounts[i].cr_positions[0].balance.ToString()) ? "" : _account.sub_accounts[i].cr_positions[0].balance.ToString();
                                    repayment = string.IsNullOrEmpty(_account.sub_accounts[i].cr_positions[0].inst_amount.ToString()) ? "" : _account.sub_accounts[i].cr_positions[0].inst_amount.ToString();
                                    if ((repayment.Equals("0") || string.IsNullOrEmpty(repayment)) && !string.IsNullOrEmpty(outstanding))
                                        repayment = reportController.RepaymentFlat(12, ConvertHelper.ConvertToDecimal(outstanding, 0), 6).ToString();
                                    if ((capacityCode.Equals("Joint") || capacityCode.Equals("Partner")) && !string.IsNullOrWhiteSpace(repayment) && double.TryParse(repayment, out double parseRepayment))
                                        repayment = (parseRepayment / 2).ToString();

                                    for (int j = 0; j < row5; j++)
                                    {
                                        TextBox txtOriginalHirePurchase = (TextBox)(isMain ? gvMainHirePurchase.Rows[j].Cells[0].FindControl("txtMainOriginalVal") : gvCoHirePurchase.Rows[j].Cells[0].FindControl("txtCoOriginalVal"));
                                        TextBox txtOutstandingHirePurchase = (TextBox)(isMain ? gvMainHirePurchase.Rows[j].Cells[1].FindControl("txtMainOutstandinglVal") : gvCoHirePurchase.Rows[j].Cells[1].FindControl("txtCoOutstandinglVal"));
                                        TextBox txtRepaymentHirePurchase = (TextBox)(isMain ? gvMainHirePurchase.Rows[j].Cells[2].FindControl("txtMainRepaymentlVal") : gvCoHirePurchase.Rows[j].Cells[2].FindControl("txtCoRepaymentlVal"));
                                        if (String.IsNullOrEmpty(txtOriginalHirePurchase.Text) && String.IsNullOrEmpty(txtOutstandingHirePurchase.Text) && String.IsNullOrEmpty(txtRepaymentHirePurchase.Text))
                                        {
                                            txtOriginalHirePurchase.Text = original;
                                            txtOutstandingHirePurchase.Text = outstanding;
                                            txtRepaymentHirePurchase.Text = repayment;
                                            break;
                                        }
                                    }
                                }
                                break;
                            case "7"://Credit Card
                                for (int i = 0; i < _account.sub_accounts.Count(); i++)
                                {
                                    outstanding = string.IsNullOrEmpty(_account.sub_accounts[i].cr_positions[0].balance.ToString()) ? "" : _account.sub_accounts[i].cr_positions[0].balance.ToString();
                                    if (!String.IsNullOrEmpty(outstanding))
                                        if (ConvertHelper.ConvertToDecimal(outstanding, 0) > 0)
                                        {
                                            repayment = reportController.RepaymentMin(12, ConvertHelper.ConvertToDecimal(outstanding, 0)).ToString();
                                            for (int j = 0; j < row10; j++)
                                            {
                                                TextBox txtOutstandingCreditCard = (TextBox)(isMain ? gvMainCreditCard.Rows[j].Cells[1].FindControl("txtMainOutstandinglVal") : gvCoCreditCard.Rows[j].Cells[1].FindControl("txtCoOutstandinglVal"));
                                                TextBox txtRepaymentCreditCard = (TextBox)(isMain ? gvMainCreditCard.Rows[j].Cells[2].FindControl("txtMainRepaymentlVal") : gvCoCreditCard.Rows[j].Cells[2].FindControl("txtCoRepaymentlVal"));
                                                if (String.IsNullOrEmpty(txtOutstandingCreditCard.Text))
                                                {
                                                    txtOutstandingCreditCard.Text = outstanding;
                                                    txtRepaymentCreditCard.Text = repayment;
                                                    break;
                                                }
                                            }
                                        }
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
            if (reportType.Equals("RAMCI") && RAMCIaccounts != null)
                foreach (var _account in RAMCIaccounts)
                {
                    string capacityCode = _account.master.item.capacity.ToString();//Own/Joint
                    string loanType = _account.sub_account[0].item.facility.ToString();
                    sql.Clear();
                    sql.Append("SELECT LoanID FROM tbl_LoanTypesXML WITH(NOLOCK) WHERE LoanTypeCode = '" + loanType + "';");
                    db.OpenTable(sql.ToString());
                    if (db.RecordCount() > 0)
                    {
                        loanid = db.Item("LoanID");
                        switch (loanid)
                        {
                            case "1"://Housing Loan
                                for (int i = 0; i < _account.sub_account.Count(); i++)
                                {
                                    original = string.IsNullOrEmpty(_account.master.item.limit.ToString()) ? "" : _account.master.item.limit.ToString();
                                    outstanding = string.IsNullOrEmpty(_account.sub_account[i].item.total_outstanding_balance.ToString()) ? "" : _account.sub_account[i].item.total_outstanding_balance.ToString();
                                    repayment = string.IsNullOrEmpty(_account.sub_account[i].item.installment_amount.ToString()) ? "" : _account.sub_account[i].item.installment_amount.ToString();
                                    if ((capacityCode.Equals("Joint".ToUpper()) || capacityCode.Equals("Partner".ToUpper())) && !string.IsNullOrWhiteSpace(repayment) && double.TryParse(repayment, out double parseRepayment))
                                        repayment = (parseRepayment / 2).ToString();

                                    for (int j = 0; j < row5; j++)
                                    {
                                        TextBox txtOriginalHousingLoan = (TextBox)(isMain ? gvMainHousingLoan.Rows[j].Cells[0].FindControl("txtMainOriginalVal") : gvCoHousingLoan.Rows[j].Cells[0].FindControl("txtCoOriginalVal"));
                                        TextBox txtOutstandingHousingLoan = (TextBox)(isMain ? gvMainHousingLoan.Rows[j].Cells[1].FindControl("txtMainOutstandinglVal") : gvCoHousingLoan.Rows[j].Cells[1].FindControl("txtCoOutstandinglVal"));
                                        TextBox txtRepaymentHousingLoan = (TextBox)(isMain ? gvMainHousingLoan.Rows[j].Cells[2].FindControl("txtMainRepaymentlVal") : gvCoHousingLoan.Rows[j].Cells[2].FindControl("txtCoRepaymentlVal"));
                                        if (String.IsNullOrEmpty(txtOriginalHousingLoan.Text) && String.IsNullOrEmpty(txtOutstandingHousingLoan.Text) && String.IsNullOrEmpty(txtRepaymentHousingLoan.Text))
                                        {
                                            txtOriginalHousingLoan.Text = original;
                                            txtOutstandingHousingLoan.Text = outstanding;
                                            txtRepaymentHousingLoan.Text = repayment;
                                            break;
                                        }
                                    }
                                }
                                break;
                            case "2"://Commercial Loan
                                for (int i = 0; i < _account.sub_account.Count(); i++)
                                {
                                    original = string.IsNullOrEmpty(_account.master.item.limit.ToString()) ? "" : _account.master.item.limit.ToString();
                                    outstanding = string.IsNullOrEmpty(_account.sub_account[i].item.total_outstanding_balance.ToString()) ? "" : _account.sub_account[i].item.total_outstanding_balance.ToString();
                                    repayment = string.IsNullOrEmpty(_account.sub_account[i].item.installment_amount.ToString()) ? "" : _account.sub_account[i].item.installment_amount.ToString();
                                    if ((capacityCode.Equals("Joint".ToUpper()) || capacityCode.Equals("Partner".ToUpper())) && !string.IsNullOrWhiteSpace(repayment) && double.TryParse(repayment, out double parseRepayment))
                                        repayment = (parseRepayment / 2).ToString();

                                    for (int j = 0; j < row5; j++)
                                    {
                                        TextBox txtOriginalCommercialLoan = (TextBox)(isMain ? gvMainCommercialLoan.Rows[j].Cells[0].FindControl("txtMainOriginalVal") : gvCoCommercialLoan.Rows[j].Cells[0].FindControl("txtCoOriginalVal"));
                                        TextBox txtOutstandingCommercialLoan = (TextBox)(isMain ? gvMainCommercialLoan.Rows[j].Cells[1].FindControl("txtMainOutstandinglVal") : gvCoCommercialLoan.Rows[j].Cells[1].FindControl("txtCoOutstandinglVal"));
                                        TextBox txtRepaymentCommercialLoan = (TextBox)(isMain ? gvMainCommercialLoan.Rows[j].Cells[2].FindControl("txtMainRepaymentlVal") : gvCoCommercialLoan.Rows[j].Cells[2].FindControl("txtCoRepaymentlVal"));
                                        if (String.IsNullOrEmpty(txtOriginalCommercialLoan.Text) && String.IsNullOrEmpty(txtOutstandingCommercialLoan.Text) && String.IsNullOrEmpty(txtRepaymentCommercialLoan.Text))
                                        {
                                            txtOriginalCommercialLoan.Text = original;
                                            txtOutstandingCommercialLoan.Text = outstanding;
                                            txtRepaymentCommercialLoan.Text = repayment;
                                            break;
                                        }
                                    }
                                }
                                break;
                            case "3"://Personal Loan
                                for (int i = 0; i < _account.sub_account.Count(); i++)
                                {
                                    original = string.IsNullOrEmpty(_account.master.item.limit.ToString()) ? "" : _account.master.item.limit.ToString();
                                    outstanding = string.IsNullOrEmpty(_account.sub_account[i].item.total_outstanding_balance.ToString()) ? "" : _account.sub_account[i].item.total_outstanding_balance.ToString();
                                    repayment = string.IsNullOrEmpty(_account.sub_account[i].item.installment_amount.ToString()) ? "" : _account.sub_account[i].item.installment_amount.ToString();
                                    if ((capacityCode.Equals("Joint".ToUpper()) || capacityCode.Equals("Partner".ToUpper())) && !string.IsNullOrWhiteSpace(repayment) && double.TryParse(repayment, out double parseRepayment))
                                        repayment = (parseRepayment / 2).ToString();

                                    for (int j = 0; j < row5; j++)
                                    {
                                        TextBox txtOriginalPersonalLoan = (TextBox)(isMain ? gvMainPersonalLoan.Rows[j].Cells[0].FindControl("txtMainOriginalVal") : gvCoPersonalLoan.Rows[j].Cells[0].FindControl("txtCoOriginalVal"));
                                        TextBox txtOutstandingPersonalLoan = (TextBox)(isMain ? gvMainPersonalLoan.Rows[j].Cells[1].FindControl("txtMainOutstandinglVal") : gvCoPersonalLoan.Rows[j].Cells[1].FindControl("txtCoOutstandinglVal"));
                                        TextBox txtRepaymentPersonalLoan = (TextBox)(isMain ? gvMainPersonalLoan.Rows[j].Cells[2].FindControl("txtMainRepaymentlVal") : gvCoPersonalLoan.Rows[j].Cells[2].FindControl("txtCoRepaymentlVal"));
                                        if (String.IsNullOrEmpty(txtOriginalPersonalLoan.Text) && String.IsNullOrEmpty(txtOutstandingPersonalLoan.Text) && String.IsNullOrEmpty(txtRepaymentPersonalLoan.Text))
                                        {
                                            txtOriginalPersonalLoan.Text = original;
                                            txtOutstandingPersonalLoan.Text = outstanding;
                                            txtRepaymentPersonalLoan.Text = repayment;
                                            break;
                                        }
                                    }
                                }
                                break;
                            case "4"://OverDraft
                                for (int i = 0; i < _account.sub_account.Count(); i++)
                                {
                                    original = string.IsNullOrEmpty(_account.master.item.limit.ToString()) ? "" : _account.master.item.limit.ToString();
                                    outstanding = string.IsNullOrEmpty(_account.sub_account[i].item.total_outstanding_balance.ToString()) ? "" : _account.sub_account[i].item.total_outstanding_balance.ToString();
                                    repayment = string.IsNullOrEmpty(_account.sub_account[i].item.installment_amount.ToString()) ? "" : _account.sub_account[i].item.installment_amount.ToString();
                                    if ((capacityCode.Equals("Joint".ToUpper()) || capacityCode.Equals("Partner".ToUpper())) && !string.IsNullOrWhiteSpace(repayment) && double.TryParse(repayment, out double parseRepayment))
                                        repayment = (parseRepayment / 2).ToString();

                                    for (int j = 0; j < row5; j++)
                                    {

                                        TextBox txtOriginalOD = (TextBox)(isMain ? gvMainOD.Rows[j].Cells[0].FindControl("txtMainOriginalVal") : gvCoOD.Rows[j].Cells[0].FindControl("txtCoOriginalVal"));
                                        TextBox txtOutstandingOD = (TextBox)(isMain ? gvMainOD.Rows[j].Cells[1].FindControl("txtMainOutstandinglVal") : gvCoOD.Rows[j].Cells[1].FindControl("txtCoOutstandinglVal"));
                                        TextBox txtRepaymentOD = (TextBox)(isMain ? gvMainOD.Rows[j].Cells[2].FindControl("txtMainRepaymentlVal") : gvCoOD.Rows[j].Cells[2].FindControl("txtCoRepaymentlVal"));
                                        if (String.IsNullOrEmpty(txtOriginalOD.Text) && String.IsNullOrEmpty(txtOutstandingOD.Text) && String.IsNullOrEmpty(txtRepaymentOD.Text))
                                        {
                                            txtOriginalOD.Text = original;
                                            txtOutstandingOD.Text = outstanding;
                                            txtRepaymentOD.Text = repayment;
                                            break;
                                        }
                                    }
                                }
                                break;
                            case "5"://X-Product
                                for (int i = 0; i < _account.sub_account.Count(); i++)
                                {
                                    original = string.IsNullOrEmpty(_account.master.item.limit.ToString()) ? "" : _account.master.item.limit.ToString();
                                    outstanding = string.IsNullOrEmpty(_account.sub_account[i].item.total_outstanding_balance.ToString()) ? "" : _account.sub_account[i].item.total_outstanding_balance.ToString();
                                    repayment = string.IsNullOrEmpty(_account.sub_account[i].item.installment_amount.ToString()) ? "" : _account.sub_account[i].item.installment_amount.ToString();
                                    if ((capacityCode.Equals("Joint".ToUpper()) || capacityCode.Equals("Partner".ToUpper())) && !string.IsNullOrWhiteSpace(repayment) && double.TryParse(repayment, out double parseRepayment))
                                        repayment = (parseRepayment / 2).ToString();

                                    for (int j = 0; j < row10; j++)
                                    {

                                        TextBox txtOriginalXProduct = (TextBox)(isMain ? gvMainXProduct.Rows[j].Cells[0].FindControl("txtMainOriginalVal") : gvCoXProduct.Rows[j].Cells[0].FindControl("txtCoOriginalVal"));
                                        TextBox txtOutstandingXProduct = (TextBox)(isMain ? gvMainXProduct.Rows[j].Cells[1].FindControl("txtMainOutstandinglVal") : gvCoXProduct.Rows[j].Cells[1].FindControl("txtCoOutstandinglVal"));
                                        TextBox txtRepaymentXProduct = (TextBox)(isMain ? gvMainXProduct.Rows[j].Cells[2].FindControl("txtMainRepaymentlVal") : gvCoXProduct.Rows[j].Cells[2].FindControl("txtCoRepaymentlVal"));
                                        if (String.IsNullOrEmpty(txtOriginalXProduct.Text) && String.IsNullOrEmpty(txtOutstandingXProduct.Text) && String.IsNullOrEmpty(txtRepaymentXProduct.Text))
                                        {
                                            txtOriginalXProduct.Text = original;
                                            txtOutstandingXProduct.Text = outstanding;
                                            txtRepaymentXProduct.Text = repayment;
                                            break;
                                        }
                                    }
                                }
                                break;
                            case "6"://Hire Purchase - Car Loan
                                for (int i = 0; i < _account.sub_account.Count(); i++)
                                {
                                    original = string.IsNullOrEmpty(_account.master.item.limit.ToString()) ? "" : _account.master.item.limit.ToString();
                                    outstanding = string.IsNullOrEmpty(_account.sub_account[i].item.total_outstanding_balance.ToString()) ? "" : _account.sub_account[i].item.total_outstanding_balance.ToString();
                                    repayment = string.IsNullOrEmpty(_account.sub_account[i].item.installment_amount.ToString()) ? "" : _account.sub_account[i].item.installment_amount.ToString();
                                    if ((capacityCode.Equals("Joint".ToUpper()) || capacityCode.Equals("Partner".ToUpper())) && !string.IsNullOrWhiteSpace(repayment) && double.TryParse(repayment, out double parseRepayment))
                                        repayment = (parseRepayment / 2).ToString();

                                    for (int j = 0; j < row5; j++)
                                    {
                                        TextBox txtOriginalHirePurchase = (TextBox)(isMain ? gvMainHirePurchase.Rows[j].Cells[0].FindControl("txtMainOriginalVal") : gvCoHirePurchase.Rows[j].Cells[0].FindControl("txtCoOriginalVal"));
                                        TextBox txtOutstandingHirePurchase = (TextBox)(isMain ? gvMainHirePurchase.Rows[j].Cells[1].FindControl("txtMainOutstandinglVal") : gvCoHirePurchase.Rows[j].Cells[1].FindControl("txtCoOutstandinglVal"));
                                        TextBox txtRepaymentHirePurchase = (TextBox)(isMain ? gvMainHirePurchase.Rows[j].Cells[2].FindControl("txtMainRepaymentlVal") : gvCoHirePurchase.Rows[j].Cells[2].FindControl("txtCoRepaymentlVal"));
                                        if (String.IsNullOrEmpty(txtOriginalHirePurchase.Text) && String.IsNullOrEmpty(txtOutstandingHirePurchase.Text) && String.IsNullOrEmpty(txtRepaymentHirePurchase.Text))
                                        {
                                            txtOriginalHirePurchase.Text = original;
                                            txtOutstandingHirePurchase.Text = outstanding;
                                            txtRepaymentHirePurchase.Text = repayment;
                                            break;
                                        }
                                    }
                                }
                                break;
                            case "7"://Credit Card
                                for (int i = 0; i < _account.sub_account.Count(); i++)
                                {
                                    outstanding = string.IsNullOrEmpty(_account.sub_account[i].item.total_outstanding_balance.ToString()) ? "" : _account.sub_account[i].item.total_outstanding_balance.ToString();

                                    if (!String.IsNullOrEmpty(outstanding))
                                        if (ConvertHelper.ConvertToDecimal(outstanding, 0) > 0)
                                        {
                                            repayment = reportController.RepaymentMin(12, ConvertHelper.ConvertToDecimal(outstanding, 0)).ToString();
                                            for (int j = 0; j < row10; j++)
                                            {
                                                TextBox txtOutstandingCreditCard = (TextBox)(isMain ? gvMainCreditCard.Rows[j].Cells[1].FindControl("txtMainOutstandinglVal") : gvCoCreditCard.Rows[j].Cells[1].FindControl("txtCoOutstandinglVal"));
                                                TextBox txtRepaymentCreditCard = (TextBox)(isMain ? gvMainCreditCard.Rows[j].Cells[2].FindControl("txtMainRepaymentlVal") : gvCoCreditCard.Rows[j].Cells[2].FindControl("txtCoRepaymentlVal"));
                                                if (string.IsNullOrEmpty(txtOutstandingCreditCard.Text) && string.IsNullOrEmpty(txtRepaymentCreditCard.Text))
                                                {
                                                    txtOutstandingCreditCard.Text = outstanding;
                                                    txtRepaymentCreditCard.Text = repayment;
                                                    break;
                                                }
                                            }
                                        }
                                }
                                break;
                            default:
                                //sqlString.displayAlert2(this, "Don't have any loan");
                                break;
                        }
                    }
                    else
                    {
                        loanid = "";
                    }
                }
            db.Close();
        }
        #endregion

        #region Controls
        protected void btnNext_Click(object sender, EventArgs e)
        {
            if (validationFormTag1())
            {
                if (string.IsNullOrEmpty(txtInterest.Text) || string.IsNullOrWhiteSpace(txtInterest.Text))
                {
                    ReportController reportController = new ReportController();
                    txtInterest.Text = reportController.getIntrestRate(6).ToString();
                }
                if (string.IsNullOrEmpty(txtTenure.Text) || string.IsNullOrWhiteSpace(txtTenure.Text))
                {
                    int MainAge = 0;
                    int CoAge = 0;
                    if (lblMainAge.Text.Trim() != string.Empty)
                        MainAge = Convert.ToInt32(lblMainAge.Text.Trim());
                    if (lblCoAge.Text.Trim() != string.Empty)
                        CoAge = Convert.ToInt32(lblCoAge.Text.Trim());
                    int tenure = reportController.getTenureYear(6);
                    /*
                        Algorithm: Take the younger applicant age, then use 70 - age; .
                         The maximum year will only be 35 even if it's greater than that.
                         If it's lower than 35, the no.of years will be taken. 
                    */
                    if (CoAge > 0)//if co-applicant exist, compare it
                    {
                        if (MainAge <= CoAge)
                        {//MainAge is younger than CoAge
                            MainAge = 70 - MainAge;//calculate max
                            if (MainAge <= tenure)//if less or equal to 35 take MainAge
                                tenure = MainAge;
                            //else if larger than 35 take 35 (else tenure=35;)
                        }
                        else
                        {//CoAge is younger than MainAge
                            CoAge = 70 - CoAge;//calculate max
                            if (CoAge <= tenure)//if less or equal to 35 take CoAge
                                tenure = CoAge;
                            //else if larger than 35 take 35 (else tenure=35;)
                        }
                    }
                    else//if no co-applicant, only check MainAge
                    {
                        MainAge = 70 - MainAge;//calculate max
                        if (MainAge <= tenure)//if less or equal to 35 take MainAge
                            tenure = MainAge;
                        //else if larger than 35 take 35 (else tenure=35;)
                    }
                    if (tenure < 0)
                        tenure = 1;//temp, can't fix due to some business logic
                    txtTenure.Text = tenure.ToString();
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Tab", "$('#HidTab').val('#tab_2');", true);
            }
        }
        protected void btnNext2_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Tab", "$('#HidTab').val('#tab_3');", true);
        }
        protected void btnBack_Click(object sender, EventArgs e)
        {

        }
        protected void txtMainIC_TextChanged(object sender, EventArgs e)
        {
            if (txtMainIC.Text.Trim() != String.Empty)
            {
                if (Validation.isValidMyCard(txtMainIC.Text.Trim()))
                {
                    string icNumber = this.txtMainIC.Text.Trim();
                    string year = icNumber.Substring(0, 1) == "0" || icNumber.Substring(0, 1) == "1" || icNumber.Substring(0, 1) == "2" ? "20" + icNumber.Substring(0, 2) : "19" + icNumber.Substring(0, 2);
                    string month = icNumber.Substring(2, 2);
                    string day = icNumber.Substring(4, 2);
                    DateTime dob = DateTime.ParseExact(month + "/" + day + "/" + year, "MM/dd/yyyy", CultureInfo.InvariantCulture);

                    DateTime today = DateTime.Now;
                    TimeSpan ts = today - dob;
                    DateTime age = DateTime.MinValue + ts;
                    int ageYears = age.Year;
                    lblMainAge.Text = ageYears.ToString();
                }
                else
                    lblMainAge.Text = "";
            }
        }
        protected void txtCoIC_TextChanged(object sender, EventArgs e)
        {
            if (txtCoIC.Text.Trim() != String.Empty)
            {
                if (Validation.isValidMyCard(txtCoIC.Text.Trim()))
                {
                    string icNumber = this.txtCoIC.Text.Trim();
                    string year = icNumber.Substring(0, 1) == "0" || icNumber.Substring(0, 1) == "1" || icNumber.Substring(0, 1) == "2" ? "20" + icNumber.Substring(0, 2) : "19" + icNumber.Substring(0, 2);
                    string month = icNumber.Substring(2, 2);
                    string day = icNumber.Substring(4, 2);
                    DateTime dob = DateTime.ParseExact(month + "/" + day + "/" + year, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    DateTime today = DateTime.Now;
                    TimeSpan ts = today - dob;
                    DateTime age = DateTime.MinValue + ts;
                    int ageYears = age.Year;
                    lblCoAge.Text = ageYears.ToString();
                }
                else
                    lblCoAge.Text = "";
            }
        }
        protected void ddlHaveCoApplicant_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlHaveCoApplicant.SelectedValue == "1")
            {
                trCoApplicantTable.Visible = true;
                trCoApplicant.Visible = true;
                divCoLoanDetail.Visible = true;
            }
            else
            {
                trCoApplicantTable.Visible = false;
                trCoApplicant.Visible = false;
                divCoLoanDetail.Visible = false;
            }
        }
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            FileUpload fupload = (FileUpload)fu1;

            if (fupload != null && fupload.HasFile)
            {
                string path = "/Upload/Temp/RequestDocument/";
                string physicalPath = Server.MapPath("~" + path);
                FileInfo Finfo = new FileInfo(fupload.PostedFile.FileName);
                string filename = DateTime.Now.ToString("ddMMyyyyhhmmssfff") + "_" + login_user.UserId + "_CCRIS_" + Finfo.Extension;
                string fullPhysical = physicalPath + filename;
                if (!Directory.Exists(physicalPath))
                    Directory.CreateDirectory(physicalPath);

                if (File.Exists(physicalPath))
                    File.Delete(fullPhysical);

                fupload.SaveAs(fullPhysical);
                lblFileUpload.Text = fullPhysical.ToString();
                Image imgDisplay = (Image)img_fu1;
                imgDisplay.Visible = true;
                imgDisplay.Attributes.Add("onclick", "window.open('" + path + filename + "')");
            }
        }
        protected void btnCalculate_Click(object sender, EventArgs e)
        {
            #region NextButtonCalculation
            if (string.IsNullOrEmpty(txtInterest.Text) || string.IsNullOrWhiteSpace(txtInterest.Text))
            {
                ReportController reportController = new ReportController();
                txtInterest.Text = reportController.getIntrestRate(6).ToString();
            }
            if (string.IsNullOrEmpty(txtTenure.Text) || string.IsNullOrWhiteSpace(txtTenure.Text))
            {
                int MainAge = 0;
                int CoAge = 0;

                if (lblMainAge.Text.Trim() != string.Empty)
                    MainAge = Convert.ToInt32(lblMainAge.Text.Trim());
                if (lblCoAge.Text.Trim() != string.Empty)
                    CoAge = Convert.ToInt32(lblCoAge.Text.Trim());
                
                int tenure = reportController.getTenureYear(6);
                /*
                    Algorithm: Take the younger applicant age, then use 70 - age; .
                     The maximum year will only be 35 even if it's greater than that.
                     If it's lower than 35, the no.of years will be taken. 
                */
                if (CoAge > 0)//if co-applicant exist, compare it
                {
                    if (MainAge <= CoAge)
                    {//MainAge is younger than CoAge
                        MainAge = 70 - MainAge;//calculate max
                        if (MainAge <= tenure)//if less or equal to 35 take MainAge
                            tenure = MainAge;
                        //else if larger than 35 take 35 (else tenure=35;)
                    }
                    else
                    {//CoAge is younger than MainAge
                        CoAge = 70 - CoAge;//calculate max
                        if (CoAge <= tenure)//if less or equal to 35 take CoAge
                            tenure = CoAge;
                        //else if larger than 35 take 35 (else tenure=35;)
                    }
                }
                else//if no co-applicant, only check MainAge
                {
                    MainAge = 70 - MainAge;//calculate max
                    if (MainAge <= tenure)//if less or equal to 35 take MainAge
                        tenure = MainAge;
                    //else if larger than 35 take 35 (else tenure=35;)
                }
                if (tenure < 0)
                    tenure = 1;//temp, can't fix due to some business logic
                txtTenure.Text = tenure.ToString();
            }
            #endregion
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();
            string requestid;
            int totalMainHouseLoan = 0;
            int totalCoHouseLoan = 0;
            string newPath = "/Upload/RequestDocument/";

            try
            {
                if (validationFormTag3())
                {
                    if (validationFormTag1())
                    {
                        if (validationFormTag2())
                        {
                            if (Request["rid"] == null)
                            {
                                //Create new Request and get Request ID
                                SqlParameter[] sqlParams =  {
                                new SqlParameter("@MemberID",  ddlMemberID.SelectedValue.ToString()),
                                new SqlParameter("@UserID",  login_user.UserId),
                                new SqlParameter("@Name", txtMainName.Text),
                                new SqlParameter("@IC", txtMainIC.Text.Trim()),
                                new SqlParameter("@NetIncome", txtMainNetIncome.Text == string.Empty?"0":txtMainNetIncome.Text.Replace(",","") ),
                                new SqlParameter("@Purchase",txtPurchasePrice.Text == string.Empty?"0" :txtPurchasePrice.Text.Replace(",","") ),
                                new SqlParameter("@LoanAmount", txtLoanAmount.Text == string.Empty?"0" :txtLoanAmount.Text.Replace(",","") ),
                                new SqlParameter("@ReportType",rblReport.SelectedValue.ToString())};

                                requestid = db.executeScalarSP("SP_AddRequestFromCalculator", sqlParams).ToString();

                                sql.Clear();
                                sql.Append(" UPDATE tbl_CTOS_Result");
                                sql.Append(" SET Request_Id  ='" + secure.RC(requestid.ToString()) + "'");
                                sql.Append(" WHERE TempId  ='" + hftemprequestId.Value + "'");
                                db.Execute(sql.ToString());

                                hftemprequestId.Value = "";

                            }

                            requestid = hfRequestID.Value.ToString();//sqlString.decryptURL(Request["rid"].ToString());
                            for (int i = 0; i < row5; i++)
                            {
                                TextBox txtoriginal = (TextBox)gvMainHousingLoan.Rows[i].Cells[0].FindControl("txtMainOriginalVal");
                                TextBox txtoutstanding = (TextBox)gvMainHousingLoan.Rows[i].Cells[1].FindControl("txtMainOutstandinglVal");
                                TextBox txtrepayment = (TextBox)gvMainHousingLoan.Rows[i].Cells[2].FindControl("txtMainRepaymentlVal");
                                decimal original = ConvertHelper.ConvertToDecimal(txtoriginal.Text, 0);
                                decimal outstanding = ConvertHelper.ConvertToDecimal(txtoutstanding.Text, 0);
                                decimal repayment = ConvertHelper.ConvertToDecimal(txtrepayment.Text, 0);

                                if (txtoriginal.Text != String.Empty || txtoutstanding.Text != String.Empty || txtrepayment.Text != String.Empty)
                                    if (original != 0 || outstanding != 0 || repayment != 0)
                                        totalMainHouseLoan = totalMainHouseLoan + 1;
                            }
                            reportController.InsertTempData(txtMainName.Text, lblMainAge.Text, txtMainIC.Text, totalMainHouseLoan,
                                true, requestid, string.IsNullOrEmpty(txtInterest.Text) ?
                                reportController.getIntrestRate(6) : ConvertHelper.ConvertToDecimal(txtInterest.Text, reportController.getIntrestRate(6)),
                                ddlHaveCoApplicant.SelectedIndex == 0, string.IsNullOrEmpty(txtMainGrossIncome.Text) ? "" : txtMainGrossIncome.Text,
                                string.IsNullOrEmpty(txtMainOtherIncome.Text) ? "" : txtMainOtherIncome.Text,
                                string.IsNullOrEmpty(ddlMainArea.SelectedValue) ? "" : ddlMainArea.SelectedValue,
                                string.IsNullOrEmpty(txtLoanAmount.Text) ? "" : txtLoanAmount.Text,
                                string.IsNullOrEmpty(txtPurchasePrice.Text) ? "" : txtPurchasePrice.Text,
                                string.IsNullOrEmpty(txtMainContact.Text) ? "" : txtMainContact.Text);

                            if (ddlHaveCoApplicant.SelectedIndex != 0)
                            {
                                for (int i = 0; i < row5; i++)
                                {
                                    TextBox txtoriginal = (TextBox)gvCoHousingLoan.Rows[i].Cells[0].FindControl("txtCoOriginalVal");
                                    TextBox txtoutstanding = (TextBox)gvCoHousingLoan.Rows[i].Cells[1].FindControl("txtCoOutstandinglVal");
                                    TextBox txtrepayment = (TextBox)gvCoHousingLoan.Rows[i].Cells[2].FindControl("txtCoRepaymentlVal");
                                    decimal original = ConvertHelper.ConvertToDecimal(txtoriginal.Text, 0);
                                    decimal outstanding = ConvertHelper.ConvertToDecimal(txtoutstanding.Text, 0);
                                    decimal repayment = ConvertHelper.ConvertToDecimal(txtrepayment.Text, 0);

                                    if (txtoriginal.Text != String.Empty || txtoutstanding.Text != String.Empty || txtrepayment.Text != String.Empty)
                                        if (original != 0 || outstanding != 0 || repayment != 0)
                                            totalCoHouseLoan = totalCoHouseLoan + 1;
                                }
                                reportController.InsertTempData(txtCoName.Text, lblCoAge.Text, txtCoIC.Text, totalCoHouseLoan,
                                    false, requestid, string.IsNullOrEmpty(txtInterest.Text) ? reportController.getIntrestRate(6) : 
                                    ConvertHelper.ConvertToDecimal(txtInterest.Text, reportController.getIntrestRate(6)),
                                    false, string.IsNullOrEmpty(txtCoGrossIncome.Text) ? "" : txtCoGrossIncome.Text,
                                    string.IsNullOrEmpty(txtCoOtherIncome.Text) ? "" : txtCoOtherIncome.Text,
                                    string.IsNullOrEmpty(ddlCoArea.SelectedValue) ? "" : ddlCoArea.SelectedValue,
                                    string.IsNullOrEmpty(txtLoanAmount.Text) ? "" : txtLoanAmount.Text,
                                    string.IsNullOrEmpty(txtPurchasePrice.Text) ? "" : txtPurchasePrice.Text);
                            }

                            //Insert CCRIS File in tbl_RequestDocs
                            newPath += requestid + "/";
                            if (lblFileUpload.Text != String.Empty)
                            {
                                if (!Directory.Exists(Server.MapPath("~" + newPath)))
                                    Directory.CreateDirectory(Server.MapPath("~" + newPath));

                                FileInfo FromFile = new FileInfo(lblFileUpload.Text);
                                string toFilePath = newPath + FromFile.Name;
                                if (FromFile.Exists)
                                    FromFile.MoveTo(Server.MapPath("~" + toFilePath));
                                sql.Append("INSERT into tbl_RequestDocs(RequestID,DocumentType , DocsURL)values(" + requestid.ToString() + ",'" + DocumentType.CCRIS + "',N'" + toFilePath + "') ");
                                db.Execute(sql.ToString());
                                LogUtil.logAction(sql.ToString(), "Insert Document Into tbl_RequestDocs - RequestID :" + requestid.ToString());
                            }

                            bool isMainOnly = false;
                            if (ddlHaveCoApplicant.SelectedIndex != 0)
                                isMainOnly = false;
                            else if (ddlHaveCoApplicant.SelectedIndex == 0)
                                isMainOnly = true;

                            reportController.GetGridViewData(gvMainHousingLoan, gvMainHirePurchase, gvMainCommercialLoan, gvMainCreditCard,
                                gvMainPersonalLoan, gvMainOD, gvMainXProduct, true);
                            if (!isMainOnly)
                                reportController.GetGridViewData(gvCoHousingLoan, gvCoHirePurchase, gvCoCommercialLoan, gvCoCreditCard,
                                    gvCoPersonalLoan, gvCoOD, gvCoXProduct, false);
                            reportController.InsertLoanData(lblMainAge.Text, lblCoAge.Text, isMainOnly);

                            sqlString.displayAlert(this, "Calculate Successfully", "/Form/Loan/CalculateReport.aspx?rid=" + sqlString.encryptURL(requestid));
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                LogUtil.logError(ex.ToString(), sql.ToString());
                sqlString.displayAlert(this, KeyVal.UnknownError);
            }
            finally { db.Close(); }
        }

        protected void btnMainCTOSCCRIS_Click(object senderr, EventArgs ee)
        {
            MainCCRISReportClick("CTOS");
        }
        protected void btnCoCTOSCCRIS_Click(object senderr, EventArgs ee)
        {
            CoCCRISReportClick("CTOS");
        }
        protected void btnMainRAMCICCRIS_Click(object sender, EventArgs e)
        {
            MainCCRISReportClick("RAMCI");
        }
        protected void btnCoRAMCICCRIS_Click(object sender, EventArgs e)
        {
            CoCCRISReportClick("RAMCI");
        }
        protected void btnViewMACTOSReport_Click(object sender, EventArgs e)
        {
            sqlString.OpenNewWindow_Center("/Form/Loan/CTOSReport.aspx?rid=" + sqlString.encryptURL(hfRequestID.Value) + "&type=" + sqlString.encryptURL("0") + "&tid=" + sqlString.encryptURL(hftemprequestId.Value), "DSR Report", 600, 800, this);
        }
        protected void btnViewCACTOSReport_Click(object sender, EventArgs e)
        {
            sqlString.OpenNewWindow_Center("/Form/Loan/CTOSReport.aspx?rid=" + sqlString.encryptURL(hfRequestID.Value) + "&type=" + sqlString.encryptURL("1") + "&tid=" + sqlString.encryptURL(hftemprequestId.Value), "DSR Report", 600, 800, this);
        }
        protected void btnViewMARAMCIReport_Click(object sender, EventArgs e)
        {
            sqlString.OpenNewWindow_Center("/Form/Loan/RAMCIReport.aspx?rid=" + sqlString.encryptURL(hfRequestID.Value) + "&type=" + sqlString.encryptURL("0") + "&tid=" + sqlString.encryptURL(hftemprequestId.Value), "DSR Report", 800, 800, this);
        }
        protected void btnViewCARAMCIReport_Click(object sender, EventArgs e)
        {
            sqlString.OpenNewWindow_Center("/Form/Loan/RAMCIReport.aspx?rid=" + sqlString.encryptURL(hfRequestID.Value) + "&type=" + sqlString.encryptURL("1") + "&tid=" + sqlString.encryptURL(hftemprequestId.Value), "DSR Report", 800, 800, this);
        }
        private void MainCCRISReportClick(string type)
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            //Clear Main Applicant Gridview TextBox
            ClearGVTextBox(gvMainCommercialLoan, true);
            ClearGVTextBox(gvMainCreditCard, true);
            ClearGVTextBox(gvMainHirePurchase, true);
            ClearGVTextBox(gvMainHousingLoan, true);
            ClearGVTextBox(gvMainOD, true);
            ClearGVTextBox(gvMainPersonalLoan, true);
            ClearGVTextBox(gvMainXProduct, true);

            if (hfMainCCRIS.Value.Trim() == string.Empty || hfMainCCRIS.Value == null)
            {
                sql.Clear();
                sql.Append(" SELECT Result ");
                sql.Append(" FROM tbl_CTOS_Result WITH(NOLOCK) ");
                sql.Append(" WHERE IsActive = 1 ");
                if (hftemprequestId.Value.Trim() == string.Empty || hftemprequestId.Value == null)
                    sql.Append(" AND Request_Id = N'" + secure.RC(hfRequestID.Value.ToString()) + "'");
                else
                    sql.Append(" AND TempId = N'" + hftemprequestId.Value + "'");
                sql.Append(" AND IC_Number ='" + secure.RC(txtMainIC.Text.Trim()) + "'");
                db.OpenTable(sql.ToString());

                if (db.RecordCount() > 0)
                {
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "ajax", "<script language='javascript'>confirmMainCCRIS();</script>", false);
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "MyFun1", "confirmMainCCRIS('" + type + "');", true);
                }
                else
                {
                    bool callAPI = true, mainReuse = false;

                    //check if reuseReport function is activated
                    db.OpenTable(@"
                        SELECT isTrue as 'Active' 
                        FROM tbl_PageController with (nolock)
                        WHERE Category = 'Reuse Report'
                        AND Name = 'Active'
                    ");
                    if (db.RecordCount() > 0)
                        if (ConvertHelper.ConvertToBoolen(db.Item("Active"), false)) //only reuse when active, off by default
                            if (reportController.checkReuseReport(secure.RC(txtMainIC.Text), true))
                                if (reportController.checkReuseReport(secure.RC(txtMainIC.Text), true, true))
                                    mainReuse = true;

                    //Main Applicant
                    if (mainReuse) //if reusing, retrieve from db
                    {
                        hfMainCCRIS.Value = "false";
                        callAPI = false;
                    }
                    if ((reportController.RetrieveXMLDataForMainCoApp(hfRequestID.Value.ToString(), txtMainName.Text, "", txtMainIC.Text.Trim(), "I",
                        ddlHaveCoApplicant.SelectedIndex > 0, lblMainAge.Text, txtLoanAmount.Text, hfMainCCRIS.Value, hfCoCCRIS.Value, true, callAPI, type)) &&
                        (!string.IsNullOrEmpty(reportController.mainResultID)) && (reportController.section_ccris != null || reportController.detail != null))
                    //report is retrieved, resultID is not null, CTOS or RAMCI report is returned
                    {
                        if (reportController.MAtype.Equals("CTOS")) //CTOS Main
                        {
                            PassDatatoLoanDetails(reportController.mainResultID, reportController.section_ccris, true);
                            btnViewMACTOSReport.Visible = true;
                        }
                        else if (reportController.MAtype.Equals("RAMCI")) //RAMCI Main
                        {
                            PassDatatoLoanDetails(reportController.mainResultID, reportController.detail, true);
                            btnViewMARAMCIReport.Visible = true;
                        }
                        sqlString.displayAlert(this, "Successfully retrieved " + type + " report.");
                    }
                    else
                        sqlString.displayAlert(this, "No " + type + " report found.");
                }
            }
            else
            {
                bool callAPI = true, mainReuse = false;

                //check if reuseReport function is activated
                db.OpenTable(@"
                    SELECT isTrue as 'Active' 
                    FROM tbl_PageController with (nolock)
                    WHERE Category = 'Reuse Report'
                    AND Name = 'Active'
                ");
                if (db.RecordCount() > 0)
                    if (ConvertHelper.ConvertToBoolen(db.Item("Active"), false)) //only reuse when active, off by default
                        if (reportController.checkReuseReport(secure.RC(txtMainIC.Text), true))
                            if (reportController.checkReuseReport(secure.RC(txtMainIC.Text), true, true))
                                mainReuse = true;

                //Main Applicant
                if (mainReuse) //if reusing, retrieve from db
                {
                    hfMainCCRIS.Value = "false";
                    callAPI = false;
                }
                if (reportController.RetrieveXMLDataForMainCoApp(hfRequestID.Value.ToString(), txtMainName.Text, "", txtMainIC.Text.Trim(), "I",
                    ddlHaveCoApplicant.SelectedIndex > 0, lblMainAge.Text, txtLoanAmount.Text, hfMainCCRIS.Value, hfCoCCRIS.Value, true, callAPI, type) &&
                    (!string.IsNullOrEmpty(reportController.mainResultID)) && (reportController.section_ccris != null || reportController.detail != null))
                {
                    if (reportController.MAtype.Equals("CTOS")) //CTOS Main
                    {
                        PassDatatoLoanDetails(reportController.mainResultID, reportController.section_ccris, true);
                        btnViewMACTOSReport.Visible = true;
                    }
                    else if (reportController.MAtype.Equals("RAMCI")) //RAMCI Main
                    {
                        PassDatatoLoanDetails(reportController.mainResultID, reportController.detail, true);
                        btnViewMARAMCIReport.Visible = true;
                    }
                    sqlString.displayAlert(this, "Successfully retrieved " + type + " report.");
                }
                else
                    sqlString.displayAlert(this, "No " + type + " report found.");
                hfMainCCRIS.Value = "";
            }
        }
        private void CoCCRISReportClick(string type)
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            //Clear Co Applicant GridView TextBox
            ClearGVTextBox(gvCoCommercialLoan, false);
            ClearGVTextBox(gvCoCreditCard, false);
            ClearGVTextBox(gvCoHirePurchase, false);
            ClearGVTextBox(gvCoHousingLoan, false);
            ClearGVTextBox(gvCoOD, false);
            ClearGVTextBox(gvCoPersonalLoan, false);
            ClearGVTextBox(gvCoXProduct, false);

            if (hfCoCCRIS.Value.Trim() == string.Empty || hfCoCCRIS.Value == null)
            {
                sql.Clear();
                sql.Append(" SELECT Result ");
                sql.Append(" FROM tbl_CTOS_Result WITH(NOLOCK) ");
                sql.Append(" WHERE IsActive = 1 ");
                if (hftemprequestId.Value.Trim() == string.Empty || hftemprequestId.Value == null)
                    sql.Append(" AND Request_Id = N'" + secure.RC(hfRequestID.Value.ToString()) + "'");
                else
                    sql.Append(" AND TempId = N'" + hftemprequestId.Value + "'");
                sql.Append(" AND IC_Number ='" + secure.RC(txtCoIC.Text.Trim()) + "'");
                db.OpenTable(sql.ToString());

                if (db.RecordCount() > 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "MyFun1", "confirmCoCCRIS('" + type + "');", true);
                }
                else
                {
                    bool callAPI = true, coReuse = false;

                    //check if reuseReport function is activated
                    db.OpenTable(@"
                        SELECT isTrue as 'Active' 
                        FROM tbl_PageController with (nolock)
                        WHERE Category = 'Reuse Report'
                        AND Name = 'Active'
                    ");
                    if (db.RecordCount() > 0)
                        if (ConvertHelper.ConvertToBoolen(db.Item("Active"), false)) //only reuse when active, off by default
                            if (reportController.checkReuseReport(secure.RC(txtCoIC.Text), false))
                                if (reportController.checkReuseReport(secure.RC(txtCoIC.Text), false, true))
                                    coReuse = true;

                    //Co Applicant
                    if (coReuse) //if reusing, retrieve from db
                    {
                        hfCoCCRIS.Value = "false";
                        callAPI = false;
                    }
                    if (reportController.RetrieveXMLDataForMainCoApp(hfRequestID.Value.ToString(), txtCoName.Text, "", txtCoIC.Text.Trim(), "I",
                        ddlHaveCoApplicant.SelectedIndex > 0, lblCoAge.Text, txtLoanAmount.Text, hfMainCCRIS.Value, hfCoCCRIS.Value, false, callAPI, type) &&
                    (!string.IsNullOrEmpty(reportController.coResultID)) && (reportController.section_ccris != null || reportController.detail != null))
                    {
                        if (reportController.CAtype.Equals("CTOS")) //CTOS Co
                        {
                            PassDatatoLoanDetails(reportController.coResultID, reportController.section_ccris, false);
                            btnViewCACTOSReport.Visible = true;
                        }
                        else if (reportController.CAtype.Equals("RAMCI")) //RAMCI Co
                        {
                            PassDatatoLoanDetails(reportController.coResultID, reportController.detail, false);
                            btnViewCARAMCIReport.Visible = true;
                        }
                        sqlString.displayAlert(this, "Successfully retrieved " + type + " report.");
                    }
                    else
                        sqlString.displayAlert(this, "No " + type + " report found.");
                }
            }
            else
            {
                bool callAPI = true, coReuse = false;

                //check if reuseReport function is activated
                db.OpenTable(@"
                    SELECT isTrue as 'Active' 
                    FROM tbl_PageController with (nolock)
                    WHERE Category = 'Reuse Report'
                    AND Name = 'Active'
                ");
                if (db.RecordCount() > 0)
                    if (ConvertHelper.ConvertToBoolen(db.Item("Active"), false)) //only reuse when active, off by default
                        if (reportController.checkReuseReport(secure.RC(txtCoIC.Text), false))
                            if (reportController.checkReuseReport(secure.RC(txtCoIC.Text), false, true))
                                coReuse = true;

                //Co Applicant
                if (coReuse) //if reusing, retrieve from db
                {
                    hfCoCCRIS.Value = "false";
                    callAPI = false;
                }
                if (reportController.RetrieveXMLDataForMainCoApp(hfRequestID.Value.ToString(), txtCoName.Text, "", txtCoIC.Text.Trim(), "I",
                    ddlHaveCoApplicant.SelectedIndex > 0, lblCoAge.Text, txtLoanAmount.Text, hfMainCCRIS.Value, hfCoCCRIS.Value, false, callAPI, type) &&
                    (!string.IsNullOrEmpty(reportController.coResultID)) && (reportController.section_ccris != null || reportController.detail != null))
                    if (reportController.CAtype.Equals("CTOS")) //CTOS Co
                    {
                        PassDatatoLoanDetails(reportController.coResultID, reportController.section_ccris, false);
                        btnViewCACTOSReport.Visible = true;
                    }
                    else if (reportController.CAtype.Equals("RAMCI")) //RAMCI Co
                    {
                        PassDatatoLoanDetails(reportController.coResultID, reportController.detail, false);
                        btnViewCARAMCIReport.Visible = true;
                    }
                hfCoCCRIS.Value = "";
                //if (result)
                //    sqlString.displayAlert(this, "Successfully retrieved CTOS report.");
                //else
                //    sqlString.displayAlert(this, "No CTOS report found.");
            }
        }
        #endregion
    }
}