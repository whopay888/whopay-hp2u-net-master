﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Serialization;
using Synergy.Controller;
using Synergy.Helper;
using Synergy.Model;
using Synergy.Util;
using CTOS.Model;
using System.Data;
using HJT.Cls.Controller;

namespace HJT.Form
{
    public partial class EasyMode : System.Web.UI.Page
    {
        LoginUserModel login_user = null;
        string netIncome = "", grossIncome = "", otherIncome = "",
            area = "", projectID = "", CTOSitem = "", specialAttention = "",
            CCRISAttention = "", dishonourCheque = "", restructuring = "",
            loanAmount = "";
        bool test = false;
        Dictionary<string, DataTable> applicantData = new Dictionary<string, DataTable>();

        protected void Page_Load(object sender, EventArgs e)
        {
            login_user = (LoginUserModel)Session[KeyVal.loginsecure];
            if (!IsPostBack)
            {
                hfAKPK.Value = "false";
                Set5RowGridView();
                Set10RowGridView();
                //divGv.Visible = true;
                gvMainCommercialLoan.Visible = false;
                gvMainCreditCard.Visible = false;
                gvMainHirePurchase.Visible = false;
                gvMainHousingLoan.Visible = false;
                gvMainOD.Visible = false;
                gvMainPersonalLoan.Visible = false;
                gvMainXProduct.Visible = false;

                ViewState["requestID"] = hfRequestID.Value.ToString();
            }
            applicantData = ViewState["applicantData"] as Dictionary<string, DataTable>;
        }

        #region PREPARE_DSR
        private void prepareDSR()
        {
            updateAndInsertTempResult();
        } //5
        private void updateAndInsertTempResult()
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();
            string resultID = "";

            sql.Clear();
            sql.Append(" SELECT ID ");
            sql.Append(" FROM tbl_CTOS_Result WITH(NOLOCK) ");
            sql.Append(" WHERE IsActive = 1 AND Request_Id = N'" + secure.RC(hfRequestID.Value.ToString()) + "' Order By ID");
            db.OpenTable(sql.ToString());

            if (db.RecordCount() > 0)
            {
                resultID = db.Item("ID");

                sql.Clear();
                sql.Append("select * ");
                sql.Append("from tbl_CTOS_Report_Loan_Details ");
                sql.Append("where tbl_CTOS_Report_Loan_Details.CTOS_Result_Id = '" + resultID + "' ");
                db.OpenTable(sql.ToString());
                sql.Clear();
                if (db.RecordCount() > 0)
                {
                    //show all ccriss
                    sql.Append("UPDATE tbl_CTOS_Report_Loan_Details SET IsShowinReport = 1 WHERE CTOS_Result_Id = '" +
                        resultID + "';");
                    db.Execute(sql.ToString());
                    LogUtil.logAction(sql.ToString(), "UPDATE CTOS_Report_Loan_Details show all details. Result_ID:" + resultID);
                }
            }
            if (!db.HasError)
                //sqlString.displayAlert2(this, Resources.resource.Action_Successful, Request.Url.AbsoluteUri);
                toRealData();
            else
                sqlString.displayAlert2(this, db.ErrorMessage);
        } //5.1
        private void toRealData()
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            sql.Append("EXEC SP_ToRealData @RequestID='" + hfRequestID.Value.ToString() + "';");
            sql.Append("EXEC SP_AddRealDetails @RequestID='" + hfRequestID.Value.ToString() + "' , @BankID='12';");

            wwdb dbUpdate = new wwdb();
            StringBuilder sqlUpdateRequest = new StringBuilder();

            sqlUpdateRequest.Append("update tbl_Request set ProjectID = ( ");
            sqlUpdateRequest.Append("select CASE WHEN a.ProjectID = 'Others' THEN b.id ELSE a.ProjectID END AS ProjectID ");
            sqlUpdateRequest.Append("from tbl_TLoanLegalAction a ");
            sqlUpdateRequest.Append("left outer join tbl_ProjectSettings b on a.ProjectID = b.ProjectID ");
            sqlUpdateRequest.Append("where a.RequestID = '" + hfRequestID.Value.ToString() + "') ");
            sqlUpdateRequest.Append("where tbl_Request.RequestID = '" + hfRequestID.Value.ToString() + "' ");

            db.Execute(sql.ToString());
            dbUpdate.Execute(sqlUpdateRequest.ToString());

            showDSR();
        } //5.2
        private void showDSR()
        {
            //reset all field
            txtName.Text = "";
            txtIC.Text = "";
            txtPrice.Text = "";
            chkterms.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Tab", "$('#HidTab').val('#tab_1');", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "focusName()", true);

            //open report in new window
            string reportIrl = "/Form/Loan/DsrReport.aspx?rid=" + sqlString.encryptURL(hfRequestID.Value.ToString()) +
                "&BankID=" + sqlString.encryptURL("12");//temp use Classic Bank for all
            sqlString.OpenNewWindow_Center(reportIrl, "Report", 800, 600, this);

            //direct to the link (Disabled to enable next user to cont use system)
            //sqlString.displayAlert2(this, Resources.resource.Action_Successful, "/Form/Loan/DsrReport.aspx?rid=" +
            //    sqlString.encryptURL(hfRequestID.Value.ToString()) + "&BankID=" + sqlString.encryptURL("12"));
        } //5.3
        #endregion

        #region Automation
        private void countFacilities(enquiry enquiry)
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            try
            {
                int housingLoan = 0, carLoan = 0, pLoan = 0, creditCard = 0, termLoan = 0;
                if (enquiry.section_ccris.accounts != null && enquiry.section_ccris.accounts.account != null)
                {
                    List<ccris_account_detail> accounts = enquiry.section_ccris.accounts.account.ToList();
                    foreach (var account in accounts)
                        foreach (var sub_account in account.sub_accounts)
                        {
                            sql.Clear();
                            sql.Append("SELECT LoanID FROM tbl_LoanTypesXML WITH(NOLOCK) WHERE LoanTypeCode = '" +
                                sub_account.facility.code + "';");
                            db.OpenTable(sql.ToString());

                            if (db.RecordCount() > 0)
                            {
                                string loanid = db.Item("LoanID");
                                switch (loanid)
                                {
                                    case "1"://Housing Loan
                                        housingLoan++;
                                        break;
                                    case "2"://Commercial Loan
                                        termLoan++;
                                        break;
                                    case "3"://Personal Loan
                                        pLoan++;
                                        break;
                                    case "4"://OverDraft
                                        termLoan++;
                                        break;
                                    case "5"://X-Product
                                        termLoan++;
                                        break;
                                    case "6"://Hire Purchase - Car Loan
                                        carLoan++;
                                        break;
                                    case "7"://Credit Card
                                        creditCard++;
                                        break;
                                    default:
                                        //sqlString.displayAlert2(this, "Don't have any loan");
                                        break;
                                }
                                break;
                            }
                        }
                }

                string advise = Environment.NewLine + "Customer has ";
                if ((housingLoan > 0 || carLoan > 0 || pLoan > 0 || creditCard > 0 || termLoan > 0))
                {
                    if (housingLoan > 0)
                        advise += housingLoan + " " + Resources.resource.Housing_Loan + ", ";
                    if (carLoan > 0)
                        advise += carLoan + " " + Resources.resource.Car_Loan + ", ";
                    if (pLoan > 0)
                        advise += pLoan + " " + Resources.resource.Personal_Loan + ", ";
                    if (creditCard > 0)
                        advise += creditCard + " " + Resources.resource.Credit_Card + ", ";
                    if (termLoan > 0)
                        advise += termLoan + " " + Resources.resource.Term_Loan + ", ";

                    advise = advise.TrimEnd(' ', ',');
                    advise += ".";
                    sql.Clear();
                    sql.Append("INSERT INTO tbl_TApplicantCreditAdvisor(requestid, advisorid,comment) VALUES(N'" +
                        secure.RC(hfRequestID.Value) + "','0','" + advise + "')");
                }
                else //no facilities
                {
                    db.OpenTable("SELECT Description FROM tbl_CreditAdvisor WHERE AdvisorID='9'");
                    advise = db.Item("Description") + Environment.NewLine + Environment.NewLine;

                    db.OpenTable("SELECT Description FROM tbl_CreditAdvisor WHERE AdvisorID='10'");
                    advise += db.Item("Description") + Environment.NewLine;

                    db.OpenTable("SELECT Description FROM tbl_CreditAdvisor WHERE AdvisorID='11'");
                    advise += db.Item("Description");

                    if (getAgeFromIC() > 30)
                        advise += Environment.NewLine + " - House Title / Car Cert" + Environment.NewLine;
                    else//<=30
                        advise += Environment.NewLine + " - Degree Cert" + Environment.NewLine;

                    db.OpenTable("SELECT Description FROM tbl_CreditAdvisor WHERE AdvisorID='12'");
                    advise += Environment.NewLine + db.Item("Description") + Environment.NewLine + Environment.NewLine;
                    db.OpenTable("SELECT Description FROM tbl_CreditAdvisor WHERE AdvisorID='13'");
                    advise += db.Item("Description");

                    db.OpenTable("SELECT ParameterValue FROM tbl_Parameter WHERE ParameterName='LoanAmountThreshold' AND Category='Advise'");
                    if (getAgeFromIC() > 30)
                    {
                        if (ConvertHelper.ConvertToDecimal(loanAmount, 0) > ConvertHelper.ConvertToDecimal(db.Item("ParameterValue"), 0))
                            advise += " big loan amount";
                    }
                    else//<=30
                    {
                        advise += " young age";
                        if (ConvertHelper.ConvertToDecimal(loanAmount, 0) > ConvertHelper.ConvertToDecimal(db.Item("ParameterValue"), 0))
                            advise += ", big loan amount";
                    }
                    advise += " & no CCRIS reference.";
                    sql.Append("INSERT INTO tbl_TApplicantCreditAdvisor(requestid, advisorid,comment) VALUES(N'" +
                            secure.RC(hfRequestID.Value) + "','0','" + advise + Environment.NewLine + Environment.NewLine + "');");
                }
                db.Execute(sql.ToString());
                LogUtil.logAction(sql.ToString(), "Easy-INSERT Advise Facilities RequestID:" + secure.RC(hfRequestID.Value));
            }
            catch (Exception ex)
            {
                LogUtil.logError(ex.Message, "Easy-Check Facilities. RequestID:" + hfRequestID.Value);
            }
        } //4.1.1.1
        private void checkSpecialAttention(enquiry enquiry)
        {
            string special_attention_acc_summary = "";
            string special_attention_acc_ccris = "";
            try
            {
                if (enquiry.section_summary.ccris.special_attention != null) 
                    special_attention_acc_summary = enquiry.section_summary.ccris.special_attention.accounts.ToString();
                if (enquiry.section_summary.ccris.special_attention != null) 
                    special_attention_acc_ccris = enquiry.section_ccris.summary.special_attention.status.ToString();
                if ((!string.IsNullOrEmpty(special_attention_acc_summary) && !string.IsNullOrWhiteSpace(special_attention_acc_ccris))
                    || (!string.IsNullOrEmpty(special_attention_acc_ccris) && !string.IsNullOrWhiteSpace(special_attention_acc_ccris)))  
                    if (special_attention_acc_summary.Equals("1") || special_attention_acc_ccris.Equals("1"))
                    {
                        wwdb db = new wwdb();
                        db.OpenTable("SELECT ApplicationResult FROM tbl_Request WHERE RequestID = '" + hfRequestID.Value.ToString() + "';");
                        if (db.RecordCount() > 0)
                        {
                            StringBuilder sql = new StringBuilder();
                            sql.Append(" UPDATE tbl_Request SET StatusID ='" + ApplicationStatus.InProgress +
                                "' , ApplicationResult ='" + ApplicationResult.Denied + "' WHERE RequestID = '" +
                                hfRequestID.Value.ToString() + "';");

                            specialAttention = "1";

                            /*
                                **Special Attention Account (SAA)**
                                - Term loan - 29/05/2017 (Last update - 31/03/2019)
                            */
                            db.OpenTable("SELECT Description FROM tbl_CreditAdvisor WHERE AdvisorID = '7'");
                            string advise = db.Item("Description");
                            var special_attention_accs = enquiry.section_ccris.special_attention_accs;
                            foreach (var special_attention_acc in special_attention_accs)
                            {
                                advise += Environment.NewLine + "- ";
                                string approval_date = "";
                                string position_date = "";
                                string facility = "";

                                if (special_attention_acc.approval_date != null)
                                    approval_date = special_attention_acc.approval_date;
                                foreach (var sub_account in special_attention_acc.sub_accounts)
                                {
                                    facility = sub_account.facility.code;
                                    db.OpenTable("SELECT LoanID FROM tbl_LoanTypesXML WHERE LoanTypeCode='" + facility + "'");
                                    switch (db.Item("LoanID"))
                                    {
                                        case "1":
                                            advise += Resources.resource.Housing_Loan;
                                            break;
                                        case "2":
                                            advise += Resources.resource.Commercial_Loan;
                                            break;
                                        case "3":
                                            advise += Resources.resource.Personal_Loan;
                                            break;
                                        case "4":
                                            advise += Resources.resource.OverDraft;
                                            break;
                                        case "5":
                                            advise += Resources.resource.Term_Loan;
                                            break;
                                        case "6":
                                            advise += Resources.resource.Car_Loan;
                                            break;
                                        case "7":
                                            advise += Resources.resource.Credit_Card;
                                            break;
                                        default:
                                            break;
                                    }
                                    advise += " - " + approval_date;
                                    foreach (var cr_position in sub_account.cr_positions)
                                        if (cr_position.position_date != null)
                                        {
                                            position_date = cr_position.position_date;
                                            advise += " (Last Update - " + position_date + ")";
                                            break;//get only the first
                                        }
                                }
                            }

                            sql.Append("INSERT INTO tbl_TApplicantCreditAdvisor(requestid, advisorid, comment) VALUES(N'" + hfRequestID.Value.ToString() + "', '0','" + advise + "');");
                            LogUtil.logAction(sql.ToString(), "Easy-Update & Insert Special Attention Account. RequestID:" + hfRequestID.Value.ToString());
                            db.Execute(sql.ToString());
                        }
                    }
            }
            catch (Exception ex)
            {
                LogUtil.logError(ex.Message, "Easy-Check SAA. RequestID:" + hfRequestID.Value);
            }
        }  //4.1.1.2
        private void checkLegalStatus(section_ccris section_ccris)
        {
            try
            {
                //int legal_total = ConvertHelper.ConvertToInt(report.enq_report.enquiry.FirstOrDefault().section_summary.ctos.legal.total, 0);
                //int legal_value = ConvertHelper.ConvertToInt(report.enq_report.enquiry.FirstOrDefault().section_summary.ctos.legal.value, 0);
                string legal_status = section_ccris.summary.legal.status;

                if (!string.IsNullOrEmpty(legal_status) && !string.IsNullOrWhiteSpace(legal_status) && legal_status.Equals("1")) //legal_total > 0 || legal_value > 0)
                {
                    wwdb db = new wwdb();
                    StringBuilder sql = new StringBuilder();

                    sql.Append(" UPDATE tbl_Request SET StatusID ='" + ApplicationStatus.InProgress +
                        "' , ApplicationResult ='" + ApplicationResult.Denied + "' WHERE RequestID = '" +
                        hfRequestID.Value + "';");

                    hfLegalStatus.Value = "true";

                    sql.Append(" INSERT INTO tbl_TApplicantCreditAdvisor(requestid, advisorid) VALUES(N'" + hfRequestID.Value.ToString() + "', '8');");
                    LogUtil.logAction(sql.ToString(), "Easy-Update & Insert Special Attention Account. RequestID:" + hfRequestID.Value);
                    db.Execute(sql.ToString());
                }
            }
            catch (Exception ex)
            {
                LogUtil.logError(ex.Message, "Easy-Check Legal Status. RequestID:" + hfRequestID.Value);
            }
        } //4.1.1.3
        private void checkCCRIS(section_ccris section_ccris)
        {
            try
            {
                if (section_ccris.accounts.account != null)
                {
                    List<ccris_account_detail> ccris_accounts = new List<ccris_account_detail>();
                    ccris_accounts = section_ccris.accounts.account.ToList();
                    wwdb db = new wwdb();
                    StringBuilder sql = new StringBuilder();
                    foreach (var account in ccris_accounts)
                    {
                        if (!string.IsNullOrEmpty(account.org1) && account.org1.Equals("PT"))//got org and org is PT
                        {
                            foreach (var sub_account in account.sub_accounts)
                                foreach (var cr_position in sub_account.cr_positions)
                                    if (!string.IsNullOrEmpty(cr_position.inst_arrears) && !string.IsNullOrWhiteSpace(cr_position.inst_arrears))
                                        if (ConvertHelper.ConvertToInt(cr_position.inst_arrears, 0) > 2)//exceed certain number
                                        {
                                            //deny

                                            //insert advise

                                        }
                        }
                        else//any other than PTPTN
                            foreach (var sub_account in account.sub_accounts)
                                foreach (var cr_position in sub_account.cr_positions)
                                    if (!string.IsNullOrEmpty(cr_position.inst_arrears) && !string.IsNullOrWhiteSpace(cr_position.inst_arrears))
                                        if (ConvertHelper.ConvertToInt(cr_position.inst_arrears, 0) > 2)
                                        {
                                            db.OpenTable("SELECT ApplicationResult FROM tbl_Request WHERE RequestID=N'" +
                                                secure.RC(hfRequestID.Value) + "'");
                                            if (!db.Item("ApplicationResult").Equals(ApplicationResult.Denied))
                                            {
                                                sql.Clear();
                                                sql.Append("UPDATE tbl_Request SET ApplicationResult = '" + ApplicationResult.Denied + "'" +
                                                    " WHERE RequestID = N'" + secure.RC(hfRequestID.Value) + "';");
                                                db.Execute(sql.ToString());
                                                LogUtil.logAction(sql.ToString(), "Easy-UPDATE tbl_Request RequestID:" + hfRequestID.Value);
                                            }

                                            //insert advise

                                        }
                    }
                }
            }catch(Exception ex)
            {
                LogUtil.logError(ex.Message, "Easy-Check CCRIS count. RequestID:"+hfRequestID.Value);
            }
        } //4.1.1.4
        private void checkAKPK(string code)
        {
            try
            {
                if (hfAKPK.Value.ToString().Equals("false") && code.Equals("K"))
                {
                    wwdb db = new wwdb();

                    db.OpenTable("SELECT ApplicationResult FROM tbl_Request WHERE RequestID = '" + hfRequestID.Value.ToString() + "';");
                    if (db.RecordCount() > 0)
                    {
                        StringBuilder sql = new StringBuilder();
                        sql.Append(" UPDATE tbl_Request SET StatusID ='" + ApplicationStatus.InProgress +
                            "' , ApplicationResult ='" + ApplicationResult.Denied + "' WHERE RequestID = '" +
                            hfRequestID.Value.ToString() + "';");

                        sql.Append("INSERT INTO tbl_TApplicantCreditAdvisor(requestid, advisorid) VALUES(N'" + hfRequestID.Value.ToString() + "', '6');");
                        LogUtil.logAction(sql.ToString(), "Easy-Update & Insert AKPK. RequestID:" + hfRequestID.Value.ToString());
                        db.Execute(sql.ToString());
                        hfAKPK.Value = "true";
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtil.logError(ex.Message, "Easy-Check AKPK. RequestID:" + hfRequestID.Value);
            }
        } //4.2.2.1
        private void checkTREX(report report)
        {

        }
        #endregion

        #region PAGE_FUNCTION
        private void Set5RowGridView()
        {
            List<int> rows = new List<int>();

            for (int i = 0; i < 5; i++)
            {
                rows.Add(i);
            }

            #region MainGridView
            gvMainCommercialLoan.DataSource = rows;
            gvMainCommercialLoan.DataBind();

            gvMainCreditCard.DataSource = rows;
            gvMainCreditCard.DataBind();

            gvMainHirePurchase.DataSource = rows;
            gvMainHirePurchase.DataBind();

            gvMainHousingLoan.DataSource = rows;
            gvMainHousingLoan.DataBind();

            gvMainOD.DataSource = rows;
            gvMainOD.DataBind();

            gvMainPersonalLoan.DataSource = rows;
            gvMainPersonalLoan.DataBind();

            gvMainXProduct.DataSource = rows;
            gvMainXProduct.DataBind();

            #endregion
        } //0.1
        private void Set10RowGridView()
        {
            List<int> rows = new List<int>();

            for (int i = 0; i < 10; i++)
            {
                rows.Add(i);
            }

            #region MainGridView

            gvMainCreditCard.DataSource = rows;
            gvMainCreditCard.DataBind();

            gvMainXProduct.DataSource = rows;
            gvMainXProduct.DataBind();

            #endregion
        } //0.2
        private void removeInvalidChar(TextBox textBox, TextBox textBox2 = null, TextBox textBox3 = null, TextBox textBox4 = null, TextBox textBox5 = null)
        {
            if (textBox != null)
                textBox.Text = textBox.Text.Replace("'", "").Replace(",", "").Replace("%", "").Replace("#", "").Replace("*", "")
                    .Replace("^", "").Replace("%", "").Replace("#", "").Replace("*", "").Replace("^", "").Replace("&", "").Replace("(", "")
                    .Replace(")", "").Replace("{", "").Replace("}", "").Replace("[", "").Replace("]", "").Replace("_", "").Replace("=", "")
                    .Replace("+", "").Replace(":", "").Replace(";", "").Replace("|", "").Replace("\\", "").Replace("?", "").Replace("`", "")
                    .Replace("~", "").Replace("<", "").Replace(">", "");
            if (textBox2 != null)
                textBox2.Text = textBox2.Text.Replace("'", "").Replace(",", "").Replace("%", "").Replace("#", "").Replace("*", "")
                    .Replace("^", "").Replace("%", "").Replace("#", "").Replace("*", "").Replace("^", "").Replace("&", "").Replace("(", "")
                    .Replace(")", "").Replace("{", "").Replace("}", "").Replace("[", "").Replace("]", "").Replace("_", "").Replace("=", "")
                    .Replace("+", "").Replace(":", "").Replace(";", "").Replace("|", "").Replace("\\", "").Replace("?", "").Replace("`", "")
                    .Replace("~", "").Replace("<", "").Replace(">", "");
            if (textBox3 != null)
                textBox3.Text = textBox3.Text.Replace("'", "").Replace(",", "").Replace("%", "").Replace("#", "").Replace("*", "")
                    .Replace("^", "").Replace("%", "").Replace("#", "").Replace("*", "").Replace("^", "").Replace("&", "").Replace("(", "")
                    .Replace(")", "").Replace("{", "").Replace("}", "").Replace("[", "").Replace("]", "").Replace("_", "").Replace("=", "")
                    .Replace("+", "").Replace(":", "").Replace(";", "").Replace("|", "").Replace("\\", "").Replace("?", "").Replace("`", "")
                    .Replace("~", "").Replace("<", "").Replace(">", "");
            if (textBox4 != null)
                textBox4.Text = textBox4.Text.Replace("'", "").Replace(",", "").Replace("%", "").Replace("#", "").Replace("*", "")
                    .Replace("^", "").Replace("%", "").Replace("#", "").Replace("*", "").Replace("^", "").Replace("&", "").Replace("(", "")
                    .Replace(")", "").Replace("{", "").Replace("}", "").Replace("[", "").Replace("]", "").Replace("_", "").Replace("=", "")
                    .Replace("+", "").Replace(":", "").Replace(";", "").Replace("|", "").Replace("\\", "").Replace("?", "").Replace("`", "")
                    .Replace("~", "").Replace("<", "").Replace(">", "");
            if (textBox5 != null)
                textBox5.Text = textBox5.Text.Replace("'", "").Replace(",", "").Replace("%", "").Replace("#", "").Replace("*", "")
                    .Replace("^", "").Replace("%", "").Replace("#", "").Replace("*", "").Replace("^", "").Replace("&", "").Replace("(", "")
                    .Replace(")", "").Replace("{", "").Replace("}", "").Replace("[", "").Replace("]", "").Replace("_", "").Replace("=", "")
                    .Replace("+", "").Replace(":", "").Replace(";", "").Replace("|", "").Replace("\\", "").Replace("?", "").Replace("`", "")
                    .Replace("~", "").Replace("<", "").Replace(">", "");
        } //1.1
        private bool validateFormTab1()
        {
            StringBuilder errMsg = new StringBuilder();
            
            if (string.IsNullOrEmpty(txtName.Text))
            {
                errMsg.Append(Resources.resource.nameEmpty + "\\n");
            }
            
            if (string.IsNullOrEmpty(txtIC.Text))
            {
                errMsg.Append(Resources.resource.icEmpty + "\\n");
            }
            else
            {
                if (!Validation.isValidMyCard(txtIC.Text))
                {
                    errMsg.Append(Resources.resource.Err_Mycard_Format + "\\n");
                }

                //if age is >20 and <76
            }

            lblErr.Text = errMsg.ToString().Replace("\\n", "</br>");

            if (errMsg.ToString().Trim() != string.Empty)
            {
                divErrorMessage.Visible = true;
                sqlString.displayAlert2(this, errMsg.ToString());
                return false;
            }

            return true;
        } //1.2
        protected void btnNext_Click(object sender, EventArgs e)
        {
            if(validateFormTab1())
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Tab", "$('#HidTab').val('#tab_2');", true);
        }
        private bool validateFormTab2()
        {
            StringBuilder errMsg = new StringBuilder();
            
            if (string.IsNullOrEmpty(txtPrice.Text))
                errMsg.Append(Resources.resource.PriceEmptyErrMsg + "\\n");
            else if (!Validation.isDecimal(txtPrice.Text))
                errMsg.Append(Resources.resource.Purchase_Price_Invalid_Format + "\\n");
            else if (!(ConvertHelper.ConvertToDecimal(txtPrice.Text, 0) > 0))
                errMsg.Append(Resources.resource.Err_Zero_Purchase_Price + "\\n");

            lblErr.Text = errMsg.ToString().Replace("\\n", "</br>");

            if (errMsg.ToString().Trim() != string.Empty)
            {
                divErrorMessage.Visible = true;
                sqlString.displayAlert2(this, errMsg.ToString());
                return false;
            }

            return true;
        } //1.3
        protected void btnNext2_Click(object sender, EventArgs e)
        {
            if(validateFormTab2())
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Tab", "$('#HidTab').val('#tab_3');", true);
        }
        private bool validateFormTab3()
        {
            StringBuilder errMsg = new StringBuilder();

            return true;
        } //1.4
        protected void btnSend_Click(object sender, EventArgs e)
        {
            removeInvalidChar(txtName, txtIC, txtPrice);
            if (validateFormTab1() && validateFormTab2() && validateFormTab3())
            {
                string requestID = insertRequestRecord();
                hfRequestID.Value = requestID;

                getInfo(requestID);
                insertTempData(requestID);
                prepareDSR();
            }
        }
        protected void btnDownload_Click(object sender, EventArgs e)
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            sql.Clear();
            sql.Append("SELECT TOP(1) DocsURL FROM tbl_ConsentDocs WHERE isDeleted = 0 OR isDeleted IS NULL"); //ToDo: add active column and enable to set by admin
            db.OpenTable(sql.ToString());

            if (db.RecordCount() > 0)
            {
                string filePath = db.Item("DocsURL");

                sqlString.OpenNewWindow_Center(filePath, "Consent Form", 600, 600, this);
            }
        }

        private string insertRequestRecord()
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            sql.Append("INSERT INTO tbl_Request	");
            sql.Append("(MemberID, StatusID, RequestAt, RequestBy, isDeleted, ReportType, BranchID, ProjectID, ProjectName)	");
            sql.Append("VALUES	");
            sql.Append("(N'" + secure.RC(login_user.UserId) + "', 'N', GETDATE(), N'" + secure.RC(login_user.UserId) + "', 0, 'L', N'" + secure.RC(login_user.BranchID) + "', NULL, 'EasyMode');	");
            db.Execute(sql.ToString());

            sql.Clear();
            sql.Append("SELECT TOP 1 ID, RequestID FROM tbl_Request WHERE RequestID IS NOT NULL ORDER BY ID DESC");
            db.OpenTable(sql.ToString());
            int requestID = ConvertHelper.ConvertToInt(db.Item("RequestID"), 0) + 1;
            int ID = ConvertHelper.ConvertToInt(db.Item("ID"), 0);

            sql.Clear();
            sql.Append("UPDATE tbl_Request SET RequestID = " + requestID + " WHERE RequestID IS NULL AND ID > '" + ID + "' AND RequestBy = '" + secure.RC(login_user.UserId) + "';");
            sql.Append("UPDATE tbl_Request SET StatusID ='" + ApplicationStatus.InProgress +
                "' , ApplicationResult ='" + ApplicationResult.Modify + "' WHERE RequestID = '" +
                requestID + "';");
            db.Execute(sql.ToString());

            sql.Clear();
            sql.Append("INSERT INTO tbl_RequestApplicant	");
            sql.Append("(RequestID, Name, MyCarD, PurchasePrice, CreateBy, CreateAt, UpdateBy, UpdateAt )	");
            sql.Append("VALUES	");
            sql.Append("(N'" + requestID + "', '" + txtName.Text + "', '" + txtIC.Text + "', '" + txtPrice.Text + "', N'" + secure.RC(login_user.UserId) + "'," +
                " GETDATE(), N'" + secure.RC(login_user.UserId) + "', GETDATE() );");
            db.Execute(sql.ToString());

            //string newPath = "/Upload/RequestDocument/";
            //newPath += requestID + "/";
            //if (!Directory.Exists(Server.MapPath("~" + newPath)))
            //    Directory.CreateDirectory(Server.MapPath("~" + newPath));

            //foreach (string imgpath in uploadImages)
            //{
            //    FileInfo FromFile = new FileInfo(imgpath);
            //    string toFilePath = newPath + FromFile.Name;
            //    FromFile.MoveTo(Server.MapPath("~" + toFilePath));
            //    string docSql = "INSERT INTO tbl_RequestDocs(RequestID, DocumentType, DocsURL)values(" + requestID + ",'" + DocumentType.NormalFiles + "',N'" + toFilePath + "') ";
            //    db.Execute(docSql.ToString());
            //}

            //sqlString.displayAlert2(this, Resources.resource.Action_Successful, Request.Url.ToString());

            return requestID.ToString();
        } //2
        private void getInfo(string requestID)
        {

            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();
            Boolean haveRecord = false;

            try
            {
                if (hfRequestID.Value != null)
                {
                    if (hfRequestID.Value != "0")
                    {
                        //Check tbl_TLoanLegalAction have Request Record or not
                        sql.Clear();
                        sql.Append(" SELECT COUNT(*) AS 'HaveRecord' ");
                        sql.Append(" FROM tbl_TLoanLegalAction WITH(NOLOCK) ");
                        sql.Append(" WHERE RequestID = N'" + secure.RC(hfRequestID.Value.ToString()) + "'");
                        db.OpenTable(sql.ToString());

                        if (db.RecordCount() > 0)
                        {
                            if (ConvertHelper.ConvertToDecimal(db.Item("HaveRecord").ToString(), 2) > 0)
                            {
                                haveRecord = true;
                            }
                            else
                            {
                                haveRecord = false;
                            }
                        }

                        //If have
                        if (haveRecord)
                        {
                            //Pass Main Applicant Record
                            sql.Clear();
                            sql.Append("SELECT Name, Age, AreaID, MyCard, ContactNumber, NetIncome, GrossIncome, OtherIncome ");
                            sql.Append("FROM tbl_TApplicants WITH(NOLOCK)");
                            sql.Append("WHERE RequestID = N'" + secure.RC(hfRequestID.Value.ToString()) + "' AND ApplicantType = N'MA';");
                            db.OpenTable(sql.ToString());

                            if (db.RecordCount() > 0)
                            {
                                string grossIncome = Math.Round(double.Parse(db.Item("GrossIncome"))).ToString();
                                string netIncome = Math.Round(double.Parse(db.Item("NetIncome"))).ToString();
                                string otherIncome = Math.Round(double.Parse(db.Item("OtherIncome"))).ToString();
                                
                                string area = db.Item("AreaID");
                                int age = ConvertHelper.ConvertToInt(db.Item("Age"), 0);
                            }
                            
                            //Pass Loan and Bank Legal Record
                            sql.Clear();
                            sql.Append(" SELECT ISNULL(Tenure, '0') AS 'Tenure', ISNULL(Interest, '0') AS 'Interest', ISNULL(PurchasePrice, '0') AS 'PurchasePrice', ISNULL(LoanAmount, '0') AS 'LoanAmount', ProjectID, ");
                            sql.Append(" CTOS, SAttention, Restructuring, DishonourCheque, CCRIS ");
                            sql.Append(" FROM tbl_TLoanLegalAction WITH(NOLOCK) ");
                            sql.Append(" WHERE RequestID = N'" + secure.RC(hfRequestID.Value.ToString()) + "'; ");
                            db.OpenTable(sql.ToString());

                            if (db.RecordCount() > 0)
                            {
                                int tenure = ConvertHelper.ConvertToInt(db.Item("Tenure"), 0);
                                string interest = ConvertHelper.ConvertToDecimal(db.Item("Interest"), (decimal)4.5).ToString("N");
                                string purchasePrice = Math.Round(double.Parse(db.Item("PurchasePrice"))).ToString();
                                loanAmount = Math.Round(double.Parse(db.Item("LoanAmount"))).ToString();

                                if (loanAmount.Equals("0.00") || loanAmount.Equals("0"))
                                    //loanAmount = (ConvertHelper.ConvertToDecimal(db.Item("PurchasePrice"), 0) * 90 / 100).ToString("N");
                                    loanAmount = (ConvertHelper.ConvertToDecimal(txtPrice.Text, 0) * PassBankMargin("12", projectID, hfRequestID.Value) / 100).ToString();
                                projectID = db.Item("ProjectID");
                                CTOSitem = db.Item("CTOS");
                                specialAttention = db.Item("SAttention");
                                CCRISAttention = db.Item("CCRIS");
                                dishonourCheque = db.Item("DishonourCheque");
                                restructuring = db.Item("Restructuring");
                            }

                            //Pass CCRIS PDF file
                            sql.Clear();
                            sql.Append(" SELECT DocsURL FROM tbl_RequestDocs WITH (NOLOCK) ");
                            sql.Append(" WHERE RequestID = N'" + secure.RC(hfRequestID.Value) + "' AND DocumentType = '" + DocumentType.CCRIS + "' ;");
                            db.OpenTable(sql.ToString());

                            if (db.RecordCount() > 0)
                                lblFileUpload.Text = db.Item("DocsURL");
                        }
                        //if don't have record
                        else
                        {
                            sql.Clear();
                            sql.Append(" SELECT r.MemberID, r.ReportType, ra.Name, ra.MyCard, ra.NetIncome, ra.PurchasePrice, ra.LoanAmount, r.ProjectID AS ID,r.ProjectName, tps.ProjectID");
                            sql.Append(" FROM tbl_Request r WITH(NOLOCK)");
                            sql.Append(" LEFT JOIN tbl_RequestApplicant ra WITH(NOLOCK) ON ra.RequestID = r.RequestID");
                            sql.Append(" LEFT JOIN dbo.tbl_ProjectSettings tps ON r.ProjectID = tps.ID");
                            sql.Append(" WHERE r.RequestID = N'" + hfRequestID.Value.ToString() + "'");
                            db.OpenTable(sql.ToString());

                            if (db.RecordCount() > 0)
                            {
                                string netIncome = Math.Round(double.Parse(db.Item("NetIncome"))).ToString();
                                string purchasePrice = Math.Round(double.Parse(db.Item("PurchasePrice"))).ToString();
                                loanAmount = Math.Round(double.Parse(db.Item("LoanAmount"))).ToString();

                                if (loanAmount.Equals("0.00") || loanAmount.Equals("0"))
                                    //loanAmount = (ConvertHelper.ConvertToDecimal(db.Item("PurchasePrice"), 0) * 90 / 100).ToString("N");
                                    loanAmount = (ConvertHelper.ConvertToDecimal(txtPrice.Text, 0) * PassBankMargin("12", projectID, hfRequestID.Value) / 100).ToString();

                                if (db.Item("ProjectID") != null && db.Item("ProjectID") != string.Empty)
                                    projectID = db.Item("ProjectID");
                            }
                        }
                    }

                }
                sql.Clear();
                sql.Append("SELECT * ");
                sql.Append("FROM tbl_TLoanDetail WITH(NOLOCK)");
                sql.Append("WHERE RequestID = N'" + secure.RC(hfRequestID.Value.ToString()) + "'");
                db.OpenTable(sql.ToString());
                var recordCount = db.RecordCount();
                if (recordCount == 0)
                {
                    hfMainCCRIS.Value = "false";
                    //RetrieveXMLDataForMainCoApp(hfRequestID.Value.ToString(), txtName.Text, "", txtIC.Text.Trim(), "I", true);
                    hfMainCCRIS.Value = "";
                }
            }
            catch (Exception ex)
            {
                LogUtil.logError(ex.ToString(), sql.ToString());
                sqlString.displayAlert(this, KeyVal.UnknownError);
            }
            finally { db.Close(); }
        } //3
        private void insertTempData(string requestID)
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();
            ReportController reportController = new ReportController();
            string requestid = hfRequestID.Value.ToString();
            int totalMainHouseLoan = 0;
            string newPath = "/Upload/RequestDocument/";

            double interest = double.Parse(reportController.getIntrestRate(6).ToString());
            int tenure2 = reportController.getTenureYear(6);
            int tenure = 70 - getAgeFromIC();
            if (tenure > tenure2)
                tenure = tenure2;

            if (getCTOSCCRISS(requestID))
            {
                try
                {
                    //Delete Record
                    sql.Clear();
                    sql.Append(" DELETE FROM tbl_TApplicants WHERE RequestID = N'" + secure.RC(requestID) + "';");
                    sql.Append(" DELETE FROM tbl_TLoanLegalAction WHERE RequestID = N'" + secure.RC(requestID) + "';");
                    sql.Append(" DELETE FROM tbl_TLoanInfo WHERE RequestID = N'" + secure.RC(requestID) + "';");
                    sql.Append(" DELETE FROM tbl_TLoanDetail WHERE RequestID = N'" + secure.RC(requestID) + "';");
                    sql.Append(" DELETE FROM tbl_RequestDocs WHERE RequestID = N'" + secure.RC(requestID) + "' AND DocumentType = N'" + DocumentType.CCRIS + "';");
                    db.Execute(sql.ToString());
                    LogUtil.logAction(sql.ToString(), "DELETE Temporary Information for RequestID :" + requestID);

                    requestid = hfRequestID.Value.ToString();

                    //Insert Main Applicant into Database
                    sql.Clear();
                    sql.Append(" INSERT INTO tbl_TApplicants");
                    sql.Append(" (RequestID, Name, Age, AreaID, MyCard, NetIncome, GrossIncome, OtherIncome, ApplicantType, CreateBy, CreateAt, UpdateBy, UpdateAt)");
                    sql.Append(" VALUES ( N'" + secure.RC(requestid.ToString()) + "', N'" + secure.RC(txtName.Text) + "', N'" + getAgeFromIC() + "', N'" + area + "',");
                    sql.Append(" N'" + secure.RC(txtIC.Text.Trim()) + "', '" + ((string.IsNullOrEmpty(netIncome)) ? "0" : netIncome) +
                        "', '" + ((string.IsNullOrEmpty(grossIncome)) ? "0" : grossIncome) + "', '" + ((string.IsNullOrEmpty(otherIncome)) ? "0" : otherIncome) + "',");
                    sql.Append(" N'MA', N'" + secure.RC(login_user.UserId) + "', GETDATE(), N'" + secure.RC(login_user.UserId) + "', GETDATE()) ; ");
                    db.Execute(sql.ToString());
                    LogUtil.logAction(sql.ToString(), "Insert Main Applicant Into tbl_TApplicants - RequestID :" + requestID);

                    //Insert Loan / Bank Legal Action
                    loanAmount = Math.Round(double.Parse(loanAmount.Replace(",", ""))).ToString();
                    sql.Clear();
                    sql.Append(" INSERT INTO tbl_TLoanLegalAction ");
                    sql.Append(" (RequestID, Tenure, Interest, PurchasePrice, LoanAmount, ProjectID, CTOS, SAttention, Restructuring, DishonourCheque, CCRIS, IsDeleted, CreateAt, CreateBy, UpdateAt, UpdateBy)");
                    sql.Append(" VALUES(N'" + secure.RC(requestid.ToString()) + "', '" + tenure +
                        "', '" + interest + "', '" +
                        ((string.IsNullOrEmpty(txtPrice.Text) || string.IsNullOrWhiteSpace(txtPrice.Text)) ? "0" : txtPrice.Text) +
                        "', '" + ((string.IsNullOrEmpty(loanAmount) || string.IsNullOrWhiteSpace(loanAmount)) ? "0" : loanAmount) +
                        "', 'Others',");
                    sql.Append(" '" + (secure.RC(CTOSitem).Equals("") ? "0" : secure.RC(CTOSitem)) + "', '" +
                        (secure.RC(specialAttention).Equals("") ? "0" : secure.RC(specialAttention)) +
                        "', '" + (secure.RC(restructuring).Equals("") ? "0" : secure.RC(restructuring)) + "', ");
                    sql.Append(" '" + (secure.RC(dishonourCheque).Equals("") ? "0" : secure.RC(dishonourCheque)) +
                        "', '" + (secure.RC(CCRISAttention).Equals("") ? "0" : secure.RC(CCRISAttention)) +
                        "', 'False', GETDATE(),'" + secure.RC(login_user.UserId) + "', GETDATE(),'" + secure.RC(login_user.UserId) + "')");
                    db.Execute(sql.ToString());
                    LogUtil.logAction(sql.ToString(), "Insert Bank Legal Action Into tbl_TLoanLegalAction - RequestID :" + requestID);

                    //Insert CCRIS File in tbl_RequestDocs
                    newPath += requestID + "/";
                    if (lblFileUpload.Text != String.Empty)
                    {
                        if (!Directory.Exists(Server.MapPath("~" + newPath)))
                            Directory.CreateDirectory(Server.MapPath("~" + newPath));

                        FileInfo FromFile = new FileInfo("");//lblFileUpload.Text);
                        string toFilePath = newPath + FromFile.Name;
                        if (FromFile.Exists)
                        {
                            FromFile.MoveTo(Server.MapPath("~" + toFilePath));
                        }
                        sql.Append("INSERT into tbl_RequestDocs(RequestID,DocumentType , DocsURL)values(" + requestID + ",'" + DocumentType.CCRIS + "',N'" + toFilePath + "') ");
                        db.Execute(sql.ToString());
                        LogUtil.logAction(sql.ToString(), "Insert Document Into tbl_RequestDocs - RequestID :" + requestID);
                    }

                    for (int i = 0; i < 5; i++)
                    {
                        TextBox txtoriginal = (TextBox)gvMainHousingLoan.Rows[i].Cells[0].FindControl("txtMainOriginalVal");
                        TextBox txtoutstanding = (TextBox)gvMainHousingLoan.Rows[i].Cells[1].FindControl("txtMainOutstandinglVal");
                        TextBox txtrepayment = (TextBox)gvMainHousingLoan.Rows[i].Cells[2].FindControl("txtMainRepaymentlVal");
                        decimal original = ConvertHelper.ConvertToDecimal(txtoriginal.Text, 0);
                        decimal outstanding = ConvertHelper.ConvertToDecimal(txtoutstanding.Text, 0);
                        decimal repayment = ConvertHelper.ConvertToDecimal(txtrepayment.Text, 0);

                        if (txtoriginal.Text != String.Empty || txtoutstanding.Text != String.Empty || txtrepayment.Text != String.Empty)
                        {
                            if (original != 0 || outstanding != 0 || repayment != 0)
                            {
                                totalMainHouseLoan = totalMainHouseLoan + 1;
                            }
                        }
                    }

                    if (totalMainHouseLoan > 0)
                    {
                        sql.Clear();
                        sql.Append(" UPDATE tbl_TApplicants ");
                        sql.Append(" SET HouseLoanNumber = N'" + totalMainHouseLoan + "' ");
                        sql.Append(" WHERE RequestID = N'" + secure.RC(requestID) + "' AND ApplicantType = N'MA'; ");
                        db.Execute(sql.ToString());
                        LogUtil.logAction(sql.ToString(), "Update Main HouseLoanNumber in tbl_TApplicants - RequestID :" + requestID);
                    }
                    else
                    {
                        sql.Clear();
                        sql.Append(" UPDATE tbl_TApplicants ");
                        sql.Append(" SET HouseLoanNumber = '0' ");
                        sql.Append(" WHERE RequestID = N'" + secure.RC(requestID) + "' AND ApplicantType = N'MA'; ");
                        db.Execute(sql.ToString());
                        LogUtil.logAction(sql.ToString(), "Update Main HouseLoanNumber in tbl_TApplicants - RequestID :" + requestID);
                    }

                    //Read all the bank id
                    string BankID = "";
                    sql.Clear();
                    sql.Append(" SELECT BankID ");
                    sql.Append(" FROM tbl_BankName WITH(NOLOCK) ");
                    sql.Append(" WHERE Status='A' AND IsDeleted = 'False' ");
                    db.OpenTable(sql.ToString());
                    if (db.RecordCount() > 0)
                    {
                        for (int h = 0; h < db.RecordCount(); h++)
                        {
                            BankID = db.Item("BankID");

                            // Main Applicant Gridview 
                            InsertDataIntoLoanDetail(gvMainHousingLoan, requestid, BankID, 1, 5, true);
                            InsertDataIntoLoanDetail(gvMainCommercialLoan, requestid, BankID, 2, 5, true);
                            InsertDataIntoLoanDetail(gvMainPersonalLoan, requestid, BankID, 3, 5, true);
                            InsertDataIntoLoanDetail(gvMainOD, requestid, BankID, 4, 5, true);
                            InsertDataIntoLoanDetail(gvMainXProduct, requestid, BankID, 5, 10, true);
                            InsertDataIntoLoanDetail(gvMainHirePurchase, requestid, BankID, 6, 5, true);
                            InsertDataIntoLoanDetail(gvMainCreditCard, requestid, BankID, 7, 10, true);

                            InsertDataIntoLoanInfo(requestid, BankID, true);

                            if (!db.Eof())
                                db.MoveNext();
                        }
                    }
                    //sqlString.displayAlert(this, "Calculate Successfully", "/Form/Loan/CalculateReport.aspx?rid=" + sqlString.encryptURL(requestID));
                }
                catch (Exception ex)
                {
                    LogUtil.logError(ex.ToString(), sql.ToString());
                    sqlString.displayAlert(this, KeyVal.UnknownError);
                }
                finally
                {
                    db.Close();
                }
            }
        } //4
        private int getAgeFromIC()
        {
            //int birthYear = ConvertHelper.ConvertToInt(txtIC.Text.Remove(1, txtIC.Text.Length-1), 0);
            //int currentYear = ConvertHelper.ConvertToInt(DateTime.Now.Year.ToString(), 2019);

            int age = (ConvertHelper.ConvertToInt(DateTime.Now.Year.ToString(), 2019) -
                ConvertHelper.ConvertToInt(GetDateFromIc(txtIC.Text).Substring(0, 4), 0));
            
            return age;
        } //4.3.6, 4.1.1.1.1
        private string GetDateFromIc(string ic)
        {
            //1955-11-22
            int icShortYear = Convert.ToInt32(ic.Substring(0, 2));
            int currentShortYear = Convert.ToInt32(DateTime.Now.Year.ToString().Substring(2, 2));
            int reduceYear = icShortYear > currentShortYear ? -1 : 0;
            string year = (Convert.ToInt32(DateTime.Now.Year.ToString().Substring(0, 2)) + reduceYear).ToString() + ic.Substring(0, 2);
            string month = ic.Substring(2, 2);
            string day = ic.Substring(4, 2);

            return year + "-" + month + "-" + day;
        } //4.3.6.1
        #endregion

        #region CALCULATION
        private decimal RepaymentFlat(int bankID, decimal LoanAmount, int type) //HirePurchase, Overdraft, Personal Loan
        {
            decimal repayment = 0;
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();
            try
            {
                decimal Interest = 0;
                int Year = 0;
                //get interest and loan year
                sql.Append(getEffectiveRate(type));
                sql.Append(" WHERE BankId = '" + bankID + "'");
                db.OpenTable(sql.ToString());
                if (db.RecordCount() > 0)
                {
                    Interest = Convert.ToDecimal(db.Item("Interest"));
                    Year = ConvertHelper.ConvertToInt32(db.Item("Year"), 0);
                }
                db.Close();

                /*
                    FORMULA FOR HirePurchase

                                        Interest (amount that saved into database is in percentage form)   +  Total amount 
                    <!-- c -->		= --------------------------------------------------------------------------------------
                                                                How many month used to pay
                                        //$$c_hp_interest/100*$c_hp_year is to obtain one year interest .
                */
                //repayment = ((Interest / 100 * Year) + 1) * LoanAmount / (Year * 12);
                //if OD(type 4), doesn't need to add outstanding amount
                repayment = (((type == 4 ? 0 : LoanAmount) + (LoanAmount * (Interest / 100)) * Year) / (Year * 12));
            }
            catch (Exception ex)
            {
                LogUtil.logError(ex.ToString(), sql.ToString());
                sqlString.displayAlert(this, KeyVal.UnknownError);
            }
            finally { db.Close(); }
            return repayment;
        } //4.2.1
        private string getEffectiveRate(int type)
        {
            string sql = string.Empty;
            switch (type)
            {
                case 1: //House
                    sql = "SELECT Year, Interest FROM tbl_houseloan ";
                    break;
                case 2://Commercial
                    sql = "SELECT Year, Interest FROM tbl_commercialloan ";
                    break;
                case 3://Personal
                    sql = "SELECT Year, Interest FROM tbl_PersonalLoan ";
                    break;
                case 4: //Overdraft
                    sql = "SELECT Year, Interest FROM tbl_overdraft";
                    break;
                case 5: //X-product
                    sql = "SELECT Year, Interest FROM tbl_xproduct";
                    break;
                case 6: //HirePurchase
                    sql = "SELECT Year, Interest FROM tbl_hirepurchase";
                    break;
            }
            return sql;
        } //4.2.1.1, 4.2.2.1, 4.2.3.1
        private decimal RepaymentMin(int bankID, decimal LoanAmount) //Credit Card
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();
            decimal repayment = LoanAmount;
            try
            {
                sql.Append("SELECT Minimum from tbl_CreditCard WHERE BankId = '" + bankID + "'");
                db.OpenTable(sql.ToString());
                decimal minimum = Convert.ToDecimal(db.Item("Minimum"));
                repayment = LoanAmount * (minimum / 100);
                repayment = repayment <= KeyVal.MinimumRepayment ? KeyVal.MinimumRepayment : repayment;
            }
            catch (Exception ex)
            {
                LogUtil.logError(ex.ToString(), sql.ToString());
                sqlString.displayAlert(this, KeyVal.UnknownError);
            }
            finally { db.Close(); }
            return repayment;
        } //4.2.2
        private decimal RepaymentEffective(int bankID, decimal LoanAmount, int type) //Housing Loan, Commercial Loan, X-Product
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();
            //int mainAge = ConvertHelper.ConvertToInt32(txtMainAge.Text.Trim(), 0);
            //int coAge = ConvertHelper.ConvertToInt32(txtCoAge.Text.Trim(),0);
            int mainAge = ConvertHelper.ConvertToInt32(getAgeFromIC().ToString().Trim(), 0);
            int coAge = 0;
            int maxAge = 70; int maxYear = 0;
            double Interest = 0;
            int Year = 0;
            decimal repayment = 0;
            try
            {

                //get interest and max loan year
                sql.Append(getEffectiveRate(type));
                sql.Append(" WHERE BankID = '" + bankID + "' ");
                db.OpenTable(sql.ToString());
                if (db.RecordCount() > 0)
                {
                    Interest = Convert.ToDouble(db.Item("Interest"));
                    Year = ConvertHelper.ConvertToInt(db.Item("Year").ToString(), 0);
                }

                db.Close();


                int Age = YoungerAge(mainAge, coAge);

                maxYear = (Age > 70) ? (maxAge = Age) : Year;
                if (Age > 70)
                    maxYear = maxAge - Age;

                /*
                FORMULA 

                          Interest*Total House loan that u want to calculate(main and co is seperated )			
                <!-- c -->   =  -----------------------------------------
                                1 - (1 + Interest)^(-Year Left to borrow,N)
				
                */
                //CONVERT YEAR TO MONTH

                double Powerfor_N = -(maxYear * 12);
                double interestforonemonth = Interest / 100 / 12;
                double power = (1 - Math.Pow((1 + interestforonemonth), Powerfor_N));
                repayment = (LoanAmount * Convert.ToDecimal(interestforonemonth)) / Convert.ToDecimal(power);

            }
            catch (Exception ex)
            {
                LogUtil.logError(ex.ToString(), sql.ToString());
                sqlString.displayAlert(this, KeyVal.UnknownError);
            }
            finally { db.Close(); }

            return repayment;
        } //4.2.3
        private int YoungerAge(int mainAge, int coAge)
        {
            int Age = 0;
            Age = mainAge <= coAge ? mainAge : coAge;
            return Age;
        } //4.2.3.1
        private decimal PassBankMargin(string bankid, string projectid, string requestid)
        {
            decimal bankMargin = 0;
            bool capping = false;
            decimal mainHouseLoanAmount = 0;
            decimal coHouseLoanAmount = 0;
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            try
            {

                sql.Clear();
                sql.Append(" SELECT HouseLoanNumber ");
                sql.Append(" FROM tbl_TApplicants WITH (NOLOCK)");
                sql.Append(" WHERE ApplicantType = 'MA' AND RequestID = '" + secure.RC(requestid) + "' ");
                db.OpenTable(sql.ToString());

                if (db.RecordCount() > 0)
                {
                    mainHouseLoanAmount = ConvertHelper.ConvertToDecimal(db.Item("HouseLoanNumber"), 0);
                }

                sql.Clear();
                sql.Append(" SELECT HouseLoanNumber ");
                sql.Append(" FROM tbl_TApplicants WITH (NOLOCK)");
                sql.Append(" WHERE ApplicantType = 'CA' AND RequestID = '" + secure.RC(requestid) + "' ");
                db.OpenTable(sql.ToString());

                if (db.RecordCount() > 0)
                {
                    coHouseLoanAmount = ConvertHelper.ConvertToDecimal(db.Item("HouseLoanNumber"), 0);
                }

                sql.Clear();
                sql.Append(" SELECT ProjectID, Capping, BankMargin, BankID ");
                sql.Append(" FROM ( ");
                sql.Append(" 	SELECT p.ProjectID, p.Capping, pb.BankMargin, pb.BankID ");
                sql.Append(" 	FROM tbl_ProjectSettings p WITH(NOLOCK) ");
                sql.Append(" 	LEFT JOIN tbl_ProjectSettings_Bank pb WITH(NOLOCK) ON pb.ProjectID = p.ProjectID ");
                sql.Append(" 	INNER JOIN tbl_BankName b WITH(NOLOCK) ON b.BankID = pb.BankID AND b.Status = 'A' AND b.IsDeleted = 'False' ");
                sql.Append(" 	WHERE p.Status = 'A' AND p.isDeleted = 'false' ");
                sql.Append(" 	) AS A ");
                sql.Append(" WHERE ProjectID = '" + secure.RC(projectid) + "' AND BankID = '" + secure.RC(bankid) + "' ;");
                db.OpenTable(sql.ToString());

                if (db.RecordCount() > 0)
                {
                    capping = ConvertHelper.ConvertToBoolen(db.Item("Capping"), false);
                    bankMargin = ConvertHelper.ConvertToDecimal(db.Item("BankMargin"), 90);
                }

                //if (capping == true)
                //{
                //    if (bankMargin > 70)
                //    {
                //        bankMargin = 70;
                //    }
                //}
                //else if (capping == false)
                //{
                //    if (mainHouseLoanAmount > 1 || coHouseLoanAmount > 1)
                //    {
                //        if (bankMargin > 70)
                //        {
                //            bankMargin = 70;
                //        }
                //    }
                //}

                if (mainHouseLoanAmount > 1 || coHouseLoanAmount > 1)
                {
                    if (bankMargin > 70)
                    {
                        bankMargin = 70;
                    }
                }

                if (bankMargin == 0)
                    bankMargin = 90;
            }
            catch (Exception ex)
            {
                LogUtil.logError(ex.ToString(), sql.ToString());
                sqlString.displayAlert(this, KeyVal.UnknownError);
            }
            finally { db.Close(); }


            return bankMargin;
        } //4.3.1
        private decimal CalculateNewLoanRepayment(string bankid, string projectid, string requestid, decimal tenure,
            decimal interest)
        {
            decimal newLoanRepayment = 0;
            decimal margin = 0;
            //decimal policy = 0;
            decimal income = 0;
            decimal oldrepayment = 0;
            decimal loanAmount = 0;

            bool isMainOnly = true;

            margin = PassBankMargin(bankid, projectid, requestid);
            income = CalculateTotalIncome(isMainOnly); // user nett + other to calculate 
            //policy = calculatorController.getBankPolicy(bankid, income, isMainOnly); // use gross income to check bank policy
            oldrepayment = CalculateTotalOldBankRepayments(bankid, requestid, isMainOnly);

            if (this.loanAmount.ToString() != String.Empty || ConvertHelper.ConvertToDecimal(this.loanAmount.ToString(), 0) != 0)
            {
                loanAmount = ConvertHelper.ConvertToDecimal(this.loanAmount.ToString(), 0);
            }
            else if (this.loanAmount.ToString() == String.Empty || ConvertHelper.ConvertToDecimal(this.loanAmount.ToString(), 0) == 0)
            {
                if (txtPrice.Text != String.Empty || ConvertHelper.ConvertToDecimal(txtPrice.Text, 0) != 0)
                {
                    loanAmount = ConvertHelper.ConvertToDecimal(txtPrice.Text, 0) * margin / 100;
                }
                else if (txtPrice.Text == String.Empty || ConvertHelper.ConvertToDecimal(txtPrice.Text, 0) == 0)
                {
                    newLoanRepayment = 0;//calculatorController.calculateLoanEligible(policy, oldrepayment, tenure, income);
                }
            }
            if (loanAmount > 0)
            {
                newLoanRepayment = calculatorController.calculateNewRepayment(loanAmount, tenure, interest);
            }
            else
            {
                newLoanRepayment = 0;
            }

            return newLoanRepayment;
        } //4.3.2
        private decimal CalculateTotalOldBankRepayments(string bankid, string requestid, bool isMainOnly = true)
        {
            decimal totalOldRepayments = 0;

            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();
            try
            {
                if (isMainOnly == true)
                {
                    sql.Clear();
                    sql.Append(" Select t.BankID, SUM(t.repayment) AS 'Repayment' ");
                    sql.Append(" FROM tbl_TLoanDetail t ");
                    sql.Append(" INNER JOIN tbl_BankName b WITH(NOLOCK) ON b.BankID = t.BankID AND b.Status = 'A' ");
                    sql.Append(" WHERE t.requestid= N'" + secure.RC(requestid) + "' AND t.isMain = 'true' AND t.BankID = N'" + secure.RC(bankid) + "' ");
                    sql.Append(" GROUP BY t.requestID, t.BankID ");
                }
                db.OpenTable(sql.ToString());

                if (db.RecordCount() > 0)
                {
                    totalOldRepayments = ConvertHelper.ConvertToDecimal(db.Item("Repayment"), 0);
                }
            }
            catch (Exception ex)
            {
                LogUtil.logError(ex.ToString(), sql.ToString());
                sqlString.displayAlert(this, KeyVal.UnknownError);
            }
            finally { db.Close(); }

            return totalOldRepayments;

        } //4.3.3
        private decimal CalculateTotalIncome(bool isMainOnly = true)
        {
            decimal totalIncome = 0;

            decimal mainOtherIncome = 0;
            decimal mainNetIncome = 0;
            
            mainNetIncome = ConvertHelper.ConvertToDecimal(netIncome, 0);
            mainOtherIncome = ConvertHelper.ConvertToDecimal(otherIncome, 0);
            
            totalIncome = mainNetIncome + mainOtherIncome;

            return totalIncome;
        } //4.3.4
        private decimal CalculateGrossIncomePolicy()
        {
            decimal totalIncome = 0;
            decimal mainGrossIncome = 0;
            decimal mainOtherIncome = 0;
            decimal mainNetIncome = 0;
            decimal coNetIncome = 0;
            decimal coOtherIncome = 0;
            decimal coGrossIncome = 0;
            decimal grossIncome = 0;
            decimal totalSalary = 0;

            mainGrossIncome = ConvertHelper.ConvertToDecimal(grossIncome, 0);
            mainNetIncome = ConvertHelper.ConvertToDecimal(netIncome, 0);
            mainOtherIncome = ConvertHelper.ConvertToDecimal(otherIncome, 0);

            grossIncome = mainGrossIncome + coGrossIncome;
            totalSalary = mainOtherIncome + mainNetIncome + coOtherIncome + coNetIncome;

            if (grossIncome != 0)
            {
                totalIncome = grossIncome;
            }
            else
            {
                totalIncome = totalSalary;
            }


            return totalIncome;
        } //4.3.5
        #endregion

        #region CTOS
        private bool getCTOSCCRISS(string requestID)
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();
            bool result = false;
            
            //Clear Main Applicant Gridview TextBox
            ClearGVTextBox(gvMainCommercialLoan, true);
            ClearGVTextBox(gvMainCreditCard, true);
            ClearGVTextBox(gvMainHirePurchase, true);
            ClearGVTextBox(gvMainHousingLoan, true);
            ClearGVTextBox(gvMainOD, true);
            ClearGVTextBox(gvMainPersonalLoan, true);
            ClearGVTextBox(gvMainXProduct, true);

            sql.Clear();
            sql.Append("INSERT INTO tbl_TApplicantCreditAdvisor(requestid , advisorid ,comment) VALUES('" + requestID + "','0','");
            sql.Append(Environment.NewLine + "');");
            db.Execute(sql.ToString());

            if (hfMainCCRIS.Value.Trim() == string.Empty || hfMainCCRIS.Value == null)
            {
                sql.Clear();
                sql.Append(" SELECT Result ");
                sql.Append(" FROM tbl_CTOS_Result WITH(NOLOCK) ");
                sql.Append(" WHERE IsActive = 1 ");
                if (hftemprequestId.Value.Trim() == string.Empty || hftemprequestId.Value == null)
                    sql.Append(" AND Request_Id = N'" + secure.RC(hfRequestID.Value.ToString()) + "'");
                else
                    sql.Append(" AND TempId = N'" + hftemprequestId.Value + "'");
                sql.Append(" AND IC_Number ='" + secure.RC(txtIC.Text.Trim()) + "'");
                db.OpenTable(sql.ToString());

                if (db.RecordCount() > 0)
                {
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "ajax", "<script language='javascript'>confirmMainCCRIS();</script>", false);
                    //ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "MyFun1", "confirmMainCCRIS();", true);
                    result = RetrieveXMLDataForMainCoApp(hfRequestID.Value.ToString(), txtName.Text, "", txtIC.Text.Trim(), "I", true);//test
                }
                else
                {
                    result = RetrieveXMLDataForMainCoApp(hfRequestID.Value.ToString(), txtName.Text, "", txtIC.Text.Trim(), "I", true);
                    //if (result)
                    //    sqlString.displayAlert(this, "Successfully retrieved " + type + " report.");
                    //else
                    //    sqlString.displayAlert(this, "No " + type + " report found.");
                }
            }
            else
            {
                result = RetrieveXMLDataForMainCoApp(hfRequestID.Value.ToString(), txtName.Text, "", txtIC.Text.Trim(), "I", true);
                hfMainCCRIS.Value = "";
                //if (result)
                //    sqlString.displayAlert(this, "Successfully retrieved CTOS report.");
                //else
                //    sqlString.displayAlert(this, "No CTOS report found.");
            }

            return result;
        } //4.1
        private bool RetrieveXMLDataForMainCoApp(string requestId, string mName, string oldIC, string newIC,
            string mType, bool callAPI = false, bool isMain = true, string apiType = "CTOS",
            bool isCoApplicant = false)
        {
            string errorResponse = string.Empty;
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();
            string decodedString = string.Empty;
            string type = "CTOS";

            string entitiesNumber = "90007";
            bool CTOSHasMultipleEntities = false;
            do
            {
                CTOSHasMultipleEntities = false;
                sql.Clear();
                sql.Append(" SELECT Result, Type ");
                sql.Append(" FROM tbl_CTOS_Result WITH(NOLOCK) ");
                sql.Append(" WHERE IsActive = 1 ");
                if (hftemprequestId.Value.Trim() == string.Empty || hftemprequestId.Value == null)
                    sql.Append(" AND Request_Id = N'" + secure.RC(requestId) + "'");
                else
                    sql.Append(" AND TempId = N'" + hftemprequestId.Value + "'");
                sql.Append(" AND IsCoApplicant = N'" + (isCoApplicant == false ? 0 : 1) + "'");
                db.OpenTable(sql.ToString());

                if (db.RecordCount() > 0)
                {
                    if (hfMainCCRIS.Value == "false" && isCoApplicant == false)
                    {
                        decodedString = db.Item("Result").ToString();
                        type = db.Item("Type").ToString();
                    }
                    else
                    {
                        sql.Clear();
                        sql.Append(" UPDATE tbl_CTOS_Result SET IsActive = 0 WHERE");
                        if (hftemprequestId.Value.Trim() == string.Empty || hftemprequestId.Value == null)
                            sql.Append(" Request_Id = N'" + secure.RC(requestId) + "'");
                        else
                            sql.Append(" TempId = N'" + hftemprequestId.Value + "'");
                        sql.Append(" AND IsCoApplicant = N'" + (isCoApplicant == false ? 0 : 1) + "'");
                        db.Execute(sql.ToString());
                    }
                }
                if(test)
                    callAPI = false;
                #region CTOS
                if (decodedString.Trim() == string.Empty && callAPI == true && apiType == "CTOS")
                {
                    type = apiType;
                    string API_URL, API_Username, API_Password, API_ContentType, API_Method, API_CompanyCode, API_AccountNo, API_Version;
                    API_URL = ConfigurationManager.AppSettings["API_URL"];
                    API_Username = ConfigurationManager.AppSettings["API_Username"];
                    API_Password = ConfigurationManager.AppSettings["API_Password"];
                    API_ContentType = ConfigurationManager.AppSettings["API_ContentType"];
                    API_Method = ConfigurationManager.AppSettings["API_Method"];
                    API_CompanyCode = ConfigurationManager.AppSettings["API_CompanyCode"];
                    API_AccountNo = ConfigurationManager.AppSettings["API_AccountNo"];
                    API_Version = ConfigurationManager.AppSettings["API_Version"];
                    
                    //convert the xml string to byte array
                    var xmlText = @"<?xml version=""1.0"" encoding=""UTF-8""?>
            <soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
                <soapenv:Body>
                    <tns:request xmlns:tns=""http://ws.proxy.xml.ctos.com.my/"">
                        <input>{0}</input>
                    </tns:request>
                </soapenv:Body>
            </soapenv:Envelope>";
                    var payload = @"<batch output=""{0}"" no=""0009"" xmlns=""http://ws.cmctos.com.my/ctosnet/request"">
                <company_code>" + API_CompanyCode + @"</company_code>
                <account_no>" + API_AccountNo + @"</account_no>
                <user_id>" + API_Username + @"</user_id>
                <record_total>1</record_total>
                <records>
                <type>{1}</type>
                <ic_lc>{2}</ic_lc>
                <nic_br>{3}</nic_br>
                <name>{4}</name>
                <mphone_nos/>
                <ref_no></ref_no>
                <dist_code>PENANG BRANCH</dist_code>
                <purpose code=""200"">Credit evaluation/account opening on subject/directors/shareholder with consent /due diligence on AMLA compliance</purpose>
                <include_consent>0</include_consent>
                <include_ctos>1</include_ctos>
                <include_trex>1</include_trex>
                <include_ccris sum=""0"">1</include_ccris>
                <include_dcheq>0</include_dcheq>
                <include_ccris_supp>0</include_ccris_supp>
                <include_fico>0</include_fico>
                <include_sfi>0</include_sfi>
                <include_ssm>0</include_ssm>
                <confirm_entity>{5}</confirm_entity>
                </records>
            </batch>";
                    //< include_ccris sum = ""0"" > 1 </ include_ccris > Turn off CCRIS summary
                    //< include_ccris_supp > 0 </ include_ccris_supp > Turn off CCRIS supplementary

                    string xmlRequestBody = string.Format(xmlText, System.Net.WebUtility.HtmlEncode(string.Format(payload, "0", mType, oldIC, newIC, mName, entitiesNumber)));
                    string _result;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    var request = (HttpWebRequest)WebRequest.Create(API_URL);
                    request.Method = API_Method;
                    request.ContentType = API_ContentType;
                    request.Headers["username"] = API_Username;
                    request.Headers["password"] = API_Password;
                    var writer = new StreamWriter(request.GetRequestStream());
                    writer.Write(xmlRequestBody);
                    writer.Close();

                    try
                    {
                        var response = (HttpWebResponse)request.GetResponse();

                        var streamResponse = response.GetResponseStream();
                        var streamRead = new StreamReader(streamResponse);

                        _result = streamRead.ReadToEnd().Trim();

                        int startIndex = _result.IndexOf("<return>") + 8;
                        int endIndex = _result.IndexOf("</return>");

                        var result2 = _result.Substring(startIndex, endIndex - startIndex);
                        byte[] data = Convert.FromBase64String(result2);
                        decodedString = Encoding.UTF8.GetString(data);
                        streamRead.Close();
                        streamResponse.Close();
                        response.Close();
                    }
                    catch (Exception ex)
                    {
                        LogUtil.logError(ex.Message, "Error pulling " + apiType + " report. RequestID:"+requestId);
                        string externalip = new WebClient().DownloadString("http://icanhazip.com");
                        Console.WriteLine(externalip);
                        return false;
                    }

                    StringBuilder sqlInsert = new StringBuilder();
                    StringBuilder sqlValues = new StringBuilder();
                    sql.Clear();
                    sqlInsert.Clear();
                    sqlValues.Clear();
                    sqlInsert.Append(" INSERT INTO tbl_CTOS_Result ");
                    sqlInsert.Append(" (Request_Id, IC_Number, Report_Type,IsCoApplicant,Result,GeneratedDate,IsActive,Type,RequestPayload");
                    sqlValues.Append(" VALUES( N'" + secure.RC(requestId) + "', N'" + secure.RC(newIC) + "','" +
                        secure.RC(mType) + "', '0','" + secure.RC(decodedString) +
                        "',GETDATE(),1,N'" + secure.RC(type) + "',N'" + secure.RC(xmlRequestBody) + "'");
                    if (hftemprequestId.Value.Trim() != string.Empty)
                    {
                        sqlInsert.Append(",TempId");
                        sqlValues.Append(",'" + hftemprequestId.Value + "'");
                    }
                    sqlInsert.Append(")");
                    sqlValues.Append(")");

                    sql.Append(sqlInsert);
                    sql.Append(sqlValues);
                    db.Execute(sql.ToString());
                }
                #endregion

                if (test)
                {
                    db.OpenTable("SELECT Result FROM tbl_CTOS_Result WHERE Request_Id = N'" + secure.RC(requestId) + "'");
                    decodedString = db.Item("Result");
                }
                if (decodedString.Trim() != string.Empty)
                {
                    if (type == "CTOS")
                    {
                        //XmlController _controller = new XmlController();
                        //HJT.Cls.Model.XmlModel.LOAN.report _Model = new HJT.Cls.Model.XmlModel.LOAN.report();
                        //_Model = _controller.DeserializeXml<HJT.Cls.Model.XmlModel.LOAN.report>(decodedString);
                        XmlSerializer serializer = new XmlSerializer(typeof(report));
                        StringReader stringReader = new StringReader(decodedString);
                        report report = (report)serializer.Deserialize(stringReader);
                        

                        if (report.enq_report.entities != null && report.enq_report.enquiry == null)
                        {
                            decodedString = string.Empty;
                            entitiesNumber = report.enq_report.entities.FirstOrDefault().entity.FirstOrDefault().key;
                            CTOSHasMultipleEntities = true;
                        }
                        else
                        {
                            countFacilities(report.enq_report.enquiry.FirstOrDefault());
                            checkSpecialAttention(report.enq_report.enquiry.FirstOrDefault());
                            if (report.enq_report.enquiry.FirstOrDefault().section_ccris != null)
                            {
                                checkLegalStatus(report.enq_report.enquiry.FirstOrDefault().section_ccris);
                                checkCCRIS(report.enq_report.enquiry.FirstOrDefault().section_ccris);
                            }

                            var enquiry = report.enq_report.enquiry.FirstOrDefault();
                            List<ccris_account_detail> accounts = new List<ccris_account_detail>();
                            List<special_attention_acc_detail> special_attention_accounts = new List<special_attention_acc_detail>();

                            if (enquiry != null && enquiry.section_ccris != null)
                            {
                                if (enquiry.section_ccris.accounts != null && enquiry.section_ccris.accounts.account != null)
                                    accounts = enquiry.section_ccris.accounts.account.ToList();
                                if (enquiry.section_ccris.special_attention_accs != null
                                    && enquiry.section_ccris.special_attention_accs.FirstOrDefault() != null)
                                    special_attention_accounts = enquiry.section_ccris.special_attention_accs.ToList();
                            }

                            sql.Clear();
                            sql.Append(" SELECT ID ");
                            sql.Append(" FROM tbl_CTOS_Result WITH(NOLOCK) ");
                            sql.Append(" WHERE Request_Id = N'" + secure.RC(requestId) + "'");
                            sql.Append(" AND IC_Number ='" + secure.RC(newIC) + "'");
                            db.OpenTable(sql.ToString());
                            string ResultId = "";
                            if (db.RecordCount() > 0)
                            {
                                ResultId = db.Item("ID").ToString();
                            }
                            
                            PassCTOSDatatoMainLoanDetails(ResultId, accounts, special_attention_accounts);

                            return true;
                        }
                    }
                }
            } while (CTOSHasMultipleEntities);

            return false;
        } //4.1.1
        private void PassCTOSDatatoMainLoanDetails(string resultId, List<ccris_account_detail> accounts, List<special_attention_acc_detail> special_Attention_Accs)
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();
            string original = "";
            string outstanding = "";
            string repayment = "";
            string loanid = "";

            //Clear Main Applicant Gridview TextBox
            ClearGVTextBox(gvMainCommercialLoan, true);
            ClearGVTextBox(gvMainCreditCard, true);
            ClearGVTextBox(gvMainHirePurchase, true);
            ClearGVTextBox(gvMainHousingLoan, true);
            ClearGVTextBox(gvMainOD, true);
            ClearGVTextBox(gvMainPersonalLoan, true);
            ClearGVTextBox(gvMainXProduct, true);

            sql.Clear();
            sql.Append(" SELECT CTOS_Result_Id ");
            sql.Append(" FROM tbl_CTOS_Report_Loan_Details WITH(NOLOCK) ");
            sql.Append(" WHERE CTOS_Result_Id = N'" + secure.RC(resultId) + "'");
            db.OpenTable(sql.ToString());

            if (db.RecordCount() <= 0)
            {
                InsertCTOSReport(resultId, accounts);
                if (special_Attention_Accs != null)
                    InsertCTOSReport(resultId, special_Attention_Accs);
            }

            foreach (var _account in accounts)
            {
                string capacityCode = _account.capacity.code.ToString();//Own/Joint/Partner
                string loanType = _account.sub_accounts[0].facility.code.ToString();
                sql.Clear();
                sql.Append("SELECT LoanID FROM tbl_LoanTypesXML WITH(NOLOCK) WHERE LoanTypeCode = '" + loanType + "';");
                db.OpenTable(sql.ToString());
                if (db.RecordCount() > 0)
                {
                    loanid = db.Item("LoanID");
                    switch (loanid)
                    {
                        case "1"://Housing Loan
                            for (int i = 0; i < _account.sub_accounts.Count(); i++)
                            {
                                original = string.IsNullOrEmpty(_account.limit.ToString()) ? "" : _account.limit.ToString();
                                outstanding = string.IsNullOrEmpty(_account.sub_accounts[i].cr_positions[0].balance.ToString()) ? "" : _account.sub_accounts[i].cr_positions[0].balance.ToString();
                                repayment = string.IsNullOrEmpty(_account.sub_accounts[i].cr_positions[0].inst_amount.ToString()) ? "" : _account.sub_accounts[i].cr_positions[0].inst_amount.ToString();
                                if ((capacityCode.Equals("Joint") || capacityCode.Equals("Partner")) && !string.IsNullOrWhiteSpace(repayment) && double.TryParse(repayment, out double parseRepayment))
                                    repayment = (parseRepayment / 2).ToString();

                                for (int j = 0; j < 5; j++)
                                {
                                    TextBox txtMainOriginalValMainHousingLoan = (TextBox)gvMainHousingLoan.Rows[j].Cells[0].FindControl("txtMainOriginalVal");
                                    TextBox txtMainOutstandinglValMainHousingLoan = (TextBox)gvMainHousingLoan.Rows[j].Cells[1].FindControl("txtMainOutstandinglVal");
                                    TextBox txtMainRepaymentlValMainHousingLoan = (TextBox)gvMainHousingLoan.Rows[j].Cells[2].FindControl("txtMainRepaymentlVal");
                                    if (String.IsNullOrEmpty(txtMainOriginalValMainHousingLoan.Text) && String.IsNullOrEmpty(txtMainOutstandinglValMainHousingLoan.Text) && String.IsNullOrEmpty(txtMainRepaymentlValMainHousingLoan.Text))
                                    {
                                        txtMainOriginalValMainHousingLoan.Text = original;
                                        txtMainOutstandinglValMainHousingLoan.Text = outstanding;
                                        txtMainRepaymentlValMainHousingLoan.Text = repayment;
                                        break;
                                    }
                                    else
                                    {
                                        continue;
                                    }
                                }
                            }
                            break;
                        case "6"://Hire Purchase - Car Loan
                            for (int i = 0; i < _account.sub_accounts.Count(); i++)
                            {
                                original = string.IsNullOrEmpty(_account.limit.ToString()) ? "" : _account.limit.ToString();
                                outstanding = string.IsNullOrEmpty(_account.sub_accounts[i].cr_positions[0].balance.ToString()) ? "" : _account.sub_accounts[i].cr_positions[0].balance.ToString();
                                repayment = string.IsNullOrEmpty(_account.sub_accounts[i].cr_positions[0].inst_amount.ToString()) ? "" : _account.sub_accounts[i].cr_positions[0].inst_amount.ToString();

                                for (int j = 0; j < 5; j++)
                                {
                                    TextBox txtMainOriginalValMainHirePurchase = (TextBox)gvMainHirePurchase.Rows[j].Cells[0].FindControl("txtMainOriginalVal");
                                    TextBox txtMainOutstandinglValMainHirePurchase = (TextBox)gvMainHirePurchase.Rows[j].Cells[1].FindControl("txtMainOutstandinglVal");
                                    TextBox txtMainRepaymentlValMainHirePurchase = (TextBox)gvMainHirePurchase.Rows[j].Cells[2].FindControl("txtMainRepaymentlVal");
                                    if (String.IsNullOrEmpty(txtMainOriginalValMainHirePurchase.Text) && String.IsNullOrEmpty(txtMainOutstandinglValMainHirePurchase.Text) && String.IsNullOrEmpty(txtMainRepaymentlValMainHirePurchase.Text))
                                    {
                                        txtMainOriginalValMainHirePurchase.Text = original;
                                        txtMainOutstandinglValMainHirePurchase.Text = outstanding;
                                        txtMainRepaymentlValMainHirePurchase.Text = repayment;
                                        break;
                                    }
                                    else
                                    {
                                        continue;
                                    }
                                }
                            }
                            break;
                        case "3"://Personal Loan
                            for (int i = 0; i < _account.sub_accounts.Count(); i++)
                            {
                                original = string.IsNullOrEmpty(_account.limit.ToString()) ? "" : _account.limit.ToString();
                                outstanding = string.IsNullOrEmpty(_account.sub_accounts[i].cr_positions[0].balance.ToString()) ? "" : _account.sub_accounts[i].cr_positions[0].balance.ToString();
                                repayment = string.IsNullOrEmpty(_account.sub_accounts[i].cr_positions[0].inst_amount.ToString()) ? "" : _account.sub_accounts[i].cr_positions[0].inst_amount.ToString();

                                for (int j = 0; j < 5; j++)
                                {
                                    TextBox txtMainOriginalValMainPersonalLoan = (TextBox)gvMainPersonalLoan.Rows[j].Cells[0].FindControl("txtMainOriginalVal");
                                    TextBox txtMainOutstandinglValMainPersonalLoan = (TextBox)gvMainPersonalLoan.Rows[j].Cells[1].FindControl("txtMainOutstandinglVal");
                                    TextBox txtMainRepaymentlValMainPersonalLoan = (TextBox)gvMainPersonalLoan.Rows[j].Cells[2].FindControl("txtMainRepaymentlVal");
                                    if (String.IsNullOrEmpty(txtMainOriginalValMainPersonalLoan.Text) && String.IsNullOrEmpty(txtMainOutstandinglValMainPersonalLoan.Text) && String.IsNullOrEmpty(txtMainRepaymentlValMainPersonalLoan.Text))
                                    {
                                        txtMainOriginalValMainPersonalLoan.Text = original;
                                        txtMainOutstandinglValMainPersonalLoan.Text = outstanding;
                                        txtMainRepaymentlValMainPersonalLoan.Text = repayment;
                                        break;
                                    }
                                    else
                                    {
                                        continue;
                                    }
                                }
                            }
                            break;
                        case "2"://Commercial Loan
                            for (int i = 0; i < _account.sub_accounts.Count(); i++)
                            {
                                original = string.IsNullOrEmpty(_account.limit.ToString()) ? "" : _account.limit.ToString();
                                outstanding = string.IsNullOrEmpty(_account.sub_accounts[i].cr_positions[0].balance.ToString()) ? "" : _account.sub_accounts[i].cr_positions[0].balance.ToString();
                                repayment = string.IsNullOrEmpty(_account.sub_accounts[i].cr_positions[0].inst_amount.ToString()) ? "" : _account.sub_accounts[i].cr_positions[0].inst_amount.ToString();

                                for (int j = 0; j < 5; j++)
                                {
                                    TextBox txtMainOriginalValMainHousingLoan = (TextBox)gvMainHousingLoan.Rows[j].Cells[0].FindControl("txtMainOriginalVal");
                                    TextBox txtMainOutstandinglValMainHousingLoan = (TextBox)gvMainHousingLoan.Rows[j].Cells[1].FindControl("txtMainOutstandinglVal");
                                    TextBox txtMainRepaymentlValMainHousingLoan = (TextBox)gvMainHousingLoan.Rows[j].Cells[2].FindControl("txtMainRepaymentlVal");
                                    if (String.IsNullOrEmpty(txtMainOriginalValMainHousingLoan.Text) && String.IsNullOrEmpty(txtMainOutstandinglValMainHousingLoan.Text) && String.IsNullOrEmpty(txtMainRepaymentlValMainHousingLoan.Text))
                                    {
                                        txtMainOriginalValMainHousingLoan.Text = original;
                                        txtMainOutstandinglValMainHousingLoan.Text = outstanding;
                                        txtMainRepaymentlValMainHousingLoan.Text = repayment;
                                        break;
                                    }
                                    else
                                    {
                                        continue;
                                    }
                                }
                            }
                            break;
                        case "7"://Credit Card
                            for (int i = 0; i < _account.sub_accounts.Count(); i++)
                            {
                                outstanding = string.IsNullOrEmpty(_account.sub_accounts[i].cr_positions[0].balance.ToString()) ? "" : _account.sub_accounts[i].cr_positions[0].balance.ToString();
                                if (!String.IsNullOrEmpty(outstanding))
                                {
                                    if (ConvertHelper.ConvertToDecimal(outstanding, 0) > 0)
                                    {
                                        for (int j = 0; j < 10; j++)
                                        {

                                            TextBox txtMainOutstandinglValMainCreditCard = (TextBox)gvMainCreditCard.Rows[j].Cells[1].FindControl("txtMainOutstandinglVal");
                                            if (String.IsNullOrEmpty(txtMainOutstandinglValMainCreditCard.Text))
                                            {
                                                txtMainOutstandinglValMainCreditCard.Text = outstanding;
                                                break;
                                            }
                                            else
                                            {
                                                continue;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        continue;
                                    }
                                }
                                else
                                {
                                    continue;
                                }
                            }
                            break;
                        case "4"://OverDraft
                            for (int i = 0; i < _account.sub_accounts.Count(); i++)
                            {
                                original = string.IsNullOrEmpty(_account.limit.ToString()) ? "" : _account.limit.ToString();//check
                                outstanding = string.IsNullOrEmpty(_account.sub_accounts[i].cr_positions[0].balance.ToString()) ? "" : _account.sub_accounts[i].cr_positions[0].balance.ToString();
                                repayment = string.IsNullOrEmpty(_account.sub_accounts[i].cr_positions[0].inst_amount.ToString()) ? "" : _account.sub_accounts[i].cr_positions[0].inst_amount.ToString();
                                if ((capacityCode.Equals("Joint") || capacityCode.Equals("Partner")) && !string.IsNullOrWhiteSpace(repayment) && double.TryParse(repayment, out double parseRepayment))
                                    repayment = (parseRepayment / 2).ToString();

                                for (int j = 0; j < 5; j++)
                                {

                                    TextBox txtMainOriginalValMainOD = (TextBox)gvMainOD.Rows[j].Cells[0].FindControl("txtMainOriginalVal");
                                    TextBox txtMainOutstandinglValMainOD = (TextBox)gvMainOD.Rows[j].Cells[1].FindControl("txtMainOutstandinglVal");
                                    TextBox txtMainRepaymentlValMainOD = (TextBox)gvMainOD.Rows[j].Cells[2].FindControl("txtMainRepaymentlVal");
                                    if (String.IsNullOrEmpty(txtMainOriginalValMainOD.Text) && String.IsNullOrEmpty(txtMainOutstandinglValMainOD.Text) && String.IsNullOrEmpty(txtMainRepaymentlValMainOD.Text))
                                    {
                                        txtMainOriginalValMainOD.Text = original;
                                        txtMainOutstandinglValMainOD.Text = outstanding;
                                        txtMainRepaymentlValMainOD.Text = repayment;
                                        break;
                                    }
                                    else
                                    {
                                        continue;
                                    }
                                }
                            }
                            break;
                        case "5"://X-Product
                            for (int i = 0; i < _account.sub_accounts.Count(); i++)
                            {

                                original = string.IsNullOrEmpty(_account.limit.ToString()) ? "" : _account.limit.ToString();
                                outstanding = string.IsNullOrEmpty(_account.sub_accounts[i].cr_positions[0].balance.ToString()) ? "" : _account.sub_accounts[i].cr_positions[0].balance.ToString();
                                repayment = string.IsNullOrEmpty(_account.sub_accounts[i].cr_positions[0].inst_amount.ToString()) ? "" : _account.sub_accounts[i].cr_positions[0].inst_amount.ToString();
                                if ((capacityCode.Equals("Joint") || capacityCode.Equals("Partner")) && !string.IsNullOrWhiteSpace(repayment) && double.TryParse(repayment, out double parseRepayment))
                                    repayment = (parseRepayment / 2).ToString();

                                for (int j = 0; j < 10; j++)
                                {

                                    TextBox txtMainOriginalValMainXProduct = (TextBox)gvMainXProduct.Rows[j].Cells[0].FindControl("txtMainOriginalVal");
                                    TextBox txtMainOutstandinglValMainXProduct = (TextBox)gvMainXProduct.Rows[j].Cells[1].FindControl("txtMainOutstandinglVal");
                                    TextBox txtMainRepaymentlValMainXProduct = (TextBox)gvMainXProduct.Rows[j].Cells[2].FindControl("txtMainRepaymentlVal");
                                    if (String.IsNullOrEmpty(txtMainOriginalValMainXProduct.Text) && String.IsNullOrEmpty(txtMainOutstandinglValMainXProduct.Text) && String.IsNullOrEmpty(txtMainRepaymentlValMainXProduct.Text))
                                    {
                                        txtMainOriginalValMainXProduct.Text = original;
                                        txtMainOutstandinglValMainXProduct.Text = outstanding;
                                        txtMainRepaymentlValMainXProduct.Text = repayment;
                                        break;
                                    }
                                    else
                                    {
                                        continue;
                                    }
                                }
                            }
                            break;
                        default:
                            //sqlString.displayAlert2(this, "Don't have any loan");
                            break;
                    }
                }
                else
                {
                    loanid = "";
                }
            }

            db.Close();
        } //4.1.2
        private void ClearGVTextBox(GridView gridviewname, bool isMain = true)
        {
            if (isMain)
            {
                foreach (GridViewRow gvrow in gridviewname.Rows)
                {
                    ((TextBox)gvrow.Cells[0].FindControl("txtMainOriginalVal")).Text = "";
                    ((TextBox)gvrow.Cells[1].FindControl("txtMainOutstandinglVal")).Text = "";
                    ((TextBox)gvrow.Cells[2].FindControl("txtMainRepaymentlVal")).Text = "";
                }
            }
        } //4.1.2.1
        private void InsertCTOSReport(string resultId, List<ccris_account_detail> accounts)
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            int RowCount = 0;
            int SortOrder = 0;

            foreach (var _account in accounts)
            {
                RowCount += 1;
                SortOrder += 1;
                int colcount = 0;

                StringBuilder sqlinsert = new StringBuilder();
                StringBuilder sqlvalues = new StringBuilder();
                sql.Clear();
                sqlinsert.Clear();
                sqlvalues.Clear();
                sqlinsert.Append(" INSERT INTO tbl_CTOS_Report_Loan_Details ");
                sqlinsert.Append(" (CTOS_Result_Id,No,Date_R_R,Capacity,Lender_Type,Limit_Installment_Amount,LGL_STS,Date_Status_Updated,SortOrder");
                sqlvalues.Append(" VALUES(N'" + secure.RC(resultId) + "','" + RowCount.ToString() + "','" +
                    _account.approval_date.ToString() + "','" + secure.RC(_account.capacity.code) + "','" +
                    secure.RC(_account.lender_type.code) + "','" + _account.limit + "','" + _account.legal.status + "','" +
                    _account.legal.date + "'," + SortOrder);

                if (_account.collaterals.Count() > 0)
                {
                    foreach (var collaterals in _account.collaterals)
                    {
                        if (colcount == 0)
                        {
                            sqlinsert.Append(",Col_Type");
                            sqlvalues.Append(",'" + collaterals.code + "'");
                            sqlinsert.Append(")");
                            sqlvalues.Append(")");
                            sql.Append(sqlinsert);
                            sql.Append(sqlvalues);
                            db.Execute(sql.ToString());
                            SortOrder += 1;
                            colcount += 1;
                        }
                        else
                        {
                            sql.Clear();
                            sql.Append(" INSERT INTO tbl_CTOS_Report_Loan_Details ");
                            sql.Append(" (CTOS_Result_Id,Col_Type,SortOrder)");
                            sql.Append(" VALUES(N'" + secure.RC(resultId) + "',N'" + secure.RC(collaterals.code) + "'," + SortOrder + ")");
                            db.Execute(sql.ToString());
                            SortOrder += 1;
                            colcount += 1;
                        }
                    }
                }
                else
                {
                    sqlinsert.Append(")");
                    sqlvalues.Append(")");
                    sql.Append(sqlinsert);
                    sql.Append(sqlvalues);
                    db.Execute(sql.ToString());
                    SortOrder += 1;
                    colcount += 1;
                }


                foreach (var subaccount in _account.sub_accounts)
                {
                    var firstcr_position = subaccount.cr_positions.FirstOrDefault();
                    sql.Clear();
                    sqlinsert.Clear();
                    sqlvalues.Clear();
                    sqlinsert.Append(" INSERT INTO tbl_CTOS_Report_Loan_Details ");
                    sqlinsert.Append("(CTOS_Result_Id,Sts,Facility,Total_OutStanding_Balance,Date_Balance_Updated,Limit_Installment_Amount,Prin_Repmt_Term,SortOrder");
                    sqlvalues.Append("VALUES(N'" + secure.RC(resultId) + "',N'" + secure.RC(firstcr_position.status.code) + "','" + secure.RC(subaccount.facility.code) + "','" + firstcr_position.balance + "','" + firstcr_position.position_date + "','" + firstcr_position.inst_amount + "','" + secure.RC(subaccount.repay_term.code) + "'," + SortOrder);

                    if (!(string.IsNullOrEmpty(firstcr_position.status.code) || string.IsNullOrWhiteSpace(firstcr_position.status.code)))
                        checkAKPK(secure.RC(firstcr_position.status.code));

                    //sql.Append("INSERT INTO tbl_TApplicantCreditAdvisor(requestid , advisorid ,comment) VALUES('" + hfRequestID.Value + "','0','");
                    //sql.Append(Environment.NewLine + Environment.NewLine + "');");
                    //db.Execute(sql.ToString());
                    //sql.Clear();

                    Dictionary<string, Dictionary<string, string>> last12Month = new Dictionary<string, Dictionary<string, string>>();

                    for (int i = 0; i < 12; i++)
                    {
                        DateTime date = DateTime.Now.AddMonths(-i);
                        last12Month.Add(date.ToString("yyyyMM"), new Dictionary<string, string>());
                    }

                    foreach (var positions in subaccount.cr_positions)
                    {

                        DateTime date = DateTime.ParseExact(positions.position_date.ToString(), "dd-MM-yyyy", null);
                        Dictionary<string, string> val = new Dictionary<string, string>();
                        val.Add(positions.inst_arrears, positions.position_date);
                        last12Month[date.ToString("yyyyMM")] = val;
                    }

                    int mon = 1;
                    foreach (var item in last12Month)
                    {
                        if (item.Value.Count > 0)
                        {
                            var val = item.Value.FirstOrDefault();
                            switch (mon)
                            {
                                case 1:
                                    sqlinsert.Append(",M12_Count,M12_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 2:
                                    sqlinsert.Append(",M11_Count,M11_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 3:
                                    sqlinsert.Append(",M10_Count,M10_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 4:
                                    sqlinsert.Append(",M9_Count,M9_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 5:
                                    sqlinsert.Append(",M8_Count,M8_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 6:
                                    sqlinsert.Append(",M7_Count,M7_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 7:
                                    sqlinsert.Append(",M6_Count,M6_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 8:
                                    sqlinsert.Append(",M5_Count,M5_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 9:
                                    sqlinsert.Append(",M4_Count,M4_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 10:
                                    sqlinsert.Append(",M3_Count,M3_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 11:
                                    sqlinsert.Append(",M2_Count,M2_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 12:
                                    sqlinsert.Append(",M1_Count,M1_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                            }
                        }

                        mon += 1;
                    }

                    sqlinsert.Append(")");
                    sqlvalues.Append(")");
                    sql.Append(sqlinsert);
                    sql.Append(sqlvalues);

                    db.Execute(sql.ToString());
                    SortOrder += 1;
                    colcount += 1;
                }
            }
        } //4.1.2.2
        private void InsertCTOSReport(string resultId, List<special_attention_acc_detail> accounts)
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            int RowCount = 0;
            int SortOrder = 0;

            db.OpenTable("SELECT distinct sortorder FROM tbl_CTOS_Report_Loan_Details WITH(NOLOCK) WHERE sortorder = (SELECT " +
                "MAX(cast(sortorder as int)) FROM tbl_CTOS_Report_Loan_Details WITH(NOLOCK) WHERE CTOS_Result_Id = '" + resultId + "')");
            if (db.RecordCount() > 0)
                SortOrder = ConvertHelper.ConvertToInt(db.Item("sortorder"), 0);
            foreach (var _account in accounts)//_account = special_attention_acc
            {
                RowCount += 1;
                SortOrder += 1;
                int colcount = 0;

                StringBuilder sqlinsert = new StringBuilder();
                StringBuilder sqlvalues = new StringBuilder();
                sql.Clear();
                sqlinsert.Clear();
                sqlvalues.Clear();
                sqlinsert.Append(" INSERT INTO tbl_CTOS_Report_Loan_Details ");
                sqlinsert.Append(" (CTOS_Result_Id,No,Date_R_R,Capacity,Lender_Type,Limit_Installment_Amount,LGL_STS,Date_Status_Updated,SortOrder");
                sqlvalues.Append(" VALUES(N'" + secure.RC(resultId) + "','" + RowCount.ToString() + "','" +
                    _account.approval_date.ToString() + "','" + secure.RC(_account.capacity.code) + "','" +
                    secure.RC(_account.lender_type.code) + "','" + _account.limit + "','" + _account.legal.status + "','" +
                    _account.legal.date + "'," + SortOrder);

                if (_account.collaterals.Count() > 0)
                {
                    foreach (var collaterals in _account.collaterals)
                    {
                        if (colcount == 0)
                        {
                            sqlinsert.Append(",Col_Type");
                            sqlvalues.Append(",'" + collaterals.code + "'");
                            sqlinsert.Append(")");
                            sqlvalues.Append(")");
                            sql.Append(sqlinsert);
                            sql.Append(sqlvalues);
                            db.Execute(sql.ToString());
                            SortOrder += 1;
                            colcount += 1;
                        }
                        else
                        {
                            sql.Clear();
                            sql.Append(" INSERT INTO tbl_CTOS_Report_Loan_Details ");
                            sql.Append(" (CTOS_Result_Id,Col_Type,SortOrder)");
                            sql.Append(" VALUES(N'" + secure.RC(resultId) + "',N'" + secure.RC(collaterals.code) + "'," + SortOrder + ")");
                            db.Execute(sql.ToString());
                            SortOrder += 1;
                            colcount += 1;
                        }
                    }
                }
                else
                {
                    sqlinsert.Append(")");
                    sqlvalues.Append(")");
                    sql.Append(sqlinsert);
                    sql.Append(sqlvalues);
                    db.Execute(sql.ToString());
                    SortOrder += 1;
                    colcount += 1;
                }

                foreach (var subaccount in _account.sub_accounts)
                {
                    var firstcr_position = subaccount.cr_positions.FirstOrDefault();
                    sql.Clear();
                    sqlinsert.Clear();
                    sqlvalues.Clear();
                    sqlinsert.Append(" INSERT INTO tbl_CTOS_Report_Loan_Details ");
                    sqlinsert.Append("(CTOS_Result_Id,Sts,Facility,Total_OutStanding_Balance,Date_Balance_Updated,Limit_Installment_Amount,Prin_Repmt_Term,SortOrder");
                    sqlvalues.Append("VALUES(N'" + secure.RC(resultId) + "',N'" + secure.RC(firstcr_position.status.code) + "','" + secure.RC(subaccount.facility.code) + "','" + firstcr_position.balance + "','" + firstcr_position.position_date + "','" + firstcr_position.inst_amount + "','" + secure.RC(subaccount.repay_term.code) + "'," + SortOrder);

                    if (!(string.IsNullOrEmpty(firstcr_position.status.code) || string.IsNullOrWhiteSpace(firstcr_position.status.code)))
                        checkAKPK(secure.RC(firstcr_position.status.code));

                    Dictionary<string, Dictionary<string, string>> last12Month = new Dictionary<string, Dictionary<string, string>>();

                    for (int i = 0; i < 12; i++)
                    {
                        DateTime date = DateTime.Now.AddMonths(-i);
                        last12Month.Add(date.ToString("yyyyMM"), new Dictionary<string, string>());
                    }

                    foreach (var positions in subaccount.cr_positions)
                    {

                        DateTime date = DateTime.ParseExact(positions.position_date.ToString(), "dd-MM-yyyy", null);
                        Dictionary<string, string> val = new Dictionary<string, string>();
                        val.Add(positions.inst_arrears, positions.position_date);
                        last12Month[date.ToString("yyyyMM")] = val;
                    }

                    int mon = 1;
                    foreach (var item in last12Month)
                    {
                        if (item.Value.Count > 0)
                        {
                            var val = item.Value.FirstOrDefault();
                            switch (mon)
                            {
                                case 1:
                                    sqlinsert.Append(",M12_Count,M12_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 2:
                                    sqlinsert.Append(",M11_Count,M11_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 3:
                                    sqlinsert.Append(",M10_Count,M10_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 4:
                                    sqlinsert.Append(",M9_Count,M9_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 5:
                                    sqlinsert.Append(",M8_Count,M8_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 6:
                                    sqlinsert.Append(",M7_Count,M7_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 7:
                                    sqlinsert.Append(",M6_Count,M6_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 8:
                                    sqlinsert.Append(",M5_Count,M5_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 9:
                                    sqlinsert.Append(",M4_Count,M4_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 10:
                                    sqlinsert.Append(",M3_Count,M3_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 11:
                                    sqlinsert.Append(",M2_Count,M2_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 12:
                                    sqlinsert.Append(",M1_Count,M1_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                            }
                        }

                        mon += 1;
                    }

                    sqlinsert.Append(")");
                    sqlvalues.Append(")");
                    sql.Append(sqlinsert);
                    sql.Append(sqlvalues);

                    db.Execute(sql.ToString());
                    SortOrder += 1;
                    colcount += 1;
                }
            }
        } //4.1.2.3
        private void InsertDataIntoLoanDetail(GridView gridviewname, string requestid, string bankid,
            int gridviewtype, int noofRows, bool isMain = true)
        {
            StringBuilder sql = new StringBuilder();
            wwdb db = new wwdb();
            try
            {
                //Main Grid view for Housing Loan
                for (int i = 0; i < noofRows; i++)
                {
                    TextBox txtoriginal;
                    TextBox txtoutstanding;
                    TextBox txtrepayment;
                    txtoriginal = (TextBox)gridviewname.Rows[i].Cells[0].FindControl("txtMainOriginalVal");
                    txtoutstanding = (TextBox)gridviewname.Rows[i].Cells[1].FindControl("txtMainOutstandinglVal");
                    txtrepayment = (TextBox)gridviewname.Rows[i].Cells[2].FindControl("txtMainRepaymentlVal");

                    decimal original = 0;
                    decimal outstanding = 0;
                    decimal repayment = 0;
                    int rowID = i + 1;

                    if (txtoriginal.Text != String.Empty || txtoutstanding.Text != String.Empty || txtrepayment.Text != String.Empty)
                    {
                        if (txtoriginal.Text != String.Empty)
                        {
                            original = ConvertHelper.ConvertToDecimal(txtoriginal.Text, 0);
                        }
                        else
                        {
                            original = 0;
                        }

                        if (txtoutstanding.Text != String.Empty)
                        {
                            outstanding = ConvertHelper.ConvertToDecimal(txtoutstanding.Text, 0);
                        }
                        else
                        {
                            outstanding = 0;
                        }

                        if (txtrepayment.Text != String.Empty && ConvertHelper.ConvertToDecimal(txtrepayment.Text, 0) > 0)
                        {
                            repayment = ConvertHelper.ConvertToDecimal(txtrepayment.Text, 0);
                        }
                        else
                        {
                            //Overdraft
                            if (gridviewtype == 4)
                                if (txtoriginal.Text != String.Empty && original != 0)
                                    repayment = RepaymentFlat(ConvertHelper.ConvertToInt(bankid.ToString(), 0), original, gridviewtype);

                            if (txtoutstanding.Text != String.Empty && outstanding != 0)
                            {
                                //Hire Purchase, PLoan
                                if (gridviewtype == 6 || gridviewtype == 3)
                                {
                                    repayment = RepaymentFlat(ConvertHelper.ConvertToInt(bankid.ToString(), 0), outstanding, gridviewtype);
                                }
                                // Credit Card
                                else if (gridviewtype == 7)
                                {
                                    repayment = RepaymentMin(ConvertHelper.ConvertToInt(bankid.ToString(), 0), outstanding);
                                }
                                //Any other (except OD)
                                else if (gridviewtype != 4)
                                {
                                    repayment = RepaymentEffective(ConvertHelper.ConvertToInt(bankid, 0), outstanding, gridviewtype);
                                }
                            }
                        }
                    }

                    if (txtoriginal.Text != String.Empty || txtoutstanding.Text != String.Empty || txtrepayment.Text != String.Empty)
                    {
                        sql.Clear();
                        sql.Append(" INSERT INTO tbl_TLoanDetail ");
                        sql.Append("(RequestID, BankID, LoanID, RowID, OriginalValue, OutStandingValue, Repayment, isConstant, isMain, CreatedBy, CreatedAt, UpdatedBy, UpdatedAt)");
                        sql.Append(" VALUES( '" + secure.RC(requestid) + "', '" + secure.RC(bankid) + "', '" + gridviewtype + "', '" + secure.RC(rowID.ToString()) + "', ");
                        sql.Append(" '" + original + "', '" + outstanding + "', '" + repayment + "', 'true', '" + secure.RC(isMain.ToString()) + "', ");
                        sql.Append(" '" + secure.RC(login_user.UserId) + "', GETDATE(), '" + secure.RC(login_user.UserId) + "', GETDATE() ); ");
                        db.Execute(sql.ToString());
                        LogUtil.logAction(sql.ToString(), "Insert Data into tbl_LoanDetails - RequestID :" + requestid.ToString());
                    }
                }
            }

            catch (Exception ex)
            {
                LogUtil.logError(ex.ToString(), sql.ToString());
                sqlString.displayAlert(this, KeyVal.UnknownError);
            }
            finally { db.Close(); }
        } //4.2
        private void InsertDataIntoLoanInfo(string requestid, string bankid, bool isMainOnly = true)
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();
            decimal policy = 0;
            decimal dsr = 0;
            decimal loanAmountEligible = 0;
            decimal incomeRequired = 0;
            decimal declareIncome = 0;
            decimal income = 0;
            decimal oldRepayment = 0;
            decimal newRepayment = 0;
            string projectid = this.projectID;
            decimal tenure = 0;
            decimal interest = 0;
            decimal loanAmount = 0;
            decimal margin = 0;
            decimal policyIncome = 0;
            decimal youngerAges = 0;
            margin = PassBankMargin(bankid, projectid, requestid);

            wwdb tempdb = new wwdb();
            tempdb.OpenTable("SELECT Tenure, Interest FROM tbl_TLoanLegalAction WHERE RequestID = N'" + requestid + "'");
            tenure = ConvertHelper.ConvertToDecimal(tempdb.Item("Tenure"), 0);
            interest = ConvertHelper.ConvertToDecimal(tempdb.Item("Interest"), 0);
            newRepayment = CalculateNewLoanRepayment(bankid, projectid, requestid, tenure, interest);
            oldRepayment = CalculateTotalOldBankRepayments(bankid, requestid, isMainOnly);
            income = CalculateTotalIncome(isMainOnly);
            policyIncome = CalculateGrossIncomePolicy();
            youngerAges = getAgeFromIC();

            if (this.loanAmount.ToString() != String.Empty)
            {
                if (ConvertHelper.ConvertToDecimal(this.loanAmount, 0) > 0)
                {
                    loanAmount = ConvertHelper.ConvertToDecimal(this.loanAmount, 0);
                }
                else
                {
                    if (txtPrice.Text != String.Empty)
                    {
                        if (ConvertHelper.ConvertToDecimal(txtPrice.Text, 0) > 0)
                        {
                            loanAmount = ConvertHelper.ConvertToDecimal(txtPrice.Text, 0) * margin / 100;
                        }
                        else
                        {
                            loanAmount = 0;
                        }
                    }
                    else
                    {
                        loanAmount = 0;
                    }
                }
            }
            else
            {
                if (txtPrice.Text != String.Empty)
                {
                    if (ConvertHelper.ConvertToDecimal(txtPrice.Text, 0) > 0)
                    {
                        loanAmount = ConvertHelper.ConvertToDecimal(txtPrice.Text, 0) * margin / 100;
                    }
                    else
                    {
                        loanAmount = 0;
                    }
                }
                else
                {
                    loanAmount = 0;
                }
            }

            policy = calculatorController.getBankPolicy(bankid, policyIncome, isMainOnly);
            dsr = calculatorController.calculateDSR(oldRepayment, newRepayment, income);

            //loanAmountEligible = calculatorController.calculateLoanEligible(policy, oldRepayment, youngerAges, income, tenure);

            //newLoanRepayment = calculatorController.calculateNewRepayment(loanAmount, tenure, interest);

            incomeRequired = calculatorController.calculateIncomeRequired(policy, oldRepayment, newRepayment);

            declareIncome = income;

            try
            {
                sql.Clear();
                sql.Append(" INSERT INTO tbl_TLoanInfo ");
                sql.Append(" (RequestID, BankID, PreferRatio, DSR, ActualLoanAmount, LoanAmountEligible, NewLoanRepayment, " +
                    "IncomeRequired, DeclareIncome, CreatedBy, CreatedAt, UpdatedBy, UpdatedAt) ");
                sql.Append(" VALUES( N'" + secure.RC(requestid) + "', N'" + secure.RC(bankid) + "', '" + policy + "', '" + dsr + "', '" +
                    loanAmount.ToString().Replace(",", "") + "', '" + loanAmountEligible.ToString().Replace(",", "") + "', ");
                sql.Append(" '" + newRepayment.ToString().Replace(",", "") + "', '" + incomeRequired.ToString().Replace(",", "") +
                    "', '" + declareIncome.ToString().Replace(",", "") + "',  ");
                sql.Append(" '" + secure.RC(login_user.UserId) + "', GETDATE(), '" + secure.RC(login_user.UserId) + "', GETDATE() ); ");
                db.Execute(sql.ToString());
                LogUtil.logAction(sql.ToString(), "Insert Data into tbl_LoanInfo - RequestID :" + requestid.ToString());
            }
            catch (Exception ex)
            {
                LogUtil.logError(ex.ToString(), sql.ToString());
                sqlString.displayAlert(this, KeyVal.UnknownError);
            }
            finally { db.Close(); }
        } //4.3
        #endregion
    }
}