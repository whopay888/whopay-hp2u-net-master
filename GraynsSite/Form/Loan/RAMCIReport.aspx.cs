﻿using System;
using System.Text;
using Synergy.Util;
using Synergy.Helper;
using System.IO;
using System.Xml.Xsl;
using System.Xml;

namespace HJT.Form.Loan
{
    public partial class RAMCIReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request["RID"] as string) &&
                    !string.IsNullOrEmpty(Request["TYPE"] as string))
                {
                    string requestID = sqlString.decryptURL(Validation.GetUrlParameter(this.Page, "RID", ""));
                    string type = sqlString.decryptURL(Validation.GetUrlParameter(this.Page, "TYPE", ""));
                    string tempID = sqlString.decryptURL(Validation.GetUrlParameter(this.Page, "TID", ""));

                    wwdb db = new wwdb();
                    StringBuilder sql = new StringBuilder();

                    DateTime requestDate = DateTime.Now;
                    double expirationDay = 0;
                    sql.Clear();
                    sql.AppendFormat(@"
                        select top 1 p.ParameterValue as 'Day' from tbl_Parameter p with (nolock) 
                        where p.Category = 'ReportExpiration' and p.ParameterName = 'ReportExpirationDay'
                    ");
                    db.OpenTable(sql.ToString());
                    if (db.RecordCount() > 0)
                    {
                        expirationDay = double.Parse(db.Item("Day"));
                        sql.Clear();
                        sql.AppendFormat(@"
                            select r.RequestAt from tbl_Request r with (nolock) 
                            where r.RequestID = N'{0}'
                        ", requestID);
                        db.OpenTable(sql.ToString());
                        if (db.RecordCount() > 0)
                            requestDate = ConvertHelper.ConvertToDateTime(db.Item("RequestAt"), DateTime.Now);
                    }

                    if (DateTime.Now < requestDate.AddDays(expirationDay))
                    {
                        if (!string.IsNullOrWhiteSpace(requestID) && !string.IsNullOrWhiteSpace(type))
                        {
                            string xmlString = GetReportXml(requestID, type, tempID);

                            XmlReader xmlReader = XmlReader.Create(new StringReader(xmlString));
                            XsltSettings settings = new XsltSettings(true, false);
                            var myXslTrans = new XslCompiledTransform();
                            myXslTrans.Load(Server.MapPath(@"..\..\RAMCI\iriss.xsl"), settings, new XmlUrlResolver());

                            StringBuilder builder = new StringBuilder();
                            using (StringWriter stringWriter = new StringWriter(builder))
                            {
                                using (XmlTextWriter htmlTextWriter = new XmlTextWriter(stringWriter))
                                {
                                    myXslTrans.Transform(xmlReader, htmlTextWriter);
                                }
                            }
                            string transformedHtml = builder.ToString();

                            Response.Write(transformedHtml);
                        }
                    }
                    else
                        divReportExpiration.Visible = true;
                }
                else
                    divErrorReport.Visible = true;
            }
        }

        private string GetReportXml(string requestId, string type, string tempId)
        {
            string xml = string.Empty;
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();
            sql.Clear();
            sql.AppendFormat(@" 
                SELECT Result
                FROM tbl_CTOS_Result WITH(NOLOCK)
                WHERE IsActive = 1
                {0}
                AND IsCoApplicant = N'{1}' Order By ID
            ", (!string.IsNullOrWhiteSpace(requestId) && requestId != "0") ?
                string.Format(@"AND Request_Id = N'{0}'", secure.RC(requestId)) :
                string.Format(@"AND TempId = N'{0}'", tempId)
            , secure.RC(type));
            db.OpenTable(sql.ToString());

            if (db.RecordCount() > 0)
                xml = db.Item("Result").ToString();

            return xml;
        }

    }
}