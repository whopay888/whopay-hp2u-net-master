﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Global1.Master" CodeBehind="Calculator.aspx.cs" Inherits="HJT.Form.Loan.Calculator" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>


        function setTab() {
            if ($("#HidTab").val() == '' || $("#HidTab").val() === undefined) {
                $("#HidTab").val("#tab_1");
            }
            //[data-toggle='tab'] 

            $("a[data-toggle='tab'] ");

            $("a[href='" + $("#HidTab").val() + "' ]").click();
        }
        // window.onload = setTab();

        function FileUpload() {
            $('#<%= btnUpload.ClientID %>').click();
        }

        function pageLoad() {

            $("a[data-toggle='tab']").each(
                function (index) {
                    $(this).on("click", function () {
                        $("#HidTab").val($(this).attr("href"));

                    });
                }
            )
            setTab();
        }

        function confirmMainCCRIS(ccrisType) {
            var x = confirm('<%= GetGlobalResourceObject("resource", "CCRIS_Validation")%>');
            if (x) {
                document.getElementById('<%= hfMainCCRIS.ClientID %>').value = "true";
            }
            else {
                document.getElementById('<%= hfMainCCRIS.ClientID %>').value = "false";
            }

            if (ccrisType == 'CTOS') {
                document.getElementById('<%= btnMainCTOSCCRIS.ClientID %>').click();
            }
            else if (ccrisType == 'RAMCI') {
                document.getElementById('<%= btnMainRAMCICCRIS.ClientID %>').click();
            }
        }

        function confirmCoCCRIS(ccrisType) {
            var x = confirm('<%= GetGlobalResourceObject("resource", "CCRIS_Validation")%>');
            if (x) {
                document.getElementById('<%= hfCoCCRIS.ClientID %>').value = "true";
            }
            else {
                document.getElementById('<%= hfCoCCRIS.ClientID %>').value = "false";
            }

            if (ccrisType == 'CTOS') {
                document.getElementById('<%= btnCoCTOSCCRIS.ClientID %>').click();
            }
            else if (ccrisType == 'RAMCI') {
                document.getElementById('<%= btnCoRAMCICCRIS.ClientID %>').click();
            }
        }

    </script>
    <style>
        .nav-tabs-custom > .tab-content {
            display: inline-block;
            width: 100%;
        }

        @media (max-width:767px) {
            td div.col-sm-6 {
                padding: 0px !important;
            }
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <aside class="right-side">
        <section class="content-header">
            <h1>
                <span class="icon fa fa-fw fa-book" aria-hidden="true"></span>
                <asp:Label ID="lblPageTitle" runat="server"><i class="icon-th-list"></i><span runat="server" id="spHeader"></span></asp:Label>
            </h1>
            <asp:SiteMapPath ID="SiteMapPath1" CssClass="breadcrumb" runat="server"></asp:SiteMapPath>
        </section>
        <section class="content">
            <div class="col-md-12">
                <div runat="server">

                    <div class="box">
                        <h2>
                            <strong><%= GetGlobalResourceObject("resource", "Data_Analyser")%></strong>
                        </h2>
                        <div class="box-body">

                            <div class="col-md-12 no-padding">
                                <!-- Custom Tabs -->
                                <div class="nav-tabs-custom">
                                    <ul class="nav nav-tabs">
                                        <li class=""><a href="#tab_1" id="aTab1" data-toggle="tab" aria-expanded="false"><%= GetGlobalResourceObject("resource", "Applicant_Information") %></a></li>
                                        <li class=""><a href="#tab_2" id="aTab2" data-toggle="tab" aria-expanded="false"><%= GetGlobalResourceObject("resource", "Loan_Bank_Information") %></a></li>
                                        <li class=""><a href="#tab_3" id="aTab3" data-toggle="tab" aria-expanded="false"><%= GetGlobalResourceObject("resource", "Loan_Details") %></a></li>
                                        <asp:HiddenField runat="server" ID="HidTab" ClientIDMode="Static" />

                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane" id="tab_1">
                                            <div class="box box-primary">
                                                <asp:Panel runat="server" DefaultButton="btnNext">
                                                    <div class="box-header">
                                                        <h4><asp:Label ID="lblTitle" runat="server" /></h4>
                                                    </div>
                                                    <div class="box-body">
                                                        <table class="table table-bordered row">
                                                            <tbody>
                                                                <tr style="background: #dec18c;">
                                                                    <td colspan="2">
                                                                        <h4><strong><%= GetGlobalResourceObject("resource", "Request_Information")%></strong></h4>
                                                                    </td>
                                                                </tr>
                                                                <tr class="form-group" runat="server" id="trRequestName">
                                                                    <td style="width: 20%; border-left: 0px!important;">
                                                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Request_Name")%>&nbsp;<span style="color: Red;">*</span></label>
                                                                    </td>
                                                                    <td style="width: 80%;" class="controls">
                                                                        <%--<asp:Label runat="server" ID="lblRequestName" CssClass="form-control" Visible="false"></asp:Label>--%>
                                                                        <asp:DropDownList runat="server" ID="ddlMemberID" CssClass="form-control"></asp:DropDownList>
                                                                        <asp:HiddenField ID="hfRequestID" runat="server" Value="0" />
                                                                    </td>
                                                                </tr>
                                                                <tr class="form-group" runat="server" id="trRequestReport" visible="false">
                                                                    <td style="border-left: 0px!important;">
                                                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Request_Report_Type")%></label>
                                                                    </td>
                                                                    <td style="" class="controls">
                                                                        <asp:RadioButtonList ID="rblReport" runat="server" RepeatDirection="Horizontal">
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                </tr>
                                                                <tr style="background: #dec18c;">
                                                                    <td colspan="2">
                                                                        <h4><strong><%= GetGlobalResourceObject("resource", "Main_Applicant")%></strong></h4>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trMainApplicant" runat="server">
                                                                    <td colspan="2">
                                                                        <table class="table table-bordered">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td colspan="2" style="border-left: none !important;">
                                                                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Fill_In_The_Form")%></label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="form-group" runat="server" id="trMainName">
                                                                                    <td style="width: 20%; border-left: 0px!important;">
                                                                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Name")%>&nbsp;<span style="color: Red;">*</span></label>
                                                                                    </td>
                                                                                    <td style="width: 80%;" class="controls">
                                                                                        <asp:TextBox ID="txtMainName" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="form-group" runat="server" id="trMainIC">
                                                                                    <td style="width: 20%; border-left: 0px!important;">
                                                                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "IC")%>&nbsp;<span style="color: Red;">*</span></label>
                                                                                    </td>
                                                                                    <td style="width: 80%;" class="controls">
                                                                                        <table style="width: 100%; margin: 0px; padding: 0px; border-spacing: 0px">
                                                                                            <tr>
                                                                                                <td style="width: 90%; padding: 0px">
                                                                                                    <asp:TextBox ID="txtMainIC" runat="server" CssClass="form-control" AutoPostBack="true" OnTextChanged="txtMainIC_TextChanged"></asp:TextBox>
                                                                                                </td>
                                                                                                <td style="width: 10%;">
                                                                                                    <asp:Button ID="btnMainCTOSCCRIS" Text="<%$ Resources:Resource, CTOS%>" CssClass="btn btn-next" runat="server" OnClick="btnMainCTOSCCRIS_Click" />
                                                                                                    <asp:Button ID="btnMainRAMCICCRIS" Text="<%$ Resources:Resource, RAMCI%>" CssClass="btn btn-next" runat="server" OnClick="btnMainRAMCICCRIS_Click" />
                                                                                                    <asp:HiddenField ID="hfMainCCRIS" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="form-group" runat="server" id="trMainAge">
                                                                                    <td style="width: 20%; border-left: 0px!important;">
                                                                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Age")%>&nbsp;<span style="color: Red;"></span></label>
                                                                                    </td>
                                                                                    <td style="width: 80%;" class="controls">
                                                                                        <%--<asp:TextBox ID="txtMainAge" runat="server" CssClass="form-control"></asp:TextBox>--%>
                                                                                        <asp:Label runat="server" ID="lblMainAge"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="form-group" runat="server" id="trMainArea">
                                                                                    <td style="width: 20%; border-left: 0px!important;">
                                                                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Area")%>&nbsp;<span style="color: Red;"></span></label>
                                                                                    </td>
                                                                                    <td style="width: 80%;" class="controls">
                                                                                        <asp:DropDownList runat="server" ID="ddlMainArea"></asp:DropDownList>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="form-group" runat="server" id="trMainContact">
                                                                                    <td style="width: 20%; border-left: 0px!important;">
                                                                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Contact_Number")%>&nbsp;<span style="color: Red;"></span></label>
                                                                                    </td>
                                                                                    <td style="width: 80%;" class="controls">
                                                                                        <asp:TextBox ID="txtMainContact" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="form-group" runat="server" id="trMainNetIncome">
                                                                                    <td style="width: 20%; border-left: 0px!important;">
                                                                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Net_Income")%>&nbsp;<span style="color: Red;"></span></label>
                                                                                    </td>
                                                                                    <td style="width: 80%;" class="controls">
                                                                                        <asp:TextBox ID="txtMainNetIncome" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="form-group" runat="server" id="trMainOtherIncome" style="display:none">
                                                                                    <td style="width: 20%; border-left: 0px!important;">
                                                                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Other_Income")%>&nbsp;<span style="color: Red;"></span></label>
                                                                                    </td>
                                                                                    <td style="width: 80%;" class="controls">
                                                                                        <asp:TextBox ID="txtMainOtherIncome" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="form-group" runat="server" id="trMainGrossIncome" style="display:none">
                                                                                    <td style="width: 20%; border-left: 0px!important;">
                                                                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Gross_Income")%>&nbsp;<span style="color: Red;"></span></label>
                                                                                    </td>
                                                                                    <td style="width: 80%;" class="controls">
                                                                                        <asp:TextBox ID="txtMainGrossIncome" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr style="display:none">
                                                                                    <td style="width: 20%; border-left: 0px!important;">
                                                                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Have_Co_Applicant")%>&nbsp;?&nbsp;<span style="color: Red;"></span></label>
                                                                                    </td>
                                                                                    <td style="width: 80%;" class="controls">
                                                                                        <asp:DropDownList runat="server" ID="ddlHaveCoApplicant" AutoPostBack="true" OnSelectedIndexChanged="ddlHaveCoApplicant_SelectedIndexChanged">
                                                                                            <asp:ListItem Value="0" Text="<%$ Resources:Resource, No%>"></asp:ListItem>
                                                                                            <asp:ListItem Value="1" Text="<%$ Resources:Resource, Yes%>"></asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>

                                                                <tr style="background: #dec18c;" runat="server" id="trCoApplicant" visible="true">
                                                                    <td colspan="2">
                                                                        <h4><strong><%= GetGlobalResourceObject("resource", "Co_Applicant")%></strong></h4>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trCoApplicantTable" runat="server" visible="true">
                                                                    <td colspan="2">
                                                                        <table class="table table-bordered">
                                                                            <tbody>
                                                                                <tr class="form-group">
                                                                                    <td colspan="2" style="border-left: none !important;">
                                                                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Fill_In_The_Form")%></label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="form-group" runat="server" id="trCoName">
                                                                                    <td style="width: 20%; border-left: 0px!important;">
                                                                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Name")%>&nbsp;<span style="color: Red;">*</span></label>
                                                                                    </td>
                                                                                    <td style="width: 80%;" class="controls">
                                                                                        <asp:TextBox ID="txtCoName" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="form-group" runat="server" id="trCoIC">
                                                                                    <td style="width: 20%; border-left: 0px!important;">
                                                                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "IC")%>&nbsp;<span style="color: Red;">*</span></label>
                                                                                    </td>
                                                                                    <td style="width: 80%;" class="controls">
                                                                                        <table style="width: 100%; margin: 0px; padding: 0px; border-spacing: 0px">
                                                                                            <tr>
                                                                                                <td style="width: 90%; padding: 0px">
                                                                                                    <asp:TextBox ID="txtCoIC" runat="server" CssClass="form-control" AutoPostBack="true" OnTextChanged="txtCoIC_TextChanged"></asp:TextBox>
                                                                                                </td>
                                                                                                <td style="width: 10%;">
                                                                                                    <asp:Button ID="btnCoCTOSCCRIS" Text="<%$ Resources:Resource, CTOS%>" CssClass="btn btn-next" runat="server" OnClick="btnCoCTOSCCRIS_Click" />
                                                                                                    <asp:Button ID="btnCoRAMCICCRIS" Text="<%$ Resources:Resource, RAMCI%>" CssClass="btn btn-next" runat="server" OnClick="btnCoRAMCICCRIS_Click" />
                                                                                                    <asp:HiddenField ID="hfCoCCRIS" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>

                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="form-group" runat="server" id="trCoAge">
                                                                                    <td style="width: 20%; border-left: 0px!important;">
                                                                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Age")%>&nbsp;<span style="color: Red;"></span></label>
                                                                                    </td>
                                                                                    <td style="width: 80%;" class="controls">
                                                                                        <%--<asp:TextBox ID="txtCoAge" runat="server" CssClass="form-control"></asp:TextBox>--%>
                                                                                        <asp:Label runat="server" ID="lblCoAge"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="form-group" runat="server" id="trCoArea">
                                                                                    <td style="width: 20%; border-left: 0px!important;">
                                                                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Area")%>&nbsp;<span style="color: Red;"></span></label>
                                                                                    </td>
                                                                                    <td style="width: 80%;" class="controls">
                                                                                        <asp:DropDownList runat="server" ID="ddlCoArea"></asp:DropDownList>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="form-group" runat="server" id="trCoContact">
                                                                                    <td style="width: 20%; border-left: 0px!important;">
                                                                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Contact_Number")%>&nbsp;<span style="color: Red;"></span></label>
                                                                                    </td>
                                                                                    <td style="width: 80%;" class="controls">
                                                                                        <asp:TextBox ID="txtCoContact" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="form-group" runat="server" id="trCoNetIncome">
                                                                                    <td style="width: 20%; border-left: 0px!important;">
                                                                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Net_Income")%>&nbsp;<span style="color: Red;"></span></label>
                                                                                    </td>
                                                                                    <td style="width: 80%;" class="controls">
                                                                                        <asp:TextBox ID="txtCoNetIncome" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="form-group" runat="server" id="trCoOtherIncome" style="display:none">
                                                                                    <td style="width: 20%; border-left: 0px!important;">
                                                                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Other_Income")%>&nbsp;<span style="color: Red;"></span></label>
                                                                                    </td>
                                                                                    <td style="width: 80%;" class="controls">
                                                                                        <asp:TextBox ID="txtCoOtherIncome" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="form-group" runat="server" id="trCoGrossIncome" style="display:none">
                                                                                    <td style="width: 20%; border-left: 0px!important;">
                                                                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Gross_Income")%>&nbsp;<span style="color: Red;"></span></label>
                                                                                    </td>
                                                                                    <td style="width: 80%;" class="controls">
                                                                                        <asp:TextBox ID="txtCoGrossIncome" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>

                                                        </table>

                                                    </div>
                                                    <div class="box-footer">
                                                        <%--<asp:Button ID="btnBack" Text="<%$ Resources:Resource, Cancel%>" CssClass="btn" runat="server"
                                                            OnClick="btnBack_Click" OnClientClick="return confirm('The information is unsaved. Are you sure want to quit?')" />
                                                        <asp:Button ID="btnSubmit" Text="<%$ Resources:Resource, Submit%>" CssClass="btn btn-primary" runat="server"
                                                            OnClick="btnSubmit_Click" OnClientClick="return confirm('Are you sure want to add new information?')" />--%>
                                                        <asp:Button ID="btnNext" Text="<%$ Resources:Resource, Next%>" CssClass="btn btn-next" runat="server"
                                                            OnClick="btnNext_Click" />
                                                    </div>
                                                </asp:Panel>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab_2">
                                            <div class="box box-primary">
                                                <asp:Panel runat="server" DefaultButton="btnNext2">
                                                    <div class="box-header">
                                                        <h4>
                                                            <asp:Label ID="Label1" runat="server">
                                                            </asp:Label>

                                                        </h4>
                                                    </div>
                                                    <div class="box-body">
                                                        <table class="table table-bordered">
                                                            <tbody>
                                                                <tr style="background: #dec18c;">
                                                                    <td colspan="2">
                                                                        <h4><strong><%= GetGlobalResourceObject("resource", "Loan")%></strong></h4>
                                                                    </td>
                                                                </tr>
                                                                <tr class="form-group" runat="server" id="trTenure">
                                                                    <td style="width: 20%; border-left: 0px!important;">
                                                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Tenure")%>&nbsp;<span style="color: Red;">*</span></label>
                                                                    </td>
                                                                    <td style="width: 80%;" class="controls">
                                                                        <asp:TextBox ID="txtTenure" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr class="form-group" runat="server" id="trInterest">
                                                                    <td style="width: 20%; border-left: 0px!important;">
                                                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Interest")%>&nbsp;<span style="color: Red;">*</span></label>
                                                                    </td>
                                                                    <td style="width: 80%;" class="controls">
                                                                        <asp:TextBox ID="txtInterest" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr class="form-group" runat="server" id="trLoanAmount">
                                                                    <td style="width: 20%; border-left: 0px!important;">
                                                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Loan_Amount")%>&nbsp;<span style="color: Red;">*</span></label>
                                                                    </td>
                                                                    <td style="width: 80%;" class="controls">
                                                                        <asp:TextBox ID="txtLoanAmount" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr class="form-group" runat="server" id="trPurchasePrice">
                                                                    <td style="width: 20%; border-left: 0px!important;">
                                                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Purchase_Price")%>&nbsp;<span style="color: Red;">*</span></label>
                                                                    </td>
                                                                    <td style="width: 80%;" class="controls">
                                                                        <asp:TextBox ID="txtPurchasePrice" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr class="form-group" runat="server" id="trProjectName">
                                                                    <td style="width: 20%; border-left: 0px!important;">
                                                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Project_Name")%>&nbsp;<span style="color: Red;">*</span></label>
                                                                    </td>
                                                                    <td style="width: 80%;" class="controls">
                                                                        <asp:DropDownList runat="server" ID="ddlProjectName">
                                                                            <asp:ListItem Text="<%$ Resources:Resource, Other%>" Value="Others" />
                                                                        </asp:DropDownList>
                                                                        <br />
                                                                        <br />
                                                                        <asp:TextBox runat="server" ID="txtProjectNameOther" Visible="false" CssClass="form-control"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr class="form-group" runat="server" id="trRemark">
                                                                    <td style="width: 20%; border-left: 0px!important;">
                                                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Remark")%>&nbsp;<span style="color: Red;">*</span></label>
                                                                    </td>
                                                                    <td style="width: 80%;" class="controls">
                                                                        <asp:TextBox ID="txtRemark" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="5"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr style="background: #dec18c; display:none;">
                                                                    <td colspan="2" style="border-left: none !important;">
                                                                        <h4><strong><%= GetGlobalResourceObject("resource", "Bank_Legal_Action")%></strong></h4>
                                                                    </td>
                                                                </tr>
                                                                <tr class="form-group" runat="server" id="trCTOS" style="display:none;">
                                                                    <td style="width: 20%; border-left: 0px!important;">
                                                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "CTOS")%>&nbsp;<span style="color: Red;">*</span></label>
                                                                    </td>
                                                                    <td style="width: 80%;" class="controls">
                                                                        <asp:RadioButtonList runat="server" ID="rblCTOS" RepeatDirection="Horizontal">
                                                                            <asp:ListItem Value="True" Text="<%$ Resources:Resource, Yes%>"></asp:ListItem>
                                                                            <asp:ListItem Value="False" Text="<%$ Resources:Resource, No%>" Selected="True"></asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                </tr>
                                                                <tr class="form-group" runat="server" id="trSpecialAttention" visible="true">
                                                                    <td style="width: 20%; border-left: 0px!important;">
                                                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Special_Attention")%>&nbsp;<span style="color: Red;">*</span></label>
                                                                    </td>
                                                                    <td style="width: 80%;" class="controls">
                                                                        <asp:RadioButtonList runat="server" ID="rblSpecialAttention" RepeatDirection="Horizontal">
                                                                            <asp:ListItem Value="True" Text="<%$ Resources:Resource, Yes%>"></asp:ListItem>
                                                                            <asp:ListItem Value="False" Text="<%$ Resources:Resource, No%>" Selected="True"></asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                </tr>
                                                                <tr class="form-group" runat="server" id="trRestructuring" style="display:none;">
                                                                    <td style="width: 20%; border-left: 0px!important;">
                                                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Restructuring")%>&nbsp;<span style="color: Red;">*</span></label>
                                                                    </td>
                                                                    <td style="width: 80%;" class="controls">
                                                                        <asp:RadioButtonList runat="server" ID="rblRestructuring" RepeatDirection="Horizontal">
                                                                            <asp:ListItem Value="True" Text="<%$ Resources:Resource, Yes%>"></asp:ListItem>
                                                                            <asp:ListItem Value="False" Text="<%$ Resources:Resource, No%>" Selected="True"></asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                </tr>
                                                                <tr class="form-group" runat="server" id="trDishonourCheque" style="display:none;">
                                                                    <td style="width: 20%; border-left: 0px!important;">
                                                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Dishonour_Cheque")%>&nbsp;<span style="color: Red;">*</span></label>
                                                                    </td>
                                                                    <td style="width: 80%;" class="controls">
                                                                        <asp:RadioButtonList runat="server" ID="rblDishonourCheque" RepeatDirection="Horizontal">
                                                                            <asp:ListItem Value="True" Text="<%$ Resources:Resource, Yes%>"></asp:ListItem>
                                                                            <asp:ListItem Value="False" Text="<%$ Resources:Resource, No%>" Selected="True"></asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                </tr>
                                                                <tr class="form-group" runat="server" id="trCCRISAttention" visible="false">
                                                                    <td style="width: 20%; border-left: 0px!important;">
                                                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "CCRIS_Attention")%>&nbsp;<span style="color: Red;">*</span></label>
                                                                    </td>
                                                                    <td style="width: 80%;" class="controls">
                                                                        <asp:RadioButtonList runat="server" ID="rblCCRISAttention" RepeatDirection="Horizontal">
                                                                            <asp:ListItem Value="True" Text="<%$ Resources:Resource, Yes%>"></asp:ListItem>
                                                                            <asp:ListItem Value="False" Text="<%$ Resources:Resource, No%>" Selected="True"></asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                </tr>
                                                                <tr class="form-group" runat="server" id="trCCRISPDF" style="display:none;">
                                                                    <td style="width: 20%; border-left: 0px!important;">
                                                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "CCRIS_PDF")%>&nbsp;<span style="color: Red;">*</span></label>
                                                                    </td>
                                                                    <td style="width: 80%;" class="controls">
                                                                        <asp:Panel runat="server" ID="divUpload">
                                                                            <asp:FileUpload runat="server" ID="fu1" onchange="FileUpload()" />
                                                                            <asp:Image runat="server" ID="img_fu1" ImageUrl="/Images/Doc_Icon.png" Width="50px" Visible="false" />
                                                                            <asp:Button runat="server" ID="btnUpload" Style="display: none;" OnClick="btnUpload_Click" />
                                                                            <asp:Label runat="server" ID="lblFileUpload" Visible="false"></asp:Label>

                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                            </tbody>

                                                        </table>

                                                    </div>
                                                    <div class="box-footer">
                                                        <%--<asp:Button ID="btnBack" Text="<%$ Resources:Resource, Cancel%>" CssClass="btn" runat="server"
                                                            OnClick="btnBack_Click" OnClientClick="return confirm('The information is unsaved. Are you sure want to quit?')" />
                                                        <asp:Button ID="btnSubmit" Text="<%$ Resources:Resource, Submit%>" CssClass="btn btn-primary" runat="server"
                                                            OnClick="btnSubmit_Click" OnClientClick="return confirm('Are you sure want to add new information?')" />--%>
                                                        <asp:Button ID="btnNext2" Text="<%$ Resources:Resource, Next%>" CssClass="btn btn-next" runat="server"
                                                            OnClick="btnNext2_Click" />
                                                    </div>
                                                </asp:Panel>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab_3">
                                            <div class="box box-primary">
                                                <asp:Panel runat="server" DefaultButton="btnCalculate">
                                                    <div class="box-body">
                                                        <div class="box-primary" style="padding: 0% !important;" runat="server" id="divMainLoanDetail">
                                                            <div class="box-header" style="text-align: center !important;">
                                                                <h1 class="control-label"><%= GetGlobalResourceObject("resource", "Loan_Detail_For_Main")%>
                                                                    <asp:Button runat="server" ID="btnViewMACTOSReport" Visible="false" Text="View Main Applicant's CTOS Report" CssClass="btn btn-searh" Style="margin-left: 10px;" OnClick="btnViewMACTOSReport_Click" />
                                                                    <asp:Button runat="server" ID="btnViewMARAMCIReport" Visible="false" Text="View Main Applicant's RAMCI Report" CssClass="btn btn-searh" Style="margin-left: 10px;" OnClick="btnViewMARAMCIReport_Click" />
                                                                </h1>
                                                            </div>
                                                            <table class="table table-bordered">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="">
                                                                            <div class="col-md-4 col-sm-6" runat="server" id="divMainHousingLoan">
                                                                                <div class="box-header">
                                                                                    <h3>
                                                                                        <%= GetGlobalResourceObject("resource", "Housing_Loan")%>
                                                                                    </h3>
                                                                                </div>
                                                                                <div class="box-body">
                                                                                    <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow gvColumnSize" ID="gvMainHousingLoan" runat="server" AutoGenerateColumns="false"
                                                                                        CssClass="table table-bordered table-striped" GridLines="None"
                                                                                        EmptyDataText="<%$ Resources:Resource, No_Record_Found%>"
                                                                                        Width="100%" ShowFooter="false">
                                                                                        <PagerSettings Mode="NumericFirstLast" />
                                                                                        <Columns>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Original%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainOriginalVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Outstanding%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainOutstandinglVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Repayment%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainRepaymentlVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                    </asp:GridView>
                                                                                </div>
                                                                            </div>
                                                                        
                                                                            <div class="col-md-4 col-sm-6" runat="server" id="divMainHousingLoanRecalc" visible="false">
                                                                                <div class="box-header">
                                                                                    <h3>
                                                                                        <%= GetGlobalResourceObject("resource", "Housing_Loan")%>
                                                                                    </h3>
                                                                                </div>
                                                                                <div class="box-body">
                                                                                    <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow gvColumnSize" ID="gvMainHousingLoanRecalc" runat="server" AutoGenerateColumns="false"
                                                                                        CssClass="table table-bordered table-striped" GridLines="None"
                                                                                        EmptyDataText="<%$ Resources:Resource, No_Record_Found%>"
                                                                                        Width="100%" ShowFooter="false">
                                                                                        <PagerSettings Mode="NumericFirstLast" />
                                                                                        <Columns>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Original%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainOriginalVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Outstanding%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainOutstandinglVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Repayment%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainRepaymentlVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                    </asp:GridView>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-4 col-sm-6" runat="server" id="divMainHirePurchase">
                                                                                <div class="box-header">
                                                                                    <h3>
                                                                                        <%= GetGlobalResourceObject("resource", "Hire_Purchase")%>
                                                                                    </h3>
                                                                                </div>
                                                                                <div class="box-body">
                                                                                    <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow gvColumnSize" ID="gvMainHirePurchase" runat="server" AutoGenerateColumns="false"
                                                                                        CssClass="table table-bordered table-striped" GridLines="None"
                                                                                        EmptyDataText="<%$ Resources:Resource, No_Record_Found%>"
                                                                                        Width="100%" ShowFooter="false">
                                                                                        <PagerSettings Mode="NumericFirstLast" />
                                                                                        <Columns>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Original%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainOriginalVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Outstanding%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainOutstandinglVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Repayment%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainRepaymentlVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                    </asp:GridView>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-4 col-sm-6" runat="server" id="divMainPersonalLoan">
                                                                                <div class="box-header">
                                                                                    <h3>
                                                                                        <%= GetGlobalResourceObject("resource", "Personal_Loan")%>
                                                                                    </h3>
                                                                                </div>
                                                                                <div class="box-body">
                                                                                    <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow gvColumnSize" ID="gvMainPersonalLoan" runat="server" AutoGenerateColumns="false"
                                                                                        CssClass="table table-bordered table-striped" GridLines="None"
                                                                                        EmptyDataText="<%$ Resources:Resource, No_Record_Found%>"
                                                                                        Width="100%" ShowFooter="false">
                                                                                        <PagerSettings Mode="NumericFirstLast" />
                                                                                        <Columns>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Original%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainOriginalVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Outstanding%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainOutstandinglVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Repayment%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainRepaymentlVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                    </asp:GridView>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-4 col-sm-6" runat="server" id="divMainCreditCard">
                                                                                <div class="box-header">
                                                                                    <h3>
                                                                                        <%= GetGlobalResourceObject("resource", "Credit_Card")%>
                                                                                    </h3>
                                                                                </div>
                                                                                <div class="box-body">
                                                                                    <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow gvColumnSize" ID="gvMainCreditCard" runat="server" AutoGenerateColumns="false"
                                                                                        CssClass="table table-bordered table-striped" GridLines="None"
                                                                                        EmptyDataText="<%$ Resources:Resource, No_Record_Found%>"
                                                                                        Width="100%" ShowFooter="false">
                                                                                        <PagerSettings Mode="NumericFirstLast" />
                                                                                        <Columns>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Original%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainOriginalVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Outstanding%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainOutstandinglVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Repayment%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainRepaymentlVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                    </asp:GridView>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-4 col-sm-6" runat="server" id="divMainXProduct">
                                                                                <div class="box-header">
                                                                                    <h3>
                                                                                        <%= GetGlobalResourceObject("resource", "XProduct")%>
                                                                                    </h3>
                                                                                </div>
                                                                                <div class="box-body">
                                                                                    <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow gvColumnSize" ID="gvMainXProduct" runat="server" AutoGenerateColumns="false"
                                                                                        CssClass="table table-bordered table-striped" GridLines="None"
                                                                                        EmptyDataText="<%$ Resources:Resource, No_Record_Found%>"
                                                                                        Width="100%" ShowFooter="false">
                                                                                        <PagerSettings Mode="NumericFirstLast" />
                                                                                        <Columns>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Original%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainOriginalVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Outstanding%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainOutstandinglVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Repayment%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainRepaymentlVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                    </asp:GridView>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-4 col-sm-6" visible="false" runat="server" id="divMainCommercialLoan">
                                                                                <div class="box-header">
                                                                                    <h3>
                                                                                        <%= GetGlobalResourceObject("resource", "Commercial_Loan")%>
                                                                                    </h3>
                                                                                </div>
                                                                                <div class="box-body">
                                                                                    <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow gvColumnSize" ID="gvMainCommercialLoan" runat="server" AutoGenerateColumns="false"
                                                                                        CssClass="table table-bordered table-striped" GridLines="None"
                                                                                        EmptyDataText="<%$ Resources:Resource, No_Record_Found%>"
                                                                                        Width="100%" ShowFooter="false">
                                                                                        <PagerSettings Mode="NumericFirstLast" />
                                                                                        <Columns>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Original%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainOriginalVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Outstanding%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainOutstandinglVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Repayment%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainRepaymentlVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                    </asp:GridView>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-4 col-sm-6" visible="true" runat="server" id="divMainOD">
                                                                                <div class="box-header">
                                                                                    <h3>
                                                                                        <%= GetGlobalResourceObject("resource", "OverDraft")%>
                                                                                    </h3>
                                                                                </div>
                                                                                <div class="box-body">
                                                                                    <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow gvColumnSize" ID="gvMainOD" runat="server" AutoGenerateColumns="false"
                                                                                        CssClass="table table-bordered table-striped" GridLines="None"
                                                                                        EmptyDataText="<%$ Resources:Resource, No_Record_Found%>"
                                                                                        Width="100%" ShowFooter="false">
                                                                                        <PagerSettings Mode="NumericFirstLast" />
                                                                                        <Columns>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Original%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainOriginalVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Outstanding%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainOutstandinglVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Repayment%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainRepaymentlVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                    </asp:GridView>
                                                                                </div>
                                                                            </div>
                                                                        </td>

                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="box-primary" style="padding: 0% !important;" runat="server" id="divCoLoanDetail">
                                                            <div class="box-header" style="text-align: center !important;">
                                                                <h1 class="control-label">
                                                                    <%= GetGlobalResourceObject("resource", "Loan_Detail_For_Co")%>
                                                                    <asp:Button runat="server" ID="btnViewCACTOSReport" Visible="false" Text="View Co Applicant's CTOS Report" CssClass="btn btn-searh" Style="margin-left: 10px;" OnClick="btnViewCACTOSReport_Click" />
                                                                    <asp:Button runat="server" ID="btnViewCARAMCIReport" Visible="false" Text="View Co Applicant's RAMCI Report" CssClass="btn btn-searh" Style="margin-left: 10px;" OnClick="btnViewCARAMCIReport_Click" />
                                                                </h1>
                                                            </div>
                                                            <table class="table table-bordered">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="">
                                                                            <div class="col-md-4 col-sm-6" runat="server" id="divCoHousingLoan">
                                                                                <div class="box-header">
                                                                                    <h3>
                                                                                        <%= GetGlobalResourceObject("resource", "Housing_Loan")%>
                                                                                    </h3>
                                                                                </div>
                                                                                <div class="box-body">
                                                                                    <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow gvColumnSize" ID="gvCoHousingLoan" runat="server" AutoGenerateColumns="false"
                                                                                        CssClass="table table-bordered table-striped" GridLines="None"
                                                                                        EmptyDataText="<%$ Resources:Resource, No_Record_Found%>"
                                                                                        Width="100%" ShowFooter="false">
                                                                                        <PagerSettings Mode="NumericFirstLast" />
                                                                                        <Columns>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Original%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtCoOriginalVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Outstanding%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtCoOutstandinglVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Repayment%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtCoRepaymentlVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                    </asp:GridView>
                                                                                </div>
                                                                            </div>
                                                                        
                                                                            <div class="col-md-4 col-sm-6" runat="server" id="divCoHousingLoanRecalc" visible="false">
                                                                                <div class="box-header">
                                                                                    <h3>
                                                                                        <%= GetGlobalResourceObject("resource", "Housing_Loan")%>
                                                                                    </h3>
                                                                                </div>
                                                                                <div class="box-body">
                                                                                    <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow gvColumnSize" ID="gvCoHousingLoanRecalc" runat="server" AutoGenerateColumns="false"
                                                                                        CssClass="table table-bordered table-striped" GridLines="None"
                                                                                        EmptyDataText="<%$ Resources:Resource, No_Record_Found%>"
                                                                                        Width="100%" ShowFooter="false">
                                                                                        <PagerSettings Mode="NumericFirstLast" />
                                                                                        <Columns>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Original%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtCoOriginalVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Outstanding%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtCoOutstandinglVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Repayment%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtCoRepaymentlVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                    </asp:GridView>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-4 col-sm-6" runat="server" id="divCoHirePurchase">
                                                                                <div class="box-header">
                                                                                    <h3>
                                                                                        <%= GetGlobalResourceObject("resource", "Hire_Purchase")%>
                                                                                    </h3>
                                                                                </div>
                                                                                <div class="box-body">
                                                                                    <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow gvColumnSize" ID="gvCoHirePurchase" runat="server" AutoGenerateColumns="false"
                                                                                        CssClass="table table-bordered table-striped" GridLines="None"
                                                                                        EmptyDataText="<%$ Resources:Resource, No_Record_Found%>"
                                                                                        Width="100%" ShowFooter="false">
                                                                                        <PagerSettings Mode="NumericFirstLast" />
                                                                                        <Columns>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Original%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtCoOriginalVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Outstanding%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtCoOutstandinglVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Repayment%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtCoRepaymentlVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                    </asp:GridView>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-4 col-sm-6" runat="server" id="divCoPersonalLoan">
                                                                                <div class="box-header">
                                                                                    <h3>
                                                                                        <%= GetGlobalResourceObject("resource", "Personal_Loan")%>
                                                                                    </h3>
                                                                                </div>
                                                                                <div class="box-body">
                                                                                    <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow gvColumnSize" ID="gvCoPersonalLoan" runat="server" AutoGenerateColumns="false"
                                                                                        CssClass="table table-bordered table-striped" GridLines="None"
                                                                                        EmptyDataText="<%$ Resources:Resource, No_Record_Found%>"
                                                                                        Width="100%" ShowFooter="false">
                                                                                        <PagerSettings Mode="NumericFirstLast" />
                                                                                        <Columns>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Original%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtCoOriginalVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Outstanding%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtCoOutstandinglVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Repayment%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtCoRepaymentlVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                    </asp:GridView>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-4 col-sm-6" runat="server" id="divCoCreditCard">
                                                                                <div class="box-header">
                                                                                    <h3>
                                                                                        <%= GetGlobalResourceObject("resource", "Credit_Card")%>
                                                                                    </h3>
                                                                                </div>
                                                                                <div class="box-body">
                                                                                    <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow gvColumnSize" ID="gvCoCreditCard" runat="server" AutoGenerateColumns="false"
                                                                                        CssClass="table table-bordered table-striped" GridLines="None"
                                                                                        EmptyDataText="<%$ Resources:Resource, No_Record_Found%>"
                                                                                        Width="100%" ShowFooter="false">
                                                                                        <PagerSettings Mode="NumericFirstLast" />
                                                                                        <Columns>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Original%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtCoOriginalVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Outstanding%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtCoOutstandinglVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Repayment%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtCoRepaymentlVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                    </asp:GridView>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-4 col-sm-6" runat="server" id="divCoXProduct">
                                                                                <div class="box-header">
                                                                                    <h3>
                                                                                        <%= GetGlobalResourceObject("resource", "XProduct")%>
                                                                                    </h3>
                                                                                </div>
                                                                                <div class="box-body">
                                                                                    <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow gvColumnSize" ID="gvCoXProduct" runat="server" AutoGenerateColumns="false"
                                                                                        CssClass="table table-bordered table-striped" GridLines="None"
                                                                                        EmptyDataText="<%$ Resources:Resource, No_Record_Found%>"
                                                                                        Width="100%" ShowFooter="false">
                                                                                        <PagerSettings Mode="NumericFirstLast" />
                                                                                        <Columns>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Original%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtCoOriginalVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Outstanding%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtCoOutstandinglVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Repayment%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtCoRepaymentlVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                    </asp:GridView>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-4 col-sm-6" runat="server" id="divCommercialLoan" visible="false">
                                                                                <div class="box-header">
                                                                                    <h3>
                                                                                        <%= GetGlobalResourceObject("resource", "Commercial_Loan")%>
                                                                                    </h3>
                                                                                </div>
                                                                                <div class="box-body">
                                                                                    <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow gvColumnSize" ID="gvCoCommercialLoan" runat="server" AutoGenerateColumns="false"
                                                                                        CssClass="table table-bordered table-striped" GridLines="None"
                                                                                        EmptyDataText="<%$ Resources:Resource, No_Record_Found%>"
                                                                                        Width="100%" ShowFooter="false">
                                                                                        <PagerSettings Mode="NumericFirstLast" />
                                                                                        <Columns>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Original%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtCoOriginalVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Outstanding%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtCoOutstandinglVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Repayment%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtCoRepaymentlVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                    </asp:GridView>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-4 col-sm-6" runat="server" id="divCoOD" visible="false">
                                                                                <div class="box-header">
                                                                                    <h3>
                                                                                        <%= GetGlobalResourceObject("resource", "OverDraft")%>
                                                                                    </h3>
                                                                                </div>
                                                                                <div class="box-body">
                                                                                    <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow gvColumnSize" ID="gvCoOD" runat="server" AutoGenerateColumns="false"
                                                                                        CssClass="table table-bordered table-striped" GridLines="None"
                                                                                        EmptyDataText="<%$ Resources:Resource, No_Record_Found%>"
                                                                                        Width="100%" ShowFooter="false">
                                                                                        <PagerSettings Mode="NumericFirstLast" />
                                                                                        <Columns>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Original%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtCoOriginalVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Outstanding%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtCoOutstandinglVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Repayment%>" ItemStyle-Width="33%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtCoRepaymentlVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                    </asp:GridView>
                                                                                </div>
                                                                            </div>

                                                                        
                                                                        </td>


                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="box-footer">
                                                        <%--<asp:Button ID="btnBack" Text="<%$ Resources:Resource, Cancel%>" CssClass="btn" runat="server"
                                                            OnClick="btnBack_Click" OnClientClick="return confirm('The information is unsaved. Are you sure want to quit?')" />
                                                        <asp:Button ID="btnSubmit" Text="<%$ Resources:Resource, Submit%>" CssClass="btn btn-primary" runat="server"
                                                            OnClick="btnSubmit_Click" OnClientClick="return confirm('Are you sure want to add new information?')" />--%>
                                                        <asp:Button ID="btnCalculate" Text="<%$ Resources:Resource, Calculate%>" CssClass="btn btn-submit" runat="server"
                                                            OnClick="btnCalculate_Click" OnClientClick="return confirm('Are you sure want to calculate loan?')" />
                                                        <asp:HiddenField ID="hftemprequestId" runat="server" />
                                                    </div>
                                                </asp:Panel>
                                            </div>

                                        </div>
                                        <!-- /.tab-pane -->
                                    </div>
                                    <!-- /.tab-content -->
                                </div>
                                <!-- nav-tabs-custom -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </aside>


</asp:Content>
