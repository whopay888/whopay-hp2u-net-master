﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Global1.Master" CodeBehind="RequestList.aspx.cs" Inherits="HJT.Form.Loan.RequestList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function confirmDelete() {
            return confirm('<%= GetGlobalResourceObject("resource","Confirm_Delete")%>');
        }

        function confirmSend() {
            return confirm('<%= GetGlobalResourceObject("resource","Confirm_SendSMS")%>');
        }

        $(window).load(function () {
            var x = document.getElementById("filtercontent");
            var lblfilter = document.getElementById("lblfilter");
            if (x.style.display === "none") {
                x.style.display = "block";
                lblfilter.innerHTML = "Hide Filters"
            } else {
                x.style.display = "none";
                lblfilter.innerHTML = "Show Filters"
            }
        });

        function myFunction() {
            var x = document.getElementById("filtercontent");
            var lblfilter = document.getElementById("lblfilter");
            if (x.style.display === "none") {
                x.style.display = "block";
                lblfilter.innerHTML = "Hide Filters"
            } else {
                x.style.display = "none";
                lblfilter.innerHTML = "Show Filters"
            }
        }

        function NewFunction() {
            var lblfilter = document.getElementById("lblfilter");
            lblfilter.innerHTML = "Hide Filters"
        }

    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <aside class="right-side">
        <section class="content-header">
            <h1>
                <span class="icon fa fa-fw fa-book" aria-hidden="true"></span>
                <asp:Label ID="lblPageTitle" runat="server"><i class="icon-th-list"></i><span runat="server" id="spHeader"><%= GetGlobalResourceObject("resource", "Request_List")%></span></asp:Label>
            </h1>
            <asp:SiteMapPath ID="SiteMapPath1" CssClass="breadcrumb" runat="server"></asp:SiteMapPath>
        </section>
        <section class="content" style="margin-top:10px">
            <div class="col-md-12">
                <div runat="server">
                    <div id="filter">
                        <a href="#0" id="showfilter" onclick="myFunction()">
                            <label id="lblfilter" class="btn btn-searh">Show Filters</label></a>
                        <div id="filtercontent">
                            <div class="box">
                                <h4>
                                    <asp:Label ID="panel" runat="server" Text="<%$ Resources:Resource, Selection_Option%>"></asp:Label></h4>
                                <div class="box-body">
                                    <div class="form-group">

                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Applicant_Name")%></label>
                                        <div class="controls">
                                            <asp:TextBox runat="server" ID="txtName" CssClass="form-control"></asp:TextBox>

                                        </div>

                                    </div>

                                    <div class="form-group" runat="server" id="divBranch">

                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Branch")%></label>
                                        <div class="controls">
                                            <asp:DropDownList runat="server" CssClass="form-control" ID="ddlBranch">
                                            </asp:DropDownList>
                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Member_Name")%></label>
                                        <div class="controls">
                                            <asp:TextBox runat="server" ID="txtMemberName" CssClass="form-control"></asp:TextBox>
                                        </div>

                                    </div>

                                    <div class="form-group" runat="server" id="divreqId">

                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Request_ID")%></label>
                                        <div class="controls">
                                            <asp:TextBox runat="server" ID="txtRequestID" CssClass="form-control"></asp:TextBox>
                                        </div>

                                    </div>


                                    <div class="form-group">

                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Status")%></label>
                                        <div class="controls">
                                            <asp:DropDownList runat="server" CssClass="form-control" ID="ddlStatus">
                                            </asp:DropDownList>
                                        </div>

                                    </div>


                                </div>

                                <div class="box-footer">

                                    <asp:Button ID="btnSearch" Text="<%$ Resources:Resource, Search%>" OnClientClick="myFunction()" runat="server" CssClass="btn btn-searh" OnClick="btnSearch_Click" />

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="box">
                        <div class="box-body">
                            <div class="box-body table-responsive">
                                <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow" ID="gv" runat="server" AutoGenerateColumns="false" GridLines="None" AllowPaging="True" ShowFooter="true"
                                    EmptyDataText="<%$ Resources:Resource, No_Record_Found%>" CssClass="table table-bordered table-striped" Width="100%" OnPageIndexChanging="gv_PageIndexChanging"
                                    OnRowDataBound="gv_RowDataBound" OnDataBound="gv_DataBound">
                                    <PagerSettings Mode="NumericFirstLast" />
                                    <Columns>
                                        <asp:BoundField HeaderText="<%$ Resources:Resource, Request_ID%>" Visible="false" DataField="requestid" />
                                        <asp:BoundField HeaderText="<%$ Resources:Resource, Date%>" DataField="RequestAt" />
                                        <asp:BoundField HeaderText="<%$ Resources:Resource, Applicant_Name%>" DataField="ApplicantName" />
                                        <asp:BoundField HeaderText="<%$ Resources:Resource, Branch_Name%>" DataField="BranchName" />
                                        <asp:BoundField HeaderText="<%$ Resources:Resource, Member_Name%>" DataField="Fullname" />
                                        <asp:BoundField HeaderText="<%$ Resources:Resource, Price%>" DataField="Price" />
                                        <asp:BoundField HeaderText="<%$ Resources:Resource, Status%>" DataField="StatusID" />

                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, Uploaded_File%>">

                                            <ItemTemplate>

                                                <asp:Repeater runat="server" ID="rptFiles">
                                                    <ItemTemplate>
                                                        <p><a href='<%# Eval("URL") %>' target="_blank"><%# Eval("DocumentType") %> </a></p>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </ItemTemplate>

                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, Action%>">

                                            <ItemTemplate>
                                                <asp:Button runat="server" ID="btnCalculate" Text="<%$ Resources:Resource, Calculate%>" 
                                                    Visible="false" CssClass="btn btn-check" style="width:100px; padding-left:7px !important;"/>
                                                <asp:Button ID="btnSendSMS" runat="server" Text="<%$ Resources:Resource, SMS%>"
                                                    CssClass="btn btn-check" Style="width:47px; padding-left:8px !important" OnClick="btnSendSMS_Click"
                                                    OnClientClick="return confirmSend();" CommandArgument='<%#Eval("requestid") %>' Visible="false"/>
                                                <asp:Button runat="server" ID="btnDelete" Text="<%$ Resources:Resource, Del%>" 
                                                    CommandArgument='<%#Eval("requestid") %>' Visible="false" OnClick="btnDelete_Click" 
                                                    OnClientClick="return confirmDelete();" CssClass="btn btn-danger" 
                                                    Style="margin-bottom: 0px !important; padding-left:7px !important; width:47px;" />
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                    </Columns>
                                    <PagerTemplate>
                                        <div>
                                            <div class="dataTables_paginate paging_bootstrap">
                                                <ul class="pagination">
                                                    <li>
                                                        <asp:PlaceHolder runat="server" ID="phPreviousPaging"></asp:PlaceHolder>
                                                    </li>

                                                    <li>
                                                        <asp:PlaceHolder runat="server" ID="phPaging"></asp:PlaceHolder>
                                                    </li>

                                                    <li>
                                                        <asp:PlaceHolder runat="server" ID="phNextPaging"></asp:PlaceHolder>
                                                    </li>
                                                </ul>

                                            </div>
                                        </div>
                                    </PagerTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </aside>


</asp:Content>
