﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Model;
using Synergy.Util;
using Synergy.Controller;

namespace HJT.Form.Loan
{
    public partial class RequestList : System.Web.UI.Page
    {
        LoginUserModel login_user = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            login_user = (LoginUserModel)Session[KeyVal.loginsecure];
            divreqId.Visible = false;
            if (!IsPostBack)
            {
                binddata();
                sqlString.bindControl(gv, getRequestList());
            }

            if (gv.Rows.Count > 0)
                gridView.ApplyPaging(gv, -1);
        }

        private void binddata()
        {
            sqlString.bindControl(ddlBranch, "select BranchName as '0', BranchID as '1' " +
                "from tbl_branch where isdeleted = '0' " +
                "order by '0'", "0", "1", true);

            divBranch.Visible = login_user.isStaffAdmin();

            if (login_user.isBranchManager())
            {
                //ddlBranch.SelectedValue = login_user.BranchID;
            }
            sqlString.bindControl(ddlStatus, "select  ParameterName as '0' , ParameterValue  as '1' from tbl_parameter where category='ApplicationStatus'", "0", "1", true);
        }

        private string getRequestList()
        {
            StringBuilder sql = new StringBuilder();


            string searchFilter = string.Empty;
            if (login_user.isStaffAdmin() || login_user.UserType.Contains(KeyVal.CreditAdvisor))
                searchFilter = string.Empty;
            else if (login_user.UserAccessList.Where(_ => _.Key.Equals("function_view_branch_request")).Count() >= 1)
                searchFilter = " AND (a.memberid = '" + login_user.UserId + "' OR A.BranchID IN(SELECT BranchId FROM tbl_Branch Where ParentId = " + login_user.BranchUniqueID + " OR Id = " + login_user.BranchUniqueID + "))";
            else
                searchFilter = " AND a.memberid='" + login_user.UserId + "'";


            sql.AppendFormat(@"
                ;WITH cte AS
				(
				SELECT *,
				ROW_NUMBER() OVER (PARTITION BY dbo.tbl_RequestApplicant.RequestID ORDER BY dbo.tbl_RequestApplicant.CreateAt ASC) AS rn
				FROM tbl_RequestApplicant
				)			
				SELECT A.RequestID, a.RequestAt ,E.Fullname ,D.Name   AS 'ApplicantName' ,a.StatusID , ~a.isdeleted AS 'canedit' , b.DocumentList , ISNULL(c.BranchName ,'--') AS BranchName, a.MemberId, a.BranchId, 'Request: MYR ' + CONVERT(VARCHAR,a.PerRequestPrice)  + ' Successful: MYR ' + CONVERT(VARCHAR,a.PerSuccessfulCasePrice) AS Price
				FROM (
                SELECT * FROM tbl_Request WITH (NOLOCK) 
                )a
                LEFT JOIN 
                (	
                SELECT 
                    requestid,'['+
                    STUFF((
                    SELECT ', ' +'{{""DocumentType"" :""'+ DocumentType + '"", ""URL"" :""' + DocsURL+'""}}'
                    FROM tbl_RequestDocs WITH (NOLOCK)
                    WHERE  isdeleted=0 AND (requestid = trd.requestid)  AND DocumentType = 'N'
                    FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)')
                    ,1,2,'')+']' AS DocumentList
                FROM tbl_RequestDocs trd WITH (NOLOCK)
                GROUP BY requestid
                ) b ON a.RequestID = b.requestid
                LEFT JOIN tbl_branch c WITH (NOLOCK) ON a.BranchID = c.BranchID
                LEFT JOIN 
				(
					SELECT *
					FROM cte
					WHERE rn = 1
				) d
				ON a.RequestID = d.requestid
                LEFT JOIN tbl_MemberInfo E WITH (NOLOCK) ON A.MemberID = E.MemberId
				LEFT JOIN tbl_projectsettings PS WITH(NOLOCK) ON A.ProjectId = PS.ID
                WHERE 1=1  
            ");

            sql.Append(searchFilter);

            sql.Append(sqlString.searchTextBox("D.Name", txtName, true, true, false));

            sql.Append(sqlString.searchTextBox("E.Fullname", txtMemberName, true, true, false));

            sql.Append(sqlString.searchDropDownList("a.BranchID", ddlBranch, true, true, false));

            sql.Append(sqlString.searchTextBox("A.RequestID", txtRequestID, false, true, false));


            sql.Append(sqlString.searchDropDownList("a.StatusID", ddlStatus, true, true, false));
            sql.Append("ORDER BY a.RequestAt DESC");

            return sql.ToString();

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            sqlString.bindControl(gv, getRequestList());

            if (gv.Rows.Count > 0)
                gridView.ApplyPaging(gv, -1);

            //Page.RegisterStartupScript(this.GetType(), "CallMyFunction", "NewFunction()", true);
            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "MyFun1", "NewFunction();", true);
        }

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Repeater rptFiles = (Repeater)e.Row.FindControl("rptFiles");
                string filesList = DataBinder.Eval(e.Row.DataItem, "DocumentList").ToString();
                string canedit = DataBinder.Eval(e.Row.DataItem, "canedit").ToString();
                string StatusID = DataBinder.Eval(e.Row.DataItem, "StatusID").ToString();
                string requestID = DataBinder.Eval(e.Row.DataItem, "requestid").ToString();
                Button btnDelete = (Button)e.Row.FindControl("btnDelete");
                Button btnSendSMS = (Button)e.Row.FindControl("btnSendSMS");
                Button btnCalculate = (Button)e.Row.FindControl("btnCalculate");
                int statusIndex = gridView.GetColumnIndexByName(gv, Resources.resource.Status);

                if (filesList != string.Empty)
                {
                    var array = JArray.Parse(filesList);
                    array.ToList().ForEach(x => x["DocumentType"] = sqlString.changeLanguage("DocumentType_" + x["DocumentType"]));

                    if (array != null)
                    {
                        rptFiles.DataSource = array;
                        rptFiles.DataBind();
                    }
                }

                if (canedit.ToUpper() == "TRUE")
                {
                    if (login_user.isStaffAdmin() && StatusID != ApplicationStatus.Sent)
                    {
                        btnCalculate.Visible = true;
                        btnSendSMS.Visible = true;
                    }
                    if (login_user.UserType.Contains(KeyVal.CreditAdvisor) && StatusID == ApplicationStatus.InProgress)
                    {
                        btnCalculate.Visible = true;
                        btnSendSMS.Visible = true;
                    }

                    //if (StatusID == ApplicationStatus.New)
                    btnCalculate.Attributes.Add("onclick", "window.location.href = '" + "/Form/Loan/Calculator.aspx?rid=" + sqlString.encryptURL(requestID) + "'");
                    //else if (StatusID == ApplicationStatus.InProgress)
                    //    btnCalculate.Attributes.Add("onclick", "window.location.href ='" + "/Form/Loan/CalculateReport.aspx?rid=" + sqlString.encryptURL(requestID) + "'");

                    if (login_user.isStaffAdmin() && StatusID != ApplicationStatus.Sent)
                    {
                        btnDelete.Visible = true;
                    }
                }
                e.Row.Cells[statusIndex].Text = sqlString.changeLanguage("ApplicationStatus_" + e.Row.Cells[statusIndex].Text);
            }
        }

        protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gv.PageIndex = e.NewPageIndex;
            sqlString.bindControl(gv, getRequestList());


            if (gv.Rows.Count > 0)
                gridView.ApplyPaging(gv, e.NewPageIndex);
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            wwdb db = new wwdb();
            Button btnSender = (Button)sender;
            db.Execute("UPDATE tbl_Request SET isDeleted = 1, StatusID = 'D' WHERE RequestID='" + btnSender.CommandArgument.ToString() + "'");
            sqlString.displayAlert2(this, Resources.resource.Action_Successful, Request.Url.ToString());

        }

        protected void gv_DataBound(object sender, EventArgs e)
        {
            if (gv.Rows.Count > 0)
            {
                if (!login_user.isStaffAdmin() && !login_user.UserType.Contains(KeyVal.CreditAdvisor))
                {
                    int actionIndex = gridView.GetColumnIndexByName(gv, Resources.resource.Action);
                    gridView.hideColumn(gv, new int[] { actionIndex }, true, true);
                }

            }
        }

        protected void btnSendSMS_Click(object sender, EventArgs e)
        {
            wwdb db = new wwdb();
            Button btnSender = (Button)sender;

            //send sms
            db.OpenTable(string.Format(@"
                SELECT (SELECT TOP 1 Mobile FROM
                tbl_MemberInfo WITH(NOLOCK) WHERE MemberID = tr.MemberID) AS mobile FROM
                tbl_request tr WITH(NOLOCK) WHERE requestid = '{0}'
            ", btnSender.CommandArgument.ToString()));
            //if mobile phone found
            if (db.RecordCount() > 0)
            {
                string mobileNo = db.Item("Mobile");
                bool validFormat = false;
                if (mobileNo.StartsWith("0") && !(mobileNo.StartsWith("6")))
                {
                    mobileNo = "6";
                    mobileNo += db.Item("Mobile");
                    validFormat = true;
                }
                else if (mobileNo.StartsWith("601"))
                    validFormat = true;

                if (validFormat)//only send sms when mobileNo is in valid format to save cost
                {
                    MessagingService SMS = new MessagingService();
                    SMS.sendInvalidFileNotification(mobileNo);
                }
            }

            sqlString.displayAlert2(this, Resources.resource.Action_Successful, Request.Url.ToString());
        }
    }
}