﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Util;
using Synergy.Helper;
using HJT.Cls.Controller;

namespace HJT.Form.Loan
{
    public partial class CalculateReport : System.Web.UI.Page
    {
        Dictionary<string, string> BankList = new Dictionary<string, string>();
        string requestID = string.Empty;
        Dictionary<string, DataTable> applicantData = new Dictionary<string, DataTable>();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                requestID = Request["rid"] as string;

                if (!string.IsNullOrEmpty(requestID))
                {
                    requestID = sqlString.decryptURL(requestID);
                    ViewState["requestID"] = requestID;
                    bindControl();
                    bindExistingData();
                    bindDisplayTable();
                    bindCTOSRepeater(requestID);
                }
            }
            applicantData = ViewState["applicantData"] as Dictionary<string, DataTable>;
            BankList = ViewState["BankList"] as Dictionary<string, string>;
            requestID = ViewState["requestID"] as string;
        } 

        private void bindCTOSRepeater(string requestId)
        {

            wwdb db = new wwdb();
            int resultId;
            StringBuilder sql = new StringBuilder();
            sql.Clear();
            sql.AppendFormat(@" 
                SELECT ID ,IsCoApplicant ,Type
                FROM tbl_CTOS_Result WITH(NOLOCK)
                WHERE IsActive = 1 AND Request_Id = N'{0}' Order By ID
            ", secure.RC(requestId));
            db.OpenTable(sql.ToString());

            if (db.RecordCount() > 0)
            {
                for (int i = 0; i < db.RecordCount(); i++)
                {
                    string type = db.Item("Type").ToString();
                    bool IsCoApplicant;
                    resultId = int.Parse(db.Item("ID").ToString());
                    IsCoApplicant = Convert.ToBoolean(db.Item("IsCoApplicant").ToString());


                    DataTable dt;
                    dt = db.getDataTable(string.Format(@"
                        SELECT ID,CTOS_Result_Id, No, CASE WHEN Date_R_R IS NULL THEN NULL ELSE CONVERT(varchar,Date_R_R,101) END AS Date_R_R, Sts, 
                        Capacity, Lender_Type, Facility, Total_Outstanding_Balance, 
                        CASE WHEN Date_Balance_Updated IS NULL THEN NULL ELSE CONVERT(varchar,Date_Balance_Updated,101) END AS Date_Balance_Updated,
                        Limit_Installment_Amount, Prin_Repmt_Term, Col_Type,LGL_STS,
                        CASE WHEN Date_Status_Updated IS NULL THEN NULL ELSE CONVERT(varchar,Date_Status_Updated,101) END AS Date_Status_Updated,
                        IsShowinReport,
                        CASE WHEN M12_Count IS NULL THEN CAST('-' AS varchar(2)) ELSE CAST(M12_Count as VARCHAR(2)) END as M12_Count,
                        CASE WHEN M11_Count IS NULL THEN CAST('-' AS varchar(2)) ELSE CAST(M11_Count as VARCHAR(2)) END as M11_Count,
                        CASE WHEN M10_Count IS NULL THEN CAST('-' AS varchar(2)) ELSE CAST(M10_Count as VARCHAR(2)) END as M10_Count,
                        CASE WHEN M9_Count IS NULL THEN CAST('-' AS varchar(2)) ELSE CAST(M9_Count as VARCHAR(2)) END as M9_Count,
                        CASE WHEN M8_Count IS NULL THEN CAST('-' AS varchar(2)) ELSE CAST(M8_Count as VARCHAR(2)) END as M8_Count,
                        CASE WHEN M7_Count IS NULL THEN CAST('-' AS varchar(2)) ELSE CAST(M7_Count as VARCHAR(2)) END as M7_Count,
                        CASE WHEN M6_Count IS NULL THEN CAST('-' AS varchar(2)) ELSE CAST(M6_Count as VARCHAR(2)) END as M6_Count,
                        CASE WHEN M5_Count IS NULL THEN CAST('-' AS varchar(2)) ELSE CAST(M5_Count as VARCHAR(2)) END as M5_Count,
                        CASE WHEN M4_Count IS NULL THEN CAST('-' AS varchar(2)) ELSE CAST(M4_Count as VARCHAR(2)) END as M4_Count,
                        CASE WHEN M3_Count IS NULL THEN CAST('-' AS varchar(2)) ELSE CAST(M3_Count as VARCHAR(2)) END as M3_Count,
                        CASE WHEN M2_Count IS NULL THEN CAST('-' AS varchar(2)) ELSE CAST(M2_Count as VARCHAR(2)) END as M2_Count,
                        CASE WHEN M1_Count IS NULL THEN CAST('-' AS varchar(2)) ELSE CAST(M1_Count as VARCHAR(2)) END as M1_Count
                        FROM tbl_CTOS_Report_Loan_Details WHERE CTOS_Result_Id ={0} ORDER BY SortOrder
                    ", resultId));
                    if (IsCoApplicant == false)
                    {
                        rptLoanDetails.DataSource = dt;
                        rptLoanDetails.DataBind();
                        if (dt.Rows.Count > 0)
                        {
                            ccrisTable_MA.Visible = true;
                            btnViewMACTOSReport.Visible = type.Equals("CTOS");
                            btnViewMARAMCIReport.Visible = type.Equals("RAMCI");
                        }
                    }
                    else
                    {
                        rptCoLoanDetails.DataSource = dt;
                        rptCoLoanDetails.DataBind();
                        if (dt.Rows.Count > 0)
                        {
                            ccrisTable_CA.Visible = true;
                            btnViewCACTOSReport.Visible = type.Equals("CTOS");
                            btnViewCARAMCIReport.Visible = type.Equals("RAMCI");
                        }
                    }

                    db.MoveNext();
                }
            }
        }
        private void bindControl()
        {
            sqlString.bindControl(rblResult, "select ParameterName as '0' , ParameterValue as '1' from tbl_parameter where Category = 'ApplicationResult'", "0", "1");

            sqlString.bindControl(chkAdvisorList, "select AdvisorID , Description as 'DESC' from tbl_creditadvisor where isdeleted = 0 and status='A' ", "DESC", "AdvisorID", null);


            // bind applicant Info

            wwdb db = new wwdb();

            db.OpenTable("select Name , MyCard , ( otherincome + NetIncome) as 'Salary' ,ApplicantType from tbl_tapplicants where  RequestID ='" + requestID + "' ");

            if (db.RecordCount() > 0)
            {
                while (!db.Eof())
                {
                    if (db.Item("ApplicantType").ToString().ToUpper() == "MA")
                    {
                        lblNameMain.Text = db.Item("Name");
                        lblICMain.Text = db.Item("MyCard");
                        lblTotalSalaryMain.Text = ConvertHelper.ConvertToDecimal(db.Item("Salary"), 0).ToString("N");
                    }
                    else if (db.Item("ApplicantType").ToString().ToUpper() == "CA")
                    {
                        lblNameCo.Text = db.Item("Name");

                        lblICCo.Text = db.Item("MyCard");

                        lblTotalSalaryCo.Text = ConvertHelper.ConvertToDecimal(db.Item("Salary"), 0).ToString("N");
                    }
                    db.MoveNext();
                }
            }

            trNameCo.Visible = lblNameCo.Text != string.Empty;
            trICCo.Visible = lblICCo.Text != string.Empty;
            trSalaryCo.Visible = lblTotalSalaryCo.Text != string.Empty;
            // Bind Bank Legal Info
            db.OpenTable(string.Format(@"
                            select PurchasePrice, b.ProjectName, a.unit, a.ctos, a.SAttention, 
                            a.Restructuring, A.DishonourCheque, A.CCRIS 
                            from tbl_TLoanLegalAction a
                            left join tbl_ProjectSettings b on a.ProjectID = b.ProjectID 
                            where RequestID = '{0}'
                        ", requestID));

            if (db.RecordCount() > 0)
            {
                lblProjectName.Text = db.Item("ProjectName").ToString();
                lblPurchase.Text = ConvertHelper.ConvertToDecimal(db.Item("PurchasePrice").ToString(), 0).ToString("N");
                lblUnit.Text = db.Item("unit").ToString();
                
                lblRestructure.Text = (db.Item("Restructuring").ToString().ToUpper() == "TRUE") ? Resources.resource.Yes : Resources.resource.No;
                lblDisHonour.Text = (db.Item("DishonourCheque").ToString().ToUpper() == "TRUE") ? Resources.resource.Yes : Resources.resource.No;
                lblCTOS.Text = (db.Item("ctos").ToString().ToUpper() == "TRUE") ? Resources.resource.Yes : Resources.resource.No;
                lblCCRIS.Text = (db.Item("CCRIS").ToString().ToUpper() == "TRUE") ? Resources.resource.Yes : Resources.resource.No;
                lblSpecialAttention.Text = (db.Item("SAttention").ToString().ToUpper() == "TRUE") ? Resources.resource.Yes : Resources.resource.No;
            }
        }
        private void bindDisplayTable()
        {
            wwdb db = new wwdb();

            DataTable bankDt = db.getDataTable("select BankID , BankName from tbl_BankName where IsDeleted = 0 Order by BankName");

            for (int count = 0; count < bankDt.Rows.Count; count++)
            {
                DataRow dr = bankDt.Rows[count];
                BankList.Add(dr["BankID"].ToString(), dr["BankName"].ToString());
            }

            DataTable dt = db.getDataTable("select ParameterName as 'Name' , ParameterValue as 'ID' from tbl_parameter where category = 'ApplicantType' order by case when ParameterValue = 'MA' then 0 else 1 end asc");
            rptApplicant.DataSource = dt;
            rptApplicant.DataBind();


            rptHeader.DataSource = bankDt;
            rptHeader.DataBind();

            rptFooter.DataSource = bankDt;
            rptFooter.DataBind();



            DataTable dsrdt = db.getDataTable(string.Format(@"
                            SELECT a.BankID , b.PreferRatio, b.DSR , 
                            CAST(CONVERT(varchar, CAST(b.LoanAmountEligible AS money), 1) AS varchar) AS LoanAmountEligible , 
                            CAST(CONVERT(varchar, CAST(b.IncomeRequired AS money), 1) AS varchar) AS IncomeRequired , 
                            CAST(CONVERT(varchar, CAST(b.NewLoanRepayment AS money), 1) AS varchar) AS NewLoanRepayment , 
                            CAST(CONVERT(varchar, CAST(b.DeclareIncome   AS money), 1) AS varchar) AS DeclareIncome 
                            FROM (
                                SELECT BankID, BankName 
                                FROM tbl_BankName WITH(NOLOCK) 
                                WHERE IsDeleted = 0 
                            )a 
                            LEFT JOIN 
                            (
                                SELECT * 
                                FROM tbl_tloaninfo WITH(NOLOCK) 
                                WHERE RequestID = {0}
                            )b ON a.BankID = b.BankID
                        ", requestID));

            Dictionary<string, Literal> DSRFieldMapping = new Dictionary<string, Literal>();
            DSRFieldMapping.Add("DSR", LiDsr);
            DSRFieldMapping.Add("IncomeRequired", LiIncome);
            DSRFieldMapping.Add("NewLoanRepayment", LiRepayment);
            DSRFieldMapping.Add("PreferRatio", LiPolicy);
            DSRFieldMapping.Add("LoanAmountEligible", LiLoanEligible);



            if (dsrdt != null && dsrdt.Rows.Count > 0)
            {
                foreach (string columnname in DSRFieldMapping.Keys)
                {
                    string appendStr = string.Empty;

                    foreach (string bankID in BankList.Keys)
                    {
                        var value = dsrdt.AsEnumerable().Where(x => x["BankID"].ToString() == bankID).Select(x => x[columnname]).SingleOrDefault();
                        appendStr += "<td>";

                        if (columnname.Equals("LoanAmountEligible") && ConvertHelper.ConvertToDecimal(value, 0) < 0)
                            appendStr += 0.ToString("N");
                        else
                            appendStr += value;

                        appendStr += "</td>";
                    }
                    DSRFieldMapping[columnname].Text = appendStr;
                }
            }
            ViewState["BankList"] = BankList;
        }
        private void bindExistingData()
        {
            wwdb db = new wwdb();
            DataTable dt = db.getDataTable(string.Format(@"
                SELECT AdvisorID, Comment, Message 
                FROM tbl_TApplicantCreditAdvisor WITH(NOLOCK) 
                WHERE RequestID = '{0}' AND IsDeleted = 0
            ", requestID));
            List<string> checklist = new List<string>();
            foreach (DataRow dr in dt.Rows)
                if (dr["Comment"].ToString() != string.Empty)
                    txtComment.Text += dr["Comment"].ToString();
                else
                    checklist.Add(dr["AdvisorID"].ToString());

            foreach (DataRow dr in dt.Rows)
                if (dr["Message"].ToString() != string.Empty)
                    txtMessage.Text += dr["Message"].ToString();

            if (checklist.Count > 0)
                foreach (ListItem li in chkAdvisorList.Items)
                    li.Selected = checklist.Contains(li.Value);
            
            db.OpenTable("SELECT ApplicationResult FROM tbl_request WHERE RequestID ='" + requestID + "'");

            if (db.RecordCount() > 0)
            {
                rblResult.SelectedValue = db.Item("ApplicationResult").ToString();
                if (!String.IsNullOrEmpty(db.Item("ApplicationResult").ToString()))
                    btnSend.Visible = true;
                else
                    btnSend.Visible = false;
            }
            else
                btnSend.Visible = false;
        }

        private bool ValidateForm()
        {
            bool result = false;
            StringBuilder errMsg = new StringBuilder();
            StringBuilder sql = new StringBuilder();
            wwdb db = new wwdb();

            if (rblResult.SelectedValue == "")
            {
                errMsg.Append(Resources.resource.ApplicationStatusShouldSelect + "\\n");
            }

            string statusID = string.Empty;
            sql.Clear();
            sql.AppendFormat(@"
                select distinct r.StatusID from tbl_Request r with (nolock) inner join
                tbl_Parameter p on r.StatusID = p.ParameterValue or r.ReportType = p.ParameterValue
                where r.RequestID = '{0}' and (p.Category = 'ApplicationStatus' or p.Category = 'ReportType');
            ", requestID);
            db.OpenTable(sql.ToString());
            if (db.RecordCount() > 0)
                statusID = db.Item("StatusID");

            if (statusID.Equals(ApplicationStatus.Sent))
            {
                errMsg.Append(Resources.resource.Already_Finalised + "\\n");
            }

            if (errMsg.ToString().Trim() != string.Empty)
            {
                sqlString.displayAlert2(this, errMsg.ToString());
                return false;
            }
            else
            {
                result = true;
            }

            return result;
        }
        
        protected void rptApplicantDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Literal loanRowData = (Literal)e.Item.FindControl("loanRowData");
                string loanname = DataBinder.Eval(e.Item.DataItem, "LoanName").ToString();
                Repeater rptsender = (Repeater)sender;
                RepeaterItem RPI = (RepeaterItem)rptsender.NamingContainer;
                HiddenField Hid_ApplicantType = (HiddenField)RPI.FindControl("Hid_ApplicantType");
                DataTable dt = applicantData[Hid_ApplicantType.Value];

                DataRow dr = dt.AsEnumerable().Where(x => x["LoanName"].ToString() == loanname).SingleOrDefault();

                loanRowData.Text += "<td>" + dr[0] + "</td>";

                foreach (string DC in BankList.Keys)
                {

                    loanRowData.Text += "<td>" + dr[BankList[DC]].ToString() + "</td>";
                }

            }
        }
        protected void rptApplicant_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField Hid_ApplicantType = (HiddenField)e.Item.FindControl("Hid_ApplicantType");

                Repeater rptApplicantDetails = (Repeater)e.Item.FindControl("rptApplicantDetails");
                wwdb db = new wwdb();

                DataTable loanDT = db.getDataTable("EXEC SP_GetBankLoanDetails @RequestID='" + requestID + "'  , @Applicant='" + Hid_ApplicantType.Value + "'");
                applicantData.Add(Hid_ApplicantType.Value, loanDT);
                ViewState["applicantData"] = applicantData;
                rptApplicantDetails.DataSource = loanDT;
                rptApplicantDetails.DataBind();
            }
        }
        protected void rptLoanDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header)
            {
                if (e.Item.FindControl("litStartYear") != null)
                {
                    wwdb db = new wwdb();
                    StringBuilder sql = new StringBuilder();
                    sql.Clear();
                    sql.AppendFormat(@" 
                        SELECT GeneratedDate
                        FROM tbl_CTOS_Result WITH(NOLOCK)
                        WHERE IsActive = 1 AND Request_Id = N'{0}' AND IsCoApplicant = 0
                    ", secure.RC(requestID));
                    db.OpenTable(sql.ToString());
                    DateTime GeneratedDate = DateTime.Now;
                    if (db.RecordCount() > 0)
                    {
                        GeneratedDate = Convert.ToDateTime(db.Item("GeneratedDate").ToString());
                    }

                    DateTime date = GeneratedDate;
                    int StartYear = date.Year;
                    int EndYear = date.AddMonths(-11).Year;
                    Literal ltrStartYear = e.Item.FindControl("litStartYear") as Literal;
                    ltrStartYear.Text = StartYear.ToString();
                    Literal litEndYear = e.Item.FindControl("litEndYear") as Literal;
                    litEndYear.Text = EndYear.ToString();
                    for (int i = 0; i < 12; i++)
                    {
                        string Month = date.AddMonths(-i).ToString("MMM");
                        if (i == 0)
                        {
                            Literal lbl12thMonth = e.Item.FindControl("lbl12thMonth") as Literal;
                            lbl12thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 1)
                        {
                            Literal lbl11thMonth = e.Item.FindControl("lbl11thMonth") as Literal;
                            lbl11thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 2)
                        {
                            Literal lbl10thMonth = e.Item.FindControl("lbl10thMonth") as Literal;
                            lbl10thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 3)
                        {
                            Literal lbl9thMonth = e.Item.FindControl("lbl9thMonth") as Literal;
                            lbl9thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 4)
                        {
                            Literal lbl8thMonth = e.Item.FindControl("lbl8thMonth") as Literal;
                            lbl8thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 5)
                        {
                            Literal lbl7thMonth = e.Item.FindControl("lbl7thMonth") as Literal;
                            lbl7thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 6)
                        {
                            Literal lbl6thMonth = e.Item.FindControl("lbl6thMonth") as Literal;
                            lbl6thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 7)
                        {
                            Literal lbl5thMonth = e.Item.FindControl("lbl5thMonth") as Literal;
                            lbl5thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 8)
                        {
                            Literal lbl4thMonth = e.Item.FindControl("lbl4thMonth") as Literal;
                            lbl4thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 9)
                        {
                            Literal lbl3thMonth = e.Item.FindControl("lbl3thMonth") as Literal;
                            lbl3thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 10)
                        {
                            Literal lbl2thMonth = e.Item.FindControl("lbl2thMonth") as Literal;
                            lbl2thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 11)
                        {
                            Literal lbl1thMonth = e.Item.FindControl("lbl1thMonth") as Literal;
                            lbl1thMonth.Text = Month.Substring(0, 1);
                        }
                    }
                }
            }
            else if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                CheckBox cbox = e.Item.FindControl("chkshowinreport") as CheckBox;
                if (cbox != null)
                {
                    HiddenField hidshowinreport = e.Item.FindControl("hidshowinreport") as HiddenField;
                    if (hidshowinreport.Value.ToLower() == "true")
                    {
                        cbox.Checked = true;
                    }
                    else
                    {
                        cbox.Checked = false;
                    }
                }

            }
        }
        protected void rptCoLoanDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header)
            {
                if (e.Item.FindControl("litStartYear") != null)
                {
                    wwdb db = new wwdb();
                    StringBuilder sql = new StringBuilder();
                    sql.Clear();
                    sql.AppendFormat(@" SELECT GeneratedDate
                        FROM tbl_CTOS_Result WITH(NOLOCK)
                        WHERE IsActive = 1 AND Request_Id = N'{0}' AND IsCoApplicant = 1
                    ", secure.RC(requestID));
                    db.OpenTable(sql.ToString());
                    DateTime GeneratedDate = DateTime.Now;
                    if (db.RecordCount() > 0)
                    {
                        GeneratedDate = Convert.ToDateTime(db.Item("GeneratedDate").ToString());
                    }

                    DateTime date = GeneratedDate;
                    int StartYear = date.Year;
                    int EndYear = date.AddMonths(-11).Year;
                    Literal ltrStartYear = e.Item.FindControl("litStartYear") as Literal;
                    ltrStartYear.Text = StartYear.ToString();
                    Literal litEndYear = e.Item.FindControl("litEndYear") as Literal;
                    litEndYear.Text = EndYear.ToString();
                    for (int i = 0; i < 12; i++)
                    {
                        string Month = date.AddMonths(-i).ToString("MMM");
                        if (i == 0)
                        {
                            Literal lbl12thMonth = e.Item.FindControl("lbl12thMonth") as Literal;
                            lbl12thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 1)
                        {
                            Literal lbl11thMonth = e.Item.FindControl("lbl11thMonth") as Literal;
                            lbl11thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 2)
                        {
                            Literal lbl10thMonth = e.Item.FindControl("lbl10thMonth") as Literal;
                            lbl10thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 3)
                        {
                            Literal lbl9thMonth = e.Item.FindControl("lbl9thMonth") as Literal;
                            lbl9thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 4)
                        {
                            Literal lbl8thMonth = e.Item.FindControl("lbl8thMonth") as Literal;
                            lbl8thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 5)
                        {
                            Literal lbl7thMonth = e.Item.FindControl("lbl7thMonth") as Literal;
                            lbl7thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 6)
                        {
                            Literal lbl6thMonth = e.Item.FindControl("lbl6thMonth") as Literal;
                            lbl6thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 7)
                        {
                            Literal lbl5thMonth = e.Item.FindControl("lbl5thMonth") as Literal;
                            lbl5thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 8)
                        {
                            Literal lbl4thMonth = e.Item.FindControl("lbl4thMonth") as Literal;
                            lbl4thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 9)
                        {
                            Literal lbl3thMonth = e.Item.FindControl("lbl3thMonth") as Literal;
                            lbl3thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 10)
                        {
                            Literal lbl2thMonth = e.Item.FindControl("lbl2thMonth") as Literal;
                            lbl2thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 11)
                        {
                            Literal lbl1thMonth = e.Item.FindControl("lbl1thMonth") as Literal;
                            lbl1thMonth.Text = Month.Substring(0, 1);
                        }
                    }
                }
            }
            else if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                CheckBox cbox = e.Item.FindControl("chkshowinreport") as CheckBox;
                if (cbox != null)
                {
                    HiddenField hidshowinreport = e.Item.FindControl("hidshowinreport") as HiddenField;
                    if (hidshowinreport.Value.ToLower() == "true")
                    {
                        cbox.Checked = true;
                    }
                    else
                    {
                        cbox.Checked = false;
                    }
                }
            }
        }

        #region Controls
        protected void btnView_Click(object sender, EventArgs e)
        {
            string bankID = ((Button)sender).CommandArgument.ToString();
            sqlString.OpenNewWindow_Center("/Form/Loan/DSRReportTemp.aspx?rid=" + sqlString.encryptURL(requestID) + "&bankid=" + sqlString.encryptURL(bankID), "DSR Report", 600, 800, this);

        }
        protected void btnSend_Click(object sender, EventArgs e)
        {
            wwdb db = new wwdb();
            List<string> SelectedBankList = new List<string>();
            StringBuilder sql = new StringBuilder();
            //string currentStatus = string.Empty;

            if (ValidateForm())
            {
                //advise
                sql.Append("UPDATE tbl_TApplicantCreditAdvisor SET IsDeleted = 1 WHERE RequestID = '" + requestID + "' AND IsDeleted = 0 ; ");
                if (chkAdvisorList.SelectedIndex != -1)
                    foreach (ListItem li in chkAdvisorList.Items)
                        if (li.Selected)
                            sql.Append("INSERT INTO tbl_TApplicantCreditAdvisor(requestid , advisorid ) VALUES('" + requestID + "','" + li.Value + "');");

                //sql.Append("DELETE FROM tbl_tapplicantcreditadvisor where requestid=N'" + requestID + "';");
                //if (!string.IsNullOrEmpty(txtComment.Text))
                sql.Append("INSERT INTO tbl_TApplicantCreditAdvisor(requestid , advisorid ,comment, message) VALUES('" + requestID + "','0','"
                    + txtComment.Text + "', '" + txtMessage.Text + "');");

                foreach (RepeaterItem item in rptLoanDetails.Items)
                {
                    //to get the dropdown of each line
                    CheckBox chkshowinreport = (CheckBox)item.FindControl("chkshowinreport");
                    HiddenField hidid = (HiddenField)item.FindControl("hidid");

                    sql.Append("UPDATE tbl_CTOS_Report_Loan_Details SET IsShowinReport = " + (chkshowinreport.Checked ? "1" : "0") + " WHERE ID = '" + hidid.Value + "';");
                }
                foreach (RepeaterItem item in rptCoLoanDetails.Items)
                {
                    //to get the dropdown of each line
                    CheckBox chkshowinreport = (CheckBox)item.FindControl("chkshowinreport");
                    HiddenField hidid = (HiddenField)item.FindControl("hidid");

                    sql.Append("UPDATE tbl_CTOS_Report_Loan_Details SET IsShowinReport = " + (chkshowinreport.Checked ? "1" : "0") + " WHERE ID = '" + hidid.Value + "';");
                }

                db.Execute(sql.ToString());//save

                foreach (RepeaterItem RPI in rptHeader.Items)
                {
                    CheckBox chkSelect = (CheckBox)RPI.FindControl("chkSelect");
                    HiddenField Hid_Bank = (HiddenField)RPI.FindControl("Hid_Bank");

                    if (chkSelect.Checked)
                    {
                        SelectedBankList.Add(Hid_Bank.Value);
                    }
                }

                if (SelectedBankList.Count < 1)
                    //sqlString.displayAlert2(this, Resources.resource.Err_Bank_Empty_Checked);
                    SelectedBankList.Add("12");//auto select Classic Bank if it's not selected
                //else{
                ReportController reportController = new ReportController();
                bool result = reportController.SaveReport(requestID, SelectedBankList, true);

                if (result)
                    sqlString.displayAlert2(this, Resources.resource.Action_Successful, "/Form/Report/ClientList.aspx");
                //}
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            if (ValidateForm())
            {
                sql.AppendFormat(@"
                    UPDATE tbl_Request SET StatusID ='{0}' , 
                    ApplicationResult = {1}
                    WHERE RequestID = '{2}';
                ", ApplicationStatus.InProgress, 
                rblResult.SelectedIndex == -1 ? "null" : "'" + rblResult.SelectedValue + "'"
                , requestID);
                sql.AppendFormat(@"
                    UPDATE tbl_TApplicantCreditAdvisor SET IsDeleted = 1 
                    WHERE RequestID = '{0}' AND IsDeleted = 0;
                ", requestID);
                if (chkAdvisorList.SelectedIndex != -1)
                    foreach (ListItem li in chkAdvisorList.Items)
                        if (li.Selected)
                            sql.AppendFormat(@"
                                INSERT INTO tbl_TApplicantCreditAdvisor(requestid, advisorid) 
                                VALUES('{0}','{1}');
                            ", requestID, li.Value);

                string autoCode = string.Empty;
                db.OpenTable("SELECT autocode FROM tbl_tapplicantcreditadvisor where requestid = N'" + requestID + "'");
                if (db.RecordCount() > 0)
                    autoCode = db.Item("autocode");
                sql.Append("DELETE FROM tbl_tapplicantcreditadvisor where requestid=N'" + requestID + "';");
                //if (!string.IsNullOrEmpty(txtComment.Text))
                sql.AppendFormat(@"
                    INSERT INTO tbl_TApplicantCreditAdvisor(requestid, advisorid, comment, message, autocode) 
                    VALUES('{0}','0','{1}', '{2}', '{3}');
                ", requestID, txtComment.Text, txtMessage.Text, autoCode);

                foreach (RepeaterItem item in rptLoanDetails.Items)
                {
                    //to get the dropdown of each line
                    CheckBox chkshowinreport = (CheckBox)item.FindControl("chkshowinreport");
                    HiddenField hidid = (HiddenField)item.FindControl("hidid");

                    sql.Append("UPDATE tbl_CTOS_Report_Loan_Details SET IsShowinReport = " + (chkshowinreport.Checked ? "1" : "0") + " WHERE ID = '" + hidid.Value + "';");
                }

                foreach (RepeaterItem item in rptCoLoanDetails.Items)
                {
                    //to get the dropdown of each line
                    CheckBox chkshowinreport = (CheckBox)item.FindControl("chkshowinreport");
                    HiddenField hidid = (HiddenField)item.FindControl("hidid");

                    sql.Append("UPDATE tbl_CTOS_Report_Loan_Details SET IsShowinReport = " + (chkshowinreport.Checked ? "1" : "0") + " WHERE ID = '" + hidid.Value + "';");
                }

                db.Execute(sql.ToString());

                if (!db.HasError)
                    sqlString.displayAlert2(this, Resources.resource.Action_Successful, Request.Url.AbsoluteUri);
            }
        }
        protected void btnViewMACTOSReport_Click(object sender, EventArgs e)
        {
            sqlString.OpenNewWindow_Center("/Form/Loan/CTOSReport.aspx?rid=" + sqlString.encryptURL(requestID) + "&type=" + sqlString.encryptURL("0"), "DSR Report", 600, 800, this);
        }
        protected void btnViewCACTOSReport_Click(object sender, EventArgs e)
        {
            sqlString.OpenNewWindow_Center("/Form/Loan/CTOSReport.aspx?rid=" + sqlString.encryptURL(requestID) + "&type=" + sqlString.encryptURL("1"), "DSR Report", 600, 800, this);
        }
        protected void btnViewMARAMCIReport_Click(object sender, EventArgs e)
        {
            sqlString.OpenNewWindow_Center("/Form/Loan/RAMCIReport.aspx?rid=" + sqlString.encryptURL(requestID) + "&type=" + sqlString.encryptURL("0"), "DSR Report", 800, 800, this);
        }
        protected void btnViewCARAMCIReport_Click(object sender, EventArgs e)
        {
            sqlString.OpenNewWindow_Center("/Form/Loan/RAMCIReport.aspx?rid=" + sqlString.encryptURL(requestID) + "&type=" + sqlString.encryptURL("1"), "DSR Report", 800, 800, this);
        }
        #endregion

        protected void chkCAshowall_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chkCAshowall = (CheckBox)rptCoLoanDetails.Controls[0].Controls[0].FindControl("chkCAshowall");
            foreach (RepeaterItem item in rptCoLoanDetails.Items)
            {
                CheckBox chkshowinreport = (CheckBox)item.FindControl("chkshowinreport");
                if (chkCAshowall.Checked)
                    chkshowinreport.Checked = true;
                else
                    chkshowinreport.Checked = false;
            }
        }
        protected void chkMAshowall_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chkMAshowall = (CheckBox)rptLoanDetails.Controls[0].Controls[0].FindControl("chkMAshowall");
            foreach (RepeaterItem item in rptLoanDetails.Items)
            {
                CheckBox chkshowinreport = (CheckBox)item.FindControl("chkshowinreport");
                if (chkMAshowall.Checked)
                    chkshowinreport.Checked = true;
                else
                    chkshowinreport.Checked = false;
            }
        }
    }
}