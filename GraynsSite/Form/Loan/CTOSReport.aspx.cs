﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Util;
using Synergy.Helper;
using HJT.Cls.Helper;
using System.IO;
using System.Xml.Xsl;
using System.Xml;

namespace HJT.Form.Loan
{
    public partial class CTOSReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request["RID"] as string) &&
                    !string.IsNullOrEmpty(Request["TYPE"] as string))
                {
                    string requestID = sqlString.decryptURL(Validation.GetUrlParameter(this.Page, "RID", ""));
                    string type = sqlString.decryptURL(Validation.GetUrlParameter(this.Page, "TYPE", ""));
                    string tempID = sqlString.decryptURL(Validation.GetUrlParameter(this.Page, "TID", ""));

                    wwdb db = new wwdb();
                    StringBuilder sql = new StringBuilder();

                    DateTime requestDate = DateTime.Now;
                    double expirationDay = 0;
                    sql.Clear();
                    sql.AppendFormat(@"
                        select top 1 p.ParameterValue as 'Day' from tbl_Parameter p with (nolock) 
                        where p.Category = 'ReportExpiration' and p.ParameterName = 'ReportExpirationDay'
                    ");
                    db.OpenTable(sql.ToString());
                    if (db.RecordCount() > 0)
                    {
                        expirationDay = double.Parse(db.Item("Day"));
                        sql.Clear();
                        sql.AppendFormat(@"
                            select r.RequestAt from tbl_Request r with (nolock) 
                            where r.RequestID = N'{0}'
                        ", requestID);
                        db.OpenTable(sql.ToString());
                        if (db.RecordCount() > 0)
                            requestDate = ConvertHelper.ConvertToDateTime(db.Item("RequestAt"), DateTime.Now);
                    }

                    if (DateTime.Now < requestDate.AddDays(expirationDay))
                    {
                        if (!string.IsNullOrWhiteSpace(requestID) && !string.IsNullOrWhiteSpace(type))
                        {
                            string xmlString = GetReportXml(requestID, type, tempID);
                            xmlString = xmlString.Replace(string.Format(@"<user id=""synmo_xml"">Synergy Moments Sdn Bhd</user>"),
                                string.Format(@"<user id="" ""> </user>"))
                                .Replace(string.Format(@"<company id=""SYNMO"">Synergy Moments Sdn Bhd : B2b Production</company>"),
                                string.Format(@"<company id="" ""> </company>"))
                                .Replace(string.Format(@"<account>SYNMO</account>"),
                                string.Format(@"<account> </account>"));

                            XmlReader xmlReader = XmlReader.Create(new StringReader(xmlString));
                            var myXslTrans = new XslCompiledTransform();
                            myXslTrans.Load(Server.MapPath(@"..\..\CTOS\ctos_report.xsl"));

                            StringBuilder builder = new StringBuilder();
                            using (StringWriter stringWriter = new StringWriter(builder))
                            {
                                using (XmlTextWriter htmlTextWriter = new XmlTextWriter(stringWriter))
                                {
                                    try
                                    {
                                        myXslTrans.Transform(xmlReader, htmlTextWriter);
                                    }
                                    catch (Exception ex)
                                    {
                                        LogUtil.logError(ex.Message, "Report reading error. RequestID: " + requestID + ". Type: " + type);
                                        sqlString.displayAlert(this, ex.Message + " .\\n" +
                                            Resources.resource.unknownError);
                                    }
                                }
                            }
                            string transformedHtml = builder.ToString();

                            Response.Write(transformedHtml);
                        }
                    }
                    else
                        divReportExpiration.Visible = true;
                }
                else
                    divErrorReport.Visible = true;
            }
        }

        private string GetReportXml(string requestId, string type, string tempId)
        {
            string xml = string.Empty;
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();
            sql.Clear();
            sql.Append(" SELECT Result ");
            sql.Append(" FROM tbl_CTOS_Result WITH(NOLOCK) ");
            sql.Append(" WHERE IsActive = 1");
            if (!string.IsNullOrWhiteSpace(requestId) && requestId != "0")
                sql.Append("  AND Request_Id = N'" + secure.RC(requestId) + "'");
            else
                sql.Append("  AND TempId = N'" + tempId + "'");
            sql.Append(" AND IsCoApplicant = N'" + secure.RC(type) + "' Order By ID");
            db.OpenTable(sql.ToString());

            if (db.RecordCount() > 0)
            {
                xml = db.Item("Result").ToString();
            }

            return xml;
        }

    }
}