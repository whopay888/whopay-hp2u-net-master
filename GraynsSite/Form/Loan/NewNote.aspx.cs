﻿using HJT.Cls.Controller;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Controller;
using Synergy.Helper;
using Synergy.Model;
using Synergy.Util;

namespace HJT.Form.Loan
{
    public partial class NewNote : Page
    {
        LoginUserModel login_user = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            login_user = (LoginUserModel)Session[KeyVal.loginsecure];
            if (!IsPostBack)
            {
                string requestID = Request["rid"] as string;

                if (!string.IsNullOrEmpty(requestID))
                {
                    requestID = sqlString.decryptURL(Request["rid"].ToString());
                    hfRequestID.Value = requestID;
                    List<string> access = checkACL();

                    if (access.Contains("function_sequence_of_events"))
                    {
                        tab_1_content.Visible = true;
                        tab_2_content.Visible = true;
                        bindData();
                        sqlString.bindControl(gv, getHistory(null));
                        checkACL_SOE(hfRequestID.Value);
                    }
                    if (access.Contains("function_edit_request"))
                    {
                        tab_3_content.Visible = true;
                        bindUserPageData();
                    }
                }
            }

            if (gv.Rows.Count > 0)
                gridView.ApplyPaging(gv, -1);
        }

        private List<string> checkACL()
        {
            List<string> access = new List<string>();
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            sql.Clear();
            sql.AppendFormat(@"
                select m.[Key] from tbl_MemberRole mr with (nolock)
                inner join tbl_RoleMenu rm with (nolock) on rm.RoleID = mr.RoleCode
                inner join tbl_Menu m with (nolock) on m.rowID = rm.MenuID
                where (m.[Key] = 'function_edit_request' or m.[Key] = 'function_sequence_of_events') 
				and mr.MemberId = '{0}'
            ", login_user.UserId);
            db.OpenTable(sql.ToString());
            if (db.RecordCount() > 0)
                while (!db.Eof())
                {
                    access.Add(db.Item("Key"));
                    db.MoveNext();
                }

            return access;
        }

        #region SequenceOfEvent
        private void checkACL_SOE(string requestID)
        {
            wwdb db = new wwdb();
            wwdb db2 = new wwdb();
            StringBuilder sql = new StringBuilder();

            db.OpenTable("SELECT RequestBy FROM tbl_Request WHERE RequestID=N'" + requestID + "'");
            if (db.RecordCount() > 0)
            {
                sql.AppendFormat(@"
                    select rm.* from tbl_RoleMenu rm with (nolock)
                    inner join tbl_Menu m with (nolock) on rm.MenuID=m.rowID
                    where m.[Key] = 'function_insert_others_sequence_of_events' 
                    and rm.RoleID IN ({0})
                ", string.Join(",", login_user.UserRole));
                db2.OpenTable(sql.ToString());

                if (db.Item("RequestBy").Equals(login_user.UserId) || (db2.RecordCount() > 0))
                {
                    divManagerMsg.Visible = false;
                    btnSave.Visible = true;
                }
                else
                {
                    divManagerMsg.Visible = true;
                    btnSave.Visible = false;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Tab", "$('#HidTab').val('#tab_2');", true);
                }
            }

            if (login_user.isStaffAdmin())
                divPaymentStatus.Visible = true;
            else
                divPaymentStatus.Visible = false;
        }

        private string getHistory(IDictionary<string, string> sortBy)
        {
            StringBuilder sql = new StringBuilder();

            //CASE WHEN Bank IS NULL THEN '-'
	        //                 WHEN Bank LIKE '' THEN '-'
	        //                 WHEN Bank LIKE '%NULL%' THEN '-' ELSE Bank END AS 'Bank', 
            //                CASE WHEN LetterOffer IS NULL THEN '-'
	        //                 WHEN LetterOffer LIKE '' THEN '-'
	        //                 WHEN LetterOffer LIKE '%NULL%' THEN '-' ELSE LetterOffer END AS 'LetterOffer', 
            //                CASE WHEN LoanAgreement IS NULL THEN '-'
	        //                 WHEN LoanAgreement LIKE '' THEN '-'
	        //                 WHEN LoanAgreement LIKE '%NULL%' THEN '-' ELSE LoanAgreement END AS 'LoanAgreement', 

            sql.AppendFormat(@" 
                SELECT n.UpdatedAt as 'Date', 
                CASE WHEN n.[Document] IS NULL THEN '-'
	                WHEN n.[Document] LIKE '' THEN '-'
	                WHEN n.[Document] LIKE '%NULL%' THEN '-' ELSE p.ParameterName END AS 'Document', 
                n.NoteContent as 'Remark', mi.Fullname as 'CreatedBy'
                FROM tbl_Note n with (nolock) inner join
                tbl_Parameter p with (nolock) on n.Document = p.ParameterValue right join
				tbl_MemberInfo mi with (nolock) on n.CreatedBy = mi.MemberId
                where p.Category = 'StatusDocument' and RequestID = N'{0}' 
            ", hfRequestID.Value);
            sql.Append("order by ");

            //ordering
            if (sortBy != null && sortBy.Any())
            {
                int i = 0;
                foreach (var item in sortBy)
                {
                    string key = item.Key;
                    string value = item.Value;

                    if (i > 0)
                        sql.Append(",");

                    switch (key)
                    {
                        case "Date":
                            sql.Append("UpdatedAt " + value);
                            break;
                        default:
                            break;
                    }
                    i++;
                }
            }
            else
                sql.Append("n.UpdatedAt desc");

            return sql.ToString();
        }
        
        protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            Dictionary<string, string> sortBy = new Dictionary<string, string>();
            if (ViewState["SortBy"] != null)
            { sortBy = (Dictionary<string, string>)ViewState["SortBy"]; }

            gv.PageIndex = e.NewPageIndex;
            sqlString.bindControl(gv, getHistory(sortBy));
            
            if (gv.Rows.Count > 0)
                gridView.ApplyPaging(gv, e.NewPageIndex);

        }

        protected void gv_Sorting(object sender, GridViewSortEventArgs e)
        {
            string expression = e.SortExpression;
            string direction = null;

            //3 state sorting
            //ASC,DESC,null
            if (GetGridViewSortDirection(expression) == "ASC")
            {
                SetGridViewSortDirection(expression, null);
                direction = null;
            }
            else if (GetGridViewSortDirection(expression) == null)
            {
                SetGridViewSortDirection(expression, "ASC");
                direction = "ASC";
            }

            Dictionary<string, string> sortBy = new Dictionary<string, string>();
            if (direction != null)
                sortBy.Add(e.SortExpression, direction);

            ViewState["SortBy"] = sortBy;

            sqlString.bindControl(gv, getHistory(sortBy));

            if (gv.Rows.Count > 0)
                gridView.ApplyPaging(gv, -1);
        }

        private string GetGridViewSortDirection(string sortExpression)
        {
            if (ViewState["sortDirection" + sortExpression] == null)
                return null;
            else
                return (string)ViewState["sortDirection" + sortExpression];
        }

        private void SetGridViewSortDirection(string sortExpression, string value)
        {
            ViewState["sortDirection" + sortExpression] = value;
        }

        private void bindData()
        {
            StringBuilder sql = new StringBuilder();
            wwdb db = new wwdb();

            sql.Clear();
            sql.Append("SELECT PaymentStatus FROM tbl_Request WHERE RequestID=N'" + hfRequestID.Value + "'");
            db.OpenTable(sql.ToString());
            if (db.RecordCount() > 0)
                if (!string.IsNullOrEmpty(db.Item("PaymentStatus")))
                {
                    btnSuccess.Visible = false;
                    btnFail.Visible = false;
                    lblPaymentStatus.Text = db.Item("PaymentStatus").Equals("1") ? Resources.resource.Success : Resources.resource.Failed;
                }
                else
                {
                    btnSuccess.Visible = true;
                    btnFail.Visible = true;
                    lblPaymentStatus.Text = Resources.resource.None;
                }

            //sql.Append("SELECT CASE WHEN ParameterName='None' THEN '-' ELSE ParameterName END AS '0', ParameterValue AS '1' " +
            //    "FROM tbl_Parameter WHERE Category='Status' ORDER BY Sort");
            sql.Clear();
            sql.Append("SELECT ParameterValue + '.' + ParameterName AS '0', ParameterValue AS '1' FROM tbl_Parameter WITH (NOLOCK) WHERE Category='StatusDocument' ORDER BY Sort");
            sqlString.bindControl(ddlDocument, sql.ToString(), "0", "1", false);

            sql.Clear();
            sql.AppendFormat(@" 
                SELECT top (1) p.ParameterValue as 'Document'
                FROM tbl_Note with (nolock) inner join
                tbl_Parameter p with (nolock) on tbl_Note.Document = p.ParameterValue
                where p.Category = 'StatusDocument' and RequestID = N'{0}'
                ORDER BY id desc
            ", hfRequestID.Value);
            db.OpenTable(sql.ToString());
            if (db.RecordCount() > 0 && !string.IsNullOrEmpty(db.Item("Document")))
                ddlDocument.SelectedIndex = ConvertHelper.ConvertToInt(db.Item("Document"), 1) - 1;
            //sql.Clear();
            //sql.Append("SELECT ParameterName AS '0', ParameterValue AS '1' FROM tbl_Parameter WITH (NOLOCK) WHERE Category='StatusBank' ORDER BY Sort");
            //sqlString.bindControl2(ddlBank, sql.ToString(), "0", "1", true);
            //sql.Clear();
            //sql.Append("SELECT ParameterName AS '0', ParameterValue AS '1' FROM tbl_Parameter WITH (NOLOCK) WHERE Category='StatusLO' ORDER BY Sort");
            //sqlString.bindControl2(ddlLetterOffer, sql.ToString(), "0", "1", true);
            //sql.Clear();
            //sql.Append("SELECT ParameterName AS '0', ParameterValue AS '1' FROM tbl_Parameter WITH (NOLOCK) WHERE Category='StatusLA' ORDER BY Sort");
            //sqlString.bindControl2(ddlLoanAgreement, sql.ToString(), "0", "1", true);

            if (!string.IsNullOrEmpty(hfRequestID.Value))
            {
                txtRequestID.Text = hfRequestID.Value;

                sql.Clear();
                sql.AppendFormat(@"
                    SELECT ApplicationResult, RequestAt, ProjectID, ProjectName
                    FROM tbl_Request with (nolock)
                    WHERE RequestID = N'{0}'
                ", hfRequestID.Value);
                db.OpenTable(sql.ToString());

                if (db.RecordCount() > 0)
                {
                    txtRequestDate.Text = db.Item("RequestAt");

                    string applicationResultString = db.Item("ApplicationResult");
                    wwdb db2 = new wwdb();
                    db2.OpenTable("SELECT ParameterName, ParameterValue FROM tbl_Parameter WITH (NOLOCK) WHERE Category = 'ApplicationResult'");
                    if (db2.RecordCount() > 0)
                        while (!db2.Eof())
                        {
                            if (applicationResultString.Equals(db2.Item("ParameterValue")))
                            {
                                txtApplicationResult.Text = db2.Item("ParameterName");
                                break;
                            }
                            db2.MoveNext();
                        }

                    string projectID = string.Empty;
                    projectID = db.Item("ProjectID");
                    if (!string.IsNullOrEmpty(db.Item("ProjectName")))
                        txtProjectName.Text = db.Item("ProjectName");//get projectname if it's other proj

                    if (!string.IsNullOrEmpty(projectID))//get ProjectName if project in ProjectSettings
                    {
                        sql.Clear();
                        sql.Append("SELECT ProjectName FROM tbl_ProjectSettings with (nolock) WHERE ID = '" + projectID + "'");
                        db.OpenTable(sql.ToString());

                        if (db.RecordCount() > 0)
                            txtProjectName.Text = db.Item("ProjectName");
                    }
                }

                sql.Clear();
                sql.Append("SELECT Name FROM tbl_Applicants WITH (NOLOCK) WHERE RequestID='" + hfRequestID.Value + "' AND ApplicantType='MA'");
                db.OpenTable(sql.ToString());
                if (db.RecordCount() > 0)
                    if (!string.IsNullOrEmpty(db.Item("Name")))
                        txtApplicantName.Text = db.Item("Name");
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (validateForm())
            {
                StringBuilder sql = new StringBuilder();
                wwdb db = new wwdb();

                db.OpenTable("SELECT TOP 1 NoteID FROM tbl_Note ORDER BY ID DESC");
                int noteID = ConvertHelper.ConvertToInt(db.Item("NoteID"), 0) + 1;

                sql.Clear();
                //sql.Append("INSERT INTO tbl_Note (NoteID, RequestID, NoteContent, IsDeleted, CreatedBy, CreatedAt, UpdatedBy, UpdatedAt, " +
                //    "Bank, Document, LetterOffer, LoanAgreement) " +
                //    "VALUES('" + noteID.ToString() + "', N'" + hfRequestID.Value + "', N'" + (string.IsNullOrEmpty(txtRemark.Text) ? (object)DBNull.Value : txtRemark.Text) + "', " +
                //    "'0', '" + login_user.UserId + "', GETDATE(), '" + login_user.UserId + "', GETDATE(), '" +
                //    ((ddlBank.SelectedIndex > 0) ? ddlBank.SelectedItem.ToString() : "NULL") + "', '" +
                //    ((ddlDocument.SelectedIndex > 0) ? ddlDocument.SelectedItem.ToString() : "NULL") + "', '" +
                //    ((ddlLetterOffer.SelectedIndex > 0) ? ddlLetterOffer.SelectedItem.ToString() : "NULL") + "', '" +
                //    ((ddlLoanAgreement.SelectedIndex > 0) ? ddlLoanAgreement.SelectedItem.ToString() : "NULL") + "')");
                sql.AppendFormat(@"
                    INSERT INTO tbl_Note (NoteID, RequestID, NoteContent, IsDeleted, CreatedBy, CreatedAt, UpdatedBy, UpdatedAt, Document) 
                    VALUES('{0}', N'{1}', N'{2}', '0', '{3}', GETDATE(), '{3}', GETDATE(), '{4}')
                ", noteID.ToString()
                , hfRequestID.Value
                , string.IsNullOrEmpty(txtRemark.Text) ? (object)DBNull.Value : txtRemark.Text
                , login_user.UserId
                , ddlDocument.SelectedValue);
                db.Execute(sql.ToString());
                LogUtil.logAction(sql.ToString(), "INSERT New Note into tbl_Note. RequestID:" + hfRequestID.Value + ". NoteID:" + noteID);

                if (!db.HasError)
                {
                    sqlString.displayAlert(this, Resources.resource.ActionSuccessfulMsg);
                    refresh();
                }
                else
                    sqlString.displayAlert(this, db.ErrorMessage);
            }
        }

        protected void btnSuccess_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(hfRequestID.Value))
            {
                wwdb db = new wwdb();
                Button btnsender = (Button)sender;
                //string rid = btnsender.CommandArgument.ToString();
                string rid = hfRequestID.Value;
                string sql = "UPDATE tbl_Request SET PaymentStatus = 1 WHERE RequestID = " + rid;
                db.Execute(sql);

                #region Billing
                SqlParameter[] billingSPParams = {
                     new SqlParameter("@RequestID", rid),
                      new SqlParameter("@RequestType", "Successful Case"),
                       new SqlParameter("@CreatedBy", login_user.UserId)
                };

                db.executeScalarSP("SP_AddBillingTransaction", billingSPParams);

                //StringBuilder sql1 = new StringBuilder();
                //sql1.Append("SELECT tr.PerSuccessfulCasePrice,tr.PerRequestPrice,tr.FreeRequestCount, COUNT(ta.ID) AS ApplicantCount, tr.MemberID FROM dbo.tbl_Request tr LEFT JOIN dbo.tbl_Applicants ta ON tr.RequestID = ta.RequestID WHERE  tr.RequestId =" + rid + " GROUP BY tr.PerSuccessfulCasePrice,tr.PerRequestPrice,tr.FreeRequestCount, tr.MemberID");
                //db.OpenTable(sql1.ToString());
                //if (db.RecordCount() > 0)
                //{
                //    var requestAmount = db.Item("PerSuccessfulCasePrice");
                //    var MemberId = db.Item("MemberId").ToString();
                //    var FreeRequestCount = ConvertHelper.ConvertToDecimal(db.Item("FreeRequestCount").ToString(), 0);
                //    var ApplicantCount = db.Item("ApplicantCount").ToString();
                //    Decimal amount = Convert.ToDecimal(requestAmount) * (FreeRequestCount > 0 ? FreeRequestCount : Convert.ToInt32(ApplicantCount));
                //    int IsFreeRequest = (Convert.ToDecimal(FreeRequestCount) > 0 ? 1 : 0);

                //    StringBuilder sql3 = new StringBuilder();
                //    sql3.Append("SELECT * FROM tbl_UserTransactions WHERE RequestType = 'Request' AND RequestId =" + rid);
                //    db.OpenTable(sql3.ToString());

                //    StringBuilder sql2 = new StringBuilder();
                //    if (db.RecordCount() > 1)
                //    {
                //        sql2.Append("INSERT INTO tbl_UserTransactions(MemberId,Amount,Status,RequestType,RequestId,CreatedAt,IsFreeRequest,Remark) VALUES('" + MemberId + "',-" + amount + ",1,'Successful Case'," + rid + ", GETDATE(),1,'TWO APPLICANTS (1 FREE REQUEST)');");
                //        sql2.Append("INSERT INTO tbl_UserTransactions(MemberId,Amount,Status,RequestType,RequestId,CreatedAt,IsFreeRequest,Remark) VALUES('" + MemberId + "',-" + amount + ",1,'Successful Case'," + rid + ", GETDATE(),0,'TWO APPLICANTS (NON FREE REQUEST)');");
                //        sql2.Append("IF (SELECT 1 FROM tbl_UserCreditBalance WHERE MemberID ='" + MemberId + "') > 0 ");
                //        sql2.Append("UPDATE tbl_UserCreditBalance SET CreditUsed = ISNULL(CreditUsed,0) - " + amount + " WHERE MemberId ='" + db.Item("MemberId").ToString() + "';");
                //        sql2.Append("ELSE ");
                //        sql2.Append("INSERT INTO tbl_UserCreditBalance(MemberId,CreditUsed,CreatedAt) VALUES('" + MemberId + "',-" + amount + ",GETDATE()); ");
                //    }
                //    else if(db.RecordCount() == 1 && IsFreeRequest == 1)
                //    {
                //        sql2.Append("INSERT INTO tbl_UserTransactions(MemberId,Amount,Status,RequestType,RequestId,CreatedAt,IsFreeRequest) VALUES('" + MemberId + "',-" + amount + ",1,'Successful Case'," + rid + ", GETDATE(),1);");
                //    }

                //    if (IsFreeRequest == 0)
                //    {
                //        sql2.Append("INSERT INTO tbl_UserTransactions(MemberId,Amount,Status,RequestType,RequestId,CreatedAt,IsFreeRequest) VALUES('" + MemberId + "',-" + amount + ",1,'Successful Case'," + rid + ", GETDATE(),0);");
                //        sql2.Append("IF (SELECT 1 FROM tbl_UserCreditBalance WHERE MemberID ='" + MemberId + "') > 0 ");
                //        sql2.Append("UPDATE tbl_UserCreditBalance SET CreditUsed = ISNULL(CreditUsed,0) - " + amount + " WHERE MemberId ='" + db.Item("MemberId").ToString() + "';");
                //        sql2.Append("ELSE ");
                //        sql2.Append("INSERT INTO tbl_UserCreditBalance(MemberId,CreditUsed,CreatedAt) VALUES('" + MemberId + "',-" + amount + ",GETDATE());");
                //    }
                //    db.Execute(sql2.ToString());
                //}
                #endregion

                // sqlString.bindControl(gv, getClientList(null));
                lblPaymentStatus.Text = Resources.resource.Success;
                btnSuccess.Visible = false;
                btnFail.Visible = false;
            }
        }

        protected void btnFailed_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(hfRequestID.Value))
            {
                wwdb db = new wwdb();
                Button btnsender = (Button)sender;
                //string rid = btnsender.CommandArgument.ToString();
                string rid = hfRequestID.Value;
                string sql = "UPDATE tbl_Request SET PaymentStatus = 0 WHERE RequestID = " + rid;
                db.Execute(sql);
                //sqlString.bindControl(gv, getClientList(null));
                lblPaymentStatus.Text = Resources.resource.Failed;
                btnSuccess.Visible = false;
                btnFail.Visible = false;
            }
        }

        private void refresh()
        {
            txtRemark.Text = "";
            bindData();
            //ddlBank.SelectedIndex = 0;
            //ddlLetterOffer.SelectedIndex = 0;
            //ddlLoanAgreement.SelectedIndex = 0;
            sqlString.bindControl(gv, getHistory(null));
        }

        private bool validateForm()
        {
            StringBuilder errorMsg = new StringBuilder();

            //if (ddlDocument.SelectedIndex <= 0 && ddlBank.SelectedIndex <= 0
            //    && ddlLetterOffer.SelectedIndex <= 0 && ddlLoanAgreement.SelectedIndex <= 0)
            if (ddlDocument.SelectedIndex <= -1) //temp off
                errorMsg.Append(Resources.resource.Please_Select_Status+"\\n");

            if (!string.IsNullOrEmpty(txtRemark.Text))
            {
                ReportController reportController = new ReportController();
                TextBox textBox = null;
                reportController.removeInvalidChar(textBox, textBox, txtRemark);
            }

            if (!string.IsNullOrEmpty(errorMsg.ToString()))
                lblErr.Text = errorMsg.ToString().Replace("\\n", "</br>");

            if (lblErr.Text != string.Empty)
            {
                divErrorMessage.Visible = true;
                sqlString.displayAlert2(this, errorMsg.ToString());
                return false;
            }

            return true;
        }
        #endregion

        #region User Page
        private void bindUserPageData()
        {
            if (!string.IsNullOrEmpty(hfRequestID.Value))
            {
                wwdb db = new wwdb();
                StringBuilder sql = new StringBuilder();

                txtCurrentCommitment.Enabled = false;
                txtPurchasePrice.Enabled = false;
                txtIncomeRequired.Enabled = false;
                txtDSR.Enabled = false;
                txtNewInstallment.Enabled = false;

                #region Request Information
                sql.Clear();
                sql.AppendFormat(@"
                    SELECT MemberID, BranchID, ApplicationResult, ProjectID, ProjectName
                    FROM tbl_Request with (nolock)
                    WHERE RequestID = N'{0}'
                ", hfRequestID.Value);
                db.OpenTable(sql.ToString());
                if (db.RecordCount() > 0)
                {
                    string memberID, branchID, projectID;
                    memberID = db.Item("MemberID");
                    branchID = db.Item("BranchID");

                    #region ApplicationResult
                    string applicationResultString = db.Item("ApplicationResult");
                    if (applicationResultString.Equals(ApplicationResult.Approved))
                        lblApplicationResult.Text = Resources.resource.Green;
                    else if (applicationResultString.Equals(ApplicationResult.Denied))
                        lblApplicationResult.Text = Resources.resource.Red;
                    else if (applicationResultString.Equals(ApplicationResult.Modify))
                        lblApplicationResult.Text = Resources.resource.Yellow;
                    #endregion

                    #region ProjectName
                    projectID = db.Item("ProjectID");
                    if (!db.Item("ProjectName").Equals(string.Empty))
                        lblProjectName.Text = db.Item("ProjectName") + " (" + Resources.resource.Other_Project + ")";//get projectname if it's other proj

                    if (!projectID.Equals(string.Empty))//get ProjectName if project in ProjectSettings
                    {
                        sql.Clear();
                        sql.Append("SELECT ProjectName FROM tbl_ProjectSettings with (nolock) WHERE ID = '" + projectID + "'");
                        db.OpenTable(sql.ToString());

                        if (db.RecordCount() > 0)
                            lblProjectName.Text = db.Item("ProjectName");
                    }
                    #endregion

                    #region Member
                    //get member name
                    sql.Clear();
                    sql.Append("SELECT Fullname FROM tbl_MemberInfo with (nolock) WHERE MemberID = '" + memberID + "'");
                    db.OpenTable(sql.ToString());

                    if (db.RecordCount() > 0)
                        lblMemberName.Text = db.Item("Fullname");

                    //get member branch
                    sql.Clear();
                    sql.Append("SELECT BranchName FROM tbl_Branch with (nolock) WHERE BranchID = '" + branchID + "'");
                    db.OpenTable(sql.ToString());

                    if (db.RecordCount() > 0)
                        lblBranchName.Text = db.Item("BranchName");
                    #endregion

                    #region Advise
                    lblAdvise.Text = "";
                    DataTable dt = db.getDataTable("SELECT Comment FROM tbl_ApplicantCreditAdvisor WITH(NOLOCK) WHERE RequestID = N'" + hfRequestID.Value + "' AND IsDeleted = 0");
                    foreach (DataRow dr in dt.Rows)
                        if (dr["Comment"].ToString() != string.Empty)
                            lblAdvise.Text += dr["Comment"].ToString();
                    lblAdvise.Text = lblAdvise.Text.Replace("</br>", Environment.NewLine).Replace("&amp;", "&")
                        .Replace("&gt;", "").Replace("&#x0D;", Environment.NewLine).Trim('<').Trim('>');
                    #endregion

                    #region Applicant
                    sql.Clear();
                    sql.AppendFormat(@"
                        with cte as 
                        (select cr.Request_Id, cr.ID, cr.[Type], cr.IsCoApplicant 
                        from tbl_CTOS_Result cr with (nolock)
                        WHERE cr.IsActive=1 and cr.Result is not null)

                        SELECT a.[Name] as 'Name'
	                        ,a.MyCard as 'MyCard'
	                        ,a.ApplicantType as 'ApplicantType'
	                        ,cte.ID as 'CCRIS'
                            ,cte.[Type] as 'ReportType'
                        FROM tbl_Applicants a with (nolock) left join
                        cte on cte.Request_Id=a.RequestID and 
	                        cte.IsCoApplicant= 
		                        (select case when ta.ApplicantType='CA' 
					                        then 1 else 0 
				                        end as 'ApplicantType'
		                        from tbl_Applicants ta with (nolock) 
		                        where ta.ID=a.ID)
                        where a.RequestID='{0}' and a.IsDeleted=0 
                        order by a.ApplicantType desc;
                    ", hfRequestID.Value);
                    db.OpenTable(sql.ToString());
                    if (db.RecordCount() > 0)
                        while (!db.Eof())
                        {
                            if (db.Item("ApplicantType").Equals("MA"))
                            {
                                lblMainApplicantName.Text = db.Item("Name");
                                lblMainApplicantIC.Text = db.Item("MyCard");
                                if (!string.IsNullOrEmpty(db.Item("CCRIS")))
                                {
                                    //btnUserMainCCRIS.Visible = true; //use SOE instead
                                    btnUserMainCCRIS.Text += " (" + db.Item("ReportType") + ")";
                                }
                            }
                            else //if (db.Item("ApplicantType").Equals("CA"))
                            {
                                trUserCoApplicant.Visible = true;
                                lblCoApplicantName.Text = db.Item("Name");
                                lblCoApplicantIC.Text = db.Item("MyCard");
                                if (!string.IsNullOrEmpty(db.Item("CCRIS")))
                                {
                                    //btnUserCoCCRIS.Visible = true; //use SOE instead
                                    btnUserCoCCRIS.Text += " (" + db.Item("ReportType") + ")";
                                }
                            }
                            db.MoveNext();
                        }
                    #endregion
                }
                #endregion

                #region Loan Information
                ReportController reportController = new ReportController();
                sql.Clear();
                sql.AppendFormat(@"
                    SELECT lla.Tenure, lla.PurchasePrice, lla.LoanAmount as 'LoanAmount1', li.LoanAmount as 'LoanAmount2',
                    li.NewLoanRepayment as 'NewInstallment', li.IncomeRequired, r.ApplicationResult as 'Profile',
					li.DSR, li.LoanAmountEligible as 'LoanEligible', li.DeclareIncome as 'NetIncome'
                    FROM tbl_LoanLegalAction lla inner join 
                    tbl_LoanInfo li on lla.RequestID = li.RequestID inner join
					tbl_Request r on li.RequestID = r.RequestID inner join
						(select ra.RequestID, sum(ra.NetIncome) as 'NetIncome' from tbl_RequestApplicant ra
						where ra.RequestID = N'{0}'
						group by ra.RequestID)tra on tra.RequestID = r.RequestID
                    where lla.RequestID = N'{0}'
                ", hfRequestID.Value);
                db.OpenTable(sql.ToString());
                if (db.RecordCount() > 0)
                {
                    txtTenure.Text = !string.IsNullOrEmpty(db.Item("Tenure")) ? db.Item("Tenure") : "N/A";
                    txtPurchasePrice.Text = !string.IsNullOrEmpty(db.Item("PurchasePrice")) ?
                        double.Parse(db.Item("PurchasePrice")).ToString("N0") : "N/A";
                    if (!string.IsNullOrEmpty(db.Item("LoanAmount1")))
                        txtLoanAmount.Text = double.Parse(db.Item("LoanAmount1")).ToString("N0");
                    else if (!string.IsNullOrEmpty(db.Item("LoanAmount2")))
                        txtLoanAmount.Text = double.Parse(db.Item("LoanAmount2")).ToString("N0");
                    txtNewInstallment.Text = !string.IsNullOrEmpty(db.Item("NewInstallment")) ?
                        ConvertHelper.ConvertToDecimal(db.Item("NewInstallment"), 0).ToString("N0") : "N/A";
                    txtIncomeRequired.Text = !string.IsNullOrEmpty(db.Item("IncomeRequired")) ?
                        ConvertHelper.ConvertToDecimal(db.Item("IncomeRequired"), 0).ToString("N0") : "N/A";
                    txtDSR.Text = !string.IsNullOrEmpty(db.Item("DSR")) ? db.Item("DSR") : "N/A";
                    txtNetIncome.Text = !string.IsNullOrEmpty(db.Item("NetIncome")) ?
                        double.Parse(db.Item("NetIncome")).ToString("N0") : "N/A";

                    bool isMainOnly = true;
                    if (trUserCoApplicant.Visible || !string.IsNullOrEmpty(lblCoApplicantName.Text))
                        isMainOnly = false;
                    decimal oldRepayment = reportController.CalculateTotalOldBankRepayments("12", hfRequestID.Value, isMainOnly);
                    txtCurrentCommitment.Text = !string.IsNullOrEmpty(oldRepayment.ToString()) ?
                        double.Parse(oldRepayment.ToString()).ToString("N0") : "N/A";

                    sqlString.bindControl(gvLoanEligible, getBankList());
                }
                #endregion
            }
        }
        private string getBankList()
        {
            StringBuilder sql = new StringBuilder();

            sql.AppendFormat(@"
                SELECT
                    bn.BankID, bn.BankName,
                    case WHEN isnull(li.LoanAmountEligible, tli.LoanAmountEligible) < 0
	                    THEN '0'
	                    ELSE isnull(cast(isnull(li.LoanAmountEligible, tli.LoanAmountEligible) as varchar), 'N/A')
                    END as 'LoanAmountEligible'
                FROM tbl_BankName bn with (nolock) left join
                tbl_LoanInfo li WITH (NOLOCK) on li.BankID = bn.BankID and li.RequestID = '{0}' left join
                tbl_tloaninfo tli with (nolock) on tli.BankID = bn.BankID and tli.RequestID = '{0}'
                WHERE bn.IsDeleted = 0 and IsSelectedDefault = 1
                order by cast(bn.BankID as int)
            ", hfRequestID.Value);

            return sql.ToString();
        }
        protected void btnMainCCRIS_Click(object sender, EventArgs e)
        {
            string rid = hfRequestID.Value;

            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();
            sql.Clear();
            sql.AppendFormat(@" 
                SELECT ID, IsCoApplicant, Type
                FROM tbl_CTOS_Result WITH(NOLOCK)
                WHERE IsActive = 1 AND IsCoApplicant = 0 AND Request_Id = N'{0}' Order By ID
            ", secure.RC(rid));
            db.OpenTable(sql.ToString());
            if (db.RecordCount() > 0)
            {
                string type = db.Item("Type").ToString();

                if (type.Equals("CTOS"))
                    sqlString.OpenNewWindow_Center("/Form/Loan/CTOSReport.aspx?rid=" + sqlString.encryptURL(rid) + "&type=" + sqlString.encryptURL("0"), "DSR Report", 600, 800, this);
                if (type.Equals("RAMCI"))
                    sqlString.OpenNewWindow_Center("/Form/Loan/RAMCIReport.aspx?rid=" + sqlString.encryptURL(rid) + "&type=" + sqlString.encryptURL("0"), "DSR Report", 800, 800, this);
            }
        }
        protected void btnCoCCRIS_Click(object sender, EventArgs e)
        {
            string rid = hfRequestID.Value;

            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();
            sql.Clear();
            sql.AppendFormat(@" 
                SELECT ID, IsCoApplicant, Type
                FROM tbl_CTOS_Result WITH(NOLOCK)
                WHERE IsActive = 1 AND IsCoApplicant = 1 AND Request_Id = N'{0}' Order By ID
            ", secure.RC(rid));
            db.OpenTable(sql.ToString());
            if (db.RecordCount() > 0)
            {
                string type = db.Item("Type").ToString();

                if (type.Equals("CTOS"))
                    sqlString.OpenNewWindow_Center("/Form/Loan/CTOSReport.aspx?rid=" + sqlString.encryptURL(rid) + "&type=" + sqlString.encryptURL("1"), "DSR Report", 600, 800, this);
                if (type.Equals("RAMCI"))
                    sqlString.OpenNewWindow_Center("/Form/Loan/RAMCIReport.aspx?rid=" + sqlString.encryptURL(rid) + "&type=" + sqlString.encryptURL("1"), "DSR Report", 800, 800, this);
            }
        }
        protected void btnRecalculateUserPage_Click(object sender, EventArgs e)
        {
            ReportController rc = new ReportController();

            string mainAge = string.Empty, coAge = string.Empty;

            bool isMainOnly = true;
            if (trUserCoApplicant.Visible || !string.IsNullOrEmpty(lblCoApplicantName.Text))
                isMainOnly = false;

            decimal purchasePrice = ConvertHelper.ConvertToDecimal(secure.RC(txtPurchasePrice.Text), 0);
            decimal netIncome = ConvertHelper.ConvertToDecimal(secure.RC(txtNetIncome.Text), 0);
            decimal policy = calculatorController.getBankPolicy("12", netIncome, isMainOnly);
            decimal oldRepayment = ConvertHelper.ConvertToDecimal(txtCurrentCommitment.Text, 0);

            //Default Tenure
            decimal defaultTenure = rc.getTenureYear(6);
            decimal maxYear = 70 - rc.YoungerAge(ConvertHelper.ConvertToInt(mainAge, 0), ConvertHelper.ConvertToInt(coAge, 0));
            if (maxYear > defaultTenure)
                maxYear = defaultTenure;
            defaultTenure = maxYear;

            //NewInstallment
            decimal newInstallment = calculatorController.calculateNewRepayment(ConvertHelper.ConvertToDecimal(txtLoanAmount.Text, 0),
                ConvertHelper.ConvertToDecimal(txtTenure.Text, defaultTenure), rc.getIntrestRate(6));
            txtNewInstallment.Text = double.Parse(newInstallment.ToString()).ToString("N0");

            //DSR
            decimal dsr = calculatorController.calculateDSR(oldRepayment, newInstallment, netIncome);
            txtDSR.Text = double.Parse(dsr.ToString()).ToString("N0");

            //IncomeRequired
            decimal incomeRequired = calculatorController.calculateIncomeRequired(policy, oldRepayment, ConvertHelper.ConvertToDecimal(txtNewInstallment.Text, 0));
            txtIncomeRequired.Text = double.Parse(incomeRequired.ToString()).ToString("N0");

            //LoanEligible - GV
            for (int i = 0; i < gvLoanEligible.Rows.Count; i++)
            {
                TextBox gvtxtLoanEligible = (TextBox)gvLoanEligible.Rows[i].Cells[2].FindControl("gvtxtLoanEligible");
                decimal youngerAge = rc.YoungerAge(ConvertHelper.ConvertToInt(mainAge, 0),
                                        ConvertHelper.ConvertToInt(coAge, 0));
                string bankID = gvLoanEligible.Rows[i].Cells[0].Text;//(i + 1).ToString();

                //Calculating tenure for each bank
                //Default Tenure
                defaultTenure = rc.getTenureYear(6, bankID);
                maxYear = 70 - youngerAge;
                if (maxYear > defaultTenure)
                    maxYear = defaultTenure;
                defaultTenure = maxYear;
                decimal tenure = ConvertHelper.ConvertToDecimal(txtTenure.Text, defaultTenure);

                //Calculating policy for each bank
                policy = calculatorController.getBankPolicy(bankID, netIncome, isMainOnly);

                //Calculating repayment for each bank
                oldRepayment = rc.CalculateTotalOldBankRepayments(bankID, hfRequestID.Value, isMainOnly);

                //Calculating loan eligible using policy, tenure, and oldRepayment from each bank

                //LoanEligible
                decimal loanEligible = calculatorController.calculateLoanEligible(policy, oldRepayment,
                                    youngerAge, netIncome, tenure, bankID);
                if (loanEligible < 0)
                    loanEligible = 0;

                gvtxtLoanEligible.Text = loanEligible.ToString("N0");
            }
        }
        protected void btnSaveUserPage_Click(object sender, EventArgs e)
        {
            btnRecalculateUserPage_Click(btnRecalculateUserPage, EventArgs.Empty);

            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            string requestID = hfRequestID.Value.ToString();
            if (!string.IsNullOrEmpty(requestID) && validateForm2())
            {
                #region Loan Information
                sql.Clear();
                sql.AppendFormat(@"
                    UPDATE tbl_LoanInfo SET LoanAmount = '{0}', IncomeRequired = '{1}', NewLoanRepayment = '{2}', 
                        DSR = '{3}', DeclareIncome = '{4}' 
                    WHERE RequestID = N'{5}';
                ", string.IsNullOrEmpty(secure.RC(txtLoanAmount.Text)) ? "0" : secure.RC(txtLoanAmount.Text)
                , string.IsNullOrEmpty(secure.RC(txtIncomeRequired.Text)) ? "0" : secure.RC(txtIncomeRequired.Text)
                , string.IsNullOrEmpty(secure.RC(txtNewInstallment.Text)) ? "0" : secure.RC(txtNewInstallment.Text)
                , string.IsNullOrEmpty(secure.RC(txtDSR.Text)) ? "0" : secure.RC(txtDSR.Text)
                , string.IsNullOrEmpty(secure.RC(txtNetIncome.Text)) ? "0" : secure.RC(txtNetIncome.Text)
                , requestID);
                sql.AppendFormat(@"
                    UPDATE tbl_LoanLegalAction SET LoanAmount = '{0}', Tenure = '{1}', PurchasePrice = '{2}' 
                    WHERE RequestID = N'{3}';
                ", string.IsNullOrEmpty(secure.RC(txtLoanAmount.Text)) ? "0" : secure.RC(txtLoanAmount.Text)
                , string.IsNullOrEmpty(secure.RC(txtTenure.Text)) ? "0" : secure.RC(txtTenure.Text)
                , string.IsNullOrEmpty(secure.RC(txtPurchasePrice.Text)) ? "0" : secure.RC(txtPurchasePrice.Text)
                , requestID);

                //Loan Eligible - GV
                for (int i = 0; i < gvLoanEligible.Rows.Count; i++)
                {
                    TextBox gvtxtLoanEligible = (TextBox)gvLoanEligible.Rows[i].Cells[2].FindControl("gvtxtLoanEligible");
                    string bankID = gvLoanEligible.Rows[i].Cells[0].Text;//(i + 1).ToString();
                    sql.AppendFormat(@"
                        UPDATE tbl_TLoanInfo SET LoanAmountEligible = '{0}' WHERE RequestID = N'{1}' and BankID = '{2}';
                    ", string.IsNullOrEmpty(secure.RC(gvtxtLoanEligible.Text)) ?
                        secure.RC(gvtxtLoanEligible.Text.Equals("N/A") ? "0" : "0") : secure.RC(gvtxtLoanEligible.Text)
                    , requestID
                    , bankID);
                    if (bankID.Equals("12"))
                    {
                        sql.AppendFormat(@"
                            UPDATE tbl_LoanInfo SET LoanAmountEligible = '{0}' WHERE RequestID = N'{1}' and BankID = '{2}';
                        ", string.IsNullOrEmpty(secure.RC(gvtxtLoanEligible.Text)) ? "" : secure.RC(gvtxtLoanEligible.Text)
                        , requestID
                        , bankID);
                    }
                }
                #endregion
                db.Execute(sql.ToString());

                if (db.HasError)
                {
                    sqlString.displayAlert(this, Resources.resource.Submit_Error);
                    LogUtil.logError("Updating Request. RequestID: " + hfRequestID.Value, sql.ToString());
                }
                else
                    Response.Redirect("/Form/Report/ClientList.aspx");
            }
        }
        private bool validateForm2()
        {
            StringBuilder errMsg = new StringBuilder();
            ReportController reportController = new ReportController();

            txtLoanAmount.Text = reportController.removeComma(txtLoanAmount);
            txtPurchasePrice.Text = reportController.removeComma(txtPurchasePrice);
            txtTenure.Text = reportController.removeComma(txtTenure);
            txtIncomeRequired.Text = reportController.removeComma(txtIncomeRequired);
            txtNewInstallment.Text = reportController.removeComma(txtNewInstallment);
            txtNetIncome.Text = reportController.removeComma(txtNetIncome);
            txtCurrentCommitment.Text = reportController.removeComma(txtCurrentCommitment);

            bool gvHasError = false;
            for (int i = 0; i < gvLoanEligible.Rows.Count; i++)
            {
                TextBox gvtxtLoanEligible = (TextBox)gvLoanEligible.Rows[i].Cells[2].FindControl("gvtxtLoanEligible");
                gvtxtLoanEligible.Text = reportController.removeComma(gvtxtLoanEligible);
                if (!gvHasError)
                    if (!long.TryParse(gvtxtLoanEligible.Text, out _))
                        if (!decimal.TryParse(gvtxtLoanEligible.Text, out _))
                        {
                            errMsg.Append("12 Banks' " + Resources.resource.Loan_Eligible_Invalid_Format + "\\n");
                            gvHasError = true; //cant change to break; due to removing comma
                        }
            }

            if (!long.TryParse(txtCurrentCommitment.Text, out _))
                if (!decimal.TryParse(txtCurrentCommitment.Text, out _))
                    errMsg.Append(Resources.resource.Current_Commitment_Invalid_Format + "\\n");
            if (!long.TryParse(txtNetIncome.Text, out _))
                if (!decimal.TryParse(txtNetIncome.Text, out _))
                    errMsg.Append(Resources.resource.Net_Income_Invalid_Format + "\\n");
            if (!long.TryParse(txtLoanAmount.Text, out _))
                if (!decimal.TryParse(txtLoanAmount.Text, out _))
                    errMsg.Append(Resources.resource.Loan_Amount_Invalid_Format + "\\n");
            if (!long.TryParse(txtPurchasePrice.Text, out _))
                if (!decimal.TryParse(txtPurchasePrice.Text, out _))
                    errMsg.Append(Resources.resource.Purchase_Price_Invalid_Format + "\\n");
            if (!int.TryParse(txtTenure.Text, out _))
                errMsg.Append(Resources.resource.TenureShouldBeInteger + "\\n");
            if (!long.TryParse(txtIncomeRequired.Text, out _))
                if (!decimal.TryParse(txtIncomeRequired.Text, out _))
                    errMsg.Append(Resources.resource.Income_Required_Only_Number + "\\n");
            if (!long.TryParse(txtNewInstallment.Text, out _))
                if (!decimal.TryParse(txtNewInstallment.Text, out _))
                    errMsg.Append(Resources.resource.New_Installment_Only_Number + "\\n");

            lblErr.Text = errMsg.ToString().Replace("\\n", "</br>");

            if (lblErr.Text != string.Empty)
            {
                divErrorMessage.Visible = true;
                sqlString.displayAlert2(this, errMsg.ToString());
                return false;
            }
            else
                divErrorMessage.Visible = false;

            return true;
        }
        private bool checkACL_EditRequest()
        {


            return false;
        }

        protected void gvLoanEligible_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //Loan Amount Eligible
                string loanAmountEligible = ConvertHelper.ConvertToDecimal(
                                                DataBinder.Eval(e.Row.DataItem, "LoanAmountEligible").ToString(), 0
                                            ).ToString("N0");
                TextBox gvtxtLoanEligible = (TextBox)e.Row.Cells[2].FindControl("gvtxtLoanEligible");

                gvtxtLoanEligible.Text = loanAmountEligible;
                gvtxtLoanEligible.Enabled = false;
            }
        }
        #endregion
    }
}