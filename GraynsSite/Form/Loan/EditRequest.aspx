﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditRequest.aspx.cs" MasterPageFile="~/Global1.Master" Inherits="HJT.Form.Loan.EditRequest" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function confirmSubmit() {
            return confirm('<%= GetGlobalResourceObject("resource" , "Confirm_Changes") %>');
        }
        
        function setTab() {
            if ($("#HidTab").val() == '' || $("#HidTab").val() === undefined) {
                $("#HidTab").val("#tab_1");
            }
            $("a[data-toggle='tab'] ");

            $("a[href='" + $("#HidTab").val() + "' ]").click();
        }

        function pageLoad() {
            $("a[data-toggle='tab']").each(
                function (index) {
                    $(this).on("click", function () {
                        $("#HidTab").val($(this).attr("href"));

                    });
                }
            )
            setTab();
        }
    </script>
    <style>
        #tab_2 .table-bordered {
            width: 40%;
        }
        #tab_2 .table-gvLoanEligible {
            Width: 80%;
        }

        @media only screen and (max-width: 1500px) {
            #tab_2 .table-bordered {
                width: 50%;
            }

            #tab_2 .table-gvLoanEligible {
                Width: 100%;
            }
        }

        @media only screen and (max-width: 650px){
            #tab_2 #table1 .form-group, 
            #tab_2 #table1 .form-control{
                font-size: 10px;
            }

            #tab_2 #table2 .form-group, 
            #tab_2 #table2 .form-control{
                font-size: 10px;
            }
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <aside class="right-side">
        <section class="content-header">
            <h1>
                <span class="icon fa fa-fw fa-book" aria-hidden="true"></span>
                <asp:Label ID="lblPageTitle" runat="server"><i class="icon-th-list"></i><span runat="server" id="spHeader"><%= GetGlobalResourceObject("resource", "Request_Amendment")%></span></asp:Label>
            </h1>
            <asp:SiteMapPath ID="SiteMapPath1" CssClass="breadcrumb" runat="server"></asp:SiteMapPath>
        </section>
        <section class="content">
            <div class="col-md-12">
                <div class="box" runat="server">
                    <div id="divErrorMessage" runat="server" visible="false" class="alert alert-danger alert-dismissable">
                        <i class="fa fa-ban"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <b><%= GetGlobalResourceObject("resource", "Alert")%></b>
                        <br />
                        <asp:Label runat="server" ID="lblErr"></asp:Label>
                    </div>
                    <ul class="nav nav-tabs">
                        <li class=""><a href="#tab_1" id="aTab1" data-toggle="tab" aria-expanded="false"><%= GetGlobalResourceObject("resource", "Basic_Information") %></a></li>
                        <li class=""><a href="#tab_2" id="aTab2" data-toggle="tab" aria-expanded="false"><%= GetGlobalResourceObject("resource", "Loan_Detail") %></a></li>
                        <asp:HiddenField runat="server" ID="HidTab" ClientIDMode="Static" />
                    </ul>
                    <div class="tab-content box-body" runat="server" id="divBody">
                        <asp:HiddenField runat="server" ID="hfRequestID"/>
                        <div class="tab-pane" id="tab_1">
                            <asp:Panel runat="server" DefaultButton="btnSave1">
                                <table class="table table-bordered" style="width:100%">
                                    <tr runat="server" visible="false" class="form-group" style="width:100%">
                                        <td style="width:20%"><%= GetGlobalResourceObject("resource", "ID")%></td>
                                        <td class="controls" style="width:80%">
                                            <asp:TextBox ReadOnly="true" runat="server" ID="txtID" CssClass="form-control"> </asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr runat="server" visible="false" class="form-group" style="width:100%">
                                        <td style="width:20%"><%= GetGlobalResourceObject("resource", "RequestID")%></td>
                                        <td class="controls" style="width:80%">
                                            <asp:TextBox ReadOnly="true" runat="server" ID="txtRequestID" CssClass="form-control"> </asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="form-group" style="width:100%">
                                        <td style="width:20%"><%= GetGlobalResourceObject("resource", "Member_Name")%></td>
                                        <td class="controls" style="width:80%">
                                            <asp:TextBox ReadOnly="true" runat="server" ID="txtMemberName" CssClass="form-control"> </asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="form-group" style="width:100%">
                                        <td style="width:20%"><%= GetGlobalResourceObject("resource", "Branch")%></td>
                                        <td class="controls" style="width:80%">
                                            <asp:TextBox ReadOnly="true" runat="server" ID="txtBranch" CssClass="form-control"> </asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="form-group" style="width:100%">
                                        <td style="width:20%"><%= GetGlobalResourceObject("resource", "Applicant_Information")%></td>
                                        <td class="controls" style="width:80%">
                                            <table style="width:100%;">
                                                <tr class="form-group" style="width:100%">
                                                    <td style="width:15%"/>
                                                    <td style="width:45%"><%= GetGlobalResourceObject("resource", "Applicant_Name")%></td>
                                                    <td style="width:30%"><%= GetGlobalResourceObject("resource", "IC")%></td>
                                                    <td style="width:10%"><%= GetGlobalResourceObject("resource", "CCRIS")%></td>
                                                </tr>
                                                <tr class="form-group" style="width:100%">
                                                    <td style="width:15%"><%= GetGlobalResourceObject("resource", "Applicant")%> 1</td>
                                                    <td class="controls" style="width:45%">
                                                        <asp:TextBox ReadOnly="true" runat="server" ID="txtMainApplicantName" CssClass="form-control"> </asp:TextBox>
                                                    </td>
                                                    <td class="controls" style="width:30%">
                                                        <asp:TextBox ReadOnly="true" runat="server" ID="txtMainIC" CssClass="form-control"> </asp:TextBox>
                                                    </td>
                                                    <td style="width:10%">
                                                        <asp:Button runat="server" Visible="false" ID="btnMainCCRIS" Text="<%$ Resources:Resource, Main_Applicant %>" 
                                                            CssClass="btn btn-searh" OnClick="btnMainCCRIS_Click" Font-Size="Small" />
                                                    </td>
                                                </tr>
                                                <tr class="form-group" style="width:100%" runat="server" visible="false" id="trCoApplicantName">
                                                    <td style="width:15%"><%= GetGlobalResourceObject("resource", "Applicant")%> 2</td>
                                                    <td class="controls" style="width:45%">
                                                        <asp:TextBox ReadOnly="true" runat="server" ID="txtCoApplicantName" CssClass="form-control"> </asp:TextBox>
                                                    </td>
                                                    <td class="controls" style="width:30%">
                                                        <asp:TextBox ReadOnly="true" runat="server" ID="txtCoIC" CssClass="form-control"> </asp:TextBox>
                                                    </td>
                                                    <td style="width:10%">
                                                        <asp:Button runat="server" Visible="false" ID="btnCoCCRIS" Text="<%$ Resources:Resource, Co_Applicant %>" 
                                                            CssClass="btn btn-searh" OnClick="btnCoCCRIS_Click" Font-Size="Small" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="form-group" style="width:100%">
                                        <td style="width:20%"><%= GetGlobalResourceObject("resource", "ApplicationResult")%></td>
                                        <td class="controls" style="width:80%">
                                            <asp:RadioButtonList ID="rblApplicationResult" runat="server" RepeatDirection="Horizontal"></asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr class="form-group" style="width:100%">
                                        <td style="width:20%"><%= GetGlobalResourceObject("resource","SPA_Signed") %></td>
                                        <td class="controls" style="width:40%">
                                            <asp:Label runat="server" ID="lblPaymentStatus" Font-Bold="true" />
                                            <asp:Button runat="server" ID="btnSuccess" Text="<%$ Resources:Resource, Set_As_Success%>" OnClick="btnSuccess_Click" 
                                                OnClientClick="return confirmSubmit(); " CssClass="btn btn-view" Visible="false" />
                                            <asp:Button runat="server" ID="btnFail" Text="<%$ Resources:Resource, Set_As_Fail%>" OnClick="btnFailed_Click" 
                                                OnClientClick="return confirmSubmit(); " CssClass="btn btn-view" Visible="false" />
                                        </td>
                                    </tr>
                                    <tr class="form-group" style="width:100%">
                                        <td style="width:20%"><%= GetGlobalResourceObject("resource", "ProjectName")%></td>
                                        <td class="controls" style="width:80%">
                                            <asp:TextBox ReadOnly="true" runat="server" ID="txtProjectName" CssClass="form-control"> </asp:TextBox>
                                            <asp:DropDownList runat="server" ID="ddlProjectName"/>
                                        </td>
                                    </tr>
                                    <tr class="form-group" style="width:100%">
                                        <td style="width:20%"><%= GetGlobalResourceObject("resource", "Advise")%></td>
                                        <td class="controls" style="width:80%">
                                            <asp:TextBox runat="server" ID="txtAdvise" CssClass="form-control" TextMode="MultiLine" Rows="15"> </asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="form-group" style="width:100%">
                                        <td style="width:20%"><%= GetGlobalResourceObject("resource", "Message")%></td>
                                        <td class="controls" style="width:80%">
                                            <asp:TextBox runat="server" ID="txtMessage" CssClass="form-control" TextMode="MultiLine" Rows="4"> </asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                                <div class="box-footer">
                                    <asp:Button ID="btnRefresh1" Text="<%$ Resources:Resource, Refresh%>" runat="server" CssClass="btn btn-primary" OnClick="btnRefresh1_Click" />
                                    <asp:Button ID="btnSave1" Text="<%$ Resources:Resource, Save%>" runat="server" CssClass="btn btn-primary" OnClientClick="return confirmSubmit(); " OnClick="btnSave1_Click" />
                                </div>
                            </asp:Panel>
                        </div>
                        <div class="tab-pane" id="tab_2">
                            <asp:Panel runat="server" DefaultButton="btnSave2">
                                <table class="table table-bordered" style="float:left;" id="table1">
                                    <tr runat="server" class="form-group" style="width:100%">
                                        <td style="width:50%"><%= GetGlobalResourceObject("resource", "Profile")%></td>
                                        <td class="controls" style="width:50%" runat="server" id="tdApplicationResult">
                                            <asp:TextBox ReadOnly="true" runat="server" ID="txtApplicationResult" CssClass="form-control"> </asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr runat="server" class="form-group" style="width:100%">
                                        <td style="width:50%"><%= GetGlobalResourceObject("resource", "Current_Commitment")%></td>
                                        <td class="controls" style="width:50%">
                                            <asp:TextBox ReadOnly="true" Text="N/A" runat="server" ID="txtCurrentCommitment" CssClass="form-control"> </asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr runat="server" class="form-group" style="width:50%">
                                        <td style="width:50%"><%= GetGlobalResourceObject("resource", "Purchase_Price")%></td>
                                        <td class="controls" style="width:50%">
                                            <asp:TextBox runat="server" ID="txtPurchasePrice" CssClass="form-control"> </asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr runat="server" class="form-group" style="width:50%">
                                        <td style="width:50%"><%= GetGlobalResourceObject("resource", "Income_Required")%></td>
                                        <td class="controls" style="width:50%">
                                            <asp:TextBox runat="server" ID="txtIncomeRequired" CssClass="form-control"> </asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr runat="server" class="form-group" style="width:50%">
                                        <td style="width:50%"><%= GetGlobalResourceObject("resource", "Net_Income")%></td>
                                        <td class="controls" style="width:50%">
                                            <asp:TextBox runat="server" ID="txtNetIncome" CssClass="form-control"> </asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                                <table class="table table-bordered" style="float:left;" id="table2">
                                    <tr runat="server" class="form-group" style="width:100%">
                                        <td style="width:50%"><%= GetGlobalResourceObject("resource", "Tenure")%></td>
                                        <td class="controls" style="width:50%">
                                            <asp:TextBox runat="server" ID="txtTenure" CssClass="form-control"> </asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr runat="server" class="form-group" style="width:50%">
                                        <td style="width:50%"><%= GetGlobalResourceObject("resource", "DSR")%></td>
                                        <td class="controls" style="width:50%">
                                            <asp:TextBox runat="server" ID="txtDSR" CssClass="form-control"> </asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr runat="server" class="form-group" style="width:50%">
                                        <td style="width:50%"><%= GetGlobalResourceObject("resource", "Loan_Amount")%></td>
                                        <td class="controls" style="width:50%">
                                            <asp:TextBox runat="server" ID="txtLoanAmount" CssClass="form-control"> </asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr runat="server" class="form-group" style="width:50%">
                                        <td style="width:50%"><%= GetGlobalResourceObject("resource", "New_Installment")%></td>
                                        <td class="controls" style="width:50%">
                                            <asp:TextBox runat="server" ID="txtNewInstallment" CssClass="form-control"> </asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr runat="server" class="form-group" style="width:50%">
                                        <td style="width:50%"><%= GetGlobalResourceObject("resource", "Loan_Eligible")%></td>
                                        <td class="controls" style="width:50%">
                                            <asp:TextBox runat="server" ID="txtLoanEligible" CssClass="form-control"> </asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                                <div style="margin-top: 10px; display:inline-block; width:100%;">
                                    <asp:GridView ID="gvLoanEligible" runat="server" OnRowDataBound="gvLoanEligible_RowDataBound" 
                                        AutoGenerateColumns="false" CssClass="table-gvLoanEligible">
                                        <Columns>
                                            <asp:BoundField HeaderText="<%$ Resources:Resource, ID %>" DataField="BankID" ItemStyle-Width="10%" 
                                                ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" />
                                            <asp:BoundField HeaderText="<%$ Resources:Resource, Bank_Name %>" DataField="BankName" ItemStyle-Width="20%" />
                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Loan_Eligible %>" ItemStyle-Width="50%">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="gvtxtLoanEligible" Width="100%" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--<asp:BoundField HeaderText="<%$ Resources:Resource, Selected %>" DataField="Selected" />--%>
                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Selected %>" ItemStyle-Width="20%" >
                                                <ItemTemplate>
                                                    <asp:CheckBox runat="server" ID="chkSelected" Enabled="false" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                                <div class="box-footer" style="width:100%; display:inline-block; margin-top: 10px;">
                                    <asp:Button ID="btnRefresh2" Text="<%$ Resources:Resource, Refresh%>" runat="server" CssClass="btn btn-primary" OnClick="btnRefresh2_Click" />
                                    <asp:Button ID="btnSave2" Text="<%$ Resources:Resource, Save%>" runat="server" CssClass="btn btn-primary" OnClientClick="return confirmSubmit(); " OnClick="btnSave2_Click" />
                                    <asp:Button ID="btnRecalculate" Text="<%$ Resources:Resource, Recalculate%>" runat="server" CssClass="btn btn-primary" OnClick="btnRecalculate_Click"/>
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </aside>
</asp:Content>