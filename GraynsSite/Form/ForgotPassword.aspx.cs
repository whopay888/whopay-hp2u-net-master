﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Util;
using Synergy.Model;
using Synergy.UI;
using Synergy.Controller;

namespace WMElegance.Form
{
    public partial class ForgotPassword : System.Web.UI.Page
    {
        #region ErrorMsg Class

        error errormessage = new error();

        #endregion

        #region Captchar Variable

        public Captcha Captchar_Class;

        //private String Captchar_Text = "qpweirutyghfjdksamxncbzvZABCDZABCD#EFGHJK%MNPQR&STUVWX$YZ23456789";
        private String Captchar_Text = "0123456789";
        private Int16 Captchar_Length = 4;
        private Double CaptChar_FontSize = 15;
        private String CaptChar_FontFamily = "Verdana";
        private String CaptChar_BackgroundImgPath = "/images/CaptCharBG.png";
        private String CaptChar_TextColor = "Black";

        #endregion

        #region PAGE EVENT

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                //Clear All Session On Page Load -------------------------------
                Session.Remove("CaptchaClass");
                Session.Remove(KeyVal.session_member);
                Session.Remove(KeyVal.session_clogin);
                //--------------------------------------------------------------

                //Load CaptChar() ----------------------------------------------
                while (Session["CaptchaClass"] == null)
                { this.LoadCaptcha(); }
                //--------------------------------------------------------------

                this.TxtUserId.Focus();

                this.Bind_BackgroundImage();
            }
        }

        #endregion

        #region CAPTCHAR

        public void LoadCaptcha()
        {
            String text = this.Generate_RandomText();
            Session["cctext"] = text;
            ViewState.Add("captcha", text);
            Session.Add("CaptchaClass", this.GET_CaptCharClass());//add captcha object to Session
            Session.Add("captcha", text);//add captcha text to session             
            Im1.ImageUrl = "/captcha/CaptchaHandler.ashx";
            LogUtil.logError("Forgotpassword.aspx", Session["captcha"] + "||" + text);
                        

        }
        protected void Unnamed_ServerClick(object sender, EventArgs e)
        {
            string a = HttpContext.Current.Request.Url.AbsolutePath;

            a += "?a=" + sqlString.encryptURL("asdfNSIDOF");

            //if (Request.QueryString["path"] != null)
            //    a += "&path=" + sqlString.encryptURL(sqlString.decryptURL(Request.QueryString["path"].ToString()));

            //if (TxtUserId.Text.Trim() != string.Empty)
            //    a += "&id=" + sqlString.encryptURL(TxtUserId.Text);

            //if (TxtPassword.Text.Trim() != string.Empty)
            //    a += "&pa=" + sqlString.encryptURL(TxtPassword.Text);

            Response.Redirect(a);
        }

        private Captcha GET_CaptCharClass()
        {
            Session.Remove("CaptchaClass");
            Captchar_Class = new Captcha();

            Captchar_Class.FontSize = this.CaptChar_FontSize;
            Captchar_Class.FontFamily = this.CaptChar_FontFamily;
            Captchar_Class.BackgroundImagePath = this.CaptChar_BackgroundImgPath;
            Captchar_Class.TextColor = this.CaptChar_TextColor;
            return Captchar_Class;
        }

        private string Generate_RandomText()
        {

            char[] letters = Captchar_Text.ToCharArray();

            string text = string.Empty;
            Random r = new Random();
            int num = -1;

            for (int i = 0; i < this.Captchar_Length; i++)
            {
                num = (int)(r.NextDouble() * (letters.Length - 1));
                text += letters[num].ToString();
            }
            return text;
        }

        #endregion

        #region VALIDATION FUNCTION

        private Boolean Submit_Validation()
        {
            this.LblError.Text = String.Empty;
            Boolean Validate_Success = false;
            if (this.RequireField_Validation())
            {
                if (this.CaptChar_Validation())
                {
                    if (this.System_Validation())
                    {
                        Validate_Success = true;
                    }
                }
            }
            return Validate_Success;
        }

        private Boolean RequireField_Validation()
        {
            String NewLine = "<br/>";
            String ErrMsg = Resources.resource.InputRequiredField ;
            String ErrField = String.Empty;


            String UserName = this.TxtUserId.Text.Trim();
            String CaptChar = this.TxtVerificationCode.Text.Trim();

            if (String.IsNullOrEmpty(UserName))
            { ErrField += "-" + Resources.resource.Member_ID + "." + NewLine; }

            if (String.IsNullOrEmpty(CaptChar))
            { ErrField += "-" + Resources.resource.VerificationCode_Input + NewLine; }

            if (String.IsNullOrEmpty(ErrField))
            { return true; }
            else
            {
                string strMsg = (ErrMsg + ErrField).Replace("<br/>", "\\n");
                //Show error Message ---------------------------                      
                this.LblError.Text = errormessage.AlertText(ErrMsg + ErrField);
                //----------------------------------------------
                sqlString.displayAlert(this, strMsg);
                return false;
            }
        }

        private Boolean CaptChar_Validation()
        {
            //Perform Checking If Verification Code is visible;
            Boolean CaptChar_IsValidate = false;
            if (this.TxtVerificationCode.Visible == true)
            {
                String CaptChar_Val = this.TxtVerificationCode.Text.Trim();
                if (Session["captcha"] != null)
                {
                    if (CaptChar_Val.ToLower() == Session["captcha"].ToString().ToLower())
                    { CaptChar_IsValidate = true; }
                    else
                    //{ this.LblError.Text = sqlString.changeLanguage("* Incorrect Verification Code"); }
                    { 
                        this.LblError.Text = errormessage.AlertText(Resources.resource.InvalidVerificationCodeError);

                        LogUtil.logError("Forgotpassword.aspx", Session["captcha"] + "||" + CaptChar_Val);
                        sqlString.displayAlert(this, Resources.resource.InvalidVerificationCodeError);
                    
                    }
                }
                else
                {
                    this.LblError.Text = errormessage.AlertText(Resources.resource.VerificationCode_Expired);
                    this.IBtnRefresh_Click(null, null);
                    sqlString.displayAlert(this, Resources.resource.VerificationCode_Expired);
                }
            }
            else { CaptChar_IsValidate = true; }

            return CaptChar_IsValidate;
        }

        private Boolean System_Validation()
        {
            //Validate If User Name IS Correct ------------------------------
            Boolean System_IsValidate = false;
            String User_Id = this.TxtUserId.Text.Trim();



            String SQL = "SELECT TOP 1 Login_Password AS 'Password', fullName AS 'MemberName', memberId AS 'MemberID', email AS 'MemberEmail'  FROM tbl_login TL " +
           " LEFT JOIN tbl_MemberInfo TM ON TL.Login_Id = TM.MemberId " +
           " WHERE (Login_Id = N'" + secure.RC(User_Id) + "' or tm.username=N'" + secure.RC(User_Id) + "')";
         

            wwdb db = new wwdb();

            db.OpenTable(SQL); db.Close();
            if (!db.HasError && db.RecordCount() == 1)
            {
                System_IsValidate = true;
            }
            else { this.LblError.Text = errormessage.AlertText(Resources.resource.usernameInvalidError);
            sqlString.displayAlert(this, Resources.resource.usernameInvalidError);
            }

            return System_IsValidate;
            //---------------------------------------------------------------            
        }

        #endregion

        #region FUNCTION
        
        private char[] CharacterSet()
        {
            char[] letters = {'Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','S','Y','Z','1','2','3','4','5','6','7','8','9','♫'};

            return letters;
        }

        private string GetRandomText()
        {
            char[] letters = CharacterSet();
            string text = string.Empty;
            Random r = new Random();
            int num = -1;

            for (int i = 0; i < 6; i++)
            {
                num = (int)(r.NextDouble() * (letters.Length - 1));
                text += letters[num].ToString();
            }
            return text;
        }

        private Boolean Send_ResetLink()
        {
            Boolean Send_Complete = false;
            String User_Id = this.TxtUserId.Text.Trim();

            String SQL = "SELECT TOP 1 Login_Password AS 'Password', fullName AS 'MemberName' , username as 'loginID', memberId AS 'MemberID', email AS 'MemberEmail'  FROM tbl_login TL " +
            " LEFT JOIN tbl_MemberInfo TM ON TL.Login_Id = TM.MemberId " +
            " WHERE (Login_Id = N'" + secure.RC(User_Id) + "' or tm.username=N'" + secure.RC(User_Id) + "')";
           

            wwdb db = new wwdb();
            db.OpenTable(SQL);
            if (!db.HasError && db.RecordCount() == 1)
            {
                String Member_Id = db.Item("MemberID");
                string username = db.Item("loginID");
                String Member_Email = db.Item("MemberEmail"); this.HFEmail.Value = Member_Email;
                String Member_Name = db.Item("MemberName");
                String Member_Password = db.Item("Password");

                //Insert into [tbl_ResetPassword] As A Record -------------------------------------------
                //SQL = "EXEC [SPR_PasswordResetRequest] N'" + secure.RC(Member_Id) + "'";
                //db.OpenTable(SQL);

                //if (!db.HasError && db.RecordCount() == 1)
                //{
                //    String Token_ID = db.Item("TokenID");
                //    String Encry_TokenID = secure.EURC(secure.Encrypt(Token_ID, true));
                //    String Start_Time = db.Item("StartTime");
                //    String End_Time = db.Item("EndTime");

                //Send Email Notification To Member -----------------------------------------------------

                Random rnd = new Random();
                string rndPass = string.Empty;

                //for (int i = 0; i < 6; i++)
                //{
                //    rndPass += rnd.Next(0, 9);
                //}

                //if (rndPass != "")
                //{
                //    rndPass = secure.Encrypt(rndPass,true);
                //}

                rndPass = GetRandomText();

                if (rndPass != string.Empty)
                {
                    rndPass = secure.Encrypt(rndPass, true);
                }

                EmailController EmailCtrl = new EmailController();
                //temporary change from Member_Password to 
                if (EmailCtrl.SendForgotPassword(username, Member_Name, Member_Email, rndPass))
                {
                    Send_Complete = true;

                    if (rndPass != "")
                    {
                        SQL = "";
                        SQL = " UPDATE tbl_Login SET login_password=N'" + secure.RC(rndPass) + "',firstLogin=0 WHERE login_id=N'" + secure.RC(Member_Id) + "';";

                        db.Execute(SQL);

                        LogUtil.logAction(SQL, "Reset New Password (From ForgotPassword.aspx)");
                    }
                }
                else { this.LblError.Text = errormessage.AlertText("An error occured while sending email notification, please try again later."); }
                //---------------------------------------------------------------------------------------
                //}
                //else { this.LblError.Text = errormessage.AlertText("An error occued while updating records, please try again later."); }
                ////---------------------------------------------------------------------------------------                
            }
            else { this.LblError.Text = errormessage.AlertText(Resources.resource.usernameInvalidError); }

            db.Close();
            return Send_Complete;
        }

        private void Bind_BackgroundImage()
        {
            String SQL = String.Empty;
            wwdb db = new wwdb();

            SQL = "SELECT TOP 1 FullPath, ImageURl, ToolTip, URL FROM tbl_ImagePath TI WHERE TI.[IsDeleted] = 0 " +
                  " ORDER BY RowId DESC ";
            db.OpenTable(SQL); db.Close();
            if (!db.HasError && db.RecordCount() == 1)
            {
                String ImgPath = db.Item("ImageUrl");
                String ToolTip = db.Item("ToolTip");
                String LinkURL = db.Item("URL");

                String ALink_StartTag = ""; String ALink_EndTag = "";
                if (!String.IsNullOrEmpty(LinkURL))
                {
                    ALink_StartTag = "<a href='" + LinkURL + "' target='_blank'>";
                    ALink_EndTag = "</a>";
                }
                
                String ImgTag = "<img src='" + ImgPath + "' width='600px'; height='600px' alt='" + ToolTip + "'; title='" + ToolTip + "';></img>";
                //this.LtrBackground.Text = ALink_StartTag + ImgTag + ALink_EndTag;
                string hiddenValue = ImgPath ;
               

                Uri myUri;
                if (Uri.TryCreate(ImgPath, UriKind.RelativeOrAbsolute, out myUri))
                {
                    imgsrc.Value = ImgPath;
                }
                else
                {
                    imgsrc.Value = "0";
                }

                
            }

        }
        #endregion

        #region BUTTON EVENT

        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            if (this.Submit_Validation())
            {
                //Send Reset password Link To Member
                if (this.Send_ResetLink())
                {
                    String Email_Val = this.HFEmail.Value;
                    String Complete_Msg = Resources.resource.ForgotPassword_Complete;
                    this.LblResetComplete.Text = Complete_Msg.Replace("[EMAIL]", Email_Val);

                    //this.Pnl_ResetComplete.Visible = true;

                    //this.Pnl_ResetPassword.Visible = false;
                }
            }
        }

        protected void IBtnRefresh_Click(object sender, ImageClickEventArgs e)
        {
            this.LoadCaptcha();
        }

        #endregion

        protected void BtnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Form/Login.aspx");
        }
    }
}