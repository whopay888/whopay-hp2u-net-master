﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Model;
using Synergy.Util;

namespace HJT.Form.User
{
    public partial class ChangePassword : System.Web.UI.Page
    {

        LoginUserModel login_user = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            login_user = (LoginUserModel)Session[KeyVal.loginsecure];
            txtOldPass.Attributes.Add("Value", txtOldPass.Text);
            txtNewPassword.Attributes.Add("Value", txtNewPassword.Text);
            txtNewPasswordConfirm.Attributes.Add("Value", txtNewPasswordConfirm.Text);
        }

        private bool validateForm()
        {
            StringBuilder errMsg = new StringBuilder();


            if (string.IsNullOrEmpty(txtOldPass.Text))
                errMsg.Append(Resources.resource.oldPassEmpty + "\\n");
            else
            {
                wwdb db = new wwdb();
                db.OpenTable("select top 1 1 from tbl_login where login_id ='" + login_user.UserId + "' and login_password=N'" + secure.Encrypt(txtOldPass.Text, true) + "'");

                if (db.RecordCount() < 1)
                    errMsg.Append(Resources.resource.oldPassWrongError + "\\n");


            }

            if (string.IsNullOrEmpty(txtNewPassword.Text))
                errMsg.Append(Resources.resource.newPasswordEmpty + "\\n");




            if (string.IsNullOrEmpty(txtNewPasswordConfirm.Text))
                errMsg.Append(Resources.resource.newConfPasswordEmpty + "\\n");


            if (txtNewPassword.Text != txtNewPasswordConfirm.Text)
                errMsg.Append(Resources.resource.PasswordNotMatch + "\\n");



            lblErr.Text = errMsg.ToString().Replace("\\n", "</br>");



            if (errMsg.Length > 0)
            {
                divErrorMessage.Visible = true;
                sqlString.displayAlert2(this, errMsg.ToString());
                return false;
            }
            else
            {
                divErrorMessage.Visible = false;
                return true;
            }

        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            if (validateForm())
            {
                wwdb db = new wwdb();
                db.Execute("update tbl_login set login_password = N'" + secure.Encrypt(txtNewPassword.Text, true) + "' where login_id ='" + login_user.UserId + "'");
                sqlString.displayAlert2(this, Resources.resource.Action_Successful, Request.Url.ToString());
            }
        }
    }
}