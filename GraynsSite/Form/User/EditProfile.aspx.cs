﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Model;
using Synergy.Util;

namespace HJT.Form.User
{
    public partial class EditProfile : System.Web.UI.Page
    {
        LoginUserModel login_user = null;
        string uploadImage = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            login_user = (LoginUserModel)Session[KeyVal.loginsecure];
            if (!IsPostBack)
            {
                binddata();
                Session["uploadImages"] = uploadImage;
            }
            uploadImage = Session["uploadImages"].ToString();
        }

        private void binddata()
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            sql.Clear();
            sql.AppendFormat(@"
                SELECT mi.[Fullname] as 'Name', mi.[IC], mi.[Gender], mi.[DOB], 
                    mi.[HomeTelephone], mi.[Mobile], mi.[Email], mi.[Status], 
                    mi.[SignUpdate] as 'SignupDate', mi.[UpdatedBy], mi.[UpdatedAt], 
                    mi.[UploadDocument] as 'ImageURL'
                FROM [tbl_MemberInfo] mi WITH (nolock)
                where MemberId = '{0}'
            ", login_user.UserId);
            db.OpenTable(sql.ToString());
            if (db.RecordCount() > 0)
            {
                txtName.Text = db.Item("Name");
                lblSignupDate.Text = db.Item("SignupDate");
                if (!string.IsNullOrEmpty(db.Item("ImageURL")))
                {
                    imgPreview.ImageUrl = db.Item("ImageURL");
                    imgPreview.Visible = true;
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (validateForm())
            {
                wwdb db = new wwdb();
                StringBuilder sql = new StringBuilder();

                string toFilePath;
                if (!(string.IsNullOrEmpty(hid_FUID.Value) || string.IsNullOrWhiteSpace(hid_FUID.Value)))
                {
                    string newPath = "/Upload/ProfileImage/";
                    if (!Directory.Exists(Server.MapPath("~" + newPath)))
                        Directory.CreateDirectory(Server.MapPath("~" + newPath));

                    string imgpath = uploadImage;
                    FileInfo FromFile = new FileInfo(imgpath);
                    toFilePath = newPath + FromFile.Name;
                    FromFile.MoveTo(Server.MapPath("~" + toFilePath));
                }
                else
                    toFilePath = uploadImage;

                sql.Clear();
                sql.AppendFormat(@"
                    UPDATE tbl_MemberInfo
                    SET Fullname = '{0}' {1}
                    WHERE MemberId = N'{2}';
                ", secure.RC(txtName.Text)
                , imgPreview.Visible ? string.Format(@"
                    , UploadDocument = N'{0}'
                    ", toFilePath) : ""
                , login_user.UserId);

                db.Execute(sql.ToString());
                if (db.HasError)
                {
                    LogUtil.logError(db.ErrorMessage, sql.ToString());
                    sqlString.displayAlert(this, Resources.resource.unknownError);
                }
                else
                    sqlString.displayAlert(this, Resources.resource.ActionSuccessfulMsg, Request.Url.ToString());
            }
        }
        private bool validateForm()
        {
            StringBuilder errMsg = new StringBuilder();

            if (string.IsNullOrEmpty(txtName.Text))
                errMsg.Append(Resources.resource.memberNameEmptyErrorMsg + "\\n");

            //if (string.IsNullOrEmpty(hid_FUID.Value) || string.IsNullOrWhiteSpace(hid_FUID.Value))
            //    errMsg.Append(Resources.resource.Upload_Empty + "\\n");

            if (!File.Exists(uploadImage))
            {
                uploadImage = imgPreview.ImageUrl;
                //errMsg.Append("* fail to upload the file, please try again." + "\\n");

                //Session["uploadImages"] = new List<string>();
                //img_fu1.Visible = false;
                //img_fu1.Attributes.Remove("onclick");
            }

            if (errMsg.ToString().Trim() != string.Empty)
            {
                sqlString.displayAlert2(this, errMsg.ToString());
                return false;
            }

            return true;
        }

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            sql.Clear();
            sql.AppendFormat(@"
                UPDATE tbl_MemberInfo SET UploadDocument = NULL 
                WHERE MemberId = '{0}';
            ", login_user.UserId);
            db.Execute(sql.ToString());
            if (db.HasError)
            {
                LogUtil.logError(db.ErrorMessage, sql.ToString());
                sqlString.displayAlert(this, Resources.resource.unknownError);
            }
            else
                sqlString.displayAlert(this, Resources.resource.ActionSuccessfulMsg, Request.Url.ToString());
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            FileUpload fupload = fu1;

            if (fupload != null && fupload.HasFile)
            {
                string path = "/Upload/Temp/ProfileImage/";
                string physicalPath = Server.MapPath("~" + path);
                FileInfo Finfo = new FileInfo(fupload.PostedFile.FileName);
                string filename = DateTime.Now.ToString("ddMMyyyyhhmmssfff") + "_" + login_user.UserId + Finfo.Extension;
                string fullPhysical = physicalPath + filename;
                if (!Directory.Exists(physicalPath))
                    Directory.CreateDirectory(physicalPath);

                if (File.Exists(physicalPath))
                    File.Delete(fullPhysical);

                fupload.SaveAs(fullPhysical);
                uploadImage = fullPhysical;
                Session["uploadImages"] = uploadImage;
                //Image imgDisplay = img_fu1;
                //imgDisplay.Visible = true;
                //imgDisplay.Attributes.Add("onclick", "window.open('" + path + filename + "')");

                imgPreview.Visible = true;
                imgPreview.ImageUrl = path + filename;
            }
        }
    }
}