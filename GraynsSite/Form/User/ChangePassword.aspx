﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Global1.Master" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="HJT.Form.User.ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function confirmSubmit() {
            return confirm('<%= GetGlobalResourceObject("resource","Confirm_submit")%>');
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <aside class="right-side">
        <section class="content-header">
            <h1>
                <span class="icon fa fa-fw fa-book" aria-hidden="true"></span>
                <asp:Label ID="lblPageTitle" runat="server"><i class="icon-th-list"></i><span runat="server" id="spHeader"><%= GetGlobalResourceObject("resource", "Change_Password")%></span></asp:Label>
            </h1>
            <asp:SiteMapPath ID="SiteMapPath1" CssClass="breadcrumb" runat="server" ></asp:SiteMapPath>
        </section>
        <section class="content">
            <div  class="col-md-12">
                <div id="Div1" runat="server" >
                    <div class="box">
                         <div id="divErrorMessage" runat="server" visible="false" class="alert alert-danger alert-dismissable">
                            <i class="fa fa-ban"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <b><%= GetGlobalResourceObject("resource", "Alert")%></b>
                            <br />
                            <asp:Label runat="server" ID="lblErr"></asp:Label>
                        </div>
                        <asp:Panel runat="server" DefaultButton="btnsubmit">
                            <div class="box-body">
                                <div class="form-group">
                                    <label class="control-label"><%= GetGlobalResourceObject("resource", "Old_Password")%></label>
                                    <div class="controls">
                                       <asp:TextBox runat="server" ID="txtOldPass" CssClass="form-control" TextMode="Password" ></asp:TextBox>
                                    </div>
                                </div>
                                  <div class="form-group">
                                    <label class="control-label"><%= GetGlobalResourceObject("resource", "New_Password")%></label>
                                    <div class="controls">
                                       <asp:TextBox runat="server" ID="txtNewPassword" CssClass="form-control" TextMode="Password" ></asp:TextBox>
                                    </div>
                                </div>

                                  <div class="form-group">
                                    <label class="control-label"><%= GetGlobalResourceObject("resource", "New_Password_Confirmation")%></label>
                                    <div class="controls">
                                       <asp:TextBox runat="server" ID="txtNewPasswordConfirm" CssClass="form-control"  TextMode="Password"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <asp:Button ID="btnsubmit" OnClientClick="return confirmSubmit();" Text="<%$ Resources:Resource, Submit%>" 
                                    runat="server" CssClass="btn btn-searh" OnClick="btnsubmit_Click" />
                            </div>
                        </asp:Panel>
                    </div>
                </div>
            </div>
        </section>
    </aside>
</asp:Content>