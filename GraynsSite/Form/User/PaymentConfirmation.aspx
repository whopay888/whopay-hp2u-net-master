﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaymentConfirmation.aspx.cs" Inherits="EDV.Form.User.PaymentConfirmation" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Payment Confirmation Page</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" />
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <div class="row text-center">
                <div runat="server" id="divSuccess" class="col-sm-6 col-sm-offset-3">
                    <br/>
                    <br/>
                    <h2 style="color: #0fad00">Payment Success</h2>
                    <img src="../../img/greencheck.png" width="250"/>
                    <h3>Thank you!</h3>
                    <p style="font-size: 20px; color: #5C5C5C;">
                        Your payment has been processed successfully.
                    </p>
                    <asp:Button runat="server" CssClass="btn btn-success" ID="btnRedirect" OnClick="btnRedirect_Click" Text="Back to Billing Page" />
                    <br />
                    <br />
                </div>

                <div runat="server" id="divError" class="col-sm-6 col-sm-offset-3">
                    <br/>
                    <br/>
                    <h2 style="color: red">Payment Fail</h2>
                    <img src="../../img/redalert.png" width="250"/>
                    <h3>Fail to process the payment. Please contact customer support with the following error message for assistance.</h3>
                    <p style="font-size: 20px; color: #5C5C5C;">
                        <asp:Label runat="server" ID="errorMsg"></asp:Label>
                    </p>
                    <asp:Button runat="server" CssClass="btn btn-danger" ID="btnRedirect2" OnClick="btnRedirect_Click" Text="Back to Billing Page" />
                    <br />
                    <br />
                </div>
            </div>
        </div>
    </form>
</body>
</html>



