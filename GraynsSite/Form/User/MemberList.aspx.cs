﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Controller;
using Synergy.Model;
using Synergy.Util;

namespace EDV.Form.User
{
    public partial class MemberList : System.Web.UI.Page
    {
        LoginUserModel login_user = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            login_user = (LoginUserModel)Session[KeyVal.loginsecure];
            
            if (!IsPostBack)
            {
                bindData();
                sqlString.bindControl(gv, getMemberList(null));
            }

            if (gv.Rows.Count > 0)
                gridView.ApplyPaging(gv, -1);
        }
        protected void bindData()
        {
            sqlString.bindControl(ddlBranch, "select distinct BranchName as '0', BranchID as '1' " +
                "from tbl_branch where isdeleted = '0' " +
                "order by '0'", "0", "1", true);

            divBranch.Visible = login_user.isStaffAdmin();
        }

        #region GridView - GV

        private string getMemberList(IDictionary<string,string> sortBy)
        {
            StringBuilder sql = new StringBuilder();

            sql.Append(@"
                Select * from (
                    SELECT TMI.MEMBERID , TMI.Username , TMI.Fullname , TMI.Mobile , TMI.Email ,
                    TMI.Roles , TL.login_password AS 'Password' , TL.BranchID  , TRC.RoleName, tmi.status,
                    TB.BranchName, 
                    CASE WHEN TL.IsChangedPassword = 1 
	                    THEN 'YES' 
	                    ELSE 'NO' 
                    END as PasswordStatus,
                    ISNULL(tucb.CreditUsed,0) AS CreditUsed, 
                    CASE WHEN tbs.CreditLimit IS NULL 
	                    THEN (
		                    SELECT CreditLimit 
		                    FROM dbo.tbl_BillingSetting tbs1 
		                    WHERE tbs1.BranchId = 0 AND tbs1.IsActive = 1) 
	                    ELSE tbs.CreditLimit 
                    END AS CreditLimit
                    FROM TBL_MEMBERINFO TMI
                    LEFT JOIN tbl_Login TL ON TMI.MemberId = TL.login_id
                    LEFT JOIN
	                    (SELECT tmr.MemberId, 
	                    STUFF(
		                    (SELECT ',' + trc.RoleName 
		                    FROM tbl_RoleControl trc 
		                    WHERE trc.RoleID IN
			                    (SELECT RoleCode  
			                    FROM dbo.tbl_MemberRole a 
			                    WHERE a.MemberId = tmr.MemberId) 
		                    FOR XML PATH(''))
	                    ,1,1,'') AS RoleName
	                    FROM dbo.tbl_MemberRole AS tmr
	                    GROUP BY tmr.MemberId)
	                    TRC ON TMI.MemberId = TRC.MemberId
                    LEFT JOIN tbl_branch TB ON TL.BranchID = TB.BranchID
                    LEFT JOIN dbo.tbl_UserCreditBalance tucb ON TMI.MemberId = tucb.MemberId
                    LEFT JOIN 
	                    (SELECT * FROM tbl_BillingSetting 
	                    WHERE dbo.tbl_BillingSetting.DateStart <= GETDATE() 
	                    AND dbo.tbl_BillingSetting.DateEnd >= GETDATE()) tbs
                    ON TL.BranchID = tbs.BranchId
                    where 1=1 
                ");
            sql.Append(sqlString.searchTextBox("TMI.Username ", txtUsername, true, true, false));
            sql.Append(sqlString.searchTextBox("TMI.Fullname ", txtFullName, true, true, false));
            sql.Append(sqlString.searchDropDownList("TL.BranchID ", ddlBranch, true, true, false));

            sql.Append(") As A");

            sql.Append(" order by ");

            //ordering
            if (sortBy != null && sortBy.Any())
            {
                int i = 0;
                foreach (var item in sortBy)
                {
                    string key = item.Key;
                    string value = item.Value;

                    if (i > 0)
                        sql.Append(",");

                    switch (key)
                    {
                        case "CreditBalance":
                            sql.Append("cast(A.CreditUsed as decimal) " + value);
                            break;
                        case "CreditLimit":
                            sql.Append("cast(A.CreditLimit as decimal) " + value);
                            break;
                        case "Username":
                            sql.Append("a.Username " + value);
                            break;
                        case "FullName":
                            sql.Append("a.Fullname " + value);
                            break;
                        default:
                            break;
                    }
                    i++;
                }
            }
            else
                sql.Append(" A.Username asc ");

            return sql.ToString();
        }

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //TextBox txtPassword = (TextBox)e.Row.FindControl("txtPassword");
                string password = DataBinder.Eval(e.Row.DataItem, "password").ToString();
                string memberId = DataBinder.Eval(e.Row.DataItem, "MEMBERID").ToString();
                string status = DataBinder.Eval(e.Row.DataItem, "status").ToString();
                Button btnUpdate = (Button)e.Row.FindControl("btnUpdate");
                btnUpdate.Visible = true;
                //txtPassword.Text = secure.Decrypt(password, true);
                if (status != "A")
                {
                    Button btnSuspend = (Button)e.Row.FindControl("btnSuspend");
                    Button btnReactive = (Button)e.Row.FindControl("btnReactive");
                    btnSuspend.Visible = false;
                    btnUpdate.Visible = false;
                    btnReactive.Visible = true;
                }
                btnUpdate.Attributes.Add("onclick", "window.location.href = '" + "/Form/Register/EditMember.aspx?memberid=" + sqlString.encryptURL(memberId) + "'");
            }
        }
        protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            Dictionary<string, string> sortBy = new Dictionary<string, string>();
            if (ViewState["SortBy"] != null)
            { sortBy = (Dictionary<string, string>)ViewState["SortBy"]; }

            gv.PageIndex = e.NewPageIndex;
            sqlString.bindControl(gv, getMemberList(sortBy));
            if (gv.Rows.Count > 0)
                gridView.ApplyPaging(gv, e.NewPageIndex);
        }
        protected void gv_Sorting(object sender, GridViewSortEventArgs e)
        {
            string expression = e.SortExpression;
            string direction = null;

            //3 state sorting
            //ASC,DESC,null
            if (GetGridViewSortDirection(expression) == "ASC")
            {
                SetGridViewSortDirection(expression, "DESC");
                direction = "DESC";
            }
            else if (GetGridViewSortDirection(expression) == "DESC")
            {
                SetGridViewSortDirection(expression, null);
                direction = null;
            }
            else if (GetGridViewSortDirection(expression) == null)
            {
                SetGridViewSortDirection(expression, "ASC");
                direction = "ASC";
            }

            Dictionary<string, string> sortBy = new Dictionary<string, string>();
            if (direction != null)
                sortBy.Add(e.SortExpression, direction);

            ViewState["SortBy"] = sortBy;

            sqlString.bindControl(gv, getMemberList(sortBy));

            if (gv.Rows.Count > 0)
                gridView.ApplyPaging(gv, -1);
        }

        private string GetGridViewSortDirection(string sortExpression)
        {
            if (ViewState["sortDirection" + sortExpression] == null)
                return null;
            else
                return (string)ViewState["sortDirection" + sortExpression];
        }

        private void SetGridViewSortDirection(string sortExpression, string value)
        {
            ViewState["sortDirection" + sortExpression] = value;
        }
        #endregion

        #region Controls
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> sortBy = new Dictionary<string, string>();
            if (ViewState["SortBy"] != null)
            { sortBy = (Dictionary<string, string>)ViewState["SortBy"]; }

            sqlString.bindControl(gv, getMemberList(sortBy));
            if (gv.Rows.Count > 0)
                gridView.ApplyPaging(gv, -1);
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {


            GridViewRow gvr = (GridViewRow)((Button)sender).NamingContainer;
            Label Hid_MemberID = (Label)gvr.FindControl("Hid_MemberID");

            TextBox txtEmail = (TextBox)gvr.FindControl("txtEmail");
            TextBox txtMobile = (TextBox)gvr.FindControl("txtMobile");
            TextBox txtName = (TextBox)gvr.FindControl("txtName");
            TextBox txtPassword = (TextBox)gvr.FindControl("txtPassword");

            if (validationForm(txtName.Text, txtPassword.Text, txtEmail.Text, txtMobile.Text))
            {
                MemberController MC = new MemberController();
                MemberModel MM = new MemberModel();
                MM.MemberId = Hid_MemberID.Text;
                MM.Password = txtPassword.Text;
                MM.Fullname = txtName.Text;
                MM.Email = txtEmail.Text;
                MM.Mobile = txtMobile.Text;
                MC.UpdateUserInfo(MM);
                sqlString.displayAlert2(this, Resources.resource.Action_Successful , Request.Url.ToString());
                
            }

           

        }

        private bool validationForm(string fullname , string password , string email , string mobile)
        {
            bool isvalid = false;
            StringBuilder errMsg = new StringBuilder();


            if(string.IsNullOrEmpty(fullname))
            {
                errMsg.Append(Resources.resource.nameEmpty+ "\\n");
            }


            if (string.IsNullOrEmpty(password))
            {
                errMsg.Append(Resources.resource.passwordEmpty + "\\n");
            }



            if (string.IsNullOrEmpty(email))
            {
                errMsg.Append(Resources.resource.emailEmpty + "\\n");
            }
            else if(!Validation.IsValidEmail(email))
            {
                errMsg.Append(Resources.resource.emailFormatError + "\\n");
            }



            if (string.IsNullOrEmpty(mobile))
            {
                errMsg.Append(Resources.resource.mobileEmpty + "\\n");
            }


            if(errMsg.Length>0)
            {
                sqlString.displayAlert2(this, errMsg.ToString());
                isvalid = false;
            }
            else
            {
                isvalid = true ;
            }


            return isvalid;
            
        }

        protected void btnSuspend_Click(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Button)sender).NamingContainer;
            Label Hid_MemberID = (Label)gvr.FindControl("Hid_MemberID");
            MemberController MC = new MemberController();
            MemberModel MM = new MemberModel();
            MM.MemberId = Hid_MemberID.Text;
            MM.Status = MemberStatus.Suspend;
            MC.UpdateUserInfo(MM);
            sqlString.displayAlert2(this, Resources.resource.Action_Successful, Request.Url.ToString());
        }

        protected void btnReactive_Click(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Button)sender).NamingContainer;
            Label Hid_MemberID = (Label)gvr.FindControl("Hid_MemberID");
            MemberController MC = new MemberController();
            MemberModel MM = new MemberModel();
            MM.MemberId = Hid_MemberID.Text;
            MM.Status = MemberStatus.Active;
            MC.UpdateUserInfo(MM);
            sqlString.displayAlert2(this, Resources.resource.Action_Successful, Request.Url.ToString());
        }
        #endregion

        protected void btnMobile_Click(object sender, EventArgs e)
        {
            LinkButton btnSender = (LinkButton)sender;
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();
            string memberID = btnSender.CommandArgument, phoneNumber = string.Empty;

            //obtaining phoneNumber
            sql.Clear();
            sql.AppendFormat(@"
                SELECT case when mi.Mobile like '%*%'
			                then null
		                when mi.Mobile like '6%'
			                then mi.Mobile
		                else
			                '6' + mi.Mobile
		                end as 'Mobile'
                from tbl_MemberInfo mi with (nolock)
                where MemberId = '{0}'
            ", memberID);
            db.OpenTable(sql.ToString());
            if (db.RecordCount() > 0)
                if (!string.IsNullOrEmpty(db.Item("Mobile")))
                    phoneNumber = db.Item("Mobile");

            if (!string.IsNullOrEmpty(phoneNumber))
            {
                string url = "https://api.whatsapp.com/send?phone=";
                string message;

                message = string.Format(@"Hi, this is staff from WhoPay Sdn Bhd.");

                //encrypting message
                message = message.Replace("%", "%25").Replace("&", "%26");

                //appending url with phoneNumber and text message
                url += phoneNumber + (string.IsNullOrEmpty(message) ? "" : "&text=" + message);
                //encrypting url
                url = url.Replace("'", "%27").Replace(" ", "%20")
                         .Replace("@", "%40").Replace("#", "%23").Replace("$", "%24")//.Replace("%", "%25")
                         .Replace("^", "%5E")//.Replace("&", "%26")
                         .Replace(";", "%3B").Replace(",", "%2C")//.Replace(":", "%3A").Replace("/", "%2F").Replace("?", "%3F")
                         ;
                //open new tab
                ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWhatsappTab_Admin", "window.open('" + url + "');", true);
            }
        }
    }
}