﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Login.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WMElegance.Form.Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  
    <script src="../js/jquery-1.7.1.min.js"></script>
    <script>

        $(window).resize(function () {
            //centerContent();
        });


        function centerContent() {
            var container = $(window);
            var content = $('#slick .register-form');
            content.css("left", (container.width() - content.width()) / 2);
            content.css("top", (container.height() - content.height()) / 2);
        }

        $(document).ready(function () {
            var w = $(window).width();
        });

        $(window).load(function () {
            var hfValue = document.getElementById("<%=hfAnnouncementWindow.ClientID%>").value;

            if (hfValue == null || hfValue == "show")
                $('.hover_bkgr_fricc').show();
            else
                $('.hover_bkgr_fricc').hide();
            $('.trigger-open-announcement').click(function () {
                $('.hover_bkgr_fricc').show();
            });
            //$('.hover_bkgr_fricc').click(function () {
            //    $('.hover_bkgr_fricc').hide();
            //});
            $('.popupCloseButton').click(function () {
                $('.hover_bkgr_fricc').hide();
            });
        });
    </script>
    <style type="text/css">
        body {
            /*background-image: url('/img/login-bg.jpg');*/
            background-image: url('/Images/PeroduaBackground.jpg');
            position: relative;
            width: 100%;
            overflow: hidden;
            background-position: center left;
            z-index: 1;
            background-repeat: no-repeat;
            background-size: 100%;
        }
        .hover_bkgr_fricc{
            background:rgba(0,0,0,.4);
            height:100%;
            position:fixed;
            text-align:center;
            top: 0;
            width:100%;
            z-index:10000;
            display:none;
        }
        .hover_bkgr_fricc .helper{
            display:inline-block;
            height:100%;
            vertical-align:middle;
        }
        .hover_bkgr_fricc > div {
            background-color: #fff;
            box-shadow: 10px 10px 60px #555;
            display: inline-block;
            height: auto;
            max-width: 551px;
            min-height: 100px;
            vertical-align: top;
            text-align: center;
            width: 60%;
            position: relative;
            border-radius: 8px;
            padding: 15px 5%;
            top: 100px;
        }
        .popupCloseButton {
            background-color: #fff;
            border: 3px solid #999;
            border-radius: 50px;
            cursor: pointer;
            display: inline-block;
            font-family: arial;
            font-weight: bold;
            position: absolute;
            top: -20px;
            right: -20px;
            font-size: 25px;
            line-height: 30px;
            width: 30px;
            height: 30px;
            text-align: center;
        }
        .popupCloseButton:hover {
            background-color: #ccc;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="hover_bkgr_fricc" runat="server" id="divAnnouncement">
        <span class="helper"></span>
        <div>
            <div class="popupCloseButton">
                X
            </div>
            <div id="divAnnouncementContent" runat="server">
                <asp:Image runat="server" ID="imgAnnouncement" Width="100%" />
                <p style="font-size:x-large">
                    <strong><asp:Literal runat="server" ID="litAnnouncementHeader" /></strong>
                </p>
                <p style="font-size:large; font-family:Calibri">
                    <i><asp:Literal runat="server" ID="litAnnouncementMessage" /></i>
                </p>
            </div>
            <div id="divAnnouncementEmpty" runat="server" visible="false">
                <p style="font-size:xx-large">
                    <b><asp:Literal runat="server" Text="<%$ Resources:resource, No_Announcement %>" /></b>
                </p>
            </div>
        </div>
    </div>
    <div id="page-2">
        <%--<asp:LinkButton CssClass="trigger-open-announcement" style="float:right; cursor:pointer; color:yellow; font-size:larger" 
            Text="<%$ Resources:resource, Announcement %>" runat="server" Font-Bold="true" Font-Size="Large" Font-Underline="false"
            OnClick="btnOpenAnnouncement_Click" ID="btnOpenAnnouncement"/>--%>
        <a class="trigger-open-announcement" style="float:right; cursor:pointer; font-size:larger">
            <b><asp:Literal runat="server" ID="litAnnouncementText" Text="<%$ Resources:resource, Announcement %>" /></b>
            <asp:HiddenField runat="server" ID="hfAnnouncementWindow" />
        </a>
        <section id="slick">
            <div id="maintenance" runat="server" visible="false" class="register-form" style="position: absolute; z-index: 2; background: rgba(255,255,255,.5);">
                <p class="image" style="margin-bottom: 0; text-align: center; width: 150px; margin: auto;">
                    <img src="../Images/Company_Logo_Big.png" />
                </p>
                 <div class="desc" style="margin-top: 2em; margin-bottom: 1em;">
                    We are currently upgrading our system to give you the fastest and reliable service.
                    <br />
                    <br />
                    Sorry for any inconvenience caused.
                </div> 
            </div>
            <div class="register-form" id="actual" runat="server" visible="false">
                <div class="col-md-12">
                    <p class="image" style="width: 100%; /*margin: 20px auto 0;*/ margin-left: -20px;">
                        <%--<img src="../Images/Company_Logo_Big.png" />--%>
                        <img src="../Images/PeroduaLogo.png" />
                    </p>
                    <br />
                    <p class="intro" style="text-align: center; color: #222; font-weight: 700; font-size: 16px;">
                        <% = Synergy.Util.KeyVal.ProjectName %>
                    </p>

                    <p style="text-align: center; margin-bottom: 15px;"><%= GetGlobalResourceObject("resource", "Sign_in_to_your_account")%></p>
                </div>
                <div id="divLogin" class="col-md-9">
                    <div class="field">
                        <asp:TextBox ID="TxtUserId" runat="server" placeholder="<%$ Resources:Resource, Contact_Number%>" MaxLength="50" data-rule-required="true" TabIndex="1"></asp:TextBox>
                        <span class="entypo-user icon"></span>
                        <span class="slick-tip left" id="sUsernameName" runat="server"><%= GetGlobalResourceObject("resource", "Contact_Number")%></span>
                    </div>

                    <!-- Email input -->
                    <div class="field">
                        <asp:TextBox ID="TxtPassword" runat="server" TextMode="Password" data-rule-required="true" placeholder="<%$ Resources:Resource, Password_Input%>" MaxLength="50" TabIndex="2" />
                        <span class="entypo-lock icon"></span>
                        <span class="slick-tip left"><%= GetGlobalResourceObject("resource", "PasswordEnter")%></span>
                    </div>

                    <!-- Password input -->
                    <div class="field" runat="server" id="trVerification" visible="false">
                        <asp:TextBox ID="TxtVerificationCode" runat="server" class="input-block-level" placeholder="<%$ Resources:Resource, VerificationCode_Input%>" data-rule-required="true" TabIndex="3" />
                        <span class="entypo-lock icon"></span>
                        <span class="slick-tip left">
                            <%= GetGlobalResourceObject("resource", "VerificationCodeEnter")%></span>
                    </div>
                    <!-- Password repeat input -->
                    <div class="field" runat="server">
                        <div id="divImg" runat="server" visible="false">
                            <asp:Image ID="Im1" runat="server" CssClass="related1" />
                            <a runat="server" onserverclick="Unnamed_ServerClick" id="test">
                                <img src="/Images/Refresh.png" class="related1" /></a>
                        </div>
                        <span class="slick-tip left">Repeat your password</span>
                        <asp:Button ID="BtnLogin" Text="<%$ Resources:Resource, Login%>" class="send" runat="server" UseSubmitBehavior="true" OnClick="BtnLogin_Click" ValidationGroup="g1" TabIndex="4" />
                    </div>

                    <div class="field" style="display: none;">
                        <asp:UpdatePanel ID="upHidden" runat="server">
                            <ContentTemplate>
                                <input id="hidd_Count" runat="server" name="hidd_Count" type="hidden" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>

                    <div class="field">
                        <div style="color: #000000; padding: 5px 5px 5px 5px; width: 100%; text-align: center; margin: auto">
                            <a href="/Form/ForgotPassword.aspx" style="color: #188fff; font-size: 13px;"><span><%= GetGlobalResourceObject("resource", "ForgotPassword")%></span></a>
                        </div>
                         <!-- Aaron remove this -->
                        <div runat="server" visible="false" style="color: #000000; padding: 5px 5px 5px 5px; width: 100%; text-align: center; margin: auto">
                            <a href="/Form/MemberRegistration.aspx" style="color: #188fff; font-size: 13px;"><span><%= GetGlobalResourceObject("resource", "Registration")%></span></a>
                        </div>
                        
                        <div style="color: #000000; position: relative; float: left; width: 100%; text-align: center; font-size: 14px; margin: 80px auto 0px; padding: 0px;">
                           <!--%= GetGlobalResourceObject("resource", "Join_Us")%>
                            <br />
                            <!--img src="../img/Carloon_Logo.png" style="width: 100%; height: 100%; margin: 5px 5px;" /-->
                            <asp:Literal ID="LblError" runat="server"></asp:Literal>
                            <div style="color: #188fff;"text-align: center; margin-bottom: font-size: 14px;font-weight: bold">
                                <p style="text-align: center; color: #188fff; font-weight: 700; font-size: 15px;">
                                <%= GetGlobalResourceObject("resource", "Company")%>
                                </p>
                            </div>
                            <div style="color: #188fff;font-size: 10px;"text-align: center ; margin-bottom: font-size: 10px"><%= GetGlobalResourceObject("resource", "Company_Address")%></div>
                            <div style="color: #188fff;font-size: 10px;"text-align: center; margin-bottom: font-size: 10px;"><%= GetGlobalResourceObject("resource", "Company_City")%></div>
                            <asp:LinkButton ID="btnDownload" OnClick="btnDownload_Click" style="color: #ff0000 ; font-size: 10px;" runat="server">- Disclaimer | Terms | Refund Policy -</asp:LinkButton>
                         </div>                      
                    </div>      
                </div>      
            </div>                                 
        </section>        
       </div>       
    <script src="../Vielee/login/js/demo-2.js"></script>
    </asp:Content>



