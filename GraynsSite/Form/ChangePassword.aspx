﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Login.Master" CodeBehind="ChangePassword.aspx.cs" Inherits="WMElegance.Form.ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="../js/jquery-1.7.1.min.js"></script>
    <script>

        $(window).resize(function () {

            //centerContent();

        });


        function centerContent() {
            var container = $(window);
            var content = $('#slick .register-form');
            content.css("left", (container.width() - content.width()) / 2);
            content.css("top", (container.height() - content.height()) / 2);
        }

        $(document).ready(function () {
            var w = $(window).width();

        });
    </script>
    <style type="text/css">
        body {
            background-image: url('/img/login-bg.jpg');
            position: relative;
            width: 100% 100%;
            overflow: hidden;
            background-position: center left;
            z-index: 1;
            background-repeat: no-repeat;
            background-size: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="page-2">
        <section id="slick">
            <div class="register-form" id="actual" runat="server">
                <div class="col-md-12">
                    <p class="image" style="width: 100%; margin: 20px auto 0;">
                        <img src="../Images/Company_Logo_Big.png" />
                    </p>
                    <br />
                    <p class="intro" style="text-align: center; color: #222; font-weight: 700; font-size: 16px;">Change Password</p>
                </div>
                <div style="margin-bottom:90px; padding-bottom:90px;">
                    <div id="divLogin" class="col-md-9">
                        <div>
                            <div class="field">
                                <asp:TextBox ID="txtNewPassword" runat="server" placeholder="<%$ Resources:Resource, New_Password%>" CssClass="form-control" TextMode="Password"></asp:TextBox>
                                <span class="entypo-lock icon"></span>
                                <span class="slick-tip left"><%= GetGlobalResourceObject("resource", "New_Password")%></span>
                            </div>
                        </div>
                        <div>
                            <div class="field">
                                <asp:TextBox ID="txtNewPasswordConfirm" runat="server" placeholder="<%$ Resources:Resource, New_Password_Confirmation%>" CssClass="form-control" TextMode="Password"></asp:TextBox>
                                <span class="entypo-lock icon"></span>
                                <span class="slick-tip left"><%= GetGlobalResourceObject("resource", "New_Password_Confirmation")%></span>
                            </div>
                        </div>
                        <!-- submit button -->
                        <asp:Button ID="BtnSubmit" Text="<%$ Resources:Resource, Submit%>" class="send" runat="server" OnClick="BtnSubmit_Click" />

                    </div>
                    <br />
                    <br />
                    <div style="color: white; position: relative; float: left; font-size: 11px; margin: auto; padding: 20px;">
                        <asp:Label ID="LblError" runat="server" Text="" ForeColor="Red"></asp:Label>
                    </div>
                </div>
            </div>
        </section>

    </div>


    <script src="../Vielee/login/js/demo-2.js"></script>
</asp:Content>
