﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Util;

namespace HJT.Form.Register
{
    public partial class MemberRoleRegistration : System.Web.UI.UserControl
    {
        public event EventHandler RemoveUserControl;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                bindControl();
            }
        }

        private void bindControl()
        {
            wwdb db = new wwdb();
            DataTable dt = db.getDataTable("SELECT RoleID AS '1', RoleName AS '0', convert(nvarchar, havebranch ) AS 'havebranch' FROM tbl_RoleControl WITH (NOLOCK) WHERE isDeleted=0 ORDER BY RoleID ASC");
            ddlAdditionalRole.Items.Clear();
            for (int count = 0; count < dt.Rows.Count; count++)
            {
                DataRow dr = dt.Rows[count];
                ddlAdditionalRole.Items.Add(new ListItem(dr["0"].ToString(), dr["1"].ToString()));
            }
        }

        protected internal void btnCancelRole_Click(object sender, System.EventArgs e)
        {
            //Raise this event so the parent page can handle it   
            RemoveUserControl(sender, e);
        }
    }
}