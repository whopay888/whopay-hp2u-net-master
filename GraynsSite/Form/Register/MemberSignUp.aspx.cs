﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Controller;
using Synergy.Model;
using Synergy.Util;

namespace HJT.Form.Register
{
    public partial class MemberSignUp : System.Web.UI.Page
    {
        List<string> HaveBranchList = new List<string>();
        List<string> role = new List<string>();

        protected void Page_Load(object sender, EventArgs e)
        {
            AddAndRemoveDynamicControls();
            txtPassword.Attributes.Add("value", txtPassword.Text);
            if (!IsPostBack)
            {
                Session["roledroodownid"] = "0";
                bindControl();
            }
            HaveBranchList = (List<string>)Session["HaveBranchList"];
        }

        private void bindControl()
        {
            wwdb db = new wwdb();
            DataTable dt = db.getDataTable("SELECT RoleID AS '1', RoleName AS '0', convert(nvarchar, havebranch ) AS 'havebranch' FROM tbl_RoleControl WITH (NOLOCK) WHERE isDeleted=0 ORDER BY RoleID ASC");

            foreach (DataRow dr in dt.Rows)
            {
                if (dr["havebranch"].ToString() == "1")
                    HaveBranchList.Add(dr["1"].ToString());
                ddlRole.Items.Add(new ListItem(dr["0"].ToString(), dr["1"].ToString()));
            }
            Session["HaveBranchList"] = HaveBranchList;

            sqlString.bindControl(ddlBranch, "SELECT BranchID AS '1' , BranchName AS '0' FROM tbl_Branch WHERE isDeleted = 0 AND Status='A' Order By BranchName", "0", "1", true);

            ddlRole_SelectedIndexChanged(null, null);
        }

        private void bindRoleDropDown(DropDownList ddl)
        {
            wwdb db = new wwdb();
            DataTable dt = db.getDataTable("SELECT RoleID AS '1', RoleName AS '0', convert(nvarchar, havebranch ) AS 'havebranch' FROM tbl_RoleControl WITH (NOLOCK) WHERE isDeleted=0 ORDER BY RoleID ASC");

            for (int count = 0; count < dt.Rows.Count; count++)
            {
                DataRow dr = dt.Rows[count];
                ddl.Items.Add(new ListItem(dr["0"].ToString(), dr["1"].ToString()));
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (validateSubmit())
            {
                MemberController MC = new MemberController();
                MemberModel mm = GetMemberModel();
                if (MC.AddNewUser(ref mm))
                {
                    WMElegance.Form.MemberRegistration memberRegistration = new WMElegance.Form.MemberRegistration();
                    memberRegistration.AddFreeRequesttoUser(mm.MemberId);
                    sqlString.displayAlert2(this, Resources.resource.Action_Successful, Request.Url.ToString());
                }
                else
                    sqlString.displayAlert2(this, Resources.resource.unknownError);
            }
        }

        private bool validateSubmit()
        {
            StringBuilder errMsg = new StringBuilder();
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();



            errMsg.Append(checkUsername(txtUserName.Text));

            if (string.IsNullOrEmpty(txtEmail.Text))
                errMsg.Append(Resources.resource.emailEmpty + "\\n");
            else if (!Validation.IsValidEmail(txtEmail.Text))
                errMsg.Append(Resources.resource.emailFormatError + "\\n");

            if (string.IsNullOrEmpty(txtPassword.Text))
                errMsg.Append(Resources.resource.passwordEmpty + "\\n");

            if (ddlBranch.SelectedIndex == 0)
            {
                sql.Clear();
                sql.Append("Select HaveBranch AS '0' from tbl_RoleControl Where RoleID = N'" + secure.RC(ddlRole.SelectedValue.ToString()) + "';");
                db.OpenTable(sql.ToString());
                if (!db.HasError && db.RecordCount() > 0)
                    if (db.Item("0").ToString() == "True")
                        errMsg.Append(Resources.resource.CurrentRoleHaveBranch + "\\n");
            }
            else
            {
                sql.Clear();
                sql.Append("Select HaveBranch AS '0' from tbl_RoleControl Where RoleID = N'" + secure.RC(ddlRole.SelectedValue.ToString()) + "';");
                db.OpenTable(sql.ToString());
                if (!db.HasError && db.RecordCount() > 0)
                    if (db.Item("0").ToString() == "False")
                        errMsg.Append(Resources.resource.CurrentRoleNoBranch + "\\n");
            }

            role.Add(ddlRole.SelectedItem.Value);

            foreach (Control c in ph1.Controls)
            {
                if (c.GetType().Name.ToLower() == "form_register_memberroleregistration_ascx")
                {
                    MemberRoleRegistration uc = (MemberRoleRegistration)c;
                    DropDownList ddl1 = uc.FindControl("ddlAdditionalRole") as DropDownList;
                    if (!role.Contains(ddl1.SelectedItem.Value))
                        role.Add(ddl1.SelectedItem.Value);
                }
            }

            if (string.IsNullOrEmpty(txtMobile.Text))
                errMsg.Append(Resources.resource.mobileEmpty + "\\n");

            if (string.IsNullOrEmpty(txtName.Text))
                errMsg.Append(Resources.resource.nameEmpty + "\\n");

            lblErr.Text = errMsg.ToString().Replace("\\n", "</br>");

            if (lblErr.Text != string.Empty)
            {
                divErrorMessage.Visible = true;
                sqlString.displayAlert2(this, errMsg.ToString());
                return false;
            }

            return true;
        }

        private string checkUsername(string username)
        {
            string returnStr = string.Empty;
            if (username != string.Empty)
            {
                wwdb db = new wwdb();
                db.OpenTable("select top 1 1 from tbl_memberinfo where username=N'" + secure.RC(txtUserName.Text) + "'");

                if (db.RecordCount() > 0)
                    returnStr = Resources.resource.usernameDuplicateError + "\\n";
            }

            return returnStr;

        }

        protected void txtUserName_TextChanged(object sender, EventArgs e)
        {
            lblErr.Text = checkUsername(txtUserName.Text).Replace("\\n", "</br>");
            divErrorMessage.Visible = lblErr.Text.Length > 0;
        }

        protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlBranch.SelectedValue = "0";
            if (HaveBranchList.Contains(ddlRole.SelectedValue))
                ddlBranch.Enabled = true;
            else
                ddlBranch.Enabled = false;
        }

        private MemberModel GetMemberModel()
        {
            MemberModel mm = new MemberModel();
            mm.Username = txtUserName.Text;
            mm.Password = txtPassword.Text;
            mm.RoleID = ddlRole.SelectedValue;
            mm.UserRole = role;

            if (ddlBranch.SelectedValue != "0")
                mm.BranchList.BranchID = ddlBranch.SelectedValue;

            mm.Mobile = txtMobile.Text;
            mm.Fullname = txtName.Text;
            mm.Email = txtEmail.Text;
            mm.IsChangedPassword = false;

            return mm;
        }

        private void AddAndRemoveDynamicControls()
        {
            Control c = GetPostBackControl(Page);

            if ((c != null))
                if (c.ID.ToString() == "btnAddRole")
                    ltlCount.Text = (Convert.ToInt16(ltlCount.Text) + 1).ToString();

            ph1.Controls.Clear();
            int ControlID = 0;
            for (int i = 0; i <= (Convert.ToInt16(ltlCount.Text) - 1); i++)
            {
                MemberRoleRegistration DynamicUserControl = (MemberRoleRegistration)LoadControl("MemberRoleRegistration.ascx");

                while (InDeletedList("mmr" + ControlID) == true)
                    ControlID += 1;

                DynamicUserControl.ID = "mmr" + ControlID;
                DynamicUserControl.RemoveUserControl += this.HandleRemoveUserControl;

                ph1.Controls.Add(DynamicUserControl);
                ControlID += 1;
            }
            for (int count=0;count<ph1.Controls.Count;count++)
            {
                var control = ph1.Controls[count];
                var usercontrol = control as MemberRoleRegistration;
                DropDownList ddlAdditionalRole = ((DropDownList)usercontrol.FindControl("ddlAdditionalRole"));
                ddlAdditionalRole.SelectedIndexChanged += new EventHandler(ddlAdditionalRole_SelectedIndexChanged);
            }
        }

        public Control GetPostBackControl(Page page)
        {
            Control control = null;

            string ctrlname = page.Request.Params.Get("__EVENTTARGET");
            if ((ctrlname != null) & ctrlname != string.Empty)
                control = page.FindControl(ctrlname);
            else
            {
                foreach (string ctl in page.Request.Form)
                {
                    Control c = page.FindControl(ctl);
                    if (c is Button)
                    {
                        control = c;
                        break;
                    }
                }
            }
            return control;
        }

        private bool InDeletedList(string ControlID)
        {
            string[] DeletedList = ltlRemoved.Text.Split('|');
            for (int i = 0; i <= DeletedList.GetLength(0) - 1; i++)
                if (ControlID.ToLower() == DeletedList[i].ToLower())
                    return true;

            return false;
        }

        public void HandleRemoveUserControl(object sender, EventArgs e)
        {
            Button remove = (sender as Button);
            UserControl DynamicUserControl = (UserControl)remove.Parent;
            ph1.Controls.Remove((UserControl)remove.Parent);
            ltlRemoved.Text += DynamicUserControl.ID + "|";
            ltlCount.Text = (Convert.ToInt16(ltlCount.Text) - 1).ToString();
            ddlAdditionalRole_SelectedIndexChanged(sender, e);
        }

        protected void ddlAdditionalRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> userrole = new List<string>();
            userrole.Add(ddlRole.SelectedItem.Value);
            foreach (Control c in ph1.Controls)
            {
                if (c.GetType().Name.ToLower() == "form_register_memberroleregistration_ascx")
                {
                    MemberRoleRegistration uc = (MemberRoleRegistration)c;
                    DropDownList ddl1 = uc.FindControl("ddlAdditionalRole") as DropDownList;
                    if (!userrole.Contains(ddl1.SelectedItem.Value))
                    {
                        userrole.Add(ddl1.SelectedItem.Value);
                    }
                }
            }
            ddlBranch.Enabled = false;
            foreach (var item in userrole)
                if (HaveBranchList.Contains(item))
                    ddlBranch.Enabled = true;

            if (!ddlBranch.Enabled)
                ddlBranch.SelectedValue = "0";
        }
    }
}