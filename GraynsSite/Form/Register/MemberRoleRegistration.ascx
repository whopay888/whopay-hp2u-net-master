﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MemberRoleRegistration.ascx.cs" Inherits="HJT.Form.Register.MemberRoleRegistration" %>

<table style="width:100%; padding:0px; border-spacing:0px"">
    <tr>
        <td style="width: 90%; vertical-align: top">
            <asp:DropDownList runat="server" ID="ddlAdditionalRole" CssClass="form-control" AutoPostBack="true">
            </asp:DropDownList>
        </td>
        <td style="width: 10%; vertical-align: top">
            <asp:Button ID="btnCancelRole" Text="<%$ Resources:Resource, Remove%>" runat="server" CssClass="btn btn-primary" OnClick="btnCancelRole_Click" />
        </td>
    </tr>
</table>
