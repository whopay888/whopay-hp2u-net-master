﻿using HJT.Cls.Controller;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Helper;
using Synergy.Model;
using Synergy.Util;

namespace HJT.Form.Setting
{
    public partial class SponsorshipImage_Setting : System.Web.UI.Page
    {
        LoginUserModel login_user = null;
        string uploadImage = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            login_user = (LoginUserModel)Session[KeyVal.loginsecure];
            if (!IsPostBack)
            {
                sqlString.bindControl(gv, getSponsorshipImageList(false));
                sqlString.bindControl(gv2, getSponsorshipImageList(true));
                Session["uploadImages"] = uploadImage;
            }
            uploadImage = Session["uploadImages"].ToString();
        }

        #region Controls
        protected void btnAddRow_Click(object sender, EventArgs e)
        {
            if (validateForm())
                if (login_user.isStaffAdmin()) //remove this if enable other role to upload
                {
                    wwdb db = new wwdb();
                    StringBuilder sql = new StringBuilder();

                    string newPath = "/Upload/SponsorshipImages/";
                    if (!Directory.Exists(Server.MapPath("~" + newPath)))
                        Directory.CreateDirectory(Server.MapPath("~" + newPath));

                    string imgpath = uploadImage;
                    FileInfo FromFile = new FileInfo(imgpath);
                    string toFilePath = newPath + FromFile.Name;
                    FromFile.MoveTo(Server.MapPath("~" + toFilePath));

                    string sqlCol = string.Empty, sqlVal = string.Empty;
                    if (!string.IsNullOrWhiteSpace(txtFrom.Text) && !string.IsNullOrWhiteSpace(txtTo.Text) 
                        && DateTime.TryParse(txtFrom.Text, out _) && DateTime.TryParse(txtTo.Text, out _))
                    {
                        sqlCol = ", [From], [To]";
                        sqlVal = string.Format(@", '{0} 00:00', '{1} 23:59'", txtFrom.Text, txtTo.Text);
                    }

                    sql.Clear();
                    sql.AppendFormat(@"
                        INSERT INTO tbl_TSponsorshipImage (ImageName, ImageURL, isDeleted, UploadedAt, UploadedBy, isActive{3})
                        VALUES('{0}', N'{1}', 0, GETDATE(), '{2}', 0{4});
                    ", txtImageName.Text
                    , toFilePath
                    , login_user.UserId
                    , sqlCol
                    , sqlVal);

                    db.Execute(sql.ToString());

                    if (db.HasError)
                    {
                        LogUtil.logError(db.ErrorMessage, sql.ToString());
                        sqlString.displayAlert(this, Resources.resource.unknownError);
                    }
                    else
                        sqlString.displayAlert(this, Resources.resource.ActionSuccessfulMsg, Request.Url.ToString());
                }
        }
        private bool validateForm()
        {
            ReportController rc = new ReportController();
            StringBuilder errMsg = new StringBuilder();

            rc.removeInvalidChar(txtImageName);

            if (string.IsNullOrEmpty(hid_FUID.Value) || string.IsNullOrWhiteSpace(hid_FUID.Value))
                errMsg.Append(Resources.resource.Upload_Empty + "\\n");

            string imgpath = uploadImage;
            if (!File.Exists(imgpath))
            {
                errMsg.Append("* fail to upload the file, please try again." + "\\n");

                Session["uploadImages"] = new List<string>();
                img_fu1.Visible = false;
                img_fu1.Attributes.Remove("onclick");
            }

            if (!string.IsNullOrEmpty(errMsg.ToString()))
            {
                divErrorMessage.Visible = true;
                lblErr.Text = errMsg.ToString().Replace("\\n", "<br/>");
                return false;
            }
            else
                return true;
        }

        protected void btnActive_Click(object sender, EventArgs e)
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            Button btn = (Button)sender;
            string ID = ((GridViewRow)btn.NamingContainer).Cells[0].Text;
            string activation = btn.CommandArgument;

            sql.Clear();
            sql.AppendFormat(@"
                UPDATE tbl_TSponsorshipImage SET isActive = {0} WHERE ID = '{1}';
            ", activation
            , ID);
            db.Execute(sql.ToString());

            if (db.HasError)
            {
                LogUtil.logError(db.ErrorMessage, sql.ToString());
                sqlString.displayAlert(this, Resources.resource.unknownError);
            }
            else
                sqlString.displayAlert(this, Resources.resource.ActionSuccessfulMsg, Request.Url.ToString());
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            Button btnDelete = (Button)sender;
            string ID = ((GridViewRow)btnDelete.NamingContainer).Cells[0].Text;

            sql.Clear();
            sql.AppendFormat(@"
                UPDATE tbl_TSponsorshipImage SET isDeleted = 1 WHERE ID = '{0}';
            ", ID);
            db.Execute(sql.ToString());
            
            if (db.HasError)
            {
                LogUtil.logError(db.ErrorMessage, sql.ToString());
                sqlString.displayAlert(this, Resources.resource.unknownError);
            }
            else
                sqlString.displayAlert(this, Resources.resource.ActionSuccessfulMsg, Request.Url.ToString());
        }

        protected void btnView_Click(object sender, EventArgs e)
        {
            Button btnView = (Button)sender;
            string imageURl = btnView.CommandArgument;

            sqlString.OpenNewWindow_Center(imageURl, "Sponsorship Image", 600, 600, this);
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            FileUpload fupload = fu1;

            if (fupload != null && fupload.HasFile)
            {
                string path = "/Upload/Temp/SponsorshipImages/";
                string physicalPath = Server.MapPath("~" + path);
                FileInfo Finfo = new FileInfo(fupload.PostedFile.FileName);
                string filename = DateTime.Now.ToString("ddMMyyyyhhmmssfff") + "_" + login_user.UserId + Finfo.Extension;
                string fullPhysical = physicalPath + filename;
                if (!Directory.Exists(physicalPath))
                    Directory.CreateDirectory(physicalPath);

                if (File.Exists(physicalPath))
                    File.Delete(fullPhysical);

                fupload.SaveAs(fullPhysical);
                uploadImage = fullPhysical;
                Session["uploadImages"] = uploadImage;
                Image imgDisplay = img_fu1;
                imgDisplay.Visible = true;
                imgDisplay.Attributes.Add("onclick", "window.open('" + path + filename + "')");
            }
        }
        protected void btnPreview_Click(object sender, EventArgs e)
        {
            wwdb db = new wwdb();

            db.OpenTable(@"
                SELECT TOP 1 r.RequestID 
                FROM tbl_Request r with (nolock) 
                WHERE r.isDeleted = 0
                ORDER BY cast(r.RequestID as int) desc
            ");
            if (db.RecordCount() > 0)
            {
                string rid = db.Item("RequestID");

                string reportIrl = "/Form/Loan/DsrReportTemp.aspx?rid=" + sqlString.encryptURL(rid) + "&BankID=" + sqlString.encryptURL("12");
                sqlString.OpenNewWindow_Center(reportIrl, "Report", 800, 600, this);
            }
            else
                sqlString.displayAlert(this, "No sample report found for preview.");
        }
        protected void btnSaveAll_Click(object sender, EventArgs e)
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            sql.Clear();
            sql.AppendFormat(@"
                DELETE FROM tbl_SponsorshipImage WHERE 1 = 1;
            ");
            for(int count = 0; count < gv.Rows.Count; count++)
            {
                TableCellCollection gvRowCells = gv.Rows[count].Cells;
                string status = ((Label)gvRowCells[5].FindControl("lblStatus")).Text;
                string imageURL = ((HiddenField)gvRowCells[6].FindControl("hfImageURL")).Value;
                string from = gvRowCells[3].Text.Replace("&nbsp;", "").Trim();
                string to = gvRowCells[4].Text.Replace("&nbsp;", "").Trim();
                if (status.Equals("Active"))
                {
                    sql.AppendFormat(@"
                        INSERT INTO tbl_SponsorshipImage (ImageName, ImageURL, isDeleted, UploadedAt, UploadedBy, isActive, [From], [To])
                        VALUES('{0}', N'{1}', 0, '{2}', '{3}', 1, {4}, {5});
                    ", gvRowCells[1].Text
                    , imageURL
                    , gvRowCells[2].Text //change to GETDATE() for finalizedAt instead of uploadedAt
                    , login_user.UserId //finalizedBy instead of uploadedBy
                    , string.IsNullOrEmpty(from) ? "NULL" : string.Format("'{0}'", from) //DBNull.Value.ToString()
                    , string.IsNullOrEmpty(to) ? "NULL" : string.Format("'{0}'", to));
                }
            }
            db.Execute(sql.ToString());

            if (db.HasError)
            {
                LogUtil.logError(db.ErrorMessage, sql.ToString());
                sqlString.displayAlert(this, Resources.resource.unknownError);
            }
            else
                sqlString.displayAlert(this, Resources.resource.ActionSuccessfulMsg, Request.Url.ToString());
        }
        #endregion

        #region GridView
        private string getSponsorshipImageList(bool perm)
        {
            StringBuilder sql = new StringBuilder();

            sql.Clear();
            sql.AppendFormat(@"
                SELECT sp.[ID], sp.[ImageName], sp.[ImageURL], sp.[From], sp.[To],
                    sp.[UploadedAt] as 'Date', sp.[UploadedBy], sp.isActive
                FROM tbl_{0}SponsorshipImage sp with (nolock)
                WHERE isDeleted = 0
                ORDER BY sp.UploadedAt
            ", perm ? "" : "T");

            return sql.ToString();
        }
        protected void gv_DataBound(object sender, EventArgs e)
        {

        }

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                bool isActive = ConvertHelper.ConvertToBoolen(DataBinder.Eval(e.Row.DataItem, "isActive").ToString(), false);
                bool isExpired = false;

                DateTime from, to;
                from = ConvertHelper.ConvertToDateTime(DataBinder.Eval(e.Row.DataItem, "From").ToString(), DateTime.Now);
                to = ConvertHelper.ConvertToDateTime(DataBinder.Eval(e.Row.DataItem, "To").ToString(), DateTime.Now);

                if (!(from.Minute == to.Minute)) //ensure that both date is not null (null = system current date as default value) They may have same second, using minute to check
                    if (!(DateTime.Now > from && DateTime.Now < to)) //if current dateTime is not between the assigned (from, to) datetime
                        isExpired = true;

                if (isActive)
                {
                    Button btnActive = (Button)e.Row.Cells[3].FindControl("btnActive");
                    btnActive.Visible = false;
                }
                else
                {
                    Button btnInactive = (Button)e.Row.Cells[3].FindControl("btnInactive");
                    btnInactive.Visible = false;
                }

                Label lblStatus = (Label)e.Row.Cells[4].FindControl("lblStatus");
                lblStatus.Text = isExpired ? "Expired" : (isActive ? "Active" : "Inactive");
            }
        }
        protected void gv2_DataBound(object sender, EventArgs e)
        {

        }

        protected void gv2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                bool isActive = ConvertHelper.ConvertToBoolen(DataBinder.Eval(e.Row.DataItem, "isActive").ToString(), false);
                bool isExpired = false;

                DateTime from, to;
                from = ConvertHelper.ConvertToDateTime(DataBinder.Eval(e.Row.DataItem, "From").ToString(), DateTime.Now);
                to = ConvertHelper.ConvertToDateTime(DataBinder.Eval(e.Row.DataItem, "To").ToString(), DateTime.Now);

                if (!(from.Minute == to.Minute)) //ensure that both date is not null (null = system current date as default value) They may have same second, using minute to check
                    if (!(DateTime.Now > from && DateTime.Now < to)) //if current dateTime is not between the assigned (from, to) datetime
                        isExpired = true;

                Label lblStatus = (Label)e.Row.Cells[4].FindControl("lblStatus");
                lblStatus.Text = isExpired ? "Expired" : (isActive ? "Active" : "Inactive");
            }
        }
        #endregion

    }
}