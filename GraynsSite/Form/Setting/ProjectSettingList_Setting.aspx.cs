﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Synergy.Model;
using Synergy.Util;
using Synergy.Helper;
using System.Data;
using Newtonsoft.Json.Linq;

namespace WMElegance.Form.Report
{
    public partial class ProjectSettingList_Setting : System.Web.UI.Page
    {
        LoginUserModel login_user = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            login_user = (LoginUserModel)Session[KeyVal.loginsecure];
            spHeader.InnerHtml = Resources.resource.ProjectSettingsList;
            if (!IsPostBack)
            {

                if (Request["OID"] != null)
                {
                    // txtOutletID.Text = sqlString.decryptURL(Request["OID"].ToString()).Trim().ToUpper();
                }

                //  sqlString.bindControl(gv, getExData(null, null, true, false));
                ddlPageSize_SelectedIndexChanged(null, null);
            }
            else if (gv.Rows.Count > 0)
                gridView.ApplyPaging(gv, -1);
        }
        private DataTable MakePivotTable(DataTable mainTable)
        {
            DataTable pivotTable = new DataTable();
            DataRow dr = null;


            for (int i = 0; i <= mainTable.Rows.Count; i++)
            {
                pivotTable.Columns.Add(new DataColumn(mainTable.Columns[i].ColumnName, typeof(String)));
            }


            for (int cols = 0; cols < mainTable.Columns.Count; cols++)
            {
                dr = pivotTable.NewRow();
                for (int rows = 0; rows < mainTable.Rows.Count; rows++)
                {
                    if (rows < mainTable.Columns.Count)
                    {
                        dr[0] = mainTable.Columns[cols].ColumnName;
                        dr[rows + 1] = mainTable.Rows[rows][cols];
                    }
                }
                pivotTable.Rows.Add(dr);
            }
            return pivotTable;
        }

        #region Control Event

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> sortBy = new Dictionary<string, string>();
            if (ViewState["SortBy"] != null)
                sortBy = (Dictionary<string, string>)ViewState["SortBy"];

            sqlString.bindControl(gv, getExData(sortBy));

            if (gv.Rows.Count > 0)
                gridView.ApplyPaging(gv, -1);
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> sortBy = new Dictionary<string, string>();
            if (ViewState["SortBy"] != null)
                sortBy = (Dictionary<string, string>)ViewState["SortBy"];
            //if (ddlType.SelectedIndex == 0)
            sqlString.exportFunc(spHeader.InnerHtml, getExData(sortBy));
            //else
            //    //  sqlString.exportFunc(spHeader.InnerHtml, getExData(null, null, false, true));
            //    sqlString.exportFuncWithNestedGridView(spHeader.InnerHtml, new string[] { getExData(null, null, false, true), 
            //    "SELECT a.ProductCode AS '" + Resources.resource.Product_Code + "', a.ProductName AS '" + Resources.resource.Product_Name + "', a.Quantity AS '" + Resources.resource.Quantity + "' " +
            //    "FROM tbl_SalesDetails a WITH (NOLOCK) WHERE a.IsDeleted = '0' AND a.SalesID = {0}"
            //    }, new string[] { "" }, new int[] { 0 });
        }

        protected void btnExportCSV_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> sortBy = new Dictionary<string, string>();
            if (ViewState["SortBy"] != null)
                sortBy = (Dictionary<string, string>)ViewState["SortBy"];
            //if (ddlType.SelectedIndex == 0)
            //    sqlString.exportFunc(spHeader.InnerHtml, getExData(null, null, false, true));
            //else
            //    sqlString.exportFunc(spHeader.InnerHtml, getExData(null, null, false, true));

            sqlString.exportFuncWithNestedGridView(spHeader.InnerHtml, new string[] { getExData(sortBy),
                "SELECT a.ProductCode AS '" + Resources.resource.Product_Code + "', a.ProductName AS '" + Resources.resource.Product_Name + "', a.Quantity AS '" + Resources.resource.Quantity + "' " +
                "FROM tbl_SalesDetails a WITH (NOLOCK) WHERE a.IsDeleted = '0' AND a.SalesID = {0}"
                }, new string[] { "" }, new int[] { 0 });
        }

        #endregion


        #region GetData
        protected string getExData(IDictionary<string, string> sortBy)
        {
            StringBuilder sql = new StringBuilder();

            sql.Append(" SELECT * FROM ( ");
            sql.Append(" 	SELECT tps.ProjectID AS 'ProjectID', tps.ProjectName AS 'ProjectName', tps.Capping AS 'Capping', tps.Remarks AS 'Remarks', " +
                "tps.BranchId AS 'BranchID' ,ISNULL(tb.BranchName,'-') AS 'BranchName'");
            sql.Append(" 	FROM tbl_ProjectSettings tps LEFT JOIN tbl_Branch tb ON tps.BranchId = tb.ID");
            sql.Append(" 	WHERE tps.Status = 'A' AND tps.IsDeleted = 'False'");
            if (!string.IsNullOrWhiteSpace(txtProjectName.Text))
                sql.Append(" and (tps.projectname like '%" + txtProjectName.Text + "%' or tb.branchname like '%" + txtProjectName.Text + "%') ");
            sql.Append(" ) A");

            sql.Append(" order by ");

            //ordering
            if (sortBy != null && sortBy.Any())
            {
                int i = 0;
                foreach (var item in sortBy)
                {
                    string key = item.Key;
                    string value = item.Value;

                    if (i > 0)
                        sql.Append(",");

                    switch (key)
                    {
                        case "ProjectID":
                            sql.Append("cast(ProjectID as int) " + value);
                            break;
                        case "ProjectName":
                            sql.Append("ProjectName " + value);
                            break;
                        case "Capping":
                            sql.Append("Capping " + value);
                            break;
                        case "Remarks":
                            sql.Append("Remarks " + value);
                            break;
                        case "BranchID":
                            sql.Append("BranchID " + value);
                            break;
                        case "BranchName":
                            sql.Append("BranchName " + value);
                            break;
                        default:
                            break;
                    }
                    i++;
                }
            }
            else
                sql.Append("ProjectName ");

            LogUtil.logSQL(sql.ToString());
            return sql.ToString();
        }
        #endregion

        #region GridView - GV
        protected void gv_DataBound(object sender, EventArgs e)
        {
            if (gv.Rows.Count > 0)
            {
                StringBuilder sql = new StringBuilder();

                for (int a = 0; a < gv.Rows.Count; a++)
                {
                    int gvProdID = gridView.GetColumnIndexByName(gv, Resources.resource.ID);
                    //int GV_PaymentType = gridView.GetColumnIndexByName(gv, Resources.resource.Payment_Type);


                    //gv.Rows[a].Cells[GV_PaymentType].Text = HttpUtility.HtmlDecode(gv.Rows[a].Cells[GV_PaymentType].Text);

                    GridView gv3 = (GridView)gv.Rows[a].FindControl("gv3");
                    HtmlImage img = (HtmlImage)gv.Rows[a].FindControl("imgPlus");
                    HtmlGenericControl spDetails = (HtmlGenericControl)gv.Rows[a].FindControl("spDetails");
                    img.Visible = true;

                    wwdb db = new wwdb();

                    try
                    {
                        sql.Clear();
                        sql.Append(" SELECT B.BankName AS N'" + Resources.resource.Bank_Name + "', pb.BankMargin AS N'" + Resources.resource.BankMargin + "' ");
                        sql.Append(" FROM tbl_ProjectSettings_Bank pb WITH(NOLOCK) ");
                        sql.Append(" INNER JOIN tbl_BankName b WITH(NOLOCK) ON b.BankID = pb.BankID AND b.Status='A' AND b.isDeleted = 'False' ");
                        sql.Append(" INNER JOIN tbl_ProjectSettings p WITH (NOLOCK) ON p.ProjectID = pb.ProjectID AND p.Status = 'A' AND p.IsDeleted = 'False' ");
                        sql.Append(" WHERE pb.ProjectID = N'" + secure.RC(gv.Rows[a].Cells[gvProdID].Text) + "' ");

                        gv3.DataSource = sqlString.ConvertRowToColumn(db.getDataTable(sql.ToString()));//MakePivotTable(db.getDataTable(sql.ToString()));
                        gv3.DataBind();


                    }
                    catch (Exception ex) { LogUtil.logError(ex.ToString(), sql.ToString()); }
                    finally { db.Close(); }

                }

            }
        }
        
        protected void gv3_DataBound(object sender, EventArgs e)
        {
            GridView gv = (GridView)sender;

            if (gv.Rows.Count > 0)
            {
                for (int a = 0; a < gv.Rows.Count; a++)
                {
                    //gv.Rows[a].Cells[1].Visible = false;
                    gv.Rows[a].Cells[1].Text = HttpUtility.HtmlDecode(gv.Rows[a].Cells[1].Text);
                }
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            Dictionary<string, string> sortBy = new Dictionary<string, string>();
            if (ViewState["SortBy"] != null)
            { sortBy = (Dictionary<string, string>)ViewState["SortBy"]; }

            gv.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue.Trim());
            sqlString.bindControl(gv, getExData(sortBy));

            if (ddlPageSize.SelectedIndex > 0)
                if (gv.Rows.Count > 0)
                    gridView.ApplyPaging(gv, -1);
        }

        protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            Dictionary<string, string> sortBy = new Dictionary<string, string>();
            if (ViewState["SortBy"] != null)
            { sortBy = (Dictionary<string, string>)ViewState["SortBy"]; }

            gv.PageIndex = e.NewPageIndex;
            sqlString.bindControl(gv, getExData(sortBy));

            if (gv.Rows.Count > 0)
                gridView.ApplyPaging(gv, e.NewPageIndex);
        }

        private void SetGridViewSortDirection(string sortExpression, string value)
        {
            ViewState["sortDirection" + sortExpression] = value;
        }
        private string GetGridViewSortDirection(string sortExpression)
        {
            if (ViewState["sortDirection" + sortExpression] == null)
                return null;
            else
                return (string)ViewState["sortDirection" + sortExpression];
        }
        protected void gv_Sorting(object sender, GridViewSortEventArgs e)
        {
            string expression = e.SortExpression;
            string direction = null;

            //3 state sorting
            //ASC,DESC,null
            if (GetGridViewSortDirection(expression) == "ASC")
            {
                SetGridViewSortDirection(expression, "DESC");
                direction = "DESC";
            }
            else if (GetGridViewSortDirection(expression) == "DESC")
            {
                SetGridViewSortDirection(expression, null);
                direction = null;
            }
            else if (GetGridViewSortDirection(expression) == null)
            {
                SetGridViewSortDirection(expression, "ASC");
                direction = "ASC";
            }

            Dictionary<string, string> sortBy = new Dictionary<string, string>();
            if (direction != null)
                sortBy.Add(e.SortExpression, direction);

            ViewState["SortBy"] = sortBy;

            sqlString.bindControl(gv, getExData(sortBy));
            if (gv.Rows.Count > 0)
                gridView.ApplyPaging(gv, -1);
        }
        #endregion

        protected void btnAddRow_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Form/Setting/AddProjectSetting_Setting.aspx");
        }
        
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            int idIndex = gridView.GetColumnIndexByName(gv, Resources.resource.ID);
            string id = ((GridViewRow)btn.NamingContainer).Cells[idIndex].Text.ToString().Trim();
            Response.Redirect("/Form/Setting/EditProjectSetting_Setting.aspx?pid=" + sqlString.encryptURL(id));
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            int idIndex = gridView.GetColumnIndexByName(gv, Resources.resource.ID);
            string id = ((GridViewRow)btn.NamingContainer).Cells[idIndex].Text.ToString().Trim();
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            try
            {
                Dictionary<string, string> sortBy = new Dictionary<string, string>();
                if (ViewState["SortBy"] != null)
                    sortBy = (Dictionary<string, string>)ViewState["SortBy"];
                //terminal
                //sql = " UPDATE tbl_login SET login_status = '0' WHERE login_id = (SELECT memberId FROM tbl_memberInfo WITH (NOLOCK) WHERE memberid = N'" + secure.RC(id) + "'); " +
                //    " UPDATE tbl_memberInfo SET status = 'D', updatedBy = N'" + secure.RC(login_user.UserId) + "' " +
                //    " , UpdatedAt = GETDATE() WHERE memberid = N'" + secure.RC(id) + "'; ";
                sql.Clear();
                sql.AppendFormat(@" 
                    UPDATE tbl_ProjectSettings SET status = 'T', IsDeleted = 'True', 
                        updatedBy = N'{0}', UpdatedAt = GETDATE() 
                    WHERE ProjectID = N'{1}'; 
                    UPDATE tbl_ProjectSettings_Bank SET status = 'T', IsDeleted = 'True', 
                        updatedBy = N'{0}', UpdatedAt = GETDATE() 
                    WHERE ProjectID = N'{1}'; 
                ", secure.RC(login_user.UserId), secure.RC(id));
                db.Execute(sql.ToString());
                LogUtil.logAction(sql.ToString(), "Delete Project Settings and Bank");

                sqlString.displayAlert(this, "Action_Successful");
                sqlString.bindControl(gv, getExData(sortBy));
                if (gv.Rows.Count > 0)
                    gridView.ApplyPaging(gv, -1);
            }

            catch (Exception ex)
            {
                LogUtil.logError(ex.ToString(), sql.ToString()); sqlString.displayAlert(this, KeyVal.UnknownError);
            }
            finally { db.Close(); }
        }
    }
}