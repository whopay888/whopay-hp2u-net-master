﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Global1.Master" AutoEventWireup="true" CodeBehind="CreditAdvisor_Setting.aspx.cs" Inherits="WMElegance.Form.Setting.CreditAdvisor_Setting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function setTab() {
            if ($("#HidTab").val() == '' || $("#HidTab").val() === undefined) {
                $("#HidTab").val("#tab_1");
            }
            $("a[data-toggle='tab'] ");

            $("a[href='" + $("#HidTab").val() + "' ]").click();
        }

        function pageLoad() {
            $("a[data-toggle='tab']").each(
                function (index) {
                    $(this).on("click", function () {
                        $("#HidTab").val($(this).attr("href"));

                    });
                }
            )
            setTab();
        }

        function confirmUpdate() {
            return confirm('Are you sure want to update current information?');
        }

        function confirmDelete() {
            return confirm('Are you sure want to remove current information?');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <aside class="right-side">
        <section class="content-header">
            <h1>
                <asp:Label ID="lblPageTitle" runat="server"><i class="icon-th-list"></i><%= GetGlobalResourceObject("resource", "CreditAdvisorSettingBranach")%></asp:Label>
            </h1>
            <asp:SiteMapPath ID="SiteMapPath1" CssClass="breadcrumb" runat="server"></asp:SiteMapPath>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div id="divErrorMessage" runat="server" visible="false" class="alert alert-danger alert-dismissable">
                        <i class="fa fa-ban"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <b>Alert!</b>
                        <br />
                        <asp:Label runat="server" ID="lblErr"></asp:Label>
                    </div>
                    <ul class="nav nav-tabs">
                        <li class=""><a href="#tab_1" id="aTab1" data-toggle="tab" aria-expanded="false"><%= GetGlobalResourceObject("resource", "CreditAdvice_Content") %></a></li>
                        <li class=""><a href="#tab_2" id="aTab2" data-toggle="tab" aria-expanded="false"><%= GetGlobalResourceObject("resource", "CreditAdvice_Sorting") %></a></li>
                        <li class=""><a href="#tab_3" id="aTab3" data-toggle="tab" aria-expanded="false"><%= GetGlobalResourceObject("resource", "CreditAdvice_Detail_Setting") %></a></li>
                        <asp:HiddenField runat="server" ID="HidTab" ClientIDMode="Static" />
                    </ul>
                    <div class="tab-content box-body" runat="server">
                        <div class="tab-pane" id="tab_1">
                            <div class="box" runat="server" visible="false">
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr style="background: #dec18c;">
                                            <td colspan="2">
                                                <h4><strong><%= GetGlobalResourceObject("resource", "Add_New_Credit_Advisor")%></strong></h4>
                                            </td>
                                        </tr>
                                        <tr class="form-group" runat="server" id="trRegisterCountry">
                                            <td style="width: 35%; border-left: 0px!important;">
                                                <label class="control-label"><%= GetGlobalResourceObject("resource", "New_Credit_Advisor")%></label>
                                            </td>
                                            <td style="width: 50%;" class="controls">
                                                <asp:TextBox ID="txtNewAdvisor" CssClass="form-control" runat="server" placeholder="<%$ Resources:Resource, New_Credit_Advisor%>" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="box-footer">
                                    <asp:Button runat="server" ID="btnSubmit" Visible="true" CssClass="btn btn-edit" OnClick="btnSubmit_Click" Text="<%$ Resources:Resource, Add%>" OnClientClick="return confirm('Comfirm to add new record ?')" />
                                </div>
                            </div>
                            <div class="box box-primary" runat="server">
                                <div class="box-header" style="background: #dec18c;">
                                    <h4><strong><i class="icon-th-list"></i>
                                        <%= GetGlobalResourceObject("resource", "CreditAdvisorList")%></strong></h4>
                                </div>
                                <div class="box-body table-responsive">
                                    <div class="form-group box">
                                        <asp:Panel runat="server" DefaultButton="btnSearch">
                                            <div class="form-group">
                                                <label class="control-label"><%= GetGlobalResourceObject("resource", "SearchByCreditAdvisor")%></label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtSearchAdvisor" runat="server" CssClass="form-control" placeholder="<%$ Resources:Resource, SearchByCreditAdvisor%>" TextMode="MultiLine"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="box-footer">
                                                <asp:Button ID="btnSearch" Text="<%$ Resources:Resource, Search%>" runat="server" CssClass="btn btn-searh"
                                                    OnClick="btnSearch_Click" />
                                                <asp:Button ID="btnReset" Text="<%$ Resources:Resource, Reset%>" runat="server" CssClass="btn btn-export"
                                                    OnClick="btnReset_Click" />
                                            </div>
                                        </asp:Panel>
                                    </div>
                                    <div class="box">
                                        <div class="alert alert-block alert-success alert-nomargin">
                                            <%= GetGlobalResourceObject("resource", "CreditAdvisorList")%>
                                        </div>
                                        <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow" ID="gv" runat="server" AutoGenerateColumns="false"
                                            CssClass="table table-bordered table-striped" GridLines="None"
                                            EmptyDataText="<%$ Resources:Resource, No_Record_Found%>"
                                            Width="100%" ShowFooter="true">
                                            <PagerSettings Mode="NumericFirstLast" />
                                            <Columns>
                                                <asp:BoundField DataField="AdvisorID" HeaderText="<%$ Resources:Resource, ID%>" ItemStyle-Width="5%" />
                                                <asp:BoundField DataField="Category" HeaderText="<%$ Resources:Resource, Category%>" ItemStyle-Width="10%" />
                                                <asp:TemplateField HeaderText="<%$ Resources:Resource, Credit_Advisor%>" ItemStyle-Width="50%">
                                                    <ItemTemplate>
                                                        <asp:TextBox runat="server" Style="width: 100%;" Text='<%# Bind("Description") %>' CssClass="input-small" ID="txtCreditAdvisor" 
                                                            AutoPostBack="false" TextMode="MultiLine" Rows="5" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<%$ Resources:Resource, Action%>" ItemStyle-Width="10%">
                                                    <ItemTemplate>
                                                        <asp:Button ID="btnUpdate" CssClass="btn btn-edit" runat="server" Text="<%$ Resources:Resource, Update%>"
                                                            CommandArgument='<%# Container.DataItemIndex  %>' OnClick="btnUpdate_Click" 
                                                            OnClientClick="confirmUpdate()" />
                                                        <asp:Button ID="btnDelete" CssClass="btn btn-danger" runat="server" Text="<%$ Resources:Resource, Delete%> "
                                                            CommandArgument='<%# Container.DataItemIndex  %>' OnClick="btnDelete_Click" 
                                                            OnClientClick="confirmDelete()" Visible="false"/>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Remark" HeaderText="<%$ Resources:Resource, Remark%>" ItemStyle-Width="25%" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_2">
                            <div class="box box-primary" runat="server">
                                <div class="box-header" style="background: #dec18c;">
                                    <h4><strong><i class="icon-th-list"></i>
                                        <%= GetGlobalResourceObject("resource", "CreditAdvice_Type")%>
                                    </strong></h4>
                                </div>
                                <div class="box-body table-responsive">
                                    <div class="box">
                                        <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow" ID="gvCreditType" runat="server" AutoGenerateColumns="false"
                                            CssClass="table table-bordered table-striped" GridLines="None" EmptyDataText="<%$ Resources:Resource, No_Record_Found%>"
                                            Width="50%" ShowFooter="true">
                                            <PagerSettings Mode="NumericFirstLast" />
                                            <Columns>
                                                <asp:BoundField DataField="AdviseCode" HeaderText="<%$ Resources:Resource, AdviseCode%>" ItemStyle-Width="20%" />
                                                <asp:BoundField DataField="AdviseName" HeaderText="<%$ Resources:Resource, Category%>" ItemStyle-Width="30%" />
                                                <asp:TemplateField HeaderText="<%$ Resources:Resource, Sort%>" ItemStyle-Width="20%">
                                                    <ItemTemplate>
                                                        <asp:TextBox runat="server" Style="width: 100%;" Text='<%# Bind("Sort") %>' 
                                                            CssClass="input-small" ID="txtSorting" AutoPostBack="false" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                    <div class="box-footer">
                                        <asp:Button ID="btnUpdate" CssClass="btn btn-edit" runat="server" Text="<%$ Resources:Resource, Update%>"
                                            OnClick="gvCreditType_btnUpdate_Click" OnClientClick="confirmUpdate()" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_3">
                            <div class="box box-primary" runat="server">
                                <div class="box-header" style="background: #dec18c;">
                                    <h4><strong><i class="icon-th-list"></i>
                                        <%= GetGlobalResourceObject("resource", "CreditAdvice_Detail_Setting")%>
                                    </strong></h4>
                                </div>
                                <div class="box-body table-responsive">
                                    <div class="box">
                                        <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow" ID="gvAdviceDetail" runat="server" AutoGenerateColumns="false"
                                            CssClass="table table-bordered table-striped" GridLines="None" EmptyDataText="<%$ Resources:Resource, No_Record_Found%>"
                                            Width="50%" ShowFooter="true">
                                            <PagerSettings Mode="NumericFirstLast" />
                                            <Columns>
                                                <asp:BoundField DataField="Category" HeaderText="<%$ Resources:Resource, Category%>" ItemStyle-Width="20%" />
                                                <asp:TemplateField HeaderText="<%$ Resources:Resource, Value %>" ItemStyle-Width="50%">
                                                    <ItemTemplate>
                                                        <asp:TextBox runat="server" ID="txtValue" Style="width: 100%;" Text='<%# Bind("Value") %>' 
                                                            CssClass="input-small" AutoPostBack="false" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<%$ Resources:Resource, Action %>" ItemStyle-Width="30%">
                                                    <ItemTemplate>
                                                        <asp:Button ID="btnUpdate" CssClass="btn btn-edit" runat="server" Text="<%$ Resources:Resource, Update%>"
                                                            OnClick="gvAdviceDetail_btnUpdate_Click" OnClientClick="confirmUpdate()" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </aside>
</asp:Content>
