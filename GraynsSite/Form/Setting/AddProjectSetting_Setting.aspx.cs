﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Model;
using Synergy.Util;
using Synergy.Helper;

namespace WMElegance.Form.Setting
{
	public partial class AddProjectSetting_Setting : System.Web.UI.Page
	{
		LoginUserModel login_user = null;

		protected void Page_Load(object sender, EventArgs e)
		{
			login_user = (LoginUserModel)Session[KeyVal.loginsecure];

			if (!IsPostBack)
			{
				lblTitle.Text = Resources.resource.AddProject;
				sqlString.bindControl(gv, GetBankInformation());
				bindControl();
			}
		}

		private void bindControl()
		{
			sqlString.bindControl(ddlBranch, "SELECT ID AS '1' , BranchName AS '0' FROM tbl_Branch WHERE isDeleted = 0 AND Status='A' Order By BranchName", "0", "1", true);
		}

		protected string GetBankInformation()
		{

			wwdb db = new wwdb();
			StringBuilder sql = new StringBuilder();

			try
			{

				sql.Clear();
				sql.Append(" SELECT BankID AS 'BankID', BankName AS 'BankName', '0' AS 'BankMargin' FROM tbl_BankName WITH (NOLOCK) WHERE Status = 'A' AND IsDeleted = 'False';");

				LogUtil.logSQL(sql.ToString());

			}
			catch (Exception ex) { LogUtil.logError(ex.ToString(), sql.ToString()); sqlString.displayAlert(this, KeyVal.UnknownError); }
			finally { db.Close(); }

			return sql.ToString();

		}


		protected void btnBack_Click(object sender, EventArgs e)
		{
			Response.Redirect("/Form/Setting/ProjectSettingList_Setting.aspx");
		}

		protected void btnUpdate_Click(object sender, EventArgs e)
		{
			btnBack.Visible = true;
		}

		protected void txtBankMargin_TextChanged(object sender, EventArgs e)
		{
			TextBox txt = (TextBox)sender;
			StringBuilder errMsg = new StringBuilder();

			if (String.IsNullOrEmpty(txt.Text.Trim()))
			{
				errMsg.Append(Resources.resource.BankMarginCannotEmpty + "\\n");
			}
			else
			{
				if (Validation.isDecimal(txt.Text.ToString()))
				{
					if (ConvertHelper.ConvertToDecimal(txt.Text.ToString(), 2) < 0)
					{
						errMsg.Append(Resources.resource.BankMarginCannotLessThanZero + "\\n");
					}
					else if (ConvertHelper.ConvertToDecimal(txt.Text.ToString(), 2) > 100)
					{
						errMsg.Append(Resources.resource.BankMarginCannotMoreThan100 + "\\n");
					}
				}
				else
				{
					errMsg.Append(Resources.resource.BankMarginCannotString + "\\n");
				}
			}

			if (errMsg.ToString().Trim() != string.Empty)
			{
				sqlString.displayAlert2(this, errMsg.ToString());
				lblErr.Text = errMsg.Replace("\\n", "<br />").ToString();
				lblErr.Visible = true;
				divErrorMessage.Visible = true;
				lblErr.Focus();
			}
			else
			{
				lblErr.Text = "";
				lblErr.Visible = false;
				divErrorMessage.Visible = false;
			}
		}

		protected bool ValidateForm()
		{
			bool result = false;
			StringBuilder errMsg = new StringBuilder();

			if (String.IsNullOrEmpty(txtProjectName.Text))
			{
				errMsg.Append(Resources.resource.ProjectnameCannotEmpty + "\\n");
			}

			for (int i = 0; i < gv.Rows.Count; i++)
			{
				TextBox txt = (TextBox)gv.Rows[i].Cells[2].FindControl("txtBankMargin");

				if (String.IsNullOrEmpty(txt.Text.Trim()))
				{
					errMsg.Append(Resources.resource.BankMarginCannotEmpty + "\\n");
				}
				else
				{
					if (Validation.isDecimal(txt.Text.ToString()))
					{
						if (ConvertHelper.ConvertToDecimal(txt.Text.ToString(), 2) < 0)
						{
							errMsg.Append(Resources.resource.BankMarginCannotLessThanZero + "\\n");
						}
					}
					else
					{
						errMsg.Append(Resources.resource.BankMarginCannotString + "\\n");
					}
				}

			}

			if (errMsg.ToString().Trim() != string.Empty)
			{
				sqlString.displayAlert2(this, errMsg.ToString());
				lblErr.Text = errMsg.Replace("\\n", "<br />").ToString();
				lblErr.Visible = true;
				divErrorMessage.Visible = true;
				lblErr.Focus();
			}
			else
			{
				lblErr.Text = "";
				lblErr.Visible = false;
				divErrorMessage.Visible = false;
				result = true;
			}

			return result;
		}

		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			if (ValidateForm())
			{
				wwdb db = new wwdb();
				StringBuilder sql = new StringBuilder();
				string strLog;
				try
				{
					int newID;
					string remark = "";
					int? branchid = null;
					if (txtRemarks.Text != String.Empty)
					{
						remark = txtRemarks.Text;
					}
					else
					{
						remark = "";
					}

					sql.Clear();
					sql.Append("SELECT TOP 1 CAST(ProjectID AS INT) AS ProjectID FROM tbl_ProjectSettings WITH (NOLOCK) ORDER BY ProjectID desc");
					db.OpenTable(sql.ToString());
					if (db.RecordCount() > 0)
					{
						newID = ConvertHelper.ConvertToInt(db.Item("ProjectID").ToString(), 0) + 1;
					}
					else
					{
						newID = 1;
					}

					sql.Clear();

					if (int.Parse(ddlBranch.SelectedItem.Value) > 0)
					{
						var res = secure.RC(ddlBranch.SelectedItem.Value.ToString());
						if (res != string.Empty)
							branchid = Convert.ToInt32(res);
					}

					sql.Append(" INSERT INTO tbl_ProjectSettings (ProjectID, ProjectName, Capping, Remarks, Status, IsDeleted, CreatedAt, CreatedBy, UpdatedAt, UpdatedBy");
					if (branchid != null)
					{
						sql.Append(", BranchId");
					}
					sql.Append(")");
					sql.Append(" VALUES('" + secure.RC(newID.ToString()) + "', N'" + secure.RC(txtProjectName.Text) + "', N'" + secure.RC(rblCapping.SelectedValue.ToString()) + "', N'" + secure.RC(remark) + "', 'A', 'False', GETDATE(), N'" + secure.RC(login_user.UserId) + "', GETDATE(), N'" + secure.RC(login_user.UserId) + "'");
					if (branchid != null)
					{
						sql.Append("," + branchid);
					}
					sql.Append(");");
					db.Execute(sql.ToString());
					strLog = "Add Project - (ProjectID : " + newID.ToString() + " , Project Name : " + txtProjectName.Text.ToString() + " )";
					LogUtil.logAction(sql.ToString(), strLog.ToString());

					for (int i = 0; i < gv.Rows.Count; i++)
					{
						string id = gv.Rows[i].Cells[0].Text.ToString().Trim();
						TextBox txt = (TextBox)gv.Rows[i].Cells[2].FindControl("txtBankMargin");
						sql.Clear();
						sql.Append(" INSERT INTO tbl_ProjectSettings_Bank (ProjectID, BankID, BankMargin, Status, IsDeleted, CreatedAt, CreatedBy, UpdatedAt, UpdatedBy) ");
						sql.Append(" VALUES ('" + secure.RC(newID.ToString()) + "', N'" + secure.RC(id) + "', N'" + secure.RC(txt.Text) + "', 'A', 'False', GETDATE(), N'" + secure.RC(login_user.UserId) + "', GETDATE(), N'" + secure.RC(login_user.UserId) + "' )");
						db.Execute(sql.ToString());

						strLog = "Add Project Bank - (BankID : " + id.ToString() + " , ProjectID : " + newID.ToString() + " , Bank Margin : " + txt.Text.ToString() + " )";
						LogUtil.logAction(sql.ToString(), strLog.ToString());
					}

					sqlString.displayAlert2(this, Resources.resource.Action_Successful, "/Form/Setting/ProjectSettingList_Setting.aspx");
				}
				catch (Exception ex)
				{ LogUtil.logError(ex.ToString(), sql.ToString()); sqlString.displayAlert(this, KeyVal.UnknownError); }
				finally { db.Close(); }
			}
		}

		protected void btnBack2_Click(object sender, EventArgs e)
		{
			Response.Redirect("/Form/Setting/ProjectSettingList_Setting.aspx");
		}






	}
}
