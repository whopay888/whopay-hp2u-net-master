﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Model;
using Synergy.Util;
using Synergy.Helper;

namespace WMElegance.Form.Setting
{
	public partial class EditProjectSetting_Setting : System.Web.UI.Page
	{
		LoginUserModel login_user = null;

		protected void Page_Load(object sender, EventArgs e)
		{
			login_user = (LoginUserModel)Session[KeyVal.loginsecure];

			if (!IsPostBack)
			{
				int pid = 0;
				if (Request["pid"] != null)
				{
					bindControl();
					pid = ConvertHelper.ConvertToInt32(sqlString.decryptURL(Request["pid"].ToString()).Trim(), 0);
					lblTitle.Text = Resources.resource.EditProject;
					GetProjectInformation();
					sqlString.bindControl(gv, GetBankInformation());
				}
			}
		}

		private void bindControl()
		{
			sqlString.bindControl(ddlBranch, "SELECT ID AS '1' , BranchName AS '0' FROM tbl_Branch WHERE isDeleted = 0 AND Status='A' Order By BranchName", "0", "1", true);
		}

		protected void GetProjectInformation()
		{

			wwdb db = new wwdb();
			StringBuilder sql = new StringBuilder();
			if (Request["pid"] != null)
			{
				int pid = ConvertHelper.ConvertToInt32(sqlString.decryptURL(Request["pid"].ToString()).Trim(), 0);
				try
				{

					sql.Clear();
					sql.Append("SELECT ProjectID, ProjectName, Capping, Remarks, BranchId FROM tbl_ProjectSettings WITH (NOLOCK) WHERE Status = 'A' And IsDeleted = 'False' AND ProjectID = N'" + pid.ToString() + "';");
					db.OpenTable(sql.ToString());
					if (!db.HasError && db.RecordCount() > 0)
					{
						lblProjectID.Text = db.Item("ProjectID");
						txtProjectName.Text = db.Item("ProjectName");
						if (db.Item("BranchId") != null)
						{
							ddlBranch.SelectedValue = db.Item("BranchId");
						}
						txtRemarks.Text = db.Item("Remarks");
						if (db.Item("Capping") == "True")
						{
							rblCapping.SelectedValue = "True";
						}
						else
						{
							rblCapping.SelectedValue = "False";
						}
					}
				}
				catch (Exception ex)
				{
					LogUtil.logError(ex.ToString(), sql.ToString());
				}
				finally { db.Close(); }

			}

		}

		protected string GetBankInformation()
		{
			wwdb db = new wwdb();
			StringBuilder sql = new StringBuilder();
			if (Request["pid"] != null)
			{
				int pid = ConvertHelper.ConvertToInt32(sqlString.decryptURL(Request["pid"].ToString()).Trim(), 0);
				try
				{
					sql.Clear();
					sql.AppendFormat(@" 
                        Select b.BankID, b.BankName, pb.BankMargin FROM tbl_ProjectSettings_Bank pb WITH (NOLOCK)
					    INNER JOIN tbl_BankName b WITH(NOLOCK) ON b.BankID = pb.BankID AND b.IsDeleted = 'False' AND b.Status = 'A'
					    INNER JOIN tbl_ProjectSettings p WITH(NOLOCK) ON p.ProjectID = pb.ProjectID AND p.IsDeleted = 'False' AND p.Status = 'A'
					    WHERE pb.ProjectID = N'{0}' AND pb.Status= 'A' AND pb.IsDeleted = 'False';
                    ", pid.ToString());
					LogUtil.logSQL(sql.ToString());
				}
				catch (Exception ex) { LogUtil.logError(ex.ToString(), sql.ToString()); sqlString.displayAlert(this, KeyVal.UnknownError); }
				finally { db.Close(); }
			}
			return sql.ToString();
		}

		protected void btnBack_Click(object sender, EventArgs e)
		{
			Response.Redirect("/Form/Setting/ProjectSettingList_Setting.aspx");
		}

		protected void btnUpdate_Click(object sender, EventArgs e)
		{
			btnBack.Visible = true;
		}

		protected void txtBankMargin_TextChanged(object sender, EventArgs e)
		{
			TextBox txt = (TextBox)sender;
			StringBuilder errMsg = new StringBuilder();

			if (String.IsNullOrEmpty(txt.Text.Trim()))
			{
				errMsg.Append(Resources.resource.BankMarginCannotEmpty + "\\n");
			}
			else
			{
				if (Validation.isDecimal(txt.Text.ToString()))
				{
					if (ConvertHelper.ConvertToDecimal(txt.Text.ToString(), 2) < 0)
					{
						errMsg.Append(Resources.resource.BankMarginCannotLessThanZero + "\\n");
					}
					else if (ConvertHelper.ConvertToDecimal(txt.Text.ToString(), 2) > 100)
					{
						errMsg.Append(Resources.resource.BankMarginCannotMoreThan100 + "\\n");
					}
				}
				else
				{
					errMsg.Append(Resources.resource.BankMarginCannotString + "\\n");
				}
			}

			if (errMsg.ToString().Trim() != string.Empty)
			{
				sqlString.displayAlert2(this, errMsg.ToString());
				lblErr.Text = errMsg.Replace("\\n", "<br />").ToString();
				lblErr.Visible = true;
				divErrorMessage.Visible = true;
				lblErr.Focus();
			}
			else
			{
				lblErr.Text = "";
				lblErr.Visible = false;
				divErrorMessage.Visible = false;
			}
		}

		protected bool ValidateForm()
		{
			bool result = false;
			StringBuilder errMsg = new StringBuilder();

			if (String.IsNullOrEmpty(txtProjectName.Text))
			{
				errMsg.Append(Resources.resource.ProjectnameCannotEmpty + "\\n");
			}

			for (int i = 0; i < gv.Rows.Count; i++)
			{
				TextBox txt = (TextBox)gv.Rows[i].Cells[2].FindControl("txtBankMargin");

				if (String.IsNullOrEmpty(txt.Text.Trim()))
				{
					errMsg.Append(Resources.resource.BankMarginCannotEmpty + "\\n");
				}
				else
				{
					if (Validation.isDecimal(txt.Text.ToString()))
					{
						if (ConvertHelper.ConvertToDecimal(txt.Text.ToString(), 2) < 0)
						{
							errMsg.Append(Resources.resource.BankMarginCannotLessThanZero + "\\n");
						}
					}
					else
					{
						errMsg.Append(Resources.resource.BankMarginCannotString + "\\n");
					}
				}

			}

			if (errMsg.ToString().Trim() != string.Empty)
			{
				sqlString.displayAlert2(this, errMsg.ToString());
				lblErr.Text = errMsg.Replace("\\n", "<br />").ToString();
				lblErr.Visible = true;
				divErrorMessage.Visible = true;
				lblErr.Focus();
			}
			else
			{
				lblErr.Text = "";
				lblErr.Visible = false;
				divErrorMessage.Visible = false;
				result = true;
			}

			return result;
		}

		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			if (ValidateForm())
			{
				wwdb db = new wwdb();
				StringBuilder sql = new StringBuilder();
				string strLog;
				try
				{
					string remark = "";
					int? branchid = null;
					if (txtRemarks.Text != String.Empty)
					{
						remark = txtRemarks.Text;
					}
					else
					{
						remark = "";
					}

					sql.Clear();

					if (int.Parse(ddlBranch.SelectedItem.Value) > 0)
					{
						var res = secure.RC(ddlBranch.SelectedItem.Value.ToString());
						if (res != string.Empty)
							branchid = Convert.ToInt32(res);
					}

					sql.Append(" UPDATE tbl_ProjectSettings ");
					sql.Append(" SET ProjectName = N'" + secure.RC(txtProjectName.Text) + "', Capping = '" + secure.RC(rblCapping.SelectedValue.ToString()) + "', Remarks = N'" + secure.RC(txtRemarks.Text) + "', UpdatedAt = GETDATE(), UpdatedBy = N'" + secure.RC(login_user.UserId) + "' ");
					if (branchid != null)
					{
						sql.Append(", BranchId=" + branchid);
					}
					sql.Append(" WHERE ProjectID = N'" + lblProjectID.Text + "' ");
					db.Execute(sql.ToString());

					strLog = "Update Project - (ProjectID : " + lblProjectID.Text.ToString() + " , Project Name : " + txtProjectName.Text.ToString() + " )";
					LogUtil.logAction(sql.ToString(), strLog.ToString());

					for (int i = 0; i < gv.Rows.Count; i++)
					{
						string id = gv.Rows[i].Cells[0].Text.ToString().Trim();
						TextBox txt = (TextBox)gv.Rows[i].Cells[2].FindControl("txtBankMargin");
						sql.Clear();
						sql.Append(" Update tbl_ProjectSettings_Bank ");
						sql.Append(" SET BankMargin = N'" + secure.RC(txt.Text) + "', UpdatedAt = GETDATE(), UpdatedBy = N'" + secure.RC(login_user.UserId) + "' ");
						sql.Append(" WHERE ProjectID = N'" + secure.RC(lblProjectID.Text) + "' AND BankID = N'" + secure.RC(id.ToString()) + "'; ");
						db.Execute(sql.ToString());

						strLog = "Update Project Bank - (BankID : " + id.ToString() + " , ProjectID : " + lblProjectID.Text + " , Bank Margin : " + txt.Text.ToString() + " )";
						LogUtil.logAction(sql.ToString(), strLog.ToString());
					}

					sqlString.displayAlert2(this, Resources.resource.Action_Successful, "/Form/Setting/ProjectSettingList_Setting.aspx");
				}
				catch (Exception ex)
				{ LogUtil.logError(ex.ToString(), sql.ToString()); sqlString.displayAlert(this, KeyVal.UnknownError); }
				finally { db.Close(); }
			}
		}

		protected void btnBack2_Click(object sender, EventArgs e)
		{
			Response.Redirect("/Form/Setting/ProjectSettingList_Setting.aspx");
		}






	}
}
