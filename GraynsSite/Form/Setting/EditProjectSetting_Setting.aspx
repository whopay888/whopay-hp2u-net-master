﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Global1.Master" AutoEventWireup="true" CodeBehind="EditProjectSetting_Setting.aspx.cs" Inherits="WMElegance.Form.Setting.EditProjectSetting_Setting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <aside class="right-side">
        <section class="content-header">
            <h1>
                <asp:Label ID="lblPageTitle" runat="server"><i class="icon-th-list"></i><%= GetGlobalResourceObject("resource", "ProjectSettings")%></asp:Label>
            </h1>

            <asp:SiteMapPath ID="SiteMapPath1" CssClass="breadcrumb" runat="server"></asp:SiteMapPath>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div id="divErrorMessage" runat="server" visible="false" class="alert alert-danger alert-dismissable">
                        <i class="fa fa-ban"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <b>Alert!</b>
                        <br />
                        <asp:Label runat="server" ID="lblErr"></asp:Label>
                    </div>
                    <div class="box box-primary">
                        <div class="box-header">
                            <h5>
                                <i><b>
                                    <asp:Label ID="lblTitle" runat="server">
                                    </asp:Label>
                                </b></i>
                            </h5>
                        </div>

                        <div class="box-body">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr style="background: #dec18c;">
                                        <td colspan="2">
                                            <h4><strong><%= GetGlobalResourceObject("resource", "Project_Information")%></strong></h4>
                                        </td>
                                    </tr>
                                    <tr class="form-group" runat="server" id="trProjectID" visible="false">
                                        <td style="width:20%; border-left: 0px!important;">
                                            <label class="control-label"><%= GetGlobalResourceObject("resource", "ProjectID")%>&nbsp;<span style="color: Red;">*</span></label>
                                        </td>
                                        <td style="width:80%;" class="controls">
                                            <asp:Label ID="lblProjectID" runat="server" CssClass="form-control"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr class="form-group" runat="server" id="trProjectName">
                                        <td style="width:20%; border-left: 0px!important;">
                                            <label class="control-label"><%= GetGlobalResourceObject("resource", "ProjectName")%>&nbsp;<span style="color: Red;">*</span></label>
                                        </td>
                                        <td style="width:80%;" class="controls">
                                            <asp:TextBox ID="txtProjectName" runat="server" CssClass="form-control"></asp:TextBox>
                                        </td>
                                    </tr>
									<tr class="form-group" runat="server" id="tr2">
										<td style="width: 20%; border-left: 0px!important;">
											<label class="control-label"><%= GetGlobalResourceObject("resource", "Branch")%>&nbsp;<span style="color: Red;"></span></label>
										</td>
										<td style="width: 80%;" class="controls">
											<asp:DropDownList runat="server" ID="ddlBranch" CssClass="form-control">
											</asp:DropDownList>
										</td>
									</tr>
                                    <tr class="form-group" runat="server" id="trCapping">
                                        <td style="border-left: 0px!important;"><label class="control-label"><%= GetGlobalResourceObject("resource", "Capping70")%></label>
                                        </td>
                                        <td style="" class="controls">
                                            <asp:RadioButtonList ID="rblCapping" runat="server" AutoPostBack="false" RepeatDirection="Horizontal">
                                                <asp:ListItem Text="<%$ Resources:Resource, Yes%>" Value="True"></asp:ListItem>
                                                <asp:ListItem Text="<%$ Resources:Resource, No%>" Value="False" Selected="True"></asp:ListItem>

                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr class="form-group" runat="server" id="trRemarks">
                                        <td style="border-left: 0px!important;">
                                            <label class="control-label"><%= GetGlobalResourceObject("resource", "Remarks")%>&nbsp;<span style="color: Red;"></span></label>
                                        </td>
                                        <td style="" class="controls">
                                            <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="5"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr style="background: #dec18c;">
                                        <td colspan="2">
                                            <h4><strong><%= GetGlobalResourceObject("resource", "BankList")%></strong></h4>
                                        </td>
                                    </tr>
                                    <tr class="form-group" runat="server" id="tr1">
                                        <td style="width: 100%;" class="controls" colspan="2">
                                            <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow" ID="gv" runat="server" AutoGenerateColumns="false"
                                                CssClass="table table-bordered table-striped" GridLines="None"
                                                EmptyDataText="<%$ Resources:Resource, No_Record_Found%>"
                                                Width="100%" ShowFooter="true">
                                                <PagerSettings Mode="NumericFirstLast" />
                                                <Columns>
                                                    <asp:BoundField DataField="BankID" HeaderText="<%$ Resources:Resource, ID%>" ItemStyle-Width="20%" />
                                                    <asp:BoundField DataField="BankName" HeaderText="<%$ Resources:Resource, Bank_Name%>" ItemStyle-Width="30%" />
                                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, BankMargin%>" ItemStyle-Width="50%">
                                                        <ItemTemplate>
                                                            <asp:TextBox runat="server" Style="width: 100%;" Text='<%# Bind("BankMargin") %>' CssClass="input-small" ID="txtBankMargin" AutoPostBack="true" OnTextChanged="txtBankMargin_TextChanged"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </tbody>

                            </table>

                        </div>
                        <div class="box-footer">
                            <asp:Button ID="btnBack" Text="<%$ Resources:Resource, Back%>" CssClass="btn" runat="server"
                                OnClick="btnBack_Click" OnClientClick="return confirm('The information is unsaved. Are you sure want to quit?')"/>
                            <asp:Button ID="btnSubmit" Text="<%$ Resources:Resource, Submit%>" CssClass="btn btn-primary" runat="server"
                                OnClick="btnSubmit_Click" OnClientClick="return confirm('Are you sure want to update current information?') " />
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </aside>
</asp:Content>
