﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using System.Web.UI.WebControls;
using Synergy.Model;
using Synergy.Util;

namespace HJT.Form.Setting
{
    public partial class AnnouncementSetting : System.Web.UI.Page
    {
        LoginUserModel login_user = null;
        string uploadImage = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            login_user = (LoginUserModel)Session[KeyVal.loginsecure];
            if (!IsPostBack)
            {
                binddata();
                Session["uploadImages"] = uploadImage;
            }
            uploadImage = Session["uploadImages"].ToString();
        }

        private void binddata()
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();
            const string Announcement_Header = "AnnouncementHeader";
            const string Announcement_Message = "AnnouncementMessage";
            const string Announcement_Image = "AnnouncementImage";
            const string Announcement_TextColor = "AnnouncementTextColor";

            sql.Clear();
            sql.AppendFormat(@"
                SELECT ParameterName as '0', ParameterName as '1'
                FROM tbl_Parameter with (nolock)
                WHERE Category = 'AnnouncementTextColor'
                ORDER BY cast(Sort as int) asc
            ");
            sqlString.bindControl(ddlTextColor, sql.ToString(), "0", "1", false);

            db.OpenTable(@"
                SELECT ParameterName, ParameterValue FROM tbl_Parameter with (nolock) 
                WHERE Category = 'Announcement'
            ");
            if (db.RecordCount() > 0)
            {
                while (!db.Eof())
                {
                    string parameterName = db.Item("ParameterName");
                    string parameterValue = db.Item("ParameterValue");

                    if (parameterName.Equals(Announcement_Header))
                        txtHeader.Text = parameterValue;
                    else if (parameterName.Equals(Announcement_Message))
                        txtMessage.Text = parameterValue;
                    else if (parameterName.Equals(Announcement_Image))
                    {
                        if (!string.IsNullOrEmpty(parameterValue))
                        {
                            imgPreview.Visible = true;
                            imgPreview.ImageUrl = parameterValue;
                        }
                    }
                    else if (parameterName.Equals(Announcement_TextColor))
                        ddlTextColor.SelectedValue = parameterValue;

                    db.MoveNext();
                }
            }
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            FileUpload fupload = fu1;

            if (fupload != null && fupload.HasFile)
            {
                string path = "/Upload/Temp/AnnouncementImage/";
                string physicalPath = Server.MapPath("~" + path);
                FileInfo Finfo = new FileInfo(fupload.PostedFile.FileName);
                string filename = DateTime.Now.ToString("ddMMyyyyhhmmssfff") + "_" + login_user.UserId + Finfo.Extension;
                string fullPhysical = physicalPath + filename;
                if (!Directory.Exists(physicalPath))
                    Directory.CreateDirectory(physicalPath);

                if (File.Exists(physicalPath))
                    File.Delete(fullPhysical);

                fupload.SaveAs(fullPhysical);
                uploadImage = fullPhysical;
                Session["uploadImages"] = uploadImage;
                //Image imgDisplay = img_fu1;
                //imgDisplay.Visible = true;
                //imgDisplay.Attributes.Add("onclick", "window.open('" + path + filename + "')");

                imgPreview.Visible = true;
                imgPreview.ImageUrl = path + filename;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (validateForm())
                if (login_user.isStaffAdmin()) //remove this if enable other role to edit
                {
                    wwdb db = new wwdb();
                    StringBuilder sql = new StringBuilder();

                    string toFilePath;
                    if (!(string.IsNullOrEmpty(hid_FUID.Value) || string.IsNullOrWhiteSpace(hid_FUID.Value)))
                    {
                        string newPath = "/Upload/AnnouncementImage/";
                        if (!Directory.Exists(Server.MapPath("~" + newPath)))
                            Directory.CreateDirectory(Server.MapPath("~" + newPath));

                        string imgpath = uploadImage;
                        FileInfo FromFile = new FileInfo(imgpath);
                        toFilePath = newPath + FromFile.Name;
                        FromFile.MoveTo(Server.MapPath("~" + toFilePath));
                    }
                    else
                        toFilePath = uploadImage;

                    sql.Clear();
                    sql.AppendFormat(@"
                        UPDATE tbl_Parameter SET ParameterValue = '{0}' 
                        WHERE Category = 'Announcement' AND Parametername = 'AnnouncementHeader';
                        UPDATE tbl_Parameter SET ParameterValue = '{1}' 
                        WHERE Category = 'Announcement' AND Parametername = 'AnnouncementMessage';
                        {2}
                        UPDATE tbl_Parameter SET ParameterValue = '{3}'
                        WHERE Category = 'Announcement' AND Parametername = 'AnnouncementTextColor';
                    ", secure.RC(txtHeader.Text)
                    , secure.RC(txtMessage.Text)
                    , imgPreview.Visible ? string.Format(@"
                        UPDATE tbl_Parameter SET ParameterValue = N'{0}' 
                        WHERE Category = 'Announcement' AND Parametername = 'AnnouncementImage';
                        ", toFilePath) : ""
                    , ddlTextColor.SelectedItem.Text);

                    db.Execute(sql.ToString());
                    if (db.HasError)
                    {
                        LogUtil.logError(db.ErrorMessage, sql.ToString());
                        sqlString.displayAlert(this, Resources.resource.unknownError);
                    }
                    else
                        sqlString.displayAlert(this, Resources.resource.ActionSuccessfulMsg, Request.Url.ToString());
                }
        }
        private bool validateForm()
        {
            StringBuilder errMsg = new StringBuilder();

            //if (string.IsNullOrEmpty(hid_FUID.Value) || string.IsNullOrWhiteSpace(hid_FUID.Value))
            //    errMsg.Append(Resources.resource.Upload_Empty + "\\n");

            if (!File.Exists(uploadImage))
            {
                uploadImage = imgPreview.ImageUrl;
                //errMsg.Append("* fail to upload the file, please try again." + "\\n");

                //Session["uploadImages"] = new List<string>();
                //img_fu1.Visible = false;
                //img_fu1.Attributes.Remove("onclick");
            }

            if (!string.IsNullOrEmpty(errMsg.ToString()))
            {
                divErrorMessage.Visible = true;
                lblErr.Text = errMsg.ToString().Replace("\\n", "<br/>");
                return false;
            }
            else
                return true;
        }

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            sql.Clear();
            sql.AppendFormat(@"
                UPDATE tbl_Parameter SET ParameterValue = '' 
                WHERE Category = 'Announcement' AND Parametername = 'AnnouncementHeader';
                UPDATE tbl_Parameter SET ParameterValue = '' 
                WHERE Category = 'Announcement' AND Parametername = 'AnnouncementMessage';
                UPDATE tbl_Parameter SET ParameterValue = '' 
                WHERE Category = 'Announcement' AND Parametername = 'AnnouncementImage';
                UPDATE tbl_Parameter SET ParameterValue = 'Black' 
                WHERE Category = 'Announcement' AND Parametername = 'AnnouncementTextColor';
            ");

            db.Execute(sql.ToString());
            if (db.HasError)
            {
                LogUtil.logError(db.ErrorMessage, sql.ToString());
                sqlString.displayAlert(this, Resources.resource.unknownError);
            }
            else
                sqlString.displayAlert(this, Resources.resource.ActionSuccessfulMsg, Request.Url.ToString());
        }

        protected void btnRemoveImage_Click(object sender, EventArgs e)
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            sql.Clear();
            sql.AppendFormat(@"
                UPDATE tbl_Parameter SET ParameterValue = '' 
                WHERE Category = 'Announcement' AND Parametername = 'AnnouncementImage';
            ");
            db.Execute(sql.ToString());
            if (db.HasError)
            {
                LogUtil.logError(db.ErrorMessage, sql.ToString());
                sqlString.displayAlert(this, Resources.resource.unknownError);
            }
            else
                sqlString.displayAlert(this, Resources.resource.ActionSuccessfulMsg, Request.Url.ToString());
        }
    }
}