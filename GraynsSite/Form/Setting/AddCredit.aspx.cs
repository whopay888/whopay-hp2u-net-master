﻿using HJT.Cls.Controller;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Synergy.Helper;
using Synergy.Model;
using Synergy.Util;

namespace HJT.Form.Loan
{
    public partial class Add_Credit : System.Web.UI.Page
    {
        LoginUserModel login_user = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            login_user = (LoginUserModel)Session[KeyVal.loginsecure];
            if (!IsPostBack)
            {
                bindControl();
                sqlString.bindControl(gv, getAddCreditHistory());
                checkAccessControl();
            }
        }

        private void checkAccessControl()
        {
            if (login_user.isStaffAdmin())
            {
                gv.Visible = true;
                btnSubmit.Visible = true;
            }
            else
            {
                gv.Visible = false;
                btnSubmit.Visible = false;
            }
        }

        private void bindControl()
        {
            //wwdb db = new wwdb();
            //StringBuilder sql = new StringBuilder();

            //sql.AppendFormat(@"     
            //    SELECT mi.FullName AS '0' , mi.MemberId AS '1' 
            //    FROM tbl_MemberInfo mi inner join 
            //    tbl_UserCreditBalance ucb on mi.MemberId=ucb.MemberId
            //    WHERE Status='A' and ucb.CreditUsed is not null
            //    Order By FullName;
            //");

            sqlString.bindControl(ddlFullname, bindDdlFullnameSQL(), "0", "1", true);
            ddlFullName_SelectedIndexChanged(null, null);
        }
        private string bindDdlFullnameSQL(string name = "")
        {
            StringBuilder sql = new StringBuilder();

            sql.Clear();
            sql.AppendFormat(@"
                SELECT mi.FullName + ' (' + mi.Username + ')' AS '0', 
                    mi.MemberId AS '1' 
                FROM tbl_MemberInfo mi with (nolock) 
                WHERE mi.Status = 'A' {0}
                Order By mi.FullName
            ", string.IsNullOrEmpty(name) ? "" : string.Format(@"and mi.Fullname LIKE '%{0}%'", name));

            return sql.ToString();
        }

        private string getAddCreditHistory()
        {
            //wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            sql.Clear();
            sql.AppendFormat(@"
                SELECT mi.Fullname, 
	                case when ut.Amount > 0 
		                then '+' + cast(ut.Amount as varchar)
	                else
		                cast(ut.Amount as varchar)
	                end AS 'Amount', ut.[CreatedAt], 
	                case when ut.[CreatedBy] is null or ut.CreatedBy like ''
		                then '-'
		                else ut.CreatedBy
	                end as 'CreatedBy', 
	                case when ut.[Remark] is null or ut.Remark like ''
		                then '-'
		                else ut.Remark
	                end as 'Remark'
                FROM [tbl_UserTransactions] ut with (nolock) inner join
                tbl_MemberInfo mi with (nolock) on mi.MemberId = ut.MemberId
                where ut.RequestType like '%Payment%'
                and ut.RequestId is null and ut.Status=1
                order by CreatedAt desc; 
            ");

            return sql.ToString();
        }

        private bool validation()
        {
            StringBuilder errorMessage = new StringBuilder();

            if (ddlFullname.SelectedIndex <= 0)
                errorMessage.Append(Resources.resource.memberNameEmptyErrorMsg + "\\n");

            //if (ConvertHelper.ConvertToDecimal(txtCreditUsed.Text, 0) <= 0)
            //    errorMessage.Append(Resources.resource.CreditUsedZeroErrMsg + "\\n");
            
            if (string.IsNullOrEmpty(txtCreditAmount.Text))
                errorMessage.Append(Resources.resource.amountEmptyErrMsg + "\\n");
            //else if (ConvertHelper.ConvertToDecimal(txtCreditAmount.Text, 0) <= 0)
            //    errorMessage.Append(Resources.resource.amountZeroErrMsg + "\\n");

            if (errorMessage.ToString().Trim() != string.Empty)
            {
                sqlString.displayAlert(this, errorMessage.ToString());//.Replace("\\n", "</br>"));
                return false;
            }
            else
                return true;
        }

        private void clearControl()
        {
            txtBranchName.Text = string.Empty;
            txtContactNumber.Text = string.Empty;
            txtCreditAmount.Text = string.Empty;
            txtRemark.Text = string.Empty;
            txtBalance.Text = string.Empty;
            txtFreeRequest.Text = string.Empty;
            ddlFullname.SelectedIndex = 0;
            //bindControl();
            sqlString.bindControl(gv, getAddCreditHistory());
        }

        protected void ddlFullName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFullname.SelectedIndex != 0)
                bindMember(ddlFullname.SelectedItem.Value.ToString());
            else
                clearControl();
        }
        private void bindMember(string memberId)
        {
            wwdb db = new wwdb();
            DataTable dt = db.getDataTable(string.Format(@"
                        SELECT tmi.MemberId, tmi.DisplayName, tmi.Username, tmi.Fullname,  
	                        tmi.Mobile, tmi.Email, tl.BranchID, tb.BranchName,
	                        ucb.CreditUsed, ucb.FreeRequestCount
                        FROM dbo.tbl_MemberInfo tmi INNER JOIN 
                        dbo.tbl_Login tl ON tmi.MemberId = tl.login_id LEFT JOIN 
                        tbl_Branch tb ON tl.BranchID = tb.BranchId left join
                        tbl_UserCreditBalance ucb on ucb.MemberId = tl.login_id
                        WHERE tmi.MemberId ='{0}'
            ", memberId));
            string branchId = string.Empty;
            if (dt.Rows.Count > 0)
            {
                txtBranchName.Text = dt.Rows[0]["BranchName"].ToString();
                txtContactNumber.Text = dt.Rows[0]["Mobile"].ToString();
                txtBalance.Text = string.IsNullOrEmpty(dt.Rows[0]["CreditUsed"].ToString()) ?
                    "0" : dt.Rows[0]["CreditUsed"].ToString();
                txtFreeRequest.Text = string.IsNullOrEmpty(dt.Rows[0]["FreeRequestCount"].ToString()) ?
                    "0" : dt.Rows[0]["FreeRequestCount"].ToString();
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (validation())
            {
                wwdb db = new wwdb();
                try
                {
                    //decimal creditUsed = 0;
                    //decimal amount = ConvertHelper.ConvertToDecimal(txtCreditAmount.Text, 0);
                    //string memberID = ddlFullname.SelectedItem.Value.ToString();

                    //sql.Clear();
                    //sql.AppendFormat(@"insert into tbl_UserTransactions(MemberId, Amount, Status, RequestType, 
                    //    CreatedAt, CreatedBy, IsFreeRequest, Remark)
                    //    values('{0}', '{1}', '0', 'Manual Payment', GETDATE(), '{2}', '0', '{3}')",
                    //    memberID, amount.ToString(), login_user.UserId, secure.RC(txtRemark.Text));
                    //db.Execute(sql.ToString());
                    //LogUtil.logAction(sql.ToString(), "Manual Pay Credit. Member ID: " + memberID + ". Member Name:" +
                    //    ddlFullname.SelectedItem.ToString());

                    //db.OpenTable(string.Format(@"SELECT ucb.[CreditUsed]
                    //        FROM [dbo].[tbl_UserCreditBalance] ucb inner join
                    //        tbl_MemberInfo mi on mi.MemberId=ucb.MemberId
                    //        where ucb.MemberId='{0}';", memberID));
                    //if (db.RecordCount() > 0)
                    //    creditUsed = ConvertHelper.ConvertToDecimal(db.Item("CreditUsed"), 0) * -1;

                    ////if amount > creditUsed, creditUsed_balance will be negative
                    ////account will be prepaid and the exceed balance will be the credit balance
                    //decimal creditUsed_balance = creditUsed - amount; 

                    //sql.Clear();
                    //sql.AppendFormat(@"update tbl_UserCreditBalance SET CreditUsed = '{0}' WHERE MemberId='{1}';",
                    //    (creditUsed_balance * -1).ToString(), memberID);
                    //db.Execute(sql.ToString());
                    //LogUtil.logAction(sql.ToString(), "Update CreditUsed. MemberID: " + memberID + ". Member Name:" +
                    //    ddlFullname.SelectedItem.ToString());
                    
                    SqlParameter[] billingSPParams = {
                     new SqlParameter("@MemberId", ddlFullname.SelectedItem.Value),
                      new SqlParameter("@Amount", secure.RC(txtCreditAmount.Text)),
                       new SqlParameter("@RequestType", "Manual Payment"),
                        new SqlParameter("@CreatedBy", login_user.UserId),
                         new SqlParameter("@Remark", secure.RC(txtRemark.Text))
                    };

                    db.executeScalarSP("SP_ManualAddBillingTransaction", billingSPParams);

                    if (!db.HasError)
                    {
                        clearControl();
                        sqlString.displayAlert(this, Resources.resource.Action_Successful, Request.Url.ToString());
                    }
                    else
                    {
                        LogUtil.logError(db.ErrorMessage, "Error Add Credit");
                        sqlString.displayAlert(this, db.ErrorMessage);
                    }
                }
                catch (Exception ex) {LogUtil.logError(ex.Message, "Submitting Add Credit. ");}
                finally { db.Close(); }
            }
        }

        protected void btnSearchName_Click(object sender, EventArgs e)
        {
            ReportController reportController = new ReportController();
            reportController.removeInvalidChar("", txtSearchName);

            sqlString.bindControl(ddlFullname, bindDdlFullnameSQL(txtSearchName.Text), "0", "1", true);
        }
    }
}