﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Global1.Master" AutoEventWireup="true" CodeBehind="AccountBranch_Setting.aspx.cs" Inherits="WMElegance.Form.Setting.AccountBranch_Setting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<script>
		function FileUpload(fuid) {
			$('#<%= hid_FUID.ClientID %>').val(fuid);
			$('#<%= btnUpload.ClientID %>').click();
		}
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	<aside class="right-side">
		<section class="content-header">
			<h1>
				<asp:Label ID="lblPageTitle" runat="server"><i class="icon-th-list"></i><%= GetGlobalResourceObject("resource", "AccountBranchSetting")%></asp:Label>
			</h1>
			<asp:SiteMapPath ID="SiteMapPath1" CssClass="breadcrumb" runat="server"></asp:SiteMapPath>
		</section>
		<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div id="divErrorMessage" runat="server" visible="false" class="alert alert-danger alert-dismissable">
						<i class="fa fa-ban"></i>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<b>Alert!</b>
						<br />
						<asp:Label runat="server" ID="lblErr"></asp:Label>
					</div>
					<div class="box">
                        <asp:Panel runat="server" DefaultButton="btnSubmit">
						    <table class="table table-bordered">
							    <tbody>
								    <tr style="background: #dec18c;">
									    <td colspan="2">
										    <h4><strong><%= GetGlobalResourceObject("resource", "Add_New_Account_Branch")%></strong></h4>
									    </td>
								    </tr>
								    <tr class="form-group" runat="server" id="trRegisterCountry">
									    <td style="width: 35%; border-left: 0px!important;">
										    <label class="control-label"><%= GetGlobalResourceObject("resource", "New_Account_Branch")%></label>
									    </td>
									    <td style="width: 50%;" class="controls">
										    <asp:TextBox ID="txtNewBranch" CssClass="form-control" runat="server" placeholder="<%$ Resources:Resource, New_Account_Branch%>"></asp:TextBox>
									    </td>
								    </tr>
								    <tr class="form-group" runat="server">
									    <td style="width: 35%; border-left: 0px!important;">
										    <label class="control-label"><%= GetGlobalResourceObject("resource", "State")%></label>
									    </td>
									    <td style="width: 50%;" class="controls">
										    <asp:DropDownList ID="ddlState" CssClass="form-control" runat="server" />
									    </td>
								    </tr>
								    <tr class="form-group" runat="server" id="tr1">
									    <td style="width: 35%; border-left: 0px!important;">
										    <label class="control-label"><%= GetGlobalResourceObject("resource", "Parent_Account_Branch")%></label>
									    </td>
									    <td style="width: 50%;" class="controls">
										    <asp:DropDownList runat="server" ID="ddlBranch" CssClass="form-control">
										    </asp:DropDownList>
									    </td>
								    </tr>
								    <tr class="form-group" runat="server" id="tr2">
									    <td style="width: 35%; border-left: 0px!important;">
										    <asp:FileUpload runat="server" ID="fu1" onchange="FileUpload('fu1')" />
									    </td>
									    <td>
										    <asp:Image runat="server" ID="img_fu1" ImageUrl="/Images/Doc_Icon.png" Width="50px" Visible="false" />
										    <asp:Button runat="server" ID="btnUpload" Style="display: none;" OnClick="btnUpload_Click" />
										    <asp:HiddenField runat="server" ID="hid_FUID" />
									    </td>
								    </tr>
							    </tbody>
						    </table>
						    <div class="box-footer">
							    <asp:Button runat="server" ID="btnSubmit" Visible="true" CssClass="btn btn-edit" OnClick="btnSubmit_Click" Text="<%$ Resources:Resource, Add%>" OnClientClick="return confirm('Comfirm to add new record ?')" />
						    </div>
                        </asp:Panel>
					</div>
					<div class="box box-primary" runat="server">
						<div class="box-header" style="background: #dec18c;">
							<h4><strong><i class="icon-th-list"></i>
								<%= GetGlobalResourceObject("resource", "AccountBranchList")%></strong></h4>
						</div>
						<div class="box-body table-responsive">
							<div class="box">
								<div class="form-group">
                                    <asp:Panel runat="server" DefaultButton="btnSearch">
									    <div class="form-group">
										    <label class="control-label"><%= GetGlobalResourceObject("resource", "SearchByAccountBranch")%></label>
										    <div class="controls">
											    <asp:TextBox ID="txtSearchBranchName" runat="server" CssClass="form-control" placeholder="<%$ Resources:Resource, SearchByAccountBranch%>"></asp:TextBox>
										    </div>
									    </div>
									    <div class="box-footer">
										    <asp:Button ID="btnSearch" Text="<%$ Resources:Resource, Search%>" runat="server" CssClass="btn btn-searh"
											    OnClick="btnSearch_Click" />
										    <asp:Button ID="btnReset" Text="<%$ Resources:Resource, Reset%>" runat="server" CssClass="btn btn-export"
											    OnClick="btnReset_Click" />
									    </div>
                                    </asp:Panel>
								</div>
							</div>
							<div class="box">
								<div class="alert alert-block alert-success alert-nomargin">
									<%= GetGlobalResourceObject("resource", "AccountBranchList")%>
								</div>
								<asp:GridView AlternatingRowStyle-CssClass="table-alternateRow" ID="gv" runat="server" AutoGenerateColumns="false"
									CssClass="table table-bordered table-striped" GridLines="None"
									EmptyDataText="<%$ Resources:Resource, No_Record_Found%>"
									Width="100%" ShowFooter="true" OnRowDataBound="gv_RowDataBound">
									<PagerSettings Mode="NumericFirstLast" />
									<Columns>
										<asp:BoundField DataField="BranchID" HeaderText="<%$ Resources:Resource, ID%>" />
										<asp:BoundField DataField="BranchName" HeaderText="<%$ Resources:Resource, Branch_Name%>" />
										<asp:BoundField DataField="ParentId" Visible="false" />
                                        <asp:BoundField DataField="State" HeaderText='<%$ Resources:Resource, State %>' />
										<asp:BoundField DataField="ParentBranchName" HeaderText="<%$ Resources:Resource, Parent_Account_Branch%>" />
										<asp:TemplateField HeaderText="<%$ Resources:Resource, Branch_Image%>">
											<ItemTemplate>
												<asp:Image runat="server" ID="gv_img_fu1" ImageUrl="/Images/Doc_Icon.png" Width="50px" Visible="false" />
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="<%$ Resources:Resource, Action%>">
											<ItemTemplate>
												<asp:Button ID="btnUpdate" CssClass="btn btn-edit" runat="server" Text="<%$ Resources:Resource, Update%>"
													CommandArgument='<%# Container.DataItemIndex  %>' OnClick="btnUpdate_Click" OnClientClick="return confirm('Are you sure want to update current information?')" />
												<asp:Button ID="btnDelete" CssClass="btn btn-danger" runat="server" Text="<%$ Resources:Resource, Delete%> "
													CommandArgument='<%# Container.DataItemIndex  %>' OnClick="btnDelete_Click" OnClientClick="return confirm('Are you sure want to remove current information?')" />
											</ItemTemplate>
										</asp:TemplateField>
									</Columns>
								</asp:GridView>
							</div>
						</div>
						<div class="box-footer">
						</div>
					</div>
				</div>
			</div>
		</section>
	</aside>
</asp:Content>
