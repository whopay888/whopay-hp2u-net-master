﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Global1.Master" AutoEventWireup="true" CodeBehind="ProjectSettingList_Setting.aspx.cs" Inherits="WMElegance.Form.Report.ProjectSettingList_Setting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">
    $("[src*=plus]").live("click", function () {
        $(this).closest("tr").after("<tr><td class = 'nestedGridView' colspan = '999'>" + $(this).prev().html() + "</td></tr>")
        $(this).attr("src", "/images/minus.png");
    });
    $("[src*=minus]").live("click", function () {
        $(this).attr("src", "/images/plus.png");
        $(this).closest("tr").next().remove();
    });
    </script>
    <style>
    .tblgv3 tr td:first-child
     {
        font-weight: bold;
     }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <aside class="right-side">
        <section class="content-header">
            <h1>
                <span class="icon fa fa-fw fa-book" aria-hidden="true"></span>
                <asp:Label ID="lblPageTitle" runat="server"><i class="icon-th-list"></i><span runat="server" id="spHeader"></span></asp:Label>
            </h1>
            <asp:SiteMapPath ID="SiteMapPath1" CssClass="breadcrumb" runat="server"></asp:SiteMapPath>
        </section>
        <section class="content">
            <div id="ProjectSettingList" runat="server" class="col-md-12">
                <div runat="server" id="salesList">
                    <div class="box">
                        <h4><asp:Label ID="panel" runat="server" Text="<%$ Resources:Resource, Selection_Option%>"></asp:Label></h4>
                        <asp:Panel runat="server" DefaultButton="btnSearch">
                            <div class="box-body">
                                <div class="form-group" runat="server" id="divSales">
                                    <label class="control-label"><%= GetGlobalResourceObject("resource", "SearchByProjectName")%></label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtProjectName" placeholder="<%$ Resources:Resource, SearchByProjectName%>" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="box-footer">
                                <asp:Button ID="btnSearch" Text="<%$ Resources:Resource, Search%>" runat="server" CssClass="btn btn-searh"
                                    OnClick="btnSearch_Click" />
                                <asp:Button ID="btnExport" Text="<%$ Resources:Resource, Export_to_Excel%>" runat="server" CssClass="btn btn-export"
                                    OnClick="btnExport_Click" />
                                <asp:Button ID="btnExportCSV" Text="<%$ Resources:Resource, Export_to_CSV%>" runat="server" CssClass="btn btn-export" Visible="false"
                                    OnClick="btnExportCSV_Click" />
                            </div>
                        </asp:Panel>
                    </div>
                </div>
            </div>

            <div id="dailysearchreport" runat="server" class="col-md-12">
                <div class="box">
                    <h4>
                        <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, ProjectSettingsList%>"></asp:Label>
                        <span style="float: right; font-size: 14px; margin-top: -6px">
                            <span style="color: black;"><%= GetGlobalResourceObject("resource", "Show")%>&nbsp;:&nbsp;</span>
                            <asp:DropDownList ID="ddlPageSize" CssClass="input-small" runat="server" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged"
                                AutoPostBack="true">
                                <asp:ListItem>5</asp:ListItem>
                                <asp:ListItem>10</asp:ListItem>
                                <asp:ListItem>20</asp:ListItem>
                                <asp:ListItem>25</asp:ListItem>
                                <asp:ListItem>50</asp:ListItem>
                            </asp:DropDownList>
                            <span style="color: black;"><%= GetGlobalResourceObject("resource", "Records")%>&nbsp;</span>
                        </span>
                    </h4>

                    <div class="box-body table-responsive">
                        <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow" ID="gv" runat="server" AutoGenerateColumns="false" GridLines="None" AllowPaging="True" ShowFooter="true"
                            EmptyDataText="<%$ Resources:Resource, No_Record_Found%>" CssClass="table table-bordered table-striped" Width="100%" AllowSorting="true" 
                            OnSorting="gv_Sorting" OnPageIndexChanging="gv_PageIndexChanging" OnDataBound="gv_DataBound">
                            <PagerSettings Mode="NumericFirstLast" />
                            <Columns>
                                <asp:TemplateField HeaderText="">
                                    <ItemTemplate>
                                        <asp:Panel ID="pnlBank" runat="server" Style="display: none;">
                                            <div class="box box-color box-bordered">
                                                <div class="box-content">
                                                    <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow" ID="gv3" runat="server" AutoGenerateColumns="true" OnDataBound="gv3_DataBound" ShowHeader="false"
                                                        CssClass="table table-bordered table-striped tblgv3" GridLines="None" EmptyDataText="<%$ Resources:Resource, No_Record_Found%>"
                                                        Width="100%">
                                                        <PagerSettings Mode="NumericFirstLast" />
                                                        <Columns>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                        <img alt="" runat="server" id="imgPlus" style="cursor: pointer" src="/images/plus.png" />
                                        <%--<asp:Button ID="btnView" CssClass="btn btn-view1" runat="server" Visible="true"
                                            CommandArgument='<%# Container.DataItemIndex  %>'
                                            Text="<%$ Resources:Resource, View%>" OnClick="btnView_Click" />--%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="ProjectID" HeaderText="<%$ Resources:Resource, ID%>" SortExpression="ProjectID" />
                                <asp:BoundField DataField="ProjectName" HeaderText="<%$ Resources:Resource, ProjectName%>" SortExpression="ProjectName" />
								 <asp:BoundField DataField="BranchName" HeaderText="<%$ Resources:Resource, Branch%>" SortExpression="BranchName" />
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, Capping%>" SortExpression="Capping">
                                    <ItemTemplate>
                                        <asp:CheckBox runat="server" ID="chkCapping" Checked='<%# Bind("Capping")%>' Enabled="false"/>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Remarks" HeaderText="<%$ Resources:Resource, Remarks%>" SortExpression="Remarks" />
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, Action%>">
                                    <ItemTemplate>
                                        <asp:Button ID="btnUpdate" CssClass="btn btn-edit" runat="server" Text="<%$ Resources:Resource, Update%>"
                                            CommandArgument='<%# Container.DataItemIndex  %>' OnClick="btnUpdate_Click" OnClientClick="return confirm('Are you sure want to edit current information?')" />
                                        <asp:Button ID="btnDelete" CssClass="btn btn-danger" runat="server" Text="<%$ Resources:Resource, Delete%> "
                                            CommandArgument='<%# Container.DataItemIndex  %>' OnClick="btnDelete_Click" OnClientClick="return confirm('Are you sure want to remove current information?')" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerTemplate>
                                <div>
                                    <div class="dataTables_paginate paging_bootstrap">
                                        <ul class="pagination">
                                            <li>
                                                <asp:PlaceHolder runat="server" ID="phPreviousPaging"></asp:PlaceHolder>
                                            </li>
                                            <li>
                                                <asp:PlaceHolder runat="server" ID="phPaging"></asp:PlaceHolder>
                                            </li>
                                            <li>
                                                <asp:PlaceHolder runat="server" ID="phNextPaging"></asp:PlaceHolder>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </PagerTemplate>
                        </asp:GridView>
                    </div>
                    <div class="box-footer">
                        <asp:LinkButton runat="server" ID="btnAddRow" CssClass="btn btn-primary" OnClick="btnAddRow_Click">
                            <i class="fa fa-plus fa-plus-circle"></i> Add New Project
                        </asp:LinkButton>
                    </div>
                </div>
            </div>
            
        </section>
    </aside>


</asp:Content>
