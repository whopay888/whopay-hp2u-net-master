﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AnnouncementSetting.aspx.cs" Inherits="HJT.Form.Setting.AnnouncementSetting" MasterPageFile="~/Global1.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function confirmSubmit() {
            return confirm('<%= GetGlobalResourceObject("resource" , "Confirm_submit") %>');
        }
        function confirmRemove() {
            return confirm('<%= GetGlobalResourceObject("resource" , "Confirm_remove") %>');
        }

        function FileUpload(fuid) {
            $('#<%= hid_FUID.ClientID %>').val(fuid);
            $('#<%= btnUpload.ClientID %>').click();
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <aside class="right-side">
        <section class="content-header">
            <h1>
                <span class="icon fa fa-fw fa-book" aria-hidden="true"></span>
                <asp:Label ID="lblPageTitle" runat="server"><i class="icon-th-list"></i><span runat="server" id="spHeader"><%= GetGlobalResourceObject("resource", "Announcement_Setting")%></span></asp:Label>
            </h1>
            <asp:SiteMapPath ID="SiteMapPath1" CssClass="breadcrumb" runat="server"></asp:SiteMapPath>
        </section>
        <section class="content">
            <div class="col-md-12">
                <div runat="server">
                    <div class="box-body">
                        <div id="divErrorMessage" runat="server" visible="false" class="alert alert-danger alert-dismissable">
                            <i class="fa fa-ban"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <b>Alert!</b>
                            <br />
                            <asp:Label runat="server" ID="lblErr"></asp:Label>
                        </div>
                        <asp:Panel runat="server" DefaultButton="btnSubmit">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr class="form-group">
                                        <td style="width: 20%; border-left: 0px!important;">
                                            <label class="control-label"><%= GetGlobalResourceObject("resource", "Header")%>&nbsp;<span style="color: Red;">*</span></label>
                                        </td>
                                        <td style="width: 80%;" class="controls">
                                            <asp:TextBox ID="txtHeader" runat="server" CssClass="form-control" />
                                        </td>
                                    </tr>
                                    <tr class="form-group">
                                        <td style="width: 20%; border-left: 0px!important;">
                                            <label class="control-label"><%= GetGlobalResourceObject("resource", "Message")%></label>
                                        </td>
                                        <td style="width: 80%;" class="controls">
                                            <asp:TextBox ID="txtMessage" runat="server" TextMode="MultiLine" Rows="10" CssClass="form-control" />
                                        </td>
                                    </tr>
                                    <tr class="form-group">
                                        <td style="width: 20%; border-left: 0px!important;">
                                            <label class="control-label"><%= GetGlobalResourceObject("resource", "Image")%></label>
                                        </td>
                                        <td style="width: 80%;" class="controls">
									        <asp:FileUpload runat="server" ID="fu1" onchange="FileUpload('fu1')" />
                                            <asp:Image runat="server" ID="img_fu1" ImageUrl="/Images/Doc_Icon.png" Width="50px" Visible="false" />
                                            <asp:Button runat="server" ID="btnRemoveImage" CssClass="btn btn-danger" OnClick="btnRemoveImage_Click"
                                                Text="<%$ Resources:resource, Remove %>" OnClientClick="return confirmRemove();"/>
                                            <asp:Image runat="server" ID="imgPreview" Visible="false" />
                                            <asp:Button runat="server" ID="btnUpload" Style="display: none;" OnClick="btnUpload_Click" />
									        <asp:HiddenField runat="server" ID="hid_FUID" />
                                        </td>
                                    </tr>
                                    <tr class="form-group">
                                        <td style="width: 20%; border-left: 0px!important;">
                                            <label class="control-label"><%= GetGlobalResourceObject("resource", "Text_Color")%></label>
                                        </td>
                                        <td style="width: 80%;" class="controls">
                                            <asp:DropDownList runat="server" CssClass="form-control" ID="ddlTextColor" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="box-footer">
                                <asp:Button runat="server" ID="btnSubmit" CssClass="btn btn-edit" OnClick="btnSubmit_Click"
                                    Text="<%$ Resources:Resource, Save%>" OnClientClick="return confirmSubmit();" />
                                <asp:Button runat="server" ID="btnRemove" CssClass="btn btn-danger" OnClick="btnRemove_Click"
                                    Text="<%$ Resources:resource, Remove %>" OnClientClick="return confirmRemove();"/>
                            </div>
                        </asp:Panel>
                    </div>
                </div>
            </div>
        </section>
    </aside>
</asp:Content>
