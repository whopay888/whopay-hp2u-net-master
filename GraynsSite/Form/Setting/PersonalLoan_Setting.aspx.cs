﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Model;
using Synergy.Util;
using Synergy.Helper;

namespace WMElegance.Form.Setting
{
    public partial class PersonalLoan_Setting : System.Web.UI.Page
    {
        LoginUserModel login_user = null;

        protected void Page_Load(object sender, EventArgs e)
        {
           

            login_user = (LoginUserModel)Session[KeyVal.loginsecure];

            if (!IsPostBack)
            {
                sqlString.bindControl(gv, getExData(null, null, true));
            }

            //if (gv.Rows.Count > 0)
            //{
            //    ApplyPaging(-1);
            //}
        }


        #region GetData

        // Action[0], MemberID[1], Name[2], Ranking[3]*, Date[4], Status[5]
        protected string getExData(string columnName, string sortOrder, bool hasOrder)
        {
            StringBuilder sql = new StringBuilder();

            sql.Clear();

            sql.Append(" SELECT b.BankID AS 'BankID', b.BankName AS 'BankName', ISNULL(h.Year, '0') AS 'PYear', ISNULL(h.Interest, '0.00') AS 'Interest' FROM ");
            sql.Append(" (SELECT CAST(BankID AS INT) AS BankID, BankName from tbl_BankName WITH (NOLOCK) WHERE  IsDeleted = 'False' AND Status = 'A') AS b ");
            sql.Append(" LEFT JOIN tbl_PersonalLoan h WITH (NOLOCK) ON b.BankID = h.BankID ");
            sql.Append(" ORDER BY b.[BankID] ");

            LogUtil.logSQL(sql.ToString());

            return sql.ToString();
        }

        #endregion

        #region GridView - GV

        private SortDirection GridViewSortDirection1
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;

                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }
        }

        protected void gv_Sorting(object sender, GridViewSortEventArgs e)
        {
            string direction = "ASC";

            if (GridViewSortDirection1 == SortDirection.Ascending)
            {
                GridViewSortDirection1 = SortDirection.Descending;
                direction = "DESC";
            }
            else
            {
                GridViewSortDirection1 = SortDirection.Ascending;
                direction = "ASC";
            }

            ViewState["Field1"] = e.SortExpression; ViewState["Direction1"] = direction;

            sqlString.bindControl(gv, getExData(e.SortExpression, direction, true));
        }

        #endregion

        #region Control Event

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            sqlString.bindControl(gv, getExData(null, null, true));
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            sqlString.exportFunc("City List", getExData(null, null, false));
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            //txtSearchBankIDName.Text = "";
            sqlString.bindControl(gv, getExData(null, null, true));
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            if (ValidateForm())
            {
                try
                {

                    sql.Clear();
                    sql.Append(" INSERT INTO tbl_PersonalLoan_History(BankID, Year, Interest, CreatedBy, CreatedAt)");
                    sql.Append(" SELECT BankID, Year, Interest, '" + secure.RC(login_user.UserId) + "' AS 'CreatedBy' , GetDate() AS 'CreatedAt'");
                    sql.Append(" FROM tbl_PersonalLoan WITH(NOLOCK)");
                    db.Execute(sql.ToString());

                    for (int i = 0; i < gv.Rows.Count; i++)
                    {
                        TextBox txtyear = (TextBox)gv.Rows[i].Cells[2].FindControl("txtYear");
                        TextBox txtInterest = (TextBox)gv.Rows[i].Cells[2].FindControl("txtInterest");
                        string id = gv.Rows[i].Cells[0].Text.ToString().Trim();
                        sql.Clear();
                        sql.Append(" EXEC SP_PersonalLoan_DeleteInsert ");
                        sql.Append(" @ID = N'" + secure.RC(id.ToString()) + "' , ");
                        sql.Append(" @PYear = N'" + secure.RC(txtyear.Text) + "' , ");
                        sql.Append(" @Interest = N'" + secure.RC(txtInterest.Text) + "' , ");
                        sql.Append(" @CreatedBy = N'" + secure.RC(login_user.UserId) + "'; ");

                        db.Execute(sql.ToString());
                        string strLog = "Update Personal Loan - (BankID : " + id.ToString() + " , Year : " + txtyear.Text.ToString() + " , Interest : " + txtInterest.Text.ToString() + " )";
                        LogUtil.logAction(sql.ToString(), strLog.ToString());

                    }

                    sqlString.displayAlert(this, "Update Successful");
                    sqlString.bindControl(gv, getExData(null, null, true));
                }

                catch (Exception ex) { LogUtil.logError(ex.ToString(), sql.ToString()); sqlString.displayAlert(this, KeyVal.UnknownError); }
                finally { db.Close(); }
            }
        }

        #region GridView Control

        protected void btnView_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            //int row = Convert.ToInt32(btn.CommandArgument);
            Response.Redirect("/Form/Setting/AddCity.aspx?mid=" + sqlString.encryptURL(((GridViewRow)btn.NamingContainer).Cells[0].Text));
        }

        protected void txtYear_TextChanged(object sender, EventArgs e)
        {
            TextBox txt = (TextBox)sender;
            StringBuilder errMsg = new StringBuilder();

                if (String.IsNullOrEmpty(txt.Text.Trim()))
                {
                    errMsg.Append(Resources.resource.PersonalLoanYearCannotEmpty + "\\n");
                }
                else 
                {
                    if (Validation.isDecimal(txt.Text.ToString()))
                    {
                        if (ConvertHelper.ConvertToDecimal(txt.Text.ToString(), 2) < 0)
                        {
                            errMsg.Append(Resources.resource.PersonalLoanYearCannotLessThanZero + "\\n");
                        }
                    }
                    else
                    {
                        errMsg.Append(Resources.resource.PersonalLoanYearCannotString + "\\n");
                    }
                }
           
                if (errMsg.ToString().Trim() != string.Empty)
                {
                    sqlString.displayAlert2(this, errMsg.ToString());
                    lblErr.Text = errMsg.Replace("\\n", "<br />").ToString();
                    lblErr.Visible = true;
                    divErrorMessage.Visible = true;
                    lblErr.Focus();
                }
                else
                {
                    lblErr.Text = "";
                    lblErr.Visible = false;
                    divErrorMessage.Visible = false;
                }


        }


        protected void txtInterest_TextChanged(object sender, EventArgs e)
        {
            TextBox txt = (TextBox)sender;
            StringBuilder errMsg = new StringBuilder();

            if (String.IsNullOrEmpty(txt.Text.Trim()))
            {
                errMsg.Append(Resources.resource.PersonalLoanInterestCannotEmpty + "\\n");
            }
            else
            {
                if (Validation.isDecimal(txt.Text.ToString()))
                {
                    if (ConvertHelper.ConvertToDecimal(txt.Text.ToString(), 2) < 0)
                    {
                        errMsg.Append(Resources.resource.PersonalLoanInterestCannotLessThanZero + "\\n");
                    }
                }
                else
                {
                    errMsg.Append(Resources.resource.PersonalLoanInterestCannotString + "\\n");
                }
            }

            if (errMsg.ToString().Trim() != string.Empty)
            {
                sqlString.displayAlert2(this, errMsg.ToString());
                lblErr.Text = errMsg.Replace("\\n", "<br />").ToString();
                lblErr.Visible = true;
                divErrorMessage.Visible = true;
                lblErr.Focus();
            }
            else
            {
                lblErr.Text = "";
                lblErr.Visible = false;
                divErrorMessage.Visible = false;
            }


        }
        #endregion


        protected bool ValidateForm()
        {
            bool result = false;
            StringBuilder errMsg = new StringBuilder();

            for (int i = 0; i < gv.Rows.Count; i++)
            {
                TextBox txtYear = (TextBox)gv.Rows[i].Cells[2].FindControl("txtYear");
                TextBox txtInterest = (TextBox)gv.Rows[i].Cells[2].FindControl("txtInterest");
                if (String.IsNullOrEmpty(txtYear.Text.Trim()))
                {
                    errMsg.Append(Resources.resource.PersonalLoanYearCannotEmpty + "\\n");
                }
                else
                {
                    if (Validation.isDecimal(txtYear.Text.ToString()))
                    {
                        if (ConvertHelper.ConvertToDecimal(txtYear.Text.ToString(), 2) < 0)
                        {
                            errMsg.Append(Resources.resource.PersonalLoanYearCannotLessThanZero + "\\n");
                        }
                    }
                    else
                    {
                        errMsg.Append(Resources.resource.PersonalLoanYearCannotString + "\\n");
                    }
                }

                if (String.IsNullOrEmpty(txtInterest.Text.Trim()))
                {
                    errMsg.Append(Resources.resource.PersonalLoanInterestCannotEmpty + "\\n");
                }
                else
                {
                    if (Validation.isDecimal(txtInterest.Text.ToString()))
                    {
                        if (ConvertHelper.ConvertToDecimal(txtInterest.Text.ToString(), 2) < 0)
                        {
                            errMsg.Append(Resources.resource.PersonalLoanInterestCannotLessThanZero + "\\n");
                        }
                    }
                    else
                    {
                        errMsg.Append(Resources.resource.PersonalLoanInterestCannotString + "\\n");
                    }
                }
            }

            if (errMsg.ToString().Trim() != string.Empty)
            {
                sqlString.displayAlert2(this, errMsg.ToString());
                lblErr.Text = errMsg.Replace("\\n", "<br />").ToString();
                lblErr.Visible = true;
                divErrorMessage.Visible = true;
                lblErr.Focus();
            }
            else
            {
                lblErr.Text = "";
                lblErr.Visible = false;
                divErrorMessage.Visible = false;
                result = true;
            }

            return result;
        }

        #endregion

    }
}