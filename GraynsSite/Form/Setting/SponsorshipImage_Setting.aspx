﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SponsorshipImage_Setting.aspx.cs" Inherits="HJT.Form.Setting.SponsorshipImage_Setting" MasterPageFile="~/Global1.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .tblgv3 tr td:first-child {
            font-weight: bold;
        }
    </style>
    <script>
		function FileUpload(fuid) {
			$('#<%= hid_FUID.ClientID %>').val(fuid);
			$('#<%= btnUpload.ClientID %>').click();
        }

        function confirmActive(message) {
            return confirm("Are you sure to " + message + " this image?");
        }
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <aside class="right-side">
        <section class="content-header">
            <h1>
                <span class="icon fa fa-fw fa-book" aria-hidden="true"></span>
                <asp:Label ID="lblPageTitle" runat="server"><i class="icon-th-list"></i><span runat="server" id="spHeader"></span></asp:Label>
            </h1>
            <asp:SiteMapPath ID="SiteMapPath1" CssClass="breadcrumb" runat="server"></asp:SiteMapPath>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div id="divErrorMessage" runat="server" visible="false" class="alert alert-danger alert-dismissable">
                        <i class="fa fa-ban"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <b>Alert!</b>
                        <br />
                        <asp:Label runat="server" ID="lblErr"></asp:Label>
                    </div>
                    <div id="dailysearchreport" runat="server" class="col-md-12">
                        <div class="box">
                            <asp:Button runat="server" OnClick="btnPreview_Click" Text='<%$ Resources:resource, Preview %>' CssClass="btn btn-searh" />
                            <div>
                                <h4><b><asp:Literal runat="server" Text='<%$ Resources:resource, AddNew_SponsorshipImage %>' /></b></h4>
                                <asp:Panel runat="server" DefaultButton="btnAddRow">
                                    <table style="width: 100%;" class="table table-bordered table-striped">
                                        <tr style="width: 100%;">
                                            <td style="width: 20%">
                                                <asp:Label runat="server" Text='<%$ Resources:resource, ImageName %>' />
                                            </td>
                                            <td style="width: 20%">
                                                <asp:Label runat="server" Text='<%$ Resources:resource, SponsorshipImage %>' />
                                            </td>
                                            <td style="width: 20%">
                                                <asp:Label runat="server" Text='<%$ Resources:resource, From %>' />
                                            </td>
                                            <td style="width: 20%">
                                                <asp:Label runat="server" Text='<%$ Resources:resource, Date %>' />
                                            </td>
                                            <td style="width: 20%">
                                                <asp:Label runat="server" Text='<%$ Resources:resource, Action %>' />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%;">
                                            <td style="width: 20%">
                                                <asp:TextBox runat="server" ID="txtImageName" />
                                            </td>
                                            <td style="width: 20%">
										        <asp:FileUpload runat="server" ID="fu1" onchange="FileUpload('fu1')" />
                                                <asp:Image runat="server" ID="img_fu1" ImageUrl="/Images/Doc_Icon.png" Width="50px" Visible="false" />
                                                <asp:Button runat="server" ID="btnUpload" Style="display: none;" OnClick="btnUpload_Click" />
										        <asp:HiddenField runat="server" ID="hid_FUID" />
                                            </td>
                                            <td style="width: 20%">
                                                <asp:TextBox runat="server" ID="txtFrom" TextMode="Date" />
                                            </td>
                                            <td style="width: 20%">
                                                <asp:TextBox runat="server" ID="txtTo" TextMode="Date" />
                                            </td>
                                            <td style="width: 20%">
                                                <asp:LinkButton runat="server" ID="btnAddRow" CssClass="btn btn-primary" OnClick="btnAddRow_Click">
                                                    <i class="fa fa-plus fa-plus-circle"></i> Add New Image
                                                </asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </div>
                            <h4>
                                <b><span style="color: black;"><%= GetGlobalResourceObject("resource", "SponsorshipImageList_Temp")%>&nbsp;:&nbsp;</span></b>
                            </h4>
                            <div class="box-body table-responsive">
                                <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow" ID="gv" runat="server" AutoGenerateColumns="false" GridLines="None" ShowFooter="false"
                                    EmptyDataText="<%$ Resources:Resource, No_Record_Found%>" CssClass="table table-bordered table-striped" Width="100%" 
                                    OnDataBound="gv_DataBound" OnRowDataBound="gv_RowDataBound">
                                    <Columns>
                                        <asp:BoundField DataField="ID" HeaderText="<%$ Resources:Resource, Id%>" />
                                        <asp:BoundField DataField="ImageName" HeaderText="<%$ Resources:Resource, Name%>" />
                                        <asp:BoundField DataField="Date" HeaderText="<%$ Resources:Resource, Dates%>" />
                                        <asp:BoundField DataField="From" HeaderText="<%$Resources:resource, From %>" />
                                        <asp:BoundField DataField="To" HeaderText="<%$Resources:resource, To %>" />
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, Status%>">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblStatus" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, Action%>">
                                            <ItemTemplate>
                                                <asp:Button runat="server" ID="btnView" Text="<%$ Resources:Resource, View%>" OnClick="btnView_Click"
                                                    CommandArgument='<%#Eval("ImageURL") %>' CssClass="btn btn-view"/>
                                                <asp:Button ID="btnDelete" CssClass="btn btn-danger" runat="server" Text="<%$ Resources:Resource, Delete%>"
                                                    CommandArgument='<%# Container.DataItemIndex  %>' OnClick="btnDelete_Click"
                                                    OnClientClick="confirm('Are you sure to remove this image?')"/>
                                                <asp:Button ID="btnActive" CssClass="btn btn-edit" runat="server" Text="<%$ Resources:Resource, Set_As_Active%>"
                                                    OnClick="btnActive_Click" CommandArgument="1" OnClientClick="confirmActive('enable');" />
                                                <asp:Button ID="btnInactive" CssClass="btn btn-edit" runat="server" Text="<%$ Resources:Resource, Set_As_Inactive%>"
                                                    OnClick="btnActive_Click" CommandArgument="0" OnClientClick="confirmActive('disable');" />
                                                <asp:HiddenField runat="server" Value='<%#Eval("ImageURL") %>' ID="hfImageURL" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <div class="box-footer">
                                <asp:Button ID="btnSaveAll" CssClass="btn btn-danger" runat="server" Text="<%$ Resources:Resource, Finalize%>"
                                    OnClick="btnSaveAll_Click" OnClientClick="confirm('Are you sure finalize setting?')"/>
                            </div>
                            <h4>
                                <b><span style="color: black;"><%= GetGlobalResourceObject("resource", "SponsorshipImageList_Finalized")%>&nbsp;:&nbsp;</span></b>
                            </h4>
                            <div class="box-body table-responsive">
                                <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow" ID="gv2" runat="server" AutoGenerateColumns="false" GridLines="None" ShowFooter="false"
                                    EmptyDataText="<%$ Resources:Resource, No_Record_Found%>" CssClass="table table-bordered table-striped" Width="100%" 
                                    OnDataBound="gv2_DataBound" OnRowDataBound="gv2_RowDataBound">
                                    <Columns>
                                        <asp:BoundField DataField="ID" HeaderText="<%$ Resources:Resource, Id%>" />
                                        <asp:BoundField DataField="ImageName" HeaderText="<%$ Resources:Resource, Name%>" />
                                        <asp:BoundField DataField="Date" HeaderText="<%$ Resources:Resource, Dates%>" />
                                        <asp:BoundField DataField="From" HeaderText="<%$Resources:resource, From %>" />
                                        <asp:BoundField DataField="To" HeaderText="<%$Resources:resource, To %>" />
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, Status%>">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblStatus" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </aside>
</asp:Content>
