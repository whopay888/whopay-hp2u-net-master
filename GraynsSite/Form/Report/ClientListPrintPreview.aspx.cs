﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Model;
using Synergy.Util;
using System.Text;
using System.Drawing;

namespace HJT.Form.Report
{
    public partial class ClientListPrintPreview : Page
    {
        LoginUserModel login_user = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            login_user = (LoginUserModel)Session[KeyVal.loginsecure];
            if (!IsPostBack)
            {
                string from = secure.Decrypt(Validation.GetUrlParameter(this.Page, "from", ""), true);
                string to = secure.Decrypt(Validation.GetUrlParameter(this.Page, "to", ""), true);
                string membername = secure.Decrypt(Validation.GetUrlParameter(this.Page, "membername", ""), true);
                string applicantname = secure.Decrypt(Validation.GetUrlParameter(this.Page, "applicantname", ""), true);
                string requestid = secure.Decrypt(Validation.GetUrlParameter(this.Page, "requestid", ""), true);
                string projectid = secure.Decrypt(Validation.GetUrlParameter(this.Page, "projectid", ""), true);
                string projectname = secure.Decrypt(Validation.GetUrlParameter(this.Page, "projectname", ""), true);
                string applicationstatus = secure.Decrypt(Validation.GetUrlParameter(this.Page, "applicationstatus", ""), true);
                string parentid = secure.Decrypt(Validation.GetUrlParameter(Page, "parentbranch", ""), true);

                wwdb db = new wwdb();
                if (parentid.Equals("0"))//no parent selected
                    if (!projectid.Equals("0"))//use projectid to get parentbranch name
                    {
                        db.OpenTable(string.Format(@"
                            SELECT pa.BranchName FROM tbl_Branch pa, tbl_Branch s, tbl_ProjectSettings ps
                            WHERE s.ParentId = pa.ID and (ps.BranchId = pa.ID or ps.BranchId = s.ID)
                            and ps.ID = '{0}'
                        ", projectid));

                        lblParentBranch.Text = db.Item("BranchName");
                    }
                    else
                        lblParentBranch.Text = "-";
                else//parent selected
                {
                    db.OpenTable("SELECT BranchName from tbl_Branch where id='" + parentid + "'");
                    lblParentBranch.Text = db.Item("BranchName");
                }
                lblProject.Text = projectid.Equals("0") ? "-" : projectname;
                lblFrom.Text = string.IsNullOrWhiteSpace(from) ? "-" : from;
                lblTo.Text = string.IsNullOrWhiteSpace(to) ? "-" : to;

                sqlString.bindControl(gv, getClientList(from, to, membername, applicantname, requestid, projectid, applicationstatus, parentid, null));
            }
        }

        private string getClientList(string from, string to, string memberName, string applicantName, string requestId, string projectId, string applicationStatus, string parentID, IDictionary<string, string> sortBy)
        {

            StringBuilder sql = new StringBuilder();
            StringBuilder sql2 = new StringBuilder();
            StringBuilder sql3 = new StringBuilder();

            string searchFilter = string.Empty;
            if (login_user.isStaffAdmin())
                searchFilter = string.Empty;
            else if (login_user.UserAccessList.Where(_ => _.Key.Equals("function_view_branch_request")).Count() >= 1)
                searchFilter = " AND(A.memberid = '" + login_user.UserId + "' OR A.BranchId IN (SELECT BranchId FROM tbl_Branch Where ParentId = " + login_user.BranchUniqueID + " OR Id = " + login_user.BranchUniqueID + ")) ";
            else
                searchFilter = " AND A.MemberID='" + login_user.UserId + "'";

            sql2.Clear();
            sql3.Clear();
            if (login_user.isStaffAdmin())
            {
                sql2.AppendFormat(@"left join dbo.tbl_ProjectSettings ps with(nolock) on tbl_Request.ProjectId = PS.ID 
                            left join dbo.tbl_Branch br with(nolock) on br.BranchID = tbl_Request.BranchID or br.ID = ps.BranchId ");
                
                DropDownList ddlParentBranch = new DropDownList();
                ddlParentBranch.Items.Add(new ListItem { Text = "Select", Value = "0", Selected = parentID.Equals("0") ? true : false });
                ddlParentBranch.Items.Add(new ListItem { Text = parentID, Value = parentID, Selected = parentID.Equals("0") ? false : true });
                sql3.Append(sqlString.searchDropDownList("(br.ID", ddlParentBranch, true, true, false));
                sql3.Append(sqlString.searchDropDownList("br.ParentId", ddlParentBranch, true, false, true));
                if (!parentID.Equals("0"))
                    sql3.Append(") ");
            }

            sql.Clear();
            sql.AppendFormat(@"SELECT A.RequestID, A.RequestAt , D.Name  as 'ApplicantName', 
                        C.Fullname, B.BANKLIST, A.ApplicationResult, A.MemberId, A.BranchId, D.MyCard,
                        CASE WHEN PS.ProjectName is null 
	                        THEN 
                                (CASE WHEN A.ProjectName IS NULL
		                        THEN '-'
		                        ELSE A.ProjectName
		                        END)
	                        ELSE  PS.ProjectName 
                        END AS ProjectName
                        FROM 
                            (SELECT distinct tbl_Request.*,
                            (SELECT COUNT(*) FROM dbo.tbl_CTOS_Result tcr WHERE tcr.Request_Id = tbl_Request.RequestID AND tcr.IsActive = 1 AND IsCoApplicant = 0) as ShowMACCRIS, 
	                        (SELECT COUNT(*) FROM dbo.tbl_CTOS_Result tcr WHERE tcr.Request_Id = tbl_Request.RequestID AND tcr.IsActive = 1 AND IsCoApplicant = 1) as ShowCACCRIS, 
                            'Request: MYR ' + CONVERT(VARCHAR,(ISNULL(PerRequestPrice,0) * NumofApplicant))  
                            + ' Successful: MYR ' + CONVERT(VARCHAR,(ISNULL(PerSuccessfulCasePrice,0) * NumofApplicant)) AS Price, PaymentStatus AS PayStatus,
		                    aca.isFinal
                            FROM tbl_Request WITH (NOLOCK) 
                            LEFT JOIN 
	                        (SELECT dbo.tbl_Applicants.RequestID, COUNT(*) AS NumofApplicant FROM tbl_Applicants GROUP BY dbo.tbl_Applicants.RequestID) ta 
                            ON tbl_Request.RequestID = ta.RequestID
		                    left join dbo.tbl_ApplicantCreditAdvisor aca on aca.RequestID=tbl_Request.RequestID {0} 
                            WHERE tbl_Request.StatusID ='S' {1})A
                        LEFT JOIN
                            (
                            SELECT 
                                requestid,'['+
                                STUFF((
                                SELECT ', ' +'{{ ""ID"" :""'+ BankID + '"", ""Name"" :""' + BankName+'""}}'
                                FROM TBL_REQUESTBANK WITH (NOLOCK)
                                WHERE  isdeleted=0 AND (requestid = TRB.requestid) 
                                FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)')
                                ,1,2,'')+']' AS BANKLIST
                            FROM TBL_REQUESTBANK TRB WITH (NOLOCK)
                            GROUP BY requestid
                            ) B ON A.RequestID = B.RequestID
                        LEFT JOIN
                            (
                            SELECT 
                                RequestID,'['+
                                STUFF((
                                SELECT ', ' +'{{""DocsURL"" :""'+ DocsURL + '""}}'
                                FROM tbl_RequestDocs WITH (NOLOCK)
                                WHERE  IsDeleted=0 AND (RequestID = trd.RequestID) 
                                FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)')
                                ,1,2,'')+']' AS DocList
                            FROM tbl_RequestDocs trd WITH (NOLOCK)
                            GROUP BY trd.RequestID
                            ) E ON A.RequestID = E.RequestID
                        LEFT JOIN tbl_MemberInfo C WITH (NOLOCK) ON A.MemberID = C.MemberId
                        LEFT JOIN tbl_applicants D WITH (NOLOCK) ON A.RequestID = D.RequestID AND D.ApplicantType='MA'
                        LEFT JOIN tbl_projectsettings PS WITH(NOLOCK) ON A.ProjectId = PS.ID ", sql2.ToString(), sql3.ToString());
            sql.Append("WHERE 1=1 AND A.isdeleted=0 ");

            sql.Append(searchFilter);
            if (!string.IsNullOrWhiteSpace(from) && !string.IsNullOrWhiteSpace(to) && DateTime.TryParse(from, out var outfrom) && DateTime.TryParse(to, out var outto))
                sql.Append(" and A.RequestAt between '" + from + "' and '" + to + " 23:59' ");

            TextBox txtMemberName = new TextBox();
            txtMemberName.Text = memberName;
            sql.Append(sqlString.searchTextBox("C.Fullname", txtMemberName, true, true, false));

            TextBox txtApplicantName = new TextBox();
            txtApplicantName.Text = applicantName;
            sql.Append(sqlString.searchTextBox("D.Name", txtApplicantName, true, true, false));

            TextBox txtRequestID = new TextBox();
            txtRequestID.Text = requestId;
            sql.Append(sqlString.searchTextBox("A.RequestID", txtRequestID, false, true, false));

            DropDownList ddlProjectName = new DropDownList();
            ddlProjectName.Items.Add(new ListItem { Text = "Select", Value = "0", Selected = projectId.Equals("0")? true : false });
            ddlProjectName.Items.Add(new ListItem { Text = projectId, Value = projectId, Selected = projectId.Equals("0") ? false : true });
            sql.Append(sqlString.searchDropDownList("A.ProjectId", ddlProjectName, true, true, false));

            DropDownList ddlApplicationStatus = new DropDownList();
            ddlApplicationStatus.Items.Add(new ListItem { Text = "Select", Value = "0", Selected = applicationStatus.Equals("0") ? true : false });
            ddlApplicationStatus.Items.Add(new ListItem { Text = applicationStatus, Value = applicationStatus, Selected = applicationStatus.Equals("0") ? false : true });
            sql.Append(sqlString.searchDropDownList("A.ApplicationResult", ddlApplicationStatus, true, true, false));
            
            sql.Append("order by ");

            //ordering
            if (sortBy != null && sortBy.Any())
            {
                int i = 0;
                foreach (var item in sortBy)
                {
                    string key = item.Key;
                    string value = item.Value;

                    if (i > 0)
                        sql.Append(",");

                    switch (key)
                    {
                        case "RequestId":
                            sql.Append("cast(A.RequestID as int) " + value);
                            break;
                        case "RequestAt":
                            sql.Append("RequestAt " + value);
                            break;
                        case "Fullname":
                            sql.Append("Fullname " + value);
                            break;
                        case "ApplicantName":
                            sql.Append("ApplicantName " + value);
                            break;
                        case "ProjectName":
                            sql.Append("ProjectName" + value);
                            break;
                        case "ApplicationResult":
                            sql.Append("ApplicationResult " + value);
                            break;
                        default:
                            break;
                    }

                    i++;
                }
            }
            else
            {
                sql.Append("RequestAt asc");
            }

            return sql.ToString();

        }

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                LinkButton sortLink;

                Dictionary<string, string> sortBy = new Dictionary<string, string>();
                if (ViewState["SortBy"] != null)
                { sortBy = (Dictionary<string, string>)ViewState["SortBy"]; }

                // GridView rows with sortable columns will have a linkbutton
                // generated. Compare to the CommandArgument since this is what
                // we set our sortColumn based on.
                foreach (System.Web.UI.WebControls.TableCell currCell in e.Row.Cells)
                {
                    if (currCell.HasControls())
                    {
                        sortLink = ((LinkButton)currCell.Controls[0]);
                        if (sortBy.ContainsKey(sortLink.CommandArgument))
                        {
                            // Use the HTML safe codes for the up arrow ▲ and down arrow ▼.
                            string sortArrow = sortBy[sortLink.CommandArgument] == "ASC" ? "&#9650;" : "&#9660;";

                            sortLink.Text = sortLink.Text + " " + sortArrow;
                        }
                        else
                        {
                            sortLink.Text = sortLink.Text;
                        }
                    }
                }
            }

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string ApplicationResult = DataBinder.Eval(e.Row.DataItem, "ApplicationResult").ToString();
                int resultIndex = gridView.GetColumnIndexByName(gv, Resources.resource.Customer_Profile);
                Label lblStatus = (Label)e.Row.FindControl("lblStatus");

                Dictionary<string, Color> colorlist = new Dictionary<string, Color>();
                colorlist.Add("A", Color.Green);
                colorlist.Add("D", Color.Red);
                colorlist.Add("M", Color.Yellow);

                //  lblStatus.Text = sqlString.changeLanguage("ApplicationResult_" + ApplicationResult);
                lblStatus.Text = colorlist[ApplicationResult].Name;
                //e.Row.Cells[resultIndex].BackColor = colorlist[ApplicationResult];
            }

        }
        
        private string getExportList(string from, string to, string memberName, string applicantName, string requestId, string projectId, string applicationStatus, string parentID, IDictionary<string, string> sortBy)
        {
            StringBuilder sql = new StringBuilder();


            string searchFilter = string.Empty;
            if (login_user.isStaffAdmin())
                searchFilter = string.Empty;
            else if (login_user.UserAccessList.Where(_ => _.Key.Equals("function_view_branch_request")).Count() >= 1)
                searchFilter = " AND(A.memberid = '" + login_user.UserId + "' OR A.BranchId IN (SELECT BranchId FROM tbl_Branch Where ParentId = " + login_user.BranchUniqueID + " OR Id = " + login_user.BranchUniqueID + ")) ";
            else
                searchFilter = " AND A.MemberID='" + login_user.UserId + "'";


            //if (login_user.isBranchManager())
            //    branchID = login_user.BranchID;


            sql.AppendFormat(@"SELECT distinct cast(A.RequestAt as varchar) as 'Request Date/Time', A.PurchasePrice, A.LoanAmount, 
                                D.Name  as 'Applicant Name', cast(D.MyCard as varchar) as 'Applicant IC No.',
	                               CASE WHEN PS.ProjectName is null 
		                               THEN '-'
		                               ELSE  PS.ProjectName
	                               END AS 'Project Name',
                                C.Fullname as 'Member Name', 
	                               CASE WHEN A.ApplicationResult = 'M'
		                               THEN 'Yellow'
	                               WHEN A.ApplicationResult = 'A'
		                               THEN 'Green'
	                               WHEN A.ApplicationResult = 'D'
		                               THEN 'Red'
	                               END AS 'Application Result',
                                case when replace(replace(replace(replace(aca.Comment,'&#x0D;',''),
											'</br>',''),'&gt;',''),'&amp;','') is null 
									or replace(replace(replace(replace(aca.Comment,'&#x0D;',''),
											'</br>',''),'&gt;',''),'&amp;','') like ''
									then '-'
								else
									replace(replace(replace(replace(aca.Comment,'&#x0D;',''),
											'</br>',''),'&gt;',''),'&amp;','')
								end as Comment
                               FROM
                                   (SELECT *, 
                                   (select distinct lla.loanamount from tbl_LoanLegalAction lla with (nolock) where
                                   lla.requestid=r.requestid) as LoanAmount, 
                                   (select distinct lla.purchaseprice from tbl_LoanLegalAction lla with (nolock) where
                                   lla.requestid=r.requestid) as PurchasePrice
                                   FROM tbl_Request r WITH (NOLOCK) WHERE  StatusID ='S')A
                               LEFT JOIN tbl_MemberInfo C WITH (NOLOCK) ON A.MemberID = C.MemberId
                               LEFT JOIN tbl_applicants D WITH (NOLOCK) ON A.RequestID = D.RequestID AND D.ApplicantType='MA'
                               LEFT JOIN tbl_projectsettings PS WITH(NOLOCK) ON A.ProjectId = PS.ID
	                           left join tbl_Branch br with(nolock) on br.BranchID=a.BranchID or br.ID=ps.BranchId
	                           left join tbl_ApplicantCreditAdvisor aca with(nolock) on aca.RequestID = A.RequestID
                               WHERE 1=1 AND A.isdeleted=0 ");

            sql.Append(searchFilter);

            if (!string.IsNullOrWhiteSpace(from) && !string.IsNullOrWhiteSpace(to) && DateTime.TryParse(from, out var outfrom) && DateTime.TryParse(to, out var outto))
                sql.Append(" and A.RequestAt between '" + from + "' and '" + to + " 23:59' ");

            TextBox txtMemberName = new TextBox();
            txtMemberName.Text = memberName;
            sql.Append(sqlString.searchTextBox("C.Fullname", txtMemberName, true, true, false));

            TextBox txtApplicantName = new TextBox();
            txtApplicantName.Text = applicantName;
            sql.Append(sqlString.searchTextBox("D.Name", txtApplicantName, true, true, false));

            TextBox txtRequestID = new TextBox();
            txtRequestID.Text = requestId;
            sql.Append(sqlString.searchTextBox("A.RequestID", txtRequestID, false, true, false));

            DropDownList ddlProjectName = new DropDownList();
            ddlProjectName.Items.Add(new ListItem { Text = "Select", Value = "0", Selected = projectId.Equals("0") ? true : false });
            ddlProjectName.Items.Add(new ListItem { Text = projectId, Value = projectId, Selected = projectId.Equals("0") ? false : true });
            sql.Append(sqlString.searchDropDownList("A.ProjectId", ddlProjectName, true, true, false));

            DropDownList ddlApplicationStatus = new DropDownList();
            ddlApplicationStatus.Items.Add(new ListItem { Text = "Select", Value = "0", Selected = applicationStatus.Equals("0") ? true : false });
            ddlApplicationStatus.Items.Add(new ListItem { Text = applicationStatus, Value = applicationStatus, Selected = applicationStatus.Equals("0") ? false : true });
            sql.Append(sqlString.searchDropDownList("A.ApplicationResult", ddlApplicationStatus, true, true, false));

            DropDownList ddlParentBranch = new DropDownList();
            ddlParentBranch.Items.Add(new ListItem { Text = "Select", Value = "0", Selected = parentID.Equals("0") ? true : false });
            ddlParentBranch.Items.Add(new ListItem { Text = parentID, Value = parentID, Selected = parentID.Equals("0") ? false : true });
            sql.Append(sqlString.searchDropDownList("(br.ID", ddlParentBranch, true, true, false));
            sql.Append(sqlString.searchDropDownList("br.ParentId", ddlParentBranch, true, false, true));
            if (ddlParentBranch.SelectedIndex > 0)
                sql.Append(") ");

            sql.Append(" order by ");

            //ordering
            if (sortBy != null && sortBy.Any())
            {
                int i = 0;
                foreach (var item in sortBy)
                {
                    string key = item.Key;
                    string value = item.Value;

                    if (i > 0)
                        sql.Append(",");

                    switch (key)
                    {
                        case "RequestId":
                            sql.Append("cast(A.RequestID as int) " + value);
                            break;
                        case "RequestAt":
                            sql.Append("[Request Date/Time] " + value);
                            break;
                        case "Fullname":
                            sql.Append("Fullname " + value);
                            break;
                        case "ApplicantName":
                            sql.Append("ApplicantName " + value);
                            break;
                        case "ProjectName":
                            sql.Append("ProjectName" + value);
                            break;
                        case "ApplicationResult":
                            sql.Append("ApplicationResult " + value);
                            break;
                        default:
                            break;
                    }

                    i++;
                }
            }
            else
            {
                sql.Append("[Request Date/Time] asc");
            }

            return sql.ToString();

        }
        protected void btnExport_Click(object sender, EventArgs e)
        {
            StringBuilder sql = new StringBuilder();

            string from = secure.Decrypt(Validation.GetUrlParameter(this.Page, "from", ""), true);
            string to = secure.Decrypt(Validation.GetUrlParameter(this.Page, "to", ""), true);
            string membername = secure.Decrypt(Validation.GetUrlParameter(this.Page, "membername", ""), true);
            string applicantname = secure.Decrypt(Validation.GetUrlParameter(this.Page, "applicantname", ""), true);
            string requestid = secure.Decrypt(Validation.GetUrlParameter(this.Page, "requestid", ""), true);
            string projectid = secure.Decrypt(Validation.GetUrlParameter(this.Page, "projectid", ""), true);
            string projectname = secure.Decrypt(Validation.GetUrlParameter(this.Page, "projectname", ""), true);
            string applicationstatus = secure.Decrypt(Validation.GetUrlParameter(this.Page, "applicationstatus", ""), true);
            string parentid = secure.Decrypt(Validation.GetUrlParameter(Page, "parentbranch", ""), true);

            lblProject.Text = projectid.Equals("0") ? "-" : projectname;
            lblFrom.Text = string.IsNullOrWhiteSpace(from) ? "-" : from;
            lblTo.Text = string.IsNullOrWhiteSpace(to) ? "-" : to;

            sql.Append(getExportList(from, to, membername, applicantname, requestid, projectid, applicationstatus, parentid, null));

            sqlString.exportFunc("WhoPay_RequestList_" + (DateTime.UtcNow.ToShortDateString().ToString()).Replace("_", "-"), sql.ToString(), true);
        }

        protected void btnView_Click(object sender, EventArgs e)
        {
            Button btnsender = (Button)sender;

            GridViewRow gvr = (GridViewRow)btnsender.NamingContainer;
            DropDownList ddlBank = (DropDownList)gvr.FindControl("ddlBank");

            string rid = btnsender.CommandArgument.ToString();
            if (ddlBank.SelectedValue == "0")
                sqlString.displayAlert2(this, Resources.resource.Err_Bank_Name_Select_Empty);
            else
            {
                string reportIrl = "/Form/Loan/DsrReport.aspx?rid=" + sqlString.encryptURL(rid) + "&BankID=" + sqlString.encryptURL(ddlBank.SelectedValue);
                sqlString.OpenNewWindow_Center(reportIrl, "Report", 300, 300, this);
            }



        }
    }
}