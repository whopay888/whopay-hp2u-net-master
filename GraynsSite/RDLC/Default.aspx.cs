﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Controller;
using Synergy.Model;
using Synergy.Util;
using Synergy.Helper;
using System.Drawing;

namespace WMElegance.RDLC
{
    public partial class Default : System.Web.UI.Page
    {
        DataSet ds = new DataSet();
        DataSet ds1 = new DataSet();
        DataSet ds3 = new DataSet();
        string reportType = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            reportType = Validation.GetUrlParameter(this.Page, "reportType", null);

            if (!IsPostBack)
            {
                if (Session[KeyVal.loginsecure] != null)
                {
                    switch (reportType)
                    {
                     
                        case "DSRReport": Export_DSRReport(); break;
                        default: throw new Exception("Report type error.");
                    }
                }
            }
        }

        private void Export_DSRReport()
        {
            string requestID = secure.Decrypt(Validation.GetUrlParameter(this.Page, "RID", ""), true);
            
            
            if(requestID!=string.Empty)
            {
                wwdb db = new wwdb();
                StringBuilder sql = new StringBuilder();

                sql.AppendFormat(@"
select b.Fullname , c.Unit , c.ProjectName , a.ClientID , c.PurchasePrice , a.applicationresult from (
select * from tbl_Request where RequestID = '{0}'
)a
left join tbl_MemberInfo b on a.MemberID = b.MemberId
left join tbl_Applicants c on a.requestid = c.requestid
", requestID);


                DataTable ApplicantInfo = db.getDataTable(sql.ToString());

                Dictionary<string,string> statusColor = new Dictionary<string,string> ();

                statusColor.Add(ApplicationResult.Approved, "/Images/DsrReport/Approved_Back.png");
                statusColor.Add(ApplicationResult.Modify, "/Images/DsrReport/Modified_Back.png");
                statusColor.Add(ApplicationResult.Denied, "/Images/DsrReport/Denied_Back.png");


                Dictionary<string, string> contentList = new Dictionary<string, string>();

                contentList.Add(ApplicationResult.Approved, "Hi xxx, you did a good job ! ");
                contentList.Add(ApplicationResult.Modify, "Hi xxx, you need to put some attention ! ");
                contentList.Add(ApplicationResult.Denied, "Hi xxx, you seems not a good look today ! ");


                DataTable commitmentList = db.getDataTable("select IconImage ,ShortForm , Name  from tbl_LoanTypes");
               
              
               // DataTable dt = sqlString.ConvertRowToColumn(ApplicantInfo);

                Uri imgPath = new Uri(Server.MapPath(statusColor[ApplicantInfo.Rows[0]["applicationresult"].ToString()]));

                rv.SizeToReportContent = true;
                rv.LocalReport.ReportPath = Server.MapPath("~/RDLC/DSRReport.rdlc");
                rv.LocalReport.EnableExternalImages = true;
                rv.LocalReport.SetParameters(new ReportParameter("Unit", ApplicantInfo.Rows[0]["Unit"].ToString()));
                rv.LocalReport.SetParameters(new ReportParameter("ProjectName", ApplicantInfo.Rows[0]["ProjectName"].ToString()));
                rv.LocalReport.SetParameters(new ReportParameter("PurchasePrice", ApplicantInfo.Rows[0]["PurchasePrice"].ToString()));
                rv.LocalReport.SetParameters(new ReportParameter("UserName", ApplicantInfo.Rows[0]["Fullname"].ToString()));
                rv.LocalReport.SetParameters(new ReportParameter("Applicant", ApplicantInfo.Rows[0]["ClientID"].ToString()));
                rv.LocalReport.SetParameters(new ReportParameter("StatusContent", contentList[ApplicantInfo.Rows[0]["applicationresult"].ToString()].Replace("xxx", ApplicantInfo.Rows[0]["ClientID"].ToString())));
                rv.LocalReport.SetParameters(new ReportParameter("StatusImg", imgPath.AbsoluteUri));


         

                rv.LocalReport.Refresh();


            }


         


        }

       
    }
}