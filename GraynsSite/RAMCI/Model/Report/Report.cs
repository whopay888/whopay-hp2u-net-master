﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HJT.RAMCI.Model.Report
{



    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class xml
    {

        private string report_dateField;

        private xmlSummary summaryField;

        private xmlBanking_info banking_infoField;

        private xmlLitigation_info litigation_infoField;

        private xmlTrade_bureau trade_bureauField;

        private xmlEnquiry enquiryField;

        private xmlLegend legendField;

        private xmlEnd endField;

        /// <remarks/>
        public string report_date
        {
            get
            {
                return this.report_dateField;
            }
            set
            {
                this.report_dateField = value;
            }
        }

        /// <remarks/>
        public xmlSummary summary
        {
            get
            {
                return this.summaryField;
            }
            set
            {
                this.summaryField = value;
            }
        }

        /// <remarks/>
        public xmlBanking_info banking_info
        {
            get
            {
                return this.banking_infoField;
            }
            set
            {
                this.banking_infoField = value;
            }
        }

        /// <remarks/>
        public xmlLitigation_info litigation_info
        {
            get
            {
                return this.litigation_infoField;
            }
            set
            {
                this.litigation_infoField = value;
            }
        }

        /// <remarks/>
        public xmlTrade_bureau trade_bureau
        {
            get
            {
                return this.trade_bureauField;
            }
            set
            {
                this.trade_bureauField = value;
            }
        }

        /// <remarks/>
        public xmlEnquiry enquiry
        {
            get
            {
                return this.enquiryField;
            }
            set
            {
                this.enquiryField = value;
            }
        }

        /// <remarks/>
        public xmlLegend legend
        {
            get
            {
                return this.legendField;
            }
            set
            {
                this.legendField = value;
            }
        }

        /// <remarks/>
        public xmlEnd end
        {
            get
            {
                return this.endField;
            }
            set
            {
                this.endField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlSummary
    {

        private xmlSummaryInput_request input_requestField;

        private xmlSummaryCcris_individual_info ccris_individual_infoField;

        private xmlSummaryCcris_individual_address[] ccris_individual_addressesField;

        private xmlSummaryInfo_summary info_summaryField;

        private xmlSummaryI_score i_scoreField;

        private xmlSummaryPerson_company_interests person_company_interestsField;

        private xmlSummaryPrevious_company_interests previous_company_interestsField;

        /// <remarks/>
        public xmlSummaryInput_request input_request
        {
            get
            {
                return this.input_requestField;
            }
            set
            {
                this.input_requestField = value;
            }
        }

        /// <remarks/>
        public xmlSummaryCcris_individual_info ccris_individual_info
        {
            get
            {
                return this.ccris_individual_infoField;
            }
            set
            {
                this.ccris_individual_infoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("ccris_individual_address", IsNullable = false)]
        public xmlSummaryCcris_individual_address[] ccris_individual_addresses
        {
            get
            {
                return this.ccris_individual_addressesField;
            }
            set
            {
                this.ccris_individual_addressesField = value;
            }
        }

        /// <remarks/>
        public xmlSummaryInfo_summary info_summary
        {
            get
            {
                return this.info_summaryField;
            }
            set
            {
                this.info_summaryField = value;
            }
        }

        /// <remarks/>
        public xmlSummaryI_score i_score
        {
            get
            {
                return this.i_scoreField;
            }
            set
            {
                this.i_scoreField = value;
            }
        }

        /// <remarks/>
        public xmlSummaryPerson_company_interests person_company_interests
        {
            get
            {
                return this.person_company_interestsField;
            }
            set
            {
                this.person_company_interestsField = value;
            }
        }

        /// <remarks/>
        public xmlSummaryPrevious_company_interests previous_company_interests
        {
            get
            {
                return this.previous_company_interestsField;
            }
            set
            {
                this.previous_company_interestsField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlSummaryInput_request
    {

        private string search_nameField;

        private string old_icField;

        private string new_icField;

        private string product_codeField;

        private string nationalityField;

        /// <remarks/>
        public string search_name
        {
            get
            {
                return this.search_nameField;
            }
            set
            {
                this.search_nameField = value;
            }
        }

        /// <remarks/>
        public string old_ic
        {
            get
            {
                return this.old_icField;
            }
            set
            {
                this.old_icField = value;
            }
        }

        /// <remarks/>
        public string new_ic
        {
            get
            {
                return this.new_icField;
            }
            set
            {
                this.new_icField = value;
            }
        }

        /// <remarks/>
        public string product_code
        {
            get
            {
                return this.product_codeField;
            }
            set
            {
                this.product_codeField = value;
            }
        }

        /// <remarks/>
        public string nationality
        {
            get
            {
                return this.nationalityField;
            }
            set
            {
                this.nationalityField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlSummaryCcris_individual_info
    {

        private string nameField;

        private string new_icField;

        /// <remarks/>
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public string new_ic
        {
            get
            {
                return this.new_icField;
            }
            set
            {
                this.new_icField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlSummaryCcris_individual_address
    {

        private string addressField;

        private string date_captureField;

        private string source_fromField;

        /// <remarks/>
        public string address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>
        public string date_capture
        {
            get
            {
                return this.date_captureField;
            }
            set
            {
                this.date_captureField = value;
            }
        }

        /// <remarks/>
        public string source_from
        {
            get
            {
                return this.source_fromField;
            }
            set
            {
                this.source_fromField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlSummaryInfo_summary
    {

        private string credit_approval_countField;

        private string credit_pending_countField;

        private string dishonored_cheque_countField;

        private string special_attention_account_countField;

        private string legal_action_banking_countField;

        private string existing_facility_countField;

        private string bankruptcy_countField;

        private string legal_suit_countField;

        private string trade_bureau_countField;

        private string enquiry_countField;

        private string interest_countField;

        /// <remarks/>
        public string credit_approval_count
        {
            get
            {
                return this.credit_approval_countField;
            }
            set
            {
                this.credit_approval_countField = value;
            }
        }

        /// <remarks/>
        public string credit_pending_count
        {
            get
            {
                return this.credit_pending_countField;
            }
            set
            {
                this.credit_pending_countField = value;
            }
        }

        /// <remarks/>
        public string dishonored_cheque_count
        {
            get
            {
                return this.dishonored_cheque_countField;
            }
            set
            {
                this.dishonored_cheque_countField = value;
            }
        }

        /// <remarks/>
        public string special_attention_account_count
        {
            get
            {
                return this.special_attention_account_countField;
            }
            set
            {
                this.special_attention_account_countField = value;
            }
        }

        /// <remarks/>
        public string legal_action_banking_count
        {
            get
            {
                return this.legal_action_banking_countField;
            }
            set
            {
                this.legal_action_banking_countField = value;
            }
        }

        /// <remarks/>
        public string existing_facility_count
        {
            get
            {
                return this.existing_facility_countField;
            }
            set
            {
                this.existing_facility_countField = value;
            }
        }

        /// <remarks/>
        public string bankruptcy_count
        {
            get
            {
                return this.bankruptcy_countField;
            }
            set
            {
                this.bankruptcy_countField = value;
            }
        }

        /// <remarks/>
        public string legal_suit_count
        {
            get
            {
                return this.legal_suit_countField;
            }
            set
            {
                this.legal_suit_countField = value;
            }
        }

        /// <remarks/>
        public string trade_bureau_count
        {
            get
            {
                return this.trade_bureau_countField;
            }
            set
            {
                this.trade_bureau_countField = value;
            }
        }

        /// <remarks/>
        public string enquiry_count
        {
            get
            {
                return this.enquiry_countField;
            }
            set
            {
                this.enquiry_countField = value;
            }
        }

        /// <remarks/>
        public string interest_count
        {
            get
            {
                return this.interest_countField;
            }
            set
            {
                this.interest_countField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlSummaryI_score
    {

        private string i_scoreField;

        private string risk_gradeField;

        private xmlSummaryI_scoreKey_factor key_factorField;

        private string na_iscore_legendField;

        private string error_messageField;

        private string grade_formatField;

        /// <remarks/>
        public string i_score
        {
            get
            {
                return this.i_scoreField;
            }
            set
            {
                this.i_scoreField = value;
            }
        }

        /// <remarks/>
        public string risk_grade
        {
            get
            {
                return this.risk_gradeField;
            }
            set
            {
                this.risk_gradeField = value;
            }
        }

        /// <remarks/>
        public xmlSummaryI_scoreKey_factor key_factor
        {
            get
            {
                return this.key_factorField;
            }
            set
            {
                this.key_factorField = value;
            }
        }

        /// <remarks/>
        public string na_iscore_legend
        {
            get
            {
                return this.na_iscore_legendField;
            }
            set
            {
                this.na_iscore_legendField = value;
            }
        }

        /// <remarks/>
        public string error_message
        {
            get
            {
                return this.error_messageField;
            }
            set
            {
                this.error_messageField = value;
            }
        }

        /// <remarks/>
        public string grade_format
        {
            get
            {
                return this.grade_formatField;
            }
            set
            {
                this.grade_formatField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlSummaryI_scoreKey_factor
    {

        private string itemField;

        /// <remarks/>
        public string item
        {
            get
            {
                return this.itemField;
            }
            set
            {
                this.itemField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlSummaryPerson_company_interests
    {

        private xmlSummaryPerson_company_interestsPerson_company_interest person_company_interestField;

        /// <remarks/>
        public xmlSummaryPerson_company_interestsPerson_company_interest person_company_interest
        {
            get
            {
                return this.person_company_interestField;
            }
            set
            {
                this.person_company_interestField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlSummaryPerson_company_interestsPerson_company_interest
    {

        private string nameField;

        private string local_noField;

        private string typeField;

        private string positionField;

        private string sharesField;

        private string remarkField;

        private string last_updated_dateField;

        private string incorp_dateField;

        private string display_idField;

        private string paid_up_capitalField;

        private xmlSummaryPerson_company_interestsPerson_company_interestNature_businesses nature_businessesField;

        private xmlSummaryPerson_company_interestsPerson_company_interestRemark_bafia remark_bafiaField;

        /// <remarks/>
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public string local_no
        {
            get
            {
                return this.local_noField;
            }
            set
            {
                this.local_noField = value;
            }
        }

        /// <remarks/>
        public string type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        public string position
        {
            get
            {
                return this.positionField;
            }
            set
            {
                this.positionField = value;
            }
        }

        /// <remarks/>
        public string shares
        {
            get
            {
                return this.sharesField;
            }
            set
            {
                this.sharesField = value;
            }
        }

        /// <remarks/>
        public string remark
        {
            get
            {
                return this.remarkField;
            }
            set
            {
                this.remarkField = value;
            }
        }

        /// <remarks/>
        public string last_updated_date
        {
            get
            {
                return this.last_updated_dateField;
            }
            set
            {
                this.last_updated_dateField = value;
            }
        }

        /// <remarks/>
        public string incorp_date
        {
            get
            {
                return this.incorp_dateField;
            }
            set
            {
                this.incorp_dateField = value;
            }
        }

        /// <remarks/>
        public string display_id
        {
            get
            {
                return this.display_idField;
            }
            set
            {
                this.display_idField = value;
            }
        }

        /// <remarks/>
        public string paid_up_capital
        {
            get
            {
                return this.paid_up_capitalField;
            }
            set
            {
                this.paid_up_capitalField = value;
            }
        }

        /// <remarks/>
        public xmlSummaryPerson_company_interestsPerson_company_interestNature_businesses nature_businesses
        {
            get
            {
                return this.nature_businessesField;
            }
            set
            {
                this.nature_businessesField = value;
            }
        }

        /// <remarks/>
        public xmlSummaryPerson_company_interestsPerson_company_interestRemark_bafia remark_bafia
        {
            get
            {
                return this.remark_bafiaField;
            }
            set
            {
                this.remark_bafiaField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlSummaryPerson_company_interestsPerson_company_interestNature_businesses
    {

        private string nature_businessField;

        /// <remarks/>
        public string nature_business
        {
            get
            {
                return this.nature_businessField;
            }
            set
            {
                this.nature_businessField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlSummaryPerson_company_interestsPerson_company_interestRemark_bafia
    {

        private string remarkField;

        private string as_atField;

        private string nameField;

        private string positionField;

        private string placeField;

        private string courtField;

        /// <remarks/>
        public string remark
        {
            get
            {
                return this.remarkField;
            }
            set
            {
                this.remarkField = value;
            }
        }

        /// <remarks/>
        public string as_at
        {
            get
            {
                return this.as_atField;
            }
            set
            {
                this.as_atField = value;
            }
        }

        /// <remarks/>
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public string position
        {
            get
            {
                return this.positionField;
            }
            set
            {
                this.positionField = value;
            }
        }

        /// <remarks/>
        public string place
        {
            get
            {
                return this.placeField;
            }
            set
            {
                this.placeField = value;
            }
        }

        /// <remarks/>
        public string court
        {
            get
            {
                return this.courtField;
            }
            set
            {
                this.courtField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlSummaryPrevious_company_interests
    {

        private xmlSummaryPrevious_company_interestsPrevious_company_interest previous_company_interestField;

        /// <remarks/>
        public xmlSummaryPrevious_company_interestsPrevious_company_interest previous_company_interest
        {
            get
            {
                return this.previous_company_interestField;
            }
            set
            {
                this.previous_company_interestField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlSummaryPrevious_company_interestsPrevious_company_interest
    {

        private string typeField;

        private string reg_noField;

        private string display_idField;

        private string nameField;

        private string statusField;

        private string positionField;

        private string cessation_dateField;

        private string incorporated_dateField;

        private string classField;

        /// <remarks/>
        public string type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        public string reg_no
        {
            get
            {
                return this.reg_noField;
            }
            set
            {
                this.reg_noField = value;
            }
        }

        /// <remarks/>
        public string display_id
        {
            get
            {
                return this.display_idField;
            }
            set
            {
                this.display_idField = value;
            }
        }

        /// <remarks/>
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public string status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        public string position
        {
            get
            {
                return this.positionField;
            }
            set
            {
                this.positionField = value;
            }
        }

        /// <remarks/>
        public string cessation_date
        {
            get
            {
                return this.cessation_dateField;
            }
            set
            {
                this.cessation_dateField = value;
            }
        }

        /// <remarks/>
        public string incorporated_date
        {
            get
            {
                return this.incorporated_dateField;
            }
            set
            {
                this.incorporated_dateField = value;
            }
        }

        /// <remarks/>
        public string @class
        {
            get
            {
                return this.classField;
            }
            set
            {
                this.classField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlBanking_info
    {

        private xmlBanking_infoCcris_selected_by_you ccris_selected_by_youField;

        private string ccris_banking_warningField;

        private xmlBanking_infoCcris_banking_summary ccris_banking_summaryField;

        private xmlBanking_infoDcheqs_info dcheqs_infoField;

        private xmlBanking_infoCcris_banking_details ccris_banking_detailsField;

        /// <remarks/>
        public xmlBanking_infoCcris_selected_by_you ccris_selected_by_you
        {
            get
            {
                return this.ccris_selected_by_youField;
            }
            set
            {
                this.ccris_selected_by_youField = value;
            }
        }

        /// <remarks/>
        public string ccris_banking_warning
        {
            get
            {
                return this.ccris_banking_warningField;
            }
            set
            {
                this.ccris_banking_warningField = value;
            }
        }

        /// <remarks/>
        public xmlBanking_infoCcris_banking_summary ccris_banking_summary
        {
            get
            {
                return this.ccris_banking_summaryField;
            }
            set
            {
                this.ccris_banking_summaryField = value;
            }
        }

        /// <remarks/>
        public xmlBanking_infoDcheqs_info dcheqs_info
        {
            get
            {
                return this.dcheqs_infoField;
            }
            set
            {
                this.dcheqs_infoField = value;
            }
        }

        /// <remarks/>
        public xmlBanking_infoCcris_banking_details ccris_banking_details
        {
            get
            {
                return this.ccris_banking_detailsField;
            }
            set
            {
                this.ccris_banking_detailsField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlBanking_infoCcris_selected_by_you
    {

        private string entity_nameField;

        private string entity_id2Field;

        private string entity_idField;

        private string entity_keyField;

        /// <remarks/>
        public string entity_name
        {
            get
            {
                return this.entity_nameField;
            }
            set
            {
                this.entity_nameField = value;
            }
        }

        /// <remarks/>
        public string entity_id2
        {
            get
            {
                return this.entity_id2Field;
            }
            set
            {
                this.entity_id2Field = value;
            }
        }

        /// <remarks/>
        public string entity_id
        {
            get
            {
                return this.entity_idField;
            }
            set
            {
                this.entity_idField = value;
            }
        }

        /// <remarks/>
        public string entity_key
        {
            get
            {
                return this.entity_keyField;
            }
            set
            {
                this.entity_keyField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlBanking_infoCcris_banking_summary
    {

        private xmlBanking_infoCcris_banking_summarySummary_credit_report summary_credit_reportField;

        private xmlBanking_infoCcris_banking_summarySummary_liabilities summary_liabilitiesField;

        /// <remarks/>
        public xmlBanking_infoCcris_banking_summarySummary_credit_report summary_credit_report
        {
            get
            {
                return this.summary_credit_reportField;
            }
            set
            {
                this.summary_credit_reportField = value;
            }
        }

        /// <remarks/>
        public xmlBanking_infoCcris_banking_summarySummary_liabilities summary_liabilities
        {
            get
            {
                return this.summary_liabilitiesField;
            }
            set
            {
                this.summary_liabilitiesField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlBanking_infoCcris_banking_summarySummary_credit_report
    {

        private string approved_countField;

        private string approved_amountField;

        private string pending_countField;

        private string pending_amountField;

        /// <remarks/>
        public string approved_count
        {
            get
            {
                return this.approved_countField;
            }
            set
            {
                this.approved_countField = value;
            }
        }

        /// <remarks/>
        public string approved_amount
        {
            get
            {
                return this.approved_amountField;
            }
            set
            {
                this.approved_amountField = value;
            }
        }

        /// <remarks/>
        public string pending_count
        {
            get
            {
                return this.pending_countField;
            }
            set
            {
                this.pending_countField = value;
            }
        }

        /// <remarks/>
        public string pending_amount
        {
            get
            {
                return this.pending_amountField;
            }
            set
            {
                this.pending_amountField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlBanking_infoCcris_banking_summarySummary_liabilities
    {

        private xmlBanking_infoCcris_banking_summarySummary_liabilitiesBorrower borrowerField;

        private string legal_action_takenField;

        private string special_attention_accountField;

        /// <remarks/>
        public xmlBanking_infoCcris_banking_summarySummary_liabilitiesBorrower borrower
        {
            get
            {
                return this.borrowerField;
            }
            set
            {
                this.borrowerField = value;
            }
        }

        /// <remarks/>
        public string legal_action_taken
        {
            get
            {
                return this.legal_action_takenField;
            }
            set
            {
                this.legal_action_takenField = value;
            }
        }

        /// <remarks/>
        public string special_attention_account
        {
            get
            {
                return this.special_attention_accountField;
            }
            set
            {
                this.special_attention_accountField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlBanking_infoCcris_banking_summarySummary_liabilitiesBorrower
    {

        private string outstandingField;

        private string total_limitField;

        private string fec_limitField;

        /// <remarks/>
        public string outstanding
        {
            get
            {
                return this.outstandingField;
            }
            set
            {
                this.outstandingField = value;
            }
        }

        /// <remarks/>
        public string total_limit
        {
            get
            {
                return this.total_limitField;
            }
            set
            {
                this.total_limitField = value;
            }
        }

        /// <remarks/>
        public string fec_limit
        {
            get
            {
                return this.fec_limitField;
            }
            set
            {
                this.fec_limitField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlBanking_infoDcheqs_info
    {

        private xmlBanking_infoDcheqs_infoItem[] own_bankField;

        private xmlBanking_infoDcheqs_infoItem1[] other_bankField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("item", IsNullable = false)]
        public xmlBanking_infoDcheqs_infoItem[] own_bank
        {
            get
            {
                return this.own_bankField;
            }
            set
            {
                this.own_bankField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("item", IsNullable = false)]
        public xmlBanking_infoDcheqs_infoItem1[] other_bank
        {
            get
            {
                return this.other_bankField;
            }
            set
            {
                this.other_bankField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlBanking_infoDcheqs_infoItem
    {

        private string account_noField;

        private string inssuance_dateField;

        private string cheque_noField;

        private string amountField;

        private string remarkField;

        /// <remarks/>
        public string account_no
        {
            get
            {
                return this.account_noField;
            }
            set
            {
                this.account_noField = value;
            }
        }

        /// <remarks/>
        public string inssuance_date
        {
            get
            {
                return this.inssuance_dateField;
            }
            set
            {
                this.inssuance_dateField = value;
            }
        }

        /// <remarks/>
        public string cheque_no
        {
            get
            {
                return this.cheque_noField;
            }
            set
            {
                this.cheque_noField = value;
            }
        }

        /// <remarks/>
        public string amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>
        public string remark
        {
            get
            {
                return this.remarkField;
            }
            set
            {
                this.remarkField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlBanking_infoDcheqs_infoItem1
    {

        private string bank_noField;

        private string account_noField;

        private string inssuance_dateField;

        /// <remarks/>
        public string bank_no
        {
            get
            {
                return this.bank_noField;
            }
            set
            {
                this.bank_noField = value;
            }
        }

        /// <remarks/>
        public string account_no
        {
            get
            {
                return this.account_noField;
            }
            set
            {
                this.account_noField = value;
            }
        }

        /// <remarks/>
        public string inssuance_date
        {
            get
            {
                return this.inssuance_dateField;
            }
            set
            {
                this.inssuance_dateField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlBanking_infoCcris_banking_details
    {

        private string start_yearField;

        private string end_yearField;

        private string[] monthField;

        private xmlBanking_infoCcris_banking_detailsItem[] outstanding_creditField;

        private object credit_applicationField;

        private xmlBanking_infoCcris_banking_detailsItem[] special_attention_accountField;

        private xmlBanking_infoCcris_banking_detailsItem2[] facilities_remarkField;

        private xmlBanking_infoCcris_banking_detailsStatus_remark status_remarkField;

        private xmlBanking_infoCcris_banking_detailsLegal_remark legal_remarkField;

        /// <remarks/>
        public string start_year
        {
            get
            {
                return this.start_yearField;
            }
            set
            {
                this.start_yearField = value;
            }
        }

        /// <remarks/>
        public string end_year
        {
            get
            {
                return this.end_yearField;
            }
            set
            {
                this.end_yearField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("item", IsNullable = false)]
        public string[] month
        {
            get
            {
                return this.monthField;
            }
            set
            {
                this.monthField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("item", IsNullable = false)]
        public xmlBanking_infoCcris_banking_detailsItem[] outstanding_credit
        {
            get
            {
                return this.outstanding_creditField;
            }
            set
            {
                this.outstanding_creditField = value;
            }
        }

        /// <remarks/>
        public object credit_application
        {
            get
            {
                return this.credit_applicationField;
            }
            set
            {
                this.credit_applicationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("item", IsNullable = false)]
        public xmlBanking_infoCcris_banking_detailsItem[] special_attention_account
        {
            get
            {
                return this.special_attention_accountField;
            }
            set
            {
                this.special_attention_accountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("item", IsNullable = false)]
        public xmlBanking_infoCcris_banking_detailsItem2[] facilities_remark
        {
            get
            {
                return this.facilities_remarkField;
            }
            set
            {
                this.facilities_remarkField = value;
            }
        }

        /// <remarks/>
        public xmlBanking_infoCcris_banking_detailsStatus_remark status_remark
        {
            get
            {
                return this.status_remarkField;
            }
            set
            {
                this.status_remarkField = value;
            }
        }

        /// <remarks/>
        public xmlBanking_infoCcris_banking_detailsLegal_remark legal_remark
        {
            get
            {
                return this.legal_remarkField;
            }
            set
            {
                this.legal_remarkField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlBanking_infoCcris_banking_detailsItem
    {

        private xmlBanking_infoCcris_banking_detailsItemMaster masterField;

        private xmlBanking_infoCcris_banking_detailsItemItem[] sub_accountField;

        /// <remarks/>
        public xmlBanking_infoCcris_banking_detailsItemMaster master
        {
            get
            {
                return this.masterField;
            }
            set
            {
                this.masterField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("item", IsNullable = false)]
        public xmlBanking_infoCcris_banking_detailsItemItem[] sub_account
        {
            get
            {
                return this.sub_accountField;
            }
            set
            {
                this.sub_accountField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlBanking_infoCcris_banking_detailsItemMaster
    {

        private xmlBanking_infoCcris_banking_detailsItemMasterItem itemField;

        /// <remarks/>
        public xmlBanking_infoCcris_banking_detailsItemMasterItem item
        {
            get
            {
                return this.itemField;
            }
            set
            {
                this.itemField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlBanking_infoCcris_banking_detailsItemMasterItem
    {

        private string master_idField;

        private string dateField;

        private string capacityField;

        private string lender_typeField;

        private string limitField;

        private string legal_statusField;

        private string legal_status_dateField;

        private string collateral_typeField;

        private string financial_group_resident_statusField;

        private string collateral_type_codeField;

        /// <remarks/>
        public string master_id
        {
            get
            {
                return this.master_idField;
            }
            set
            {
                this.master_idField = value;
            }
        }

        /// <remarks/>
        public string date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>
        public string capacity
        {
            get
            {
                return this.capacityField;
            }
            set
            {
                this.capacityField = value;
            }
        }

        /// <remarks/>
        public string lender_type
        {
            get
            {
                return this.lender_typeField;
            }
            set
            {
                this.lender_typeField = value;
            }
        }

        /// <remarks/>
        public string limit
        {
            get
            {
                return this.limitField;
            }
            set
            {
                this.limitField = value;
            }
        }

        /// <remarks/>
        public string legal_status
        {
            get
            {
                return this.legal_statusField;
            }
            set
            {
                this.legal_statusField = value;
            }
        }

        /// <remarks/>
        public string legal_status_date
        {
            get
            {
                return this.legal_status_dateField;
            }
            set
            {
                this.legal_status_dateField = value;
            }
        }

        /// <remarks/>
        public string collateral_type
        {
            get
            {
                return this.collateral_typeField;
            }
            set
            {
                this.collateral_typeField = value;
            }
        }

        /// <remarks/>
        public string financial_group_resident_status
        {
            get
            {
                return this.financial_group_resident_statusField;
            }
            set
            {
                this.financial_group_resident_statusField = value;
            }
        }

        /// <remarks/>
        public string collateral_type_code
        {
            get
            {
                return this.collateral_type_codeField;
            }
            set
            {
                this.collateral_type_codeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlBanking_infoCcris_banking_detailsItemItem
    {

        private xmlBanking_infoCcris_banking_detailsItemItemItem itemField;

        /// <remarks/>
        public xmlBanking_infoCcris_banking_detailsItemItemItem item
        {
            get
            {
                return this.itemField;
            }
            set
            {
                this.itemField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlBanking_infoCcris_banking_detailsItemItemItem
    {

        private string statusField;

        private string restructure_reschedule_dateField;

        private string facilityField;

        private string total_outstanding_balanceField;

        private string total_outstanding_balance_bnmField;

        private string balance_updated_dateField;

        private string installment_amountField;

        private string principle_repayment_termField;

        private string collateral_typeField;

        private string[] credit_positionField;

        private string collateral_type_codeField;

        /// <remarks/>
        public string status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        public string restructure_reschedule_date
        {
            get
            {
                return this.restructure_reschedule_dateField;
            }
            set
            {
                this.restructure_reschedule_dateField = value;
            }
        }

        /// <remarks/>
        public string facility
        {
            get
            {
                return this.facilityField;
            }
            set
            {
                this.facilityField = value;
            }
        }

        /// <remarks/>
        public string total_outstanding_balance
        {
            get
            {
                return this.total_outstanding_balanceField;
            }
            set
            {
                this.total_outstanding_balanceField = value;
            }
        }

        /// <remarks/>
        public string total_outstanding_balance_bnm
        {
            get
            {
                return this.total_outstanding_balance_bnmField;
            }
            set
            {
                this.total_outstanding_balance_bnmField = value;
            }
        }

        /// <remarks/>
        public string balance_updated_date
        {
            get
            {
                return this.balance_updated_dateField;
            }
            set
            {
                this.balance_updated_dateField = value;
            }
        }

        /// <remarks/>
        public string installment_amount
        {
            get
            {
                return this.installment_amountField;
            }
            set
            {
                this.installment_amountField = value;
            }
        }

        /// <remarks/>
        public string principle_repayment_term
        {
            get
            {
                return this.principle_repayment_termField;
            }
            set
            {
                this.principle_repayment_termField = value;
            }
        }

        /// <remarks/>
        public string collateral_type
        {
            get
            {
                return this.collateral_typeField;
            }
            set
            {
                this.collateral_typeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("item", IsNullable = false)]
        public string[] credit_position
        {
            get
            {
                return this.credit_positionField;
            }
            set
            {
                this.credit_positionField = value;
            }
        }

        /// <remarks/>
        public string collateral_type_code
        {
            get
            {
                return this.collateral_type_codeField;
            }
            set
            {
                this.collateral_type_codeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlBanking_infoCcris_banking_detailsItem2
    {

        private string codeField;

        private string descField;

        /// <remarks/>
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        public string desc
        {
            get
            {
                return this.descField;
            }
            set
            {
                this.descField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlBanking_infoCcris_banking_detailsStatus_remark
    {

        private xmlBanking_infoCcris_banking_detailsStatus_remarkItem itemField;

        /// <remarks/>
        public xmlBanking_infoCcris_banking_detailsStatus_remarkItem item
        {
            get
            {
                return this.itemField;
            }
            set
            {
                this.itemField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlBanking_infoCcris_banking_detailsStatus_remarkItem
    {

        private string codeField;

        private string descField;

        /// <remarks/>
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        public string desc
        {
            get
            {
                return this.descField;
            }
            set
            {
                this.descField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlBanking_infoCcris_banking_detailsLegal_remark
    {

        private xmlBanking_infoCcris_banking_detailsLegal_remarkItem itemField;

        /// <remarks/>
        public xmlBanking_infoCcris_banking_detailsLegal_remarkItem item
        {
            get
            {
                return this.itemField;
            }
            set
            {
                this.itemField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlBanking_infoCcris_banking_detailsLegal_remarkItem
    {

        private string codeField;

        private string descField;

        /// <remarks/>
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        public string desc
        {
            get
            {
                return this.descField;
            }
            set
            {
                this.descField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlLitigation_info
    {

        private object legal_suit_by_regnoField;

        private object legal_suit_proclamation_by_regnoField;

        private object others_known_legal_suitField;

        private object legal_suit_by_plaintiffField;

        private xmlLitigation_infoPerson_bankruptcy person_bankruptcyField;

        /// <remarks/>
        public object legal_suit_by_regno
        {
            get
            {
                return this.legal_suit_by_regnoField;
            }
            set
            {
                this.legal_suit_by_regnoField = value;
            }
        }

        /// <remarks/>
        public object legal_suit_proclamation_by_regno
        {
            get
            {
                return this.legal_suit_proclamation_by_regnoField;
            }
            set
            {
                this.legal_suit_proclamation_by_regnoField = value;
            }
        }

        /// <remarks/>
        public object others_known_legal_suit
        {
            get
            {
                return this.others_known_legal_suitField;
            }
            set
            {
                this.others_known_legal_suitField = value;
            }
        }

        /// <remarks/>
        public object legal_suit_by_plaintiff
        {
            get
            {
                return this.legal_suit_by_plaintiffField;
            }
            set
            {
                this.legal_suit_by_plaintiffField = value;
            }
        }

        /// <remarks/>
        public xmlLitigation_infoPerson_bankruptcy person_bankruptcy
        {
            get
            {
                return this.person_bankruptcyField;
            }
            set
            {
                this.person_bankruptcyField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlLitigation_infoPerson_bankruptcy
    {

        private xmlLitigation_infoPerson_bankruptcyCase caseField;

        /// <remarks/>
        public xmlLitigation_infoPerson_bankruptcyCase @case
        {
            get
            {
                return this.caseField;
            }
            set
            {
                this.caseField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlLitigation_infoPerson_bankruptcyCase
    {

        private xmlLitigation_infoPerson_bankruptcyCaseItem itemField;

        /// <remarks/>
        public xmlLitigation_infoPerson_bankruptcyCaseItem item
        {
            get
            {
                return this.itemField;
            }
            set
            {
                this.itemField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlLitigation_infoPerson_bankruptcyCaseItem
    {

        private string status_groupField;

        private string defendant_nameField;

        private string old_icField;

        private string new_icField;

        private string case_noField;

        private string adjudication_order_dateField;

        private string adjudication_order_refField;

        private string case_settledField;

        private string case_withdrawnField;

        private string[] remarkField;

        /// <remarks/>
        public string status_group
        {
            get
            {
                return this.status_groupField;
            }
            set
            {
                this.status_groupField = value;
            }
        }

        /// <remarks/>
        public string defendant_name
        {
            get
            {
                return this.defendant_nameField;
            }
            set
            {
                this.defendant_nameField = value;
            }
        }

        /// <remarks/>
        public string old_ic
        {
            get
            {
                return this.old_icField;
            }
            set
            {
                this.old_icField = value;
            }
        }

        /// <remarks/>
        public string new_ic
        {
            get
            {
                return this.new_icField;
            }
            set
            {
                this.new_icField = value;
            }
        }

        /// <remarks/>
        public string case_no
        {
            get
            {
                return this.case_noField;
            }
            set
            {
                this.case_noField = value;
            }
        }

        /// <remarks/>
        public string adjudication_order_date
        {
            get
            {
                return this.adjudication_order_dateField;
            }
            set
            {
                this.adjudication_order_dateField = value;
            }
        }

        /// <remarks/>
        public string adjudication_order_ref
        {
            get
            {
                return this.adjudication_order_refField;
            }
            set
            {
                this.adjudication_order_refField = value;
            }
        }

        /// <remarks/>
        public string case_settled
        {
            get
            {
                return this.case_settledField;
            }
            set
            {
                this.case_settledField = value;
            }
        }

        /// <remarks/>
        public string case_withdrawn
        {
            get
            {
                return this.case_withdrawnField;
            }
            set
            {
                this.case_withdrawnField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("item", IsNullable = false)]
        public string[] remark
        {
            get
            {
                return this.remarkField;
            }
            set
            {
                this.remarkField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlTrade_bureau
    {

        private xmlTrade_bureauTrade_bureau_entity_details[] trade_bureau_entity_detailsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("trade_bureau_entity_detail", IsNullable = false)]
        public xmlTrade_bureauTrade_bureau_entity_details[] trade_bureau_entity_details
        {
            get
            {
                return this.trade_bureau_entity_detailsField;
            }
            set
            {
                this.trade_bureau_entity_detailsField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlTrade_bureauTrade_bureau_entity_details
    {
        private string subjectField;

        private string new_icField;

        private string creditor_telnoField;

        private string creditor_nameField;

        private string ref_noField;

        private string statusField;

        private string debt_typeField;

        private string credit_termField;

        private string as_at_dateField;

        private string payment_agingField;

        private string status_dateField;

        private string amountField;

        private string industryField;

        /// <remarks/>
        public string subject
        {
            get
            {
                return this.subjectField;
            }
            set
            {
                this.subjectField = value;
            }
        }

        /// <remarks/>
        public string new_ic
        {
            get
            {
                return this.new_icField;
            }
            set
            {
                this.new_icField = value;
            }
        }

        /// <remarks/>
        public string creditor_telno
        {
            get
            {
                return this.creditor_telnoField;
            }
            set
            {
                this.creditor_telnoField = value;
            }
        }

        /// <remarks/>
        public string creditor_name
        {
            get
            {
                return this.creditor_nameField;
            }
            set
            {
                this.creditor_nameField = value;
            }
        }

        /// <remarks/>
        public string ref_no
        {
            get
            {
                return this.ref_noField;
            }
            set
            {
                this.ref_noField = value;
            }
        }

        /// <remarks/>
        public string status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        public string debt_type
        {
            get
            {
                return this.debt_typeField;
            }
            set
            {
                this.debt_typeField = value;
            }
        }

        /// <remarks/>
        public string credit_term
        {
            get
            {
                return this.credit_termField;
            }
            set
            {
                this.credit_termField = value;
            }
        }

        /// <remarks/>
        public string as_at_date
        {
            get
            {
                return this.as_at_dateField;
            }
            set
            {
                this.as_at_dateField = value;
            }
        }

        /// <remarks/>
        public string payment_aging
        {
            get
            {
                return this.payment_agingField;
            }
            set
            {
                this.payment_agingField = value;
            }
        }

        /// <remarks/>
        public string status_date
        {
            get
            {
                return this.status_dateField;
            }
            set
            {
                this.status_dateField = value;
            }
        }

        /// <remarks/>
        public string amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>
        public string industry
        {
            get
            {
                return this.industryField;
            }
            set
            {
                this.industryField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlEnquiry
    {

        private xmlEnquiryPrevious_enquiry previous_enquiryField;

        /// <remarks/>
        public xmlEnquiryPrevious_enquiry previous_enquiry
        {
            get
            {
                return this.previous_enquiryField;
            }
            set
            {
                this.previous_enquiryField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlEnquiryPrevious_enquiry
    {

        private xmlEnquiryPrevious_enquiryItem[] financeField;

        private xmlEnquiryPrevious_enquiryItem2[] commercialField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("item", IsNullable = false)]
        public xmlEnquiryPrevious_enquiryItem[] finance
        {
            get
            {
                return this.financeField;
            }
            set
            {
                this.financeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("item", IsNullable = false)]
        public xmlEnquiryPrevious_enquiryItem2[] commercial
        {
            get
            {
                return this.commercialField;
            }
            set
            {
                this.commercialField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlEnquiryPrevious_enquiryItem
    {

        private string yearField;

        private string yearly_countField;

        private string[] monthField;

        /// <remarks/>
        public string year
        {
            get
            {
                return this.yearField;
            }
            set
            {
                this.yearField = value;
            }
        }

        /// <remarks/>
        public string yearly_count
        {
            get
            {
                return this.yearly_countField;
            }
            set
            {
                this.yearly_countField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("item", IsNullable = false)]
        public string[] month
        {
            get
            {
                return this.monthField;
            }
            set
            {
                this.monthField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlEnquiryPrevious_enquiryItem2
    {

        private string yearField;

        private string yearly_countField;

        private string[] monthField;

        /// <remarks/>
        public string year
        {
            get
            {
                return this.yearField;
            }
            set
            {
                this.yearField = value;
            }
        }

        /// <remarks/>
        public string yearly_count
        {
            get
            {
                return this.yearly_countField;
            }
            set
            {
                this.yearly_countField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("item", IsNullable = false)]
        public string[] month
        {
            get
            {
                return this.monthField;
            }
            set
            {
                this.monthField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlLegend
    {

        private string na_iscore_legendField;

        /// <remarks/>
        public string na_iscore_legend
        {
            get
            {
                return this.na_iscore_legendField;
            }
            set
            {
                this.na_iscore_legendField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class xmlEnd
    {

        private string subscriber_nameField;

        private string usernameField;

        private string order_dateField;

        private string order_timeField;

        private string useridField;

        private string order_idField;

        /// <remarks/>
        public string subscriber_name
        {
            get
            {
                return this.subscriber_nameField;
            }
            set
            {
                this.subscriber_nameField = value;
            }
        }

        /// <remarks/>
        public string username
        {
            get
            {
                return this.usernameField;
            }
            set
            {
                this.usernameField = value;
            }
        }

        public string order_date
        {
            get
            {
                return this.order_dateField;
            }
            set
            {
                this.order_dateField = value;
            }
        }

        public string order_time
        {
            get
            {
                return this.order_timeField;
            }
            set
            {
                this.order_timeField = value;
            }
        }

        /// <remarks/>
        public string userid
        {
            get
            {
                return this.useridField;
            }
            set
            {
                this.useridField = value;
            }
        }

        /// <remarks/>
        public string order_id
        {
            get
            {
                return this.order_idField;
            }
            set
            {
                this.order_idField = value;
            }
        }
    }



}