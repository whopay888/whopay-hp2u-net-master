﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Synergy.Model;
using Synergy.Util;

namespace Synergy.Helper
{
    public class SystemHelper
    {

        public static void AutoLogin()
        { 
            LoginUserModel login_model = new LoginUserModel();
            login_model.VerifyLogin("adminsystem", "123456");
            HttpContext.Current.Session[KeyVal.loginsecure] = login_model;
            HttpContext.Current.Session[KeyVal.session_clogin] = login_model;
        }
    }
}