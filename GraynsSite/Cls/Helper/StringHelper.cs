﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace HJT.Cls.Helper
{
    public static class StringHelper
    {
        public static string ShortenName(this string name)
        {
            string[] splittedName = name.Split(' ');

            if (splittedName != null && splittedName.Any())
            {
                int totalItem = splittedName.Count();
                name = splittedName[0];

                if (totalItem > 1)
                    name += " ";

                for (int i = 1; i < totalItem; i++)
                {
                    if(!string.IsNullOrWhiteSpace(splittedName[i]))
                        name += splittedName[i].Substring(0, 1).ToUpper() + ".";
                }

                //remove last dot
                if (totalItem > 1)
                    name = name.Remove(name.Length - 1, 1);
            }


            return name;
        }

        public static string AppendICNumber(this string name, string fullIC)
        {
            if(!string.IsNullOrWhiteSpace(fullIC))
            {
                if (fullIC.Length >= 4)
                    name += " - " + fullIC.Substring(fullIC.Length - 2, 2);
                else
                    name += " - " + fullIC;
            }

            return name;
        }

        public static string GenerateSHA256String(string inputString)
        {
            SHA256 SHA256 = SHA256Managed.Create();
            byte[] bytes = Encoding.UTF8.GetBytes(inputString);
            byte[] hash = SHA256.ComputeHash(bytes);
            return GetStringFromHash(hash);
        }

        private static string GetStringFromHash(byte[] hash)
        {
            StringBuilder result = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)
            {
                result.Append(hash[i].ToString("X2"));
            }
            return result.ToString().ToLower();
        }
    }
}