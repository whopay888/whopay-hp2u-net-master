﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Serialization;
using Synergy.Helper;

namespace Synergy.Util
{
    public class sqlString
    {

        #region Control
        /// <summary>
        /// 2018-11-04 Bind Repeater
        /// </summary>
        /// <param name="ddl"></param>
        /// <param name="sql"></param>
        public static void bindControl(Repeater rp, string sql)
        {
            wwdb db = new wwdb();

            try
            {
                LogUtil.logSQL(sql);
                rp.DataSource = db.getDataTable(sql);
                rp.DataBind();
            }
            catch (Exception ex)
            {
                LogUtil.logError(ex.ToString(), sql);
            }
            finally
            {
                db.Close();
            }
        }

        /// <summary>
        /// 2012-09-26 Bind GridView
        /// </summary>
        /// <param name="ddl"></param>
        /// <param name="sql"></param>
        public static void bindControl(GridView gv, string sql)
        {
            wwdb db = new wwdb();

            try
            {
                LogUtil.logSQL(sql);
                gv.DataSource = db.getDataTable(sql);
                gv.DataBind();
            }
            catch (Exception ex)
            {
                LogUtil.logError(ex.ToString(), sql);
            }
            finally
            {
                db.Close();
            }
        }

        /// <summary>
        /// 2012-10-03
        /// </summary>
        /// <param name="ddl"></param>
        /// <param name="sql"></param>
        public static void bindControl(DropDownList ddl, string sql, string text, string value, bool addSelect)
        {
            wwdb db = new wwdb();

            try
            {
                int record = 0;

                ddl.DataTextField = text;
                ddl.DataValueField = value;

                db.OpenTable(sql);
                record = db.RecordCount();

                if (record == 0)
                    sql = " SELECT N'" + changeLanguage("No_Record_Found") + "' AS N'" + text + "', '0' AS N'" + value + "' ";

                ddl.DataSource = db.getDataTable(sql);
                ddl.DataBind();

                if (addSelect && record > 0)
                {
                    ddl.Items.Insert(0, sqlString.changeLanguage("Please_Select_One"));
                    ddl.Items[0].Value = "0";
                }
            }
            catch (Exception ex)
            {
                LogUtil.logError(ex.ToString(), sql);
            }
            finally
            {
                db.Close();
            }
        }
        public static void bindControl2(DropDownList ddl, string sql, string text, string value, bool addSelect)
        {
            wwdb db = new wwdb();

            try
            {
                int record = 0;

                ddl.DataTextField = text;
                ddl.DataValueField = value;

                db.OpenTable(sql);
                record = db.RecordCount();

                if (record == 0)
                    sql = " SELECT N'" + changeLanguage("No_Record_Found") + "' AS N'" + text + "', '0' AS N'" + value + "' ";

                ddl.DataSource = db.getDataTable(sql);
                ddl.DataBind();

                if (addSelect && record > 0)
                {
                    ddl.Items.Insert(0, sqlString.changeLanguage("NA"));
                    ddl.Items[0].Value = "0";
                }
            }
            catch (Exception ex)
            {
                LogUtil.logError(ex.ToString(), sql);
            }
            finally
            {
                db.Close();
            }
        }

        public static void bindControl(DropDownList ddl, object datasource, string text, string value, bool addSelect)
        {


            try
            {
                int record = datasource != null ? 1 : 0;

                ddl.DataTextField = text;
                ddl.DataValueField = value;



                if (record == 0)
                {
                    ddl.ClearSelection();
                    ddl.Items.Add(new ListItem(changeLanguage("No_Record_Found"), "0"));
                }
                else
                {
                    ddl.DataSource = datasource;
                    ddl.DataBind();
                }



                if (addSelect && record > 0)
                {
                    ddl.Items.Insert(0, sqlString.changeLanguage("Please_Select_One"));
                    ddl.Items[0].Value = "0";
                }
            }
            catch (Exception ex)
            {
                LogUtil.logError(ex.ToString(), "public static void bindControl(DropDownList ddl, object datasource, string text, string value, bool addSelect)");
            }

        }

        /// <summary>
        /// 2014-03-25
        /// </summary>
        /// <param name="ddl"></param>
        /// <param name="sql"></param>
        public static void bindControl(DropDownList ddl, string sql, string text, string value, bool addSelect, string customTitle)
        {
            wwdb db = new wwdb();

            try
            {
                int record = 0;

                ddl.DataTextField = text;
                ddl.DataValueField = value;

                db.OpenTable(sql);
                record = db.RecordCount();

                if (record == 0)
                    sql = " SELECT N'" + changeLanguage("No_Record_Found") + "' AS N'" + text + "', '0' AS N'" + value + "' ";

                ddl.DataSource = db.getDataTable(sql);
                ddl.DataBind();

                if (addSelect && record > 0)
                {
                    ddl.Items.Insert(0, customTitle);
                    ddl.Items[0].Value = "0";
                }
            }
            catch (Exception ex)
            {
                LogUtil.logError(ex.ToString(), sql);
            }
            finally
            {
                db.Close();
            }
        }

        public static void bindControl(CheckBoxList cbl, string sql, string textColumn, string valueColumn, string checkedColumn)
        {
            wwdb db = new wwdb();

            try
            {
                cbl.DataTextField = textColumn;
                cbl.DataValueField = valueColumn;

                db.OpenTable(sql);

                if (db.RecordCount() == 0)
                    sql = " SELECT N'" + changeLanguage("No_Record_Found") + "' AS N'" + textColumn + "', '0' AS N'" + valueColumn + "' ";

                cbl.DataSource = db.getDataTable(sql);
                cbl.DataBind();

                if (!string.IsNullOrWhiteSpace(checkedColumn) && db.RecordCount() > 0)
                {
                    for (int h = 0; h < db.RecordCount(); h++)
                    {
                        string value = db.Item(valueColumn).ToString();
                        bool isChecked = Convert.ToBoolean(Convert.ToUInt32(db.Item(checkedColumn)));
                        var cbi = cbl.Items.FindByValue(value);
                        cbi.Selected = isChecked;

                        if (!db.Eof())
                        {
                            db.MoveNext();
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                LogUtil.logError(ex.ToString(), sql);
            }
            finally
            {
                db.Close();
            }
        }

        public static void bindControl(RadioButtonList rbl, string sql, string text, string value)
        {
            wwdb db = new wwdb();

            try
            {
                rbl.DataTextField = text;
                rbl.DataValueField = value;

                db.OpenTable(sql);

                if (db.RecordCount() == 0)
                    sql = " SELECT N'" + changeLanguage("No_Record_Found") + "' AS N'" + text + "', '0' AS N'" + value + "' ";

                rbl.DataSource = db.getDataTable(sql);
                rbl.DataBind();
            }
            catch (Exception ex)
            {
                LogUtil.logError(ex.ToString(), sql);
            }
            finally
            {
                db.Close();
            }
        }

        #endregion

        #region Language

        protected static string LanguageFileName(string langCode)
        {
            string fileprefix = "resource";
            string fileextension = ".resx";

            if (langCode == "en")
            {
                return fileprefix + fileextension;
            }
            else
            {
                return fileprefix + "." + langCode + fileextension;
            }
        }

        /// <summary>
        /// Before 2012-09-26
        /// 2012-10-01 Become case insensitive
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        public static string changeLanguage(string word)
        {
            object x = HttpContext.GetGlobalResourceObject("resource", word);

            if (x == null)
                return word;
            else
                return x.ToString();

            //if (KeyVal.changeLanguage)
            //{
            //    if (HttpContext.Current.Request.Cookies[KeyVal.cookie_language] != null)
            //    {
            //        culture = LanguageFileName(secure.Decrypt(HttpContext.Current.Request.Cookies[KeyVal.cookie_language].Value, true));// retrieve value from cookie
            //    }

            //    string filePath = HttpContext.Current.Request.PhysicalApplicationPath + lang_folder + "\\" + culture;

            //    checkLanguage(filePath, word);

            //    ResXResourceSet rset = new ResXResourceSet(filePath);

            //    if (string.IsNullOrWhiteSpace(rset.GetString(word, true)))
            //    {
            //        rset = new ResXResourceSet(HttpContext.Current.Request.PhysicalApplicationPath + lang_folder + "\\" + KeyVal.defaultCulture);
            //    }

            //    string x = rset.GetString(word, true);

            //    return x;
            //}
            //else
            //    return word;
        }

        //Before 2012-09-26
        private static void checkLanguage(string filepath, string key)
        {
            if (KeyVal.checkLanguage)
            {
                bool result = false;
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(filepath);
                XmlNodeList nodeList = xmlDoc.GetElementsByTagName("data");

                foreach (XmlNode childnode in nodeList)
                {
                    if (childnode.Attributes["name"].Value.ToUpper() == key.ToUpper())
                    {
                        result = true;
                        break;
                    }
                }

                if (!result)
                    AddNewLanguageKey(filepath, key);
            }
        }

        //Before 2012-09-26
        private static void AddNewLanguageKey(string filepath, string key)
        {
            //create new instance of XmlDocument
            XmlDocument doc = new XmlDocument();

            //load from file
            doc.Load(filepath);

            // format
            // <data name="(Auto Generate)" xml:space="preserve"> 
            //  <value>(Auto Generate)
            //  </value> 
            // </data>

            XmlNode NewNode = doc.CreateNode(XmlNodeType.Element, "data", null);
            XmlAttribute xAtt1 = doc.CreateAttribute("name");
            XmlAttribute xAtt2 = doc.CreateAttribute("xml:space");
            xAtt1.Value = key;
            xAtt2.Value = "preserve";
            NewNode.Attributes.Append(xAtt1);
            NewNode.Attributes.Append(xAtt2);

            XmlElement XEle = doc.CreateElement("value");
            XEle.InnerXml = key;

            NewNode.AppendChild(XEle);

            //add to elements collection
            doc.DocumentElement.AppendChild(NewNode);

            //save back
            doc.Save(filepath);
        }

        #endregion

        #region Role

        //public static string getRole(string memID)
        //{
        //    wwdb db = new wwdb();
        //    string sql = "", role = KeyVal.Member;

        //    try
        //    {
        //        sql = " SELECT CASE UPPER(a.login_role) WHEN N'" + secure.RC(KeyVal.AD) + "' THEN N'" + secure.RC(KeyVal.Admin) + "' " +
        //            " WHEN N'" + secure.RC(KeyVal.M) + "' THEN N'" + secure.RC(KeyVal.Member) + "' " +
        //            " WHEN N'" + secure.RC(KeyVal.C) + "' THEN N'" + secure.RC(KeyVal.Member) + "' " +
        //            " ELSE N'" + secure.RC(KeyVal.Staff) + "' END " +
        //            " FROM tbl_login a WITH (NOLOCK) WHERE a.login_id = N'" + secure.RC(memID) + "'; ";
        //        role = db.executeScalar(sql).ToString();
        //    }
        //    catch (Exception ex) { LogUtil.logError(ex.ToString(), sql); }
        //    finally { db.Close(); }

        //    return role;
        //}

        public static DataTable getControl(int userID, int value)
        {
            DataTable dt = null;
            string sql = "";
            wwdb db = new wwdb();

            try
            {
                sql = " SELECT b.controlName, a.value FROM tbl_RoleControl a WITH (NOLOCK) LEFT JOIN tbl_Control b WITH (NOLOCK) " +
                    " ON a.controlID = b.id WHERE a.roleID = N'" + secure.RC(userID.ToString()) + "' AND b.controlValue = N'" +
                    secure.RC(value.ToString()) + "'; ";

                dt = db.getDataTable(sql);
            }
            catch (Exception ex)
            { LogUtil.logError(ex.ToString(), sql); }
            finally { db.Close(); }

            return dt;
        }

        #endregion

        #region Date Time
        public static string getCurDate()
        {
            return DateTime.Now.ToString(KeyVal.DateTimeFormat, new CultureInfo("en-US"));
        }

        #endregion

        #region Popup Menu

        public static void OpenNewWindow_Center(String OpenURL,
        String PageTitle,
        int Form_Width,
        int Form_Height,
        Page curPage,
        Boolean Show_ToolBar = false,
        Boolean Show_ScrollBar = false,
        Boolean Show_StatusBar = false,
        Boolean Show_MenuBar = false,
        Boolean Show_Location = false,
        Boolean Resizable = false)
        {
            String JScript = String.Empty;
            Literal Ltr = new Literal();

            //JScript = "<script type=\"text/javascript\">";
            JScript += " var left = (screen.width - " + Form_Width + ") / 2; ";
            JScript += " var top = (screen.height - " + Form_Height + ") / 2; ";
            JScript += " window.open('" + OpenURL + "','" + PageTitle + "',";

            JScript += "'width=" + Form_Width + ",";
            JScript += "height=" + Form_Height + ",";
            JScript += "top='+top+',";
            JScript += "left='+left+',";

            #region Additional Setting

            if (Show_ToolBar) { JScript += ",toolbar=yes"; } else { JScript += ",toolbar=no"; }
            if (Show_ScrollBar) { JScript += ",scrollbars=yes"; } else { JScript += ",toolbar=no"; }
            if (Show_StatusBar) { JScript += ",status=yes"; } else { JScript += ",toolbar=no"; }
            if (Resizable) { JScript += ",resizable=yes"; } else { JScript += ",toolbar=no"; }
            if (Show_MenuBar) { JScript += ",menubar=yes"; } else { JScript += ",toolbar=no"; }
            if (Show_Location) { JScript += ",location=yes"; } else { JScript += ",toolbar=no"; }
            if (Resizable) { JScript += ",menubar=yes"; } else { JScript += ",toolbar=no"; }

            #endregion
            JScript += "')";
            //JScript += "') </script>";

            ScriptManager.RegisterStartupScript(curPage, curPage.GetType(), "popup_window", JScript, true);
        }

        #endregion

        public static DataTable ConvertRowToColumn(DataTable dt)
        {

            DataTable dt2 = new DataTable();
            for (int i = 0; i <= dt.Rows.Count; i++)
            {
                dt2.Columns.Add();
            }
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                dt2.Rows.Add();
                dt2.Rows[i][0] = dt.Columns[i].ColumnName;
            }
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    dt2.Rows[i][j + 1] = dt.Rows[j][i];
                }
            }

            return dt2;

        }

        #region Display Alert

        public static void displayAlert(Page curPage, string msg)
        {
            ScriptManager.RegisterStartupScript(curPage, curPage.GetType(), "err_msg", "alert('" + changeLanguage(msg) + "');", true);
        }

        public static void displayAlert(Page curPage, string msg, string URL)
        {
            ScriptManager.RegisterStartupScript(curPage, curPage.GetType(), "err_msg", "alert('" + changeLanguage(msg) + "'); window.location.href = '" + URL + "';", true);
        }

        public static void displayAlert2(Page curPage, string msg)
        {
            ScriptManager.RegisterStartupScript(curPage, curPage.GetType(), "err_msg", "alert('" + msg + "');", true);
        }

        public static void displayAlert2(Page curPage, string msg, string URL)
        {
            ScriptManager.RegisterStartupScript(curPage, curPage.GetType(), "err_msg", "alert('" + msg + "'); window.location.href = '" + URL + "';", true);
        }


        #endregion

        #region Other Function

        public static string decryptURL(string str)
        {
            string returnValue = "";
            if (string.IsNullOrWhiteSpace(str))
                return returnValue;

            try
            {
                returnValue = secure.Decrypt(secure.DURC(str), true);
            }
            catch (Exception ex) { LogUtil.logError(ex.ToString(), "Err Decrypt - " + str); }

            return returnValue;
        }

        public static string encryptURL(string str)
        {
            string returnValue = "";

            try
            {
                returnValue = secure.EURC(secure.Encrypt(str, true));
            }
            catch (Exception ex) { LogUtil.logError(ex.ToString(), "Err Encrypt - " + str); }

            return returnValue;
        }

        public static void exportFunc(string fileName, string sql, bool isClientList = false)
        {
            wwdb db = new wwdb();
            DataTable dt;

            try
            {
                dt = db.getDataTable(sql);

                ExcelPackage excel = new ExcelPackage();
                ExcelWorksheet workSheet = excel.Workbook.Worksheets.Add("Report");
                workSheet.Cells["A1"].LoadFromDataTable(dt, true);
                workSheet.TabColor = System.Drawing.Color.Black;
                workSheet.DefaultRowHeight = 15;
                //header
                workSheet.Row(1).Style.Font.Bold = true;
                workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                if (isClientList)
                {
                    for (int i = 1; i <= 9; i++)
                    {
                        workSheet.Column(i).AutoFit();
                        workSheet.Column(i).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        workSheet.Column(i).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        if (i == 9)
                            workSheet.Column(i).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    }
                    workSheet.Column(9).Style.WrapText = true; //advise
                    workSheet.CustomHeight = false;
                }
                using (var memoryStream = new MemoryStream())
                {
                    HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + fileName + ".xlsx");
                    excel.SaveAs(memoryStream);
                    memoryStream.WriteTo(HttpContext.Current.Response.OutputStream);
                    HttpContext.Current.Response.Flush();
                    HttpContext.Current.Response.End();

                    /*
                    System.IO.StringWriter stringWriter = new System.IO.StringWriter();
                    HtmlTextWriter htmlWriter = new HtmlTextWriter(stringWriter);

                    System.Web.UI.WebControls.TableItemStyle alternatingStyle = new TableItemStyle();
                    System.Web.UI.WebControls.TableItemStyle headerStyle = new TableItemStyle();
                    System.Web.UI.WebControls.TableItemStyle itemStyle = new TableItemStyle();
                    alternatingStyle.BackColor = System.Drawing.Color.White;
                    headerStyle.BackColor = System.Drawing.Color.White;
                    headerStyle.Font.Bold = true;
                    headerStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Center;
                    itemStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Center; ;

                    DataGrid excel = new DataGrid();
                    excel.AlternatingItemStyle.MergeWith(alternatingStyle);
                    excel.HeaderStyle.MergeWith(headerStyle);
                    excel.ItemStyle.MergeWith(itemStyle);
                    excel.GridLines = GridLines.Both;
                    excel.HeaderStyle.Font.Bold = true;
                    excel.DataSource = dt.DefaultView;   //Êä³öDataTableµÄÄÚÈÝ
                    excel.DataBind();
                    excel.RenderControl(htmlWriter);

                    //string tab = "";
                    //foreach (DataColumn dc in dt.Columns)
                    //{
                    //    HttpContext.Current.Response.Write(tab +"<th class='text' >"+ dc.ColumnName+"</th>");
                    //    tab = "\t";
                    //}
                    //HttpContext.Current.Response.Write("\n");
                    //int i;
                    //foreach (DataRow dr in dt.Rows)
                    //{
                    //    tab = "";
                    //    for (i = 0; i < dt.Columns.Count; i++)
                    //    {
                    //        HttpContext.Current.Response.Write(tab + dr[i].ToString());
                    //        tab = "\t";
                    //    }
                    //    HttpContext.Current.Response.Write("\n");
                    //}

                    //string style = @"&lt;style> .text { mso-number-format:\@; } </style> ";
                    //HttpContext.Current.Response.Write(style);

                    string sCurDate = DateTime.Now.ToShortDateString().Replace("/", "").Replace("-", "");
                    if (string.IsNullOrEmpty(fileName))
                        fileName = sCurDate;

                    fileName = fileName.Replace(" ", "_");
                    HttpContext.Current.Response.AppendHeader("content-disposition", "attachment;filename=" + fileName + ".xls");
                    HttpContext.Current.Response.Charset = "UTF-8";
                    //HttpContext.Current.Response.Charset = "GB2312";
                    HttpContext.Current.Response.ContentEncoding = Encoding.UTF8;
                    //HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("gb2312");
                    //"application/vnd.ms-excel";
                    //".xls/.txt/.doc";
                    //image/JPEG;text/HTML;image/GIF;vnd.ms-excel/msword
                    //"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                    HttpContext.Current.Response.BinaryWrite(Encoding.UTF8.GetPreamble());
                    //HttpContext.Current.Response.ContentType = ".xls";



                    HttpContext.Current.Response.Write(stringWriter.ToString());
                    HttpContext.Current.Response.End();
                    //string filestr = "d:\\data\\" + filePath; //filePathÊÇÎÄ¼þµÄÂ·¾¶
                    //int pos = filestr.LastIndexOf("\\");
                    //string file = filestr.Substring(0, pos);
                    //if (!Directory.Exists(file)) {
                    //    Directory.CreateDirectory(file);
                    //}
                    //System.IO.StreamWriter sw = new StreamWriter(filestr);
                    //sw.Write(stringWriter.ToString());
                    //sw.Close();
                    */
                }
            }
            catch (Exception ex)
            { LogUtil.logError(ex.ToString(), sql); }
            finally
            { db.Close(); HttpContext.Current.Response.End(); }

        }

        public static void exportFuncWithNestedGridView(string fileName, string[] sql, string[] nestedTitle, int[] replaceColumn)
        {
            wwdb db = new wwdb();
            DataTable dt = null;

            try
            {
                dt = db.getDataTable(sql[0]);

                string Extension = ".csv";
                if (Extension == ".csv")
                {
                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", fileName + Extension));
                    //HttpContext.Current.Response.Charset = "";
                    HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.UTF8;
                    HttpContext.Current.Response.BinaryWrite(System.Text.Encoding.UTF8.GetPreamble());
                    HttpContext.Current.Response.Cache.SetCacheability(System.Web.HttpCacheability.Public);
                    HttpContext.Current.Response.ContentType = "application/ms-excel";

                    StringBuilder sbCsv = new StringBuilder();

                    for (int headerCol = 0; headerCol <= dt.Columns.Count - 1; headerCol++)
                    {
                        if ((sbCsv.Length > 0))
                            sbCsv.Append(", ");

                        sbCsv.Append(dt.Columns[headerCol].ColumnName.ToString());
                    }

                    for (int intRow = 0; intRow <= dt.Rows.Count - 1; intRow++)
                    {
                        DataSet ds = new DataSet();

                        for (int b = 1; b < sql.Length; b++)
                        {
                            string str = sql[b];

                            DataTable subDT = new DataTable();

                            for (int d = 0; d < replaceColumn.Length; d++)
                            {
                                str = str.Replace("{" + (d).ToString() + "}", "'" + dt.Rows[intRow][replaceColumn[d]].ToString() + "'");
                            }

                            subDT = db.getDataTable(str);

                            if (subDT != null)
                                ds.Tables.Add(subDT);
                        }

                        StringBuilder sbRow = new StringBuilder();

                        for (int intCol = 0; intCol <= dt.Columns.Count - 1; intCol++)
                        {
                            if ((sbRow.Length > 0))
                                sbRow.Append(", ");
                            sbRow.Append("\t" + dt.Rows[intRow][intCol].ToString());
                        }

                        if (ds != null)
                        {
                            if (ds.Tables.Count > 0)
                            {
                                for (int c = 0; c < ds.Tables.Count; c++)
                                {
                                    if (ds.Tables[c].Rows.Count > 0)
                                    {
                                        sbRow.Append(Environment.NewLine);

                                        StringBuilder sbNestedHeader = new StringBuilder();

                                        for (int nestedHeader = 0; nestedHeader <= ds.Tables[c].Columns.Count - 1; nestedHeader++)
                                        {
                                            string title = "";

                                            if (nestedTitle != null)
                                            {
                                                title = nestedTitle[c];

                                                for (int nestedTitleReplace = 0; nestedTitleReplace < replaceColumn.Length; nestedTitleReplace++)
                                                {
                                                    title = title.Replace("{" + (nestedTitleReplace).ToString() + "}", dt.Rows[intRow][replaceColumn[nestedTitleReplace]].ToString());
                                                }
                                            }

                                            if ((sbNestedHeader.Length > 0))
                                                sbNestedHeader.Append(", ");
                                            else
                                                sbNestedHeader.Append("," + title + ", ");

                                            sbNestedHeader.Append(ds.Tables[c].Columns[nestedHeader].ColumnName.ToString());
                                        }

                                        if (sbNestedHeader.Length > 0)
                                            sbRow.Append(sbNestedHeader);

                                        for (int nestedRow = 0; nestedRow <= ds.Tables[c].Rows.Count - 1; nestedRow++)
                                        {
                                            StringBuilder sbNestedRow = new StringBuilder();

                                            for (int nestedCol = 0; nestedCol <= ds.Tables[c].Columns.Count - 1; nestedCol++)
                                            {
                                                if ((sbNestedRow.Length > 0))
                                                    sbNestedRow.Append(", ");
                                                else
                                                    sbNestedRow.Append(" , , ");

                                                sbNestedRow.Append(ds.Tables[c].Rows[nestedRow][nestedCol].ToString());
                                            }

                                            if (sbNestedRow.Length > 0)
                                            {
                                                sbRow.Append(Environment.NewLine);
                                                sbRow.Append(sbNestedRow.ToString());
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if ((sbCsv.Length > 0))
                            sbCsv.Append(Environment.NewLine);

                        sbCsv.Append(sbRow.ToString());
                    }

                    string a = sbCsv.ToString();

                    HttpContext.Current.Response.Write(sbCsv.ToString());
                    HttpContext.Current.Response.Flush();
                    HttpContext.Current.Response.End();
                }
            }
            catch (Exception ex)
            {
                LogUtil.logError(ex.ToString(), sql[0]);
            }
            finally
            {
                db.Close();
            }
        }

        #endregion

        #region Search Function Part

        //Before 2012-09-26
        /// <summary>
        /// If isLike is false then use equal
        /// If addAnd is true then will add AND at in front of string
        /// If addOr is true then will add OR at in front of string
        /// If addAnd and addOr are false then nothing will be add
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="searchWord"></param>
        /// <param name="isEqual"></param>
        /// <param name="addAnd"></param>
        /// <param name="addOr"></param>
        /// <returns></returns>
        public static string searchTextBox(string columnName, TextBox textBox, bool isLike, bool addAnd, bool addOr)
        {
            if (textBox.Text.Trim() != string.Empty)
            {
                if (addAnd && isLike)
                    return " AND " + columnName + " LIKE N'%" + secure.RC(textBox.Text) + "%' ";
                else if (addOr && isLike)
                    return " OR " + columnName + " LIKE N'%" + secure.RC(textBox.Text) + "%' ";
                else if (isLike & !addAnd && !addOr)
                    return columnName + " LIKE N'%" + secure.RC(textBox.Text) + "%' ";
                else if (addAnd && !isLike)
                    return " AND " + columnName + " = N'" + secure.RC(textBox.Text) + "' ";
                else if (addOr && !isLike)
                    return " OR " + columnName + " = N'" + secure.RC(textBox.Text) + "' ";
                else if (!addAnd && !addOr && !isLike)
                    return columnName + " = N'" + secure.RC(textBox.Text) + "' ";
            }

            return "";
        }


        //Before 2012-09-26
        /// <summary>
        /// If isValidate is true then will validate SelectedIndex != 0 
        /// If addAnd is true then will add AND at in front of string
        /// If addOr is true then will add OR at in front of string
        /// If addAnd and addOr are false then nothing will be add
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="ddlList"></param>
        /// <param name="isValidate"></param>
        /// <param name="addAnd"></param>
        /// <param name="addOr"></param>
        /// <returns></returns>
        public static string searchDropDownList(string columnName, DropDownList ddlList, bool isValidate, bool addAnd, bool addOr)
        {
            if (isValidate)
            {
                if (ddlList.SelectedIndex != 0)
                {
                    if (addAnd)
                        return " AND " + columnName + " = N'" + secure.RC(ddlList.SelectedValue.ToString()) + "' ";
                    else if (addOr)
                        return " OR " + columnName + " = N'" + secure.RC(ddlList.SelectedValue.ToString()) + "' ";
                    else if (!addAnd && !addOr)
                        return " WHERE " + columnName + " = N'" + secure.RC(ddlList.SelectedValue.ToString()) + "' ";
                }
                else
                {

                    // validate only for AnnouncementStatus in AnnouncementList.aspx
                    if (columnName == "a.announcementstatus" && ddlList.ID == "ddlAnnouncementStatus")
                    {
                        if (ddlList.SelectedValue != "-")
                        {
                            if (addAnd)
                                return " AND " + columnName + " = N'" + secure.RC(ddlList.SelectedValue.ToString()) + "' ";
                            else if (addOr)
                                return " OR " + columnName + " = N'" + secure.RC(ddlList.SelectedValue.ToString()) + "' ";
                            else if (!addAnd && !addOr)
                                return " WHERE " + columnName + " = N'" + secure.RC(ddlList.SelectedValue.ToString()) + "' ";
                        }

                    }
                }
            }
            else
            {
                if (addAnd)
                    return " AND " + columnName + " = N'" + secure.RC(ddlList.SelectedValue.ToString()) + "' ";
                else if (addOr)
                    return " OR " + columnName + " = N'" + secure.RC(ddlList.SelectedValue.ToString()) + "' ";
                else if (!addAnd && !addOr)
                    return " WHERE " + columnName + " = N'" + secure.RC(ddlList.SelectedValue.ToString()) + "' ";
            }

            return "";
        }

        #endregion

        #region Column Part

        /// <summary>
        /// Before 2012-09-26
        /// 2012-10-01 Become case insensitive
        /// </summary>
        /// <param name="column"></param>
        /// <param name="title"></param>
        /// <returns></returns>
        public static string firstColumn(string column, string title)
        {
            //if (KeyVal.changeLanguage)
            //{
            //    if (HttpContext.Current.Request.Cookies[KeyVal.cookie_language] != null)
            //    {
            //        culture = LanguageFileName(secure.Decrypt(HttpContext.Current.Request.Cookies[KeyVal.cookie_language].Value, true));// retrieve value from cookie
            //    }

            //    String filePath = HttpContext.Current.Request.PhysicalApplicationPath + lang_folder + "\\" + culture;

            //    checkLanguage(filePath, title);

            //    ResXResourceSet rset = new ResXResourceSet(filePath);

            //    if (string.IsNullOrWhiteSpace(rset.GetString(title, true)))
            //    {
            //        rset = new ResXResourceSet(HttpContext.Current.Request.PhysicalApplicationPath + lang_folder + "\\" + KeyVal.defaultCulture);
            //    }

            //    if (column.ToUpper().Contains("CASE") && column.ToUpper().Contains("WHEN"))
            //        return " " + column + " END AS N'" + rset.GetString(title, true) + "' ";
            //    else
            //        return " " + column + " AS N'" + rset.GetString(title, true) + "' ";
            //}
            //else
            //{
            //    if (column.ToUpper().Contains("CASE") && column.ToUpper().Contains("WHEN"))
            //        return " " + column + " END AS '" + title + "' ";
            //    else
            //        return " " + column + " AS '" + title + "' ";
            //}

            if (column.ToUpper().Contains("CASE") && column.ToUpper().Contains("WHEN"))
                return " " + column + " END AS N'" + changeLanguage(title) + "' ";
            else
                return " " + column + " AS N'" + changeLanguage(title) + "' ";
        }

        /// <summary>
        /// Before 2012-09-26
        /// 2012-10-01 Become case insensitive
        /// </summary>
        /// <param name="column"></param>
        /// <param name="title"></param>
        /// <returns></returns>
        public static string nextColumn(string column, string title)
        {
            //if (KeyVal.changeLanguage)
            //{
            //    if (HttpContext.Current.Request.Cookies[KeyVal.cookie_language] != null)
            //    {
            //        culture = LanguageFileName(secure.Decrypt(HttpContext.Current.Request.Cookies[KeyVal.cookie_language].Value, true));// retrieve value from cookie
            //    }

            //    String filePath = HttpContext.Current.Request.PhysicalApplicationPath + lang_folder + "\\" + culture;

            //    checkLanguage(filePath, title);

            //    ResXResourceSet rset = new ResXResourceSet(filePath);

            //    if (string.IsNullOrWhiteSpace(rset.GetString(title, true)))
            //    {
            //        rset = new ResXResourceSet(HttpContext.Current.Request.PhysicalApplicationPath + lang_folder + "\\" + KeyVal.defaultCulture);
            //    }

            //    if (column.ToUpper().Contains("CASE") && column.ToUpper().Contains("WHEN"))
            //        return " ," + column + " END AS N'" + rset.GetString(title, true) + "' ";
            //    else
            //        return " ," + column + " AS N'" + rset.GetString(title, true) + "' ";
            //}
            //else
            //{
            //    if (column.ToUpper().Contains("CASE") && column.ToUpper().Contains("WHEN"))
            //        return " ," + column + " END AS '" + title + "' ";
            //    else
            //        return " ," + column + " AS '" + title + "' ";
            //}

            if (column.ToUpper().Contains("CASE") && column.ToUpper().Contains("WHEN"))
                return " ," + column + " END AS N'" + changeLanguage(title) + "' ";
            else
                return " ," + column + " AS N'" + changeLanguage(title) + "' ";
        }

        #endregion

        public static object NullHandler(object instance, object nullIf = null)
        {
            if ((instance != null && nullIf == null) || (nullIf != null && instance != nullIf))
                return instance;

            return DBNull.Value;
        }
    }
}