﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using Synergy.Helper;
using Synergy.Model;
using Synergy.Util;

namespace Synergy.Controller
{
    public class MemberController : UtilityController
    {
        public MemberController()
        {
        }

        public MemberController(wwdb dbConn)
        {
            db = dbConn;
        }


        public Boolean AddNewUser(ref MemberModel MM)
        {
            bool isSuccess = false;
            wwdb db = new wwdb();
            try
            {

                SqlParameter[] sqlParams = {
                    new SqlParameter("@RoleID",MM.RoleID),
                     new SqlParameter("@Username",MM.Username),
                      new SqlParameter("@Password", secure.Encrypt( MM.Password,true)),
                       new SqlParameter("@Name",MM.Fullname),
                        new SqlParameter("@Email",MM.Email),
                         new SqlParameter("@Mobile",MM.Mobile),
                         new SqlParameter("@IsChangePassword", MM.IsChangedPassword)
                                          };
                MM.MemberId = db.executeScalarSP("SP_RegisterUser", sqlParams).ToString();

                isSuccess = !db.HasError;

                if (isSuccess)
                {
                    if (MM.BranchList != null && MM.BranchList.BranchID != string.Empty)
                        db.Execute("UPDATE TBL_LOGIN SET BRANCHID='" + MM.BranchList.BranchID + "' WHERE LOGIN_ID='" + MM.MemberId + "'");

                    foreach (var item in MM.UserRole)
                    {
                        db.Execute("INSERT INTO tbl_MemberRole(MemberId,RoleCode) Values('" + MM.MemberId + "','" + item + "')");
                    }


                }

            }
            catch (Exception ex)
            {
                LogUtil.logError(ex.Message, "MemberController.AddNewUser");
            }

            return isSuccess;

        }


        public bool UpdateUserInfo(MemberModel MM)
        {
            bool isSuccess = false;

            string encryptedPassword = MM.Password == null ? string.Empty : secure.Encrypt(MM.Password, true);

            wwdb db = new wwdb();
            try
            {
                SqlParameter[] sqlParams = {
                    new SqlParameter("@MemberID",  sqlString.NullHandler(MM.MemberId)),
                     new SqlParameter("@Fullname",sqlString.NullHandler(MM.Fullname) ),
                      new SqlParameter("@Email", sqlString.NullHandler(MM.Email)),
                       new SqlParameter("@Mobile",sqlString.NullHandler(MM.Mobile)),
                        new SqlParameter("@Password",sqlString.NullHandler(encryptedPassword , string.Empty)),
                         new SqlParameter("@Status",sqlString.NullHandler(MM.Status) )

                                          };
                db.executeScalarSP("SP_EditUserInfo", sqlParams);

                isSuccess = !db.HasError;

                if (isSuccess)
                {
                    if (MM.BranchList != null && MM.BranchList.BranchID != null && MM.BranchList.BranchID != string.Empty)
                        db.Execute("UPDATE TBL_LOGIN SET BRANCHID='" + MM.BranchList.BranchID + "' WHERE LOGIN_ID='" + MM.MemberId + "'");

                    if (MM.UserRole != null)
                    {
                        db.Execute("DELETE FROM tbl_MemberRole WHERE MemberId ='" + MM.MemberId + "'");

                        foreach (var item in MM.UserRole)
                        {
                            db.Execute("INSERT INTO tbl_MemberRole(MemberId,RoleCode) Values('" + MM.MemberId + "','" + item + "')");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtil.logError(ex.Message, "MemberController.UpdateUserInfo");
            }

            return isSuccess;




        }


    }
}