﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Synergy.Model
{
    /// <summary>
    /// Summary description for CountryModel
    /// </summary>
    public abstract class UtilityModel
    {
        private string _createdby;
        private string _createdat;
        private string _updatedby;
        private string _updatedat;

        /// <summary>
        /// 
        /// </summary>
        public string CreatedBy
        {
            set { _createdby = value; }
            get { return _createdby; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CreatedAt
        {
            set { _createdat = value; }
            get { return _createdat; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string UpdatedBy
        {
            set { _updatedby = value; }
            get { return _updatedby; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string UpdatedAt
        {
            set { _updatedat = value; }
            get { return _updatedat; }
        }
    }
}