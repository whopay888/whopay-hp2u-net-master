﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Synergy.Model
{
    public class BranchModel
    {

        private string _BranchID;
        public string BranchID
        {
            get { return _BranchID; }
            set { _BranchID = value; }
        }


        private string _BranchName;
        public string BranchName
        {
            get { return _BranchName; }
            set { _BranchName = value; }
        }


        private string _Status;
        public string Status
        {
            get { return _Status; }
            set { _Status = value; }
        }




    }
}
