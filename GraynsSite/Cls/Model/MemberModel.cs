﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Synergy.Model
{
    public class MemberModel
    {


        private string _RoleID;
        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }


        ///
        /// Gets or Sets MemberId
        ///
        public string MemberId
        {
            get { return _MemberId; }
            set { _MemberId = value; }
        }
        private string _MemberId;



        ///
        /// Gets or Sets Username
        ///
        public string Username
        {
            get { return _Username; }
            set { _Username = value; }
        }
        private string _Username;

        public string Password
        {
            get { return _Password; }
            set { _Password = value; }
        }
        private string _Password;


        ///
        /// Gets or Sets Fullname
        ///
        public string Fullname
        {
            get { return _Fullname; }
            set { _Fullname = value; }
        }
        private string _Fullname;


        public string Mobile
        {
            get { return _Mobile; }
            set { _Mobile = value; }
        }
        private string _Mobile;


        ///
        /// Gets or Sets Email
        ///
        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }
        private string _Email;


        ///
        /// Gets or Sets Status
        ///
        public string Status
        {
            get { return _Status; }
            set { _Status = value; }
        }
        private string _Status;


        ///
        /// Gets or Sets IsDeleted
        ///
        public bool IsDeleted
        {
            get { return _IsDeleted; }
            set { _IsDeleted = value; }
        }
        private bool _IsDeleted;

        ///
        /// Gets or Sets CreatedBy
        ///
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        private string _CreatedBy;

        ///
        /// Gets or Sets CreatedAt
        ///
        public DateTime CreatedAt
        {
            get { return _CreatedAt; }
            set { _CreatedAt = value; }
        }
        private DateTime _CreatedAt;

        ///
        /// Gets or Sets UpdatedBy
        ///
        public string UpdatedBy
        {
            get { return _UpdatedBy; }
            set { _UpdatedBy = value; }
        }
        private string _UpdatedBy;

        ///
        /// Gets or Sets UpdatedateAt
        ///
        public DateTime UpdatedateAt
        {
            get { return _UpdatedateAt; }
            set { _UpdatedateAt = value; }
        }
        private DateTime _UpdatedateAt;


        public BranchModel BranchList
        {
            get { return _BranchList; }
            set { _BranchList = value; }
        }
        private BranchModel _BranchList;

        public MemberModel()
        {
            _BranchList = new BranchModel();

        }

        public List<string> UserRole { get; set; }


        public string UploadDocument
        {
            get { return _UploadDocument; }
            set { _UploadDocument = value; }
        }
        private string _UploadDocument;

        public bool IsChangedPassword { get; set; }
    }


}