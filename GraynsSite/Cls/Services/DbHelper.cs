﻿using Synergy.Util;
using System.Configuration;

namespace HJT.Cls.Services
{
    public class DbHelper
    {
        public static string connectionString
        {
            get
            {
                var constr = ConfigurationManager.AppSettings["msSQL"];
                var decrypted = secure.Decrypt(constr, true);

                return decrypted;
            }
        }

    }
}