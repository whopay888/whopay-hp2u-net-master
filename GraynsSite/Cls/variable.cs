﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace Synergy.Util
{
    public class KeyVal
    {
        static KeyVal()
        {
            DataTable dt = new DataTable();
            StringBuilder sql = new StringBuilder();

            sql.Clear();
            sql.Append(" SELECT Name, CASE WHEN CONVERT(NVARCHAR, IsTrue) = 1 THEN 'TRUE' ELSE 'FALSE' END AS '0' FROM tbl_PageController WITH (NOLOCK); ");

            wwdb db = new wwdb();

            try
            { dt = db.getDataTable(sql.ToString()); }
            catch (Exception ex) { LogUtil.logError(ex.ToString(), sql.ToString()); }
            finally { db.Close(); }
        }

        #region SQL Control

         
        #region GridView
        public readonly static bool ControlToRight = false;
        #endregion

        #region Setting
        public readonly static bool captchaSetting = false;
        public readonly static bool checkLanguage = false;
        public readonly static bool changeLanguage = false;
        public readonly static bool testingPeriod = false;
        public readonly static bool logSQL = false;
        public readonly static bool MultiLanguage = false;
        #endregion

        #endregion

        #region Basic Information
        public readonly static string ProjectTitle = "CarLoon";
        public readonly static string ProjectName = "HP Checking System";
        #endregion

        #region Reserve Variable
        public readonly static string loginsecure = "XnpUItTSSujdHhmxbQRjWA==";
        public readonly static string session_clogin = "O0fqYQYmU2rTRIvkIr2yhw==";
        public readonly static string session_member = "O0fqYQYmU2pwnCUpuloZQg==";
        public readonly static string cookie_language = sqlString.encryptURL(ProjectName + "_cookie_language");
        public readonly static int MinimumRepayment = 50;
        #endregion
 

        #region Language
        public readonly static string defaultCulture = "resource.resx";
        public readonly static string languageFolder = "App_GlobalResources";
        public readonly static string UnknownError = "Error. Please contact system administrator.";
        public readonly static string ActionSuccess = "Action Successful.";

        #endregion

        #region Date Time
        public readonly static string DateTimeFormat = "yyyy-MM-dd HH:mm:ss";
        #endregion


        #region Role
        public readonly static string Admin = "ADMIN";
        public readonly static string Member = "MEMBER";
        public readonly static string Outlet = "OUTLET";
        public readonly static string Developer = "DEVELOPER";
        public readonly static string RealEstate = "REALESTATE";
        public readonly static string Estate = "ESTATE";
        public readonly static string Individual = "INDIVIDUAL";
        public readonly static string Staff = "STAFF";
        public readonly static string CreditAdvisor = "CREDITADVISOR";
        public readonly static string CarSaleAdvisor = "CARSALEADVISOR";
        public readonly static string AccountManager = "ACCOUNTMANAGER";
        public readonly static string User = "USER";
        public readonly static string TestUser = "TESTUSER";
        public readonly static string SpeedUser = "SPEEDUSER";

        public readonly static string AD = "AD";
        public readonly static string M = "M";
        public readonly static string C = "C";
        public readonly static string O = "O";
        public readonly static string ST = "ST";
        public readonly static string WS = "WS";
        public readonly static string DP = "DP";
        public readonly static string RE = "RE";
        public readonly static string ES = "ES";
        public readonly static string ID = "ID";
        public readonly static string SF = "SF";
        public readonly static string CA = "CA";
        public readonly static string CS = "CS";
        public readonly static string AM = "AM";
        public readonly static string US = "US";
        public readonly static string TU = "TU";
        public readonly static string SU = "SU";

        #endregion

    }
    public  class MemberStatus
    {
        public static string Suspend = "S";
        public static string Active = "A";
    }

    public class DocumentType
    {
        public static string NormalFiles = "N";
        public static string CCRIS = "C";
    }


    public class ApplicationStatus
    {
        public static string New = "N";
        public static string Sent = "S";
        public static string InProgress = "I";
        public static string Processing = "P";
    }

    public class ApplicationResult
    {
        public static string Approved = "A";
        public static string Denied = "D";
        public static string Modify = "M";
    }
}