﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.IO;
using System.Collections;
using Synergy.Model;

namespace Synergy.Util
{
    public class LogUtil
    {
        public static void logAction(string sqlstring, string sqltype)
        {
            string rawUrl = HttpContext.Current.Request.RawUrl.ToString();

            LoginUserModel loginsecure = new LoginUserModel();
            string loginID = "";

            if (HttpContext.Current.Session[KeyVal.loginsecure] != null)
            {
                loginsecure = (LoginUserModel)HttpContext.Current.Session[KeyVal.loginsecure];
                loginID = (loginsecure != null) ? loginsecure.UserId : null;
            }

            if (HttpContext.Current.Session[KeyVal.session_clogin] != null && HttpContext.Current.Session[KeyVal.session_member] != null)
            {
                LoginUserModel cLogin = (LoginUserModel)HttpContext.Current.Session[KeyVal.session_clogin];
                LoginUserModel cMember = (LoginUserModel)HttpContext.Current.Session[KeyVal.session_member];

                loginID = ((cLogin != null) ? cLogin.UserId : null) + " - " + ((cMember != null) ? cMember.UserId : null);
            }

            string Browser = HttpContext.Current.Request.Browser.Browser;
            string Version = HttpContext.Current.Request.Browser.Version;
            string IP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();

            wwdb db = new wwdb();
            string sqlInsert = "";

            try
            {
                sqlInsert = "INSERT INTO tbl_log_action (action_Sql, action_Type, " +
                    " action_loginID, action_DateTime, action_Browser, action_Browser_Version, action_IP) " +
                    " VALUES (N'" + secure.RC(sqlstring) + "', N'" + secure.RC(sqltype) + "', N'" + secure.RC(loginID) + "', GETDATE(), N'" +
                    secure.RC(Browser) + "', N'" + secure.RC(Version) + "', N'" + secure.RC(IP) + "'); ";

                db.Execute(sqlInsert,false);

                if (db.HasError)
                {
                    LoggerUtil.TextFileLogger.LogToFile("Log Action Error", db.ErrorMessage + " | " + sqlInsert, false);
                    logError(db.ErrorMessage, sqlInsert);
                }
            }
            catch (Exception ex)
            {
                LoggerUtil.TextFileLogger.LogToFile("Log Action Catch Error", sqlInsert + " | " + ex.ToString(), false);
            }
            finally
            {
                db.Close();
            }
        }

        public static void logError(string errMessage, string sqlstring)
        {
            string rawUrl = HttpContext.Current.Request.RawUrl.ToString();

            LoginUserModel loginsecure = new LoginUserModel();
            string loginID = "";

            if (HttpContext.Current.Session[KeyVal.loginsecure] != null)
            {
                loginsecure = (LoginUserModel)HttpContext.Current.Session[KeyVal.loginsecure];
                loginID = (loginsecure != null) ? loginsecure.UserId : null;
            }

            if (HttpContext.Current.Session[KeyVal.session_clogin] != null && HttpContext.Current.Session[KeyVal.session_member] != null)
            {
                LoginUserModel cLogin = (LoginUserModel)HttpContext.Current.Session[KeyVal.session_clogin];
                LoginUserModel cMember = (LoginUserModel)HttpContext.Current.Session[KeyVal.session_member];

                loginID = ((cLogin != null) ? cLogin.UserId : null) + " - " + ((cMember != null) ? cMember.UserId : null);
            }

            string Browser = HttpContext.Current.Request.Browser.Browser;
            string Version = HttpContext.Current.Request.Browser.Version;
            string IP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();

            wwdb db = new wwdb();
            string sqlInsert = "";

            try
            {
                sqlInsert = " INSERT INTO tbl_log_error (error_Message, error_SQL, error_rawURL, error_loginID, error_DateTime, " +
                    " error_IP, error_Browser, error_Browser_Version) VALUES " +
                    " (N'" + secure.RC(errMessage) + "', N'" + secure.RC(sqlstring) + "', N'" + secure.RC(rawUrl) + "', N'" +
                    secure.RC(loginID) + "', GETDATE(), N'" + secure.RC(IP) + "', N'" + secure.RC(Browser) + "', N'" + secure.RC(Version) + "')";

                db.Execute(sqlInsert,false);

                if (db.HasError)
                {
                    LoggerUtil.TextFileLogger.LogToFile("Log Error Error", db.ErrorMessage + " | " + sqlInsert, false);
                }
            }
            catch (Exception ex)
            {
                LoggerUtil.TextFileLogger.LogToFile("Log Error Catch Error", sqlInsert + " | " + ex.ToString(), false);
            }
            finally
            {
                db.Close();
            }
        }

        public static void logSQL(string sql)
        {
            if (KeyVal.logSQL)
            {
                string rawUrl = HttpContext.Current.Request.RawUrl.ToString();

                LoginUserModel loginsecure = new LoginUserModel();
                string loginID = "";

                if (HttpContext.Current.Session[KeyVal.loginsecure] != null)
                {
                    loginsecure = (LoginUserModel)HttpContext.Current.Session[KeyVal.loginsecure];
                    loginID = (loginsecure != null) ? loginsecure.UserId : null;
                }

                if (HttpContext.Current.Session[KeyVal.session_clogin] != null && HttpContext.Current.Session[KeyVal.session_member] != null)
                {
                    LoginUserModel cLogin = (LoginUserModel)HttpContext.Current.Session[KeyVal.session_clogin];
                    LoginUserModel cMember = (LoginUserModel)HttpContext.Current.Session[KeyVal.session_member];

                    loginID = ((cLogin != null) ? cLogin.UserId : null) + " - " + ((cMember != null) ? cMember.UserId : null);
                }

                wwdb db = new wwdb();
                string sqlInsert = "";
                try
                {
                    sqlInsert = "INSERT INTO tbl_log_sql (sqlString, fromURL, loginID) VALUES (N'" + secure.RC(sql) + "', N'" +
                        secure.RC(rawUrl) + "', N'" + secure.RC(loginID) + "'); ";

                    db.Execute(sqlInsert,false);

                    if (db.HasError)
                    {
                        LoggerUtil.TextFileLogger.LogToFile("Log SQL Error", db.ErrorMessage + " | " + sqlInsert, false);
                        logError(db.ErrorMessage, sqlInsert);
                    }
                }
                catch (Exception ex)
                {
                    LoggerUtil.TextFileLogger.LogToFile("Log SQL Catch Error", sqlInsert + " | " + ex.ToString(), false);
                }
                finally
                {
                    db.Close();
                }
            }
        }

        public static void logLogin(string user, string pwd, string loginStatus)
        {
            string SessionID = null;

            if (HttpContext.Current.Session != null)
            {
                SessionID = HttpContext.Current.Session.SessionID.ToString();
            }

            string Browser = HttpContext.Current.Request.Browser.Browser;
            string Version = HttpContext.Current.Request.Browser.Version;
            string IP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();

            if (HttpContext.Current.Session[KeyVal.session_clogin] != null && HttpContext.Current.Session[KeyVal.session_member] != null)
            {
                LoginUserModel cLogin = (LoginUserModel)HttpContext.Current.Session[KeyVal.session_clogin];
                LoginUserModel cMember = (LoginUserModel)HttpContext.Current.Session[KeyVal.session_member];

                user = ((cLogin != null) ? cLogin.UserId : null) + " - " + ((cMember != null) ? cMember.UserId : null) + " - " + user;
            }

            wwdb db = new wwdb();
            string sqlInsert = "";

            try
            {
                sqlInsert = " INSERT INTO tbl_log_login (log_User, log_IP, log_SessionID, log_Action, " +
                    " log_PwdAttempt, log_Browser, log_Version, log_DateTime) VALUES " +
                    "(N'" + secure.RC(user) + "', N'" + secure.RC(IP) + "', N'" + secure.RC(SessionID) + "', N'" + secure.RC(loginStatus) + "', " +
                    " N'" + secure.RC(secure.Encrypt(pwd, true)) + "', N'" + secure.RC(Browser) + "', N'" + secure.RC(Version) + "', GETDATE()); ";

                db.Execute(sqlInsert,false);

                if (db.HasError)
                {
                    LoggerUtil.TextFileLogger.LogToFile("Log Login Error", db.ErrorMessage + " | " + sqlInsert, false);
                    logError(db.ErrorMessage, sqlInsert);
                }

                if (loginStatus.Trim().ToUpper() == "FAILED")
                {
                    sqlInsert = " INSERT INTO tbl_loginAttempt (log_User, log_IP,log_datetime,isDeleted) VALUES (N'" + secure.RC(user.Trim()) + "', N'" + secure.RC(IP) + "',GETDATE(),'0'); " +
                        //" IF (SELECT COUNT(1) FROM tbl_loginAttempt WITH (NOLOCK) WHERE log_User = N'" + secure.RC(user.Trim()) + "' " +
                        //" AND log_IP = N'" + secure.RC(IP.Trim()) + "' AND isDeleted = 0) >= (SELECT parameterValue FROM tbl_Parameter WITH (NOLOCK) " +
                        //" WHERE parameterName = 'MaximumLogin') " +
                        //" BEGIN " +
                        //" INSERT INTO tbl_blockedIP (memberID, IP,createdAt,isDeleted) " +
                        //" SELECT N'" + secure.RC(user.Trim()) + "', N'" + secure.RC(IP.Trim()) + "',GETDATE(),'0' WHERE NOT EXISTS " +
                        //" (SELECT 1 FROM tbl_blockedIP WHERE memberID = N'" + secure.RC(user.Trim()) + "' AND IP = N'" +
                        //secure.RC(IP.Trim()) + "' AND isDeleted = 0); " +
                        //" END " +
                        //" ELSE " +
                        //" BEGIN SELECT 0 AS '0'; END; ";

                    db.Execute(sqlInsert , false);
                    logAction(sqlInsert, "Check For Login Attempt");
                }
                else if (loginStatus.Trim().ToUpper() == "SUCCESS")
                {
                    sqlInsert = " UPDATE tbl_loginAttempt SET isDeleted = 1 WHERE log_IP = N'" + secure.RC(IP.Trim()) + "' AND log_User = N'" + secure.RC(user.Trim()) + "'; ";

                    db.Execute(sqlInsert , false);
                    //logAction(sqlInsert, "Remove Login Attempt");
                }
            }
            catch (Exception ex)
            {
                LoggerUtil.TextFileLogger.LogToFile("Log Login Catch Error", sqlInsert + " | " + ex.ToString(), false);
            }
            finally
            {
                db.Close();
            }
        }

        public static void CheckInUser(string memberCode)
        {
            wwdb db = new wwdb();

            string SessionID = null;

            if (HttpContext.Current.Session != null)
            {
                SessionID = HttpContext.Current.Session.SessionID.ToString();
            }

            string IP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();

            if (HttpContext.Current.Session[KeyVal.session_clogin] != null && HttpContext.Current.Session[KeyVal.session_member] != null)
            {
                LoginUserModel cLogin = (LoginUserModel)HttpContext.Current.Session[KeyVal.session_clogin];
                LoginUserModel cMember = (LoginUserModel)HttpContext.Current.Session[KeyVal.session_member];

                memberCode = ((cLogin != null) ? cLogin.UserId : null) + " - " + ((cMember != null) ? cMember.UserId : null) + " - " + memberCode;
            }

            string SqlInsert = "";

            try
            {
                SqlInsert = " INSERT INTO tbl_current_login (user_Session, member_Code, login_Time, user_IP) SELECT N'" +
                secure.RC(SessionID) + "', N'" + secure.RC(memberCode) + "', GETDATE(), N'" + secure.RC(IP) + "' WHERE NOT EXISTS " +
                " (SELECT 1 FROM tbl_current_login WITH (NOLOCK) WHERE user_Session = N'" +
                secure.RC(SessionID) + "' AND user_IP = N'" + secure.RC(IP) + "'); ";

                db.Execute(SqlInsert,false);

                if (db.HasError)
                {
                    LoggerUtil.TextFileLogger.LogToFile("Insert User Check In", db.ErrorMessage + " | " + SqlInsert, false);
                    logError(db.ErrorMessage, SqlInsert);
                }
            }
            catch (Exception ex)
            {
                LoggerUtil.TextFileLogger.LogToFile("Log Check In Catch Error", SqlInsert + " | " + ex.ToString(), false);
            }
            finally
            {
                db.Close();
            }
        }

        public static void UpdateCheckInUser(string memberCode)
        {
            wwdb db = new wwdb();

            string rawUrl = HttpContext.Current.Request.RawUrl.ToString();
            string IP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();

            string SessionID;
            SessionID = HttpContext.Current.Session.SessionID.ToString();

            if (HttpContext.Current.Session[KeyVal.session_clogin] != null && HttpContext.Current.Session[KeyVal.session_member] != null)
            {
                LoginUserModel cLogin = (LoginUserModel)HttpContext.Current.Session[KeyVal.session_clogin];
                LoginUserModel cMember = (LoginUserModel)HttpContext.Current.Session[KeyVal.session_member];

                memberCode = ((cLogin != null) ? cLogin.UserId : null) + " - " + ((cMember != null) ? cMember.UserId : null) + " - " + memberCode;
            }

            string sql = "";

            try
            {
                sql = " UPDATE tbl_current_login SET user_Session = N'" + secure.RC(SessionID) + "', member_Code = N'" + secure.RC(memberCode) + "', " +
                    " last_Activity = GETDATE(), current_Page = N'" + secure.RC(rawUrl) + "', user_IP = N'" + secure.RC(IP) + "' WHERE user_Session = N'" +
                    secure.RC(SessionID) + "' AND user_IP = N'" + secure.RC(IP) + "'; " +
                    " IF @@ROWCOUNT=0 BEGIN " +
                    " INSERT INTO tbl_current_login (user_Session, member_Code, login_Time, user_IP, last_Activity, current_Page) VALUES (N'" +
                    secure.RC(SessionID) + "', N'" + secure.RC(memberCode) + "', GETDATE(), N'" + secure.RC(IP) + "', GETDATE(), N'" +
                    secure.RC(rawUrl) + "'); END ";

                db.Execute(sql);

                if (db.HasError)
                {
                    LoggerUtil.TextFileLogger.LogToFile("Update User Location", db.ErrorMessage + " | " + sql, false);
                    logError(db.ErrorMessage, sql);
                }
            }
            catch (Exception ex)
            {
                LoggerUtil.TextFileLogger.LogToFile("Log Update Check In Catch Error", sql + " | " + ex.ToString(), false);
            }
            finally
            {
                db.Close();
            }
        }
    }
}
