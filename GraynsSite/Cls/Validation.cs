﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Synergy.Util
{
    public class Validation
    {
        public static bool isNumeric(string input)
        {
            try
            {
                Convert.ToInt32(input);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool IsValidEmail(string strIn)
        {
            return System.Text.RegularExpressions.Regex.IsMatch(strIn, @"^[\w-]+(\.[\w-]+)*@([a-z0-9-]+(\.[a-z0-9-]+)*?\.[a-z]{2,6}|(\d{1,3}\.){3}\d{1,3})(:\d{4})?$");
            //^(([^<>()[\]\\.,;:\s@\""]+(\.[^<>()[\]\\.,;:\s@\""]+)*)|(\"".+\""))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$");
            //"^(?(\")(\".+?\"@)|(([0-9a-zA-Z]((\\.(?!\\.))|[-!#\\$%&'\\*\\+/=\\?\\^`\\{\\}\\|~\\w])*)(?<=[0-9a-zA-Z])@))" + "(?(\\[)(\\[(\\d{1,3}\\.){3}\\d{1,3}\\])|(([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,6}))$");
        }

        public static bool isValidMyCard(string strIC)
        {
            strIC = strIC.Trim();
            Regex objRegex = new Regex(@"^\d{2}(1[0-2]|0[1-9])(0[1-9]|1[0-9]|2[0-9]|3[0-1])\d{6}$", RegexOptions.IgnoreCase);
            //^[A-Za-z0-9]+$

            if (!objRegex.IsMatch(strIC))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static bool isDecimal(string input)
        {
            try
            {
                Convert.ToDecimal(input);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static string GetUrlParameter(System.Web.UI.Page chkpage, string keyName, string defaultValue)
        {
            if (chkpage.Request[keyName] == null)
            {
                if (defaultValue == null)
                    throw new Exception(string.Format("Url parameter({0}) is null.", keyName));
                else
                    return defaultValue;
            }
            return chkpage.Request[keyName].Trim();
        }
    }
}