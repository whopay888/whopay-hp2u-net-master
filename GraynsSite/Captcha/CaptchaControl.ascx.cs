﻿using System;

namespace Synergy.UI
{
    public partial class CaptchaControl : System.Web.UI.UserControl
    {        
        #region field initialisation

        public Captcha cc;
        private int captchaLength;
        private double fontSize;
        private string fontFamily;
        private string backgroundImagePath;
        private string textColor;
        private string successMessage;
        private string errorMessage;
        private string characterSet;

        #endregion

        /// <summary>
        /// Set Captcha class properties
        /// </summary>
        private Captcha GetCaptchaClass()
        {
            if (Session["CaptchaClass"] != null)
                cc = (Captcha)Session["CaptchaClass"];
            else
                cc = new Captcha();

            cc.FontSize = this.FontSize;
            cc.FontFamily = this.FontFamily;
            cc.BackgroundImagePath = this.BackgroundImagePath;
            cc.TextColor = this.TextColor;
            return cc;
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            B1.Text = TestButtonText;
            B2.Text = ReLoadButtonText;
            BSpeak.Text = SpeakButtonText;
            
            SetValues();

            cc = GetCaptchaClass();

            if (!IsPostBack)
            {
                LoadCaptcha();
                //Response.Write(Session["cctext"]);
            }
        }

        /// <summary>
        /// Generates random text based on CharacterSet
        /// </summary>
        private string GetRandomText()
        {
            char[] letters = CharacterSet.ToCharArray();
            string text = string.Empty;
            Random r = new Random();
            int num = -1;

            for (int i = 0; i < this.CaptchaLength; i++)
            {
                num = (int)(r.NextDouble() * (letters.Length - 1));
                text += letters[num].ToString();
            }
            return text;
        }


        protected void ValidateCaptcha(object sender, EventArgs e)
        {
            string text = T1.Text;
            if (text == (string)ViewState["captcha"])
                LStatus.Text = SuccessMessage;
            else
                LStatus.Text = ErrorMessage;
        }

        protected void LoadAnother(object sender, EventArgs e)
        {
            LoadCaptcha();
        }

        /// <summary>
        /// Set captcha
        /// </summary>
        private void LoadCaptcha()
        {
            string text = GetRandomText();
            Session["cctext"] = text;
            ViewState.Add("captcha", text);
            Session.Add("CaptchaClass", cc);//add captcha object to Session
            Session.Add("captcha", text);//add captcha text to session
            Im1.ImageUrl = "CaptchaHandler.ashx";
        }

        #region GetterSetter
        public string TestButtonText
        {
            get { return B1.Text; }
            set { B1.Text = value; }
        }

        public string SpeakButtonText
        {
            get { return BSpeak.Text; }
            set { BSpeak.Text = value; }
        }

        public string ReLoadButtonText
        {
            get { return B2.Text; }
            set { B2.Text = value; }
        }

        public string SuccessMessage
        {
            get { return successMessage; }
            set { successMessage = value; }
        }

        public string ErrorMessage
        {
            get { return errorMessage; }
            set { errorMessage = value; }
        }

        public int CaptchaLength
        {
            get { return captchaLength; }
            set
            {
                try
                {
                    int k = Convert.ToInt32(value);
                    if (k < 5 || k > 10)
                        captchaLength = 6;
                    else
                        captchaLength = k;
                }
                catch (Exception ex)
                {
                    captchaLength = 6;
                    throw ex;
                }
            }
        }

        public string FontFamily
        {
            get { return fontFamily; }
            set
            {
                if (value != string.Empty && value != null)
                    fontFamily = value;
                else
                    fontFamily = "Arial";
            }
        }

        public double FontSize
        {
            get { return fontSize; }
            set
            {
                try
                {
                    fontSize = Convert.ToInt32(value);
                    if (fontSize <= 10 && fontSize >= 24)
                        fontSize = 16;
                }
                catch (Exception ex)
                {
                    fontSize = 16;
                    throw ex;
                }
            }
        }

        public string BackgroundImagePath
        {
            get { return backgroundImagePath; }
            set
            {
                if (System.IO.File.Exists(Server.MapPath(value)))
                    backgroundImagePath = value;
                else
                    backgroundImagePath = System.Configuration.ConfigurationManager.AppSettings["defaultImagePath"];
            }
        }

        public string TextColor
        {
            get { return textColor; }
            set
            {
                if (value == string.Empty || value == null)
                    textColor = "Black";
                else
                    textColor = value;
            }
        }

        public string CharacterSet
        {
            get { return characterSet; }
            set
            {
                if (value == "" || value == null)
                    characterSet = "ZABCDEFGHIJKLMNOPQRSTUVWXYZ123456789♫";
                else
                    characterSet = value;
            }
        }

        #endregion

        /// <summary>
        /// Set default values of fields
        /// </summary>
        private void SetValues()
        {
            if (CharacterSet == null)
                CharacterSet = "";
            if(CaptchaLength == 0)
                CaptchaLength = 6;

            if(BackgroundImagePath == null)
                BackgroundImagePath = "";
            
            if(FontFamily == null)
                FontFamily = "";            
            
            if(FontSize == 0)
                FontSize = 0.0;

            if(TextColor == null)
                TextColor = "";
        }




        /// <summary>
        /// Reads the captcha characters.
        /// </summary>
        //protected void ReadCaptcha(object sender, EventArgs e)
        //{
        //    SpVoice voice = new SpVoice();

        //    char[] text = ((string)ViewState["captcha"]).ToCharArray();
            

        //    for (int i = 0; i < text.Length; i++)
        //    {
        //        voice.Speak(text[i].ToString(), SpeechVoiceSpeakFlags.SVSFDefault);
        //    }
        //}
	

    }
}